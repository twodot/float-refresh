<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_login();
?>
<h3><span class="add_property_name">Lift-off</span><span class="add_property_icon">Icon</span></h3>
<section>
	<?php
	
	
		echo '<div id="listing_info">';
		
			$q_co_listing = "SELECT
				*, 
				COUNT(*) as co_listing
				
				FROM 
				co_agent  
				
				WHERE 
				user_id=$user_id 
				AND id = (SELECT co_agent FROM properties WHERE id = $property AND user_id = $user_id) 
				";
			$r_co_listing = @mysqli_query ($dbc, $q_co_listing);
			$row_co_listing = mysqli_fetch_array($r_co_listing, MYSQLI_ASSOC);
			$co_agent_id = $row_co_listing['id'];
		
			$q_listing = "SELECT
		
				listing_name, 
				listing_email, 
				listing_phone, 
				listing_brokerage 
				
				FROM 
				users 
				
				WHERE 
				user_id=$user_id 
				";
			$r_listing = @mysqli_query ($dbc, $q_listing);
			$row_listing = mysqli_fetch_array($r_listing, MYSQLI_ASSOC);
			if(empty($row_listing['listing_name'])) {
				$listing_error = true;
			}
			else if(empty($row_listing['listing_email'])) {
				$listing_error = true;
			}
			else if(empty($row_listing['listing_phone'])) {
				$listing_error = true;
			}
			else if(empty($row_listing['listing_brokerage'])) {
				$listing_error = true;
			}
			else {
				$listing_error = false;
			}	
				
			if($listing_error) {
				echo '<div class="error" id="property_listing_error">Please <a href="/account">enter your listing information</a></div>';	
			}
			else {
				echo '<div ><h3>Listing Info </h3></div>';
				
				echo '<div id="co_agent_form">';
				$q = "SELECT
					id,
					name
					
					FROM 
					co_agent  
					
					WHERE 
					user_id=$user_id 
					";
					$r = @mysqli_query ($dbc, $q);
					
					$num_rows = mysqli_num_rows($r);
					$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
					
					if ($num_rows > 0) {
						echo '<p>Existing Agents</p>';
						echo '<form action="/add_co_agent" method="post" id="existing_listing_info_form">';
						echo '<div class="select_holder"><select name="existing_agent" id="existing_agent_select"><option value="0">Select Existing Agent</option>';
						
						$r = @mysqli_query ($dbc, $q);
						while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
							echo '<option value="' . $row['id'] . '"'; if($row['id'] == $co_agent_id) {echo 'selected="selected"'; } echo ' id="add_co_agent_select_' . $row['id'] . '" >' . $row['name'] . '</option>';
						}
						echo '</select></div></p>
						
						<input type="submit" name="submit" value="Add Existing Co-Agent" id="add_existing_co_agent" />
						<input type="hidden" name="property" value="' .  $property_id . '" />
						<input type="hidden" name="existing-submitted" value="TRUE" />
						</form>';
						echo '<br /><p>Add a New Agent</p>';
					}
					
						?>
						<form action="/add_co_agent" method="post" id="update_listing_info_form">
							 <p class="forms"><input type="text" name="name" size="40" placeholder="Name" /></p>
							 <p class="forms"><input type="text" name="email" size="40" placeholder="Email" /></p>
							 <p class="forms"><input type="text" name="phone" size="40" placeholder="Phone" /></p>
							 <p class="forms"><input type="text" name="brokerage" size="40" placeholder="Brokerage" /></p>
						
						<br />
						<input type="submit" name="submit" value="Add Co-Agent" id="add_co_agent" />
                        <button id="cancel_add_co_agent">Cancel</button>
						<input type="hidden" name="property" value="<?php echo $property_id; ?>" />
						<input type="hidden" name="submitted" value="TRUE" />
						</form>
				<?php 
						
				echo '</div>';
				echo '<div id="co_agent_holder" class="listing_info_holder co_agent">';
					if ($row_co_listing['co_listing'] != 0) {
						echo '<h4>Co Agent:</h4><p>' . $row_co_listing['name'] . '<br />' . $row_co_listing['email'] . '<br />' . $row_co_listing['phone'] . '<br />' . $row_co_listing['brokerage'] . '</p>
						<p><a href="edit_co_agent?ca=' . $row_co_listing['id'] . '&p=' . $property . '" id="edit_co_agent">Edit this agents info</a></p>';
						?>
                        <div id="update_co_agent_info_form_holder">
						<form action="/edit_co_agent" id="update_co_agent_info_form">
								 <p class="forms"><input type="text" name="name" size="40" value="<?php echo $row_co_listing['name']; ?>" placeholder="Name" /></p>
								 <p class="forms"><input type="text" name="email" size="40" value="<?php echo $row_co_listing['email']; ?>" placeholder="Email" /></p>
								 <p class="forms"><input type="text" name="phone" size="40" value="<?php echo $row_co_listing['phone']; ?>" placeholder="Phone" /></p>
								 <p class="forms"><input type="text" name="brokerage" size="40" value="<?php echo $row_co_listing['brokerage']; ?>" placeholder="Brokerage" /></p>
							
							<br />
							<input type="submit" name="submit" value="Save <?php echo $row_co_listing['name']; ?>&rsquo;s Info" id="save_co_agent_info_form"/>
                            <button id="cancel_co_agent_info_form">Cancel</button>
							<input type="hidden" name="agent" value="<?php echo $row_co_listing['id']; ?>" />
							<input type="hidden" name="property" value="<?php echo $property; ?>" />
							<input type="hidden" name="submitted" value="TRUE" />
							</form>
                        </div>
						<?php
						echo '<p><a href="add_co_agent?p=' . $property . '" id="change_co_agent_button">Change the co-listed agent</a></p>';
						
						
						echo '<form action="/add_co_agent" method="post" id="remove_co_agent_form">';
						echo '<input type="hidden" name="existing_agent" value="0" />';
							
						echo '<p><input type="submit" name="submit" class="error" value="Remove Co-Agent" id="remove_co_agent" /></p>
						<input type="hidden" name="remove" value="TRUE" />
						<input type="hidden" name="property" value="' .  $property_id . '" />
						<input type="hidden" name="existing-submitted" value="TRUE" />
						</form>';
						
					}
				echo '</div>';// END CO AGENT HOLDER
				echo '<div class="listing_info_holder"><p>' . $row_listing['listing_name'] . '<br />' . $row_listing['listing_email'] . '<br />' . $row_listing['listing_phone'] . '<br />' . $row_listing['listing_brokerage'] . '</p></div>';
			}
			
			
			
				echo '<div class="listing_info_holder"><a href=#" id="add_co_agent_show" '; if ($row_co_listing['co_listing'] != 0) {echo 'style="display:none;"';} echo ' >Add a co-listed agent</a></div>';
		
		
			
		echo '</div>'; // END LISTING INFO
		
		
		/* CRM SYSTEM
		?>
        
        <script>
			jQuery(document).ready(function( $ ) {
				$( "#crm_select" ).change(function() {
					theCrmValue = $(this).val();
					if(theCrmValue !=0) {
					  	$.ajax({
							url: "/get_crm?crm=" + theCrmValue + "&property_id=<?php echo $property_id; ?>",
							cache: false,
						}).done(function(data) {
							$('#crm_form_holder').html(data)
						});
					}
					else {
						$.ajax({
							url: "/remove_crm?crm=" + theCrmValue + "&property_id=<?php echo $property_id; ?>",
							cache: false,
						}).done(function(data) {
							$('#crm_form_holder').html(data)
						});
					}
					
				});
				$( document ).on( 'click', '#save_crm_button', function (e) {
					e.preventDefault();
					
					
					$.ajax({
						url: "/add_crm",
						data: jQuery("#add_crm_form").serialize(),
						cache: false,
					}).done(function(data) {
						
					});
					
				})
			});
		</script>
        
        <?php
		echo '<div id="add_crm">';
			$q_crm = "SELECT * FROM crm";
			$r_crm = mysqli_query($dbc, $q_crm);
			echo '<h3>CRM</h3>';
			echo '<p>Add a contact automatically to a contact response system.</p>';
			echo '<select id="crm_select" class="select_holder">';
				echo '<option value="0">Select a CRM</option>';
				while($row_crm = mysqli_fetch_array($r_crm, MYSQLI_ASSOC)) {
					echo '<option value="' . $row_crm['id'] . '"'; if ($row_crm['id'] == $crm_id) { echo 'selected="selected"'; } echo'>' . $row_crm['name'] . '</option>';
				}
			echo '</select>';
			echo '<div id="crm_form_holder">';
				if($crm_id != 0) {
					
					echo '<form action="/save_input_form_holder" id="add_crm_form">';
			
					
						if(!empty($crm_field_1_value)) {
							echo ' - <input id="field_1_name" type="text" name="field_1_name" value="' . $crm_field_1_value . '" /><br />';
						}
						if(!empty($crm_field_2_value)) {
							echo ' - <input id="field_2_name" type="text" name="field_2_name" value="' . $crm_field_2_value . '" /><br />';
						}
						if(!empty($crm_field_3_value)) {
							echo ' - <input id="field_3_name" type="text" name="field_3_name" value="' . $crm_field_3_value . '" /><br />';
						}
						if(!empty($crm_field_4_value)) {
							echo ' - <input id="field_4_name" type="text" name="field_4_name" value="' . $crm_field_4_value . '" /><br />';
						}
						if(!empty($crm_field_5_value)) {
							echo ' - <input id="field_5_name" type="text" name="field_5_name" value="' . $crm_field_5_value . '" /><br />';
						}
						if(!empty($crm_field_6_value)) {
							echo ' - <input id="field_6_name" type="text" name="field_6_name" value="' . $crm_field_6_value . '" /><br />';
						}
						
						echo '<input type="hidden" name="property_id" value="' . $property_id . '" />';
						echo '<input type="hidden" name="crm" value="' . $crm_id . '" />';
						
						echo '<br /><input type="submit" name="submit" id="save_crm_button" value="Save CRM" />';
						
					echo '</form>';
				}
			
			echo '</div>';
		echo '</div>';
		 */
		echo '<style>';
		
			if (is_null($main_beacon_title)) {
				echo ' #big_ole_activation_button { display:none; } ';
			}
			else {
				echo ' #big_ole_activation_warning { display:none; } ';
		
			}
			if (!$active) {
				echo ' #lift_off { display:none; } ';
				
			} 
			else {
				echo ' #big_ole_activation_button { display:none; } ';
			}
			
		echo '</style>';
		
		echo '<a id="big_ole_activation_button" href="/click_activation?p=' . $property . '">BIG OL&rsquo; ACTIVATION BUTTON</a>';
		echo '<div id="lift_off">You Have Lift Off</div>';
		echo '<div id="big_ole_activation_warning">You must add a beacon to &ldquo;<a href="#" id="get_beacon_step" >' . $property_title . '</a>&rdquo; before activating this listing</div>';
		
		if (isset($short_url)) {
			echo '
					<ul id="social_share_links">
						<h3>Click here to share</h3>
						<li><a href="' . $facebook_link . '" target="_blank" ><img src="/images/facebook.png" /></a></li>
						<li><a href="' . $twitter_link . '"  target="_blank" ><img src="/images/twitter.png" /></a></li>
					</ul>
					<p>Short URL | For social sharing and yard arm signs when people don&rsquo;t have the app installed.<br /><a href="' . $short_url . '" target="_blank" >' . $short_url . '</a></p>
					<p>Facebook URL | For sharing on Facebook.<br /><a href="' . $facebook_link . '" target="_blank" >' . $facebook_link . '</a></p>
					<p>Twitter URL | For sharing on Twitter.<br /><a href="' . $twitter_link . '" target="_blank" >' . $twitter_link . '</a></p>
					
					
				';
		}
		?>
</section>