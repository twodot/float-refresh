<?php
session_start();

require_once('../../includes/config.inc.php');
require_login();
if (isset($_POST['p'])) {
	$property = (int) $_POST['p'];
	$table = 'properties';
	$table_id = 'property_id';
}
if (isset($property)) {	
		if($property != 0) {
			$q = "
				SELECT 
				user_id   
				
				FROM 
				$table  
				
				WHERE 
				id = $property
			";
			
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			if ($user_id == $row['user_id']) {
				
					
					$trimmed = array_map('trim', $_POST);
					if(isset($trimmed['bed'])) {
						$beds = sanitize($trimmed['bed'], $dbc);
					}
					else {
						$beds = '';
					}
					if(isset($trimmed['bath'])) {
						$bath = sanitize($trimmed['bath'], $dbc);
					}
					else {
						$bath = '';
					}
					if(isset($trimmed['sqft'])) {
						$sqft = sanitize($trimmed['sqft'], $dbc);
					}
					else {
						$sqft = '';
					}
					if(isset($trimmed['lot_size'])) {
						$lot_size = sanitize($trimmed['lot_size'], $dbc);
					}
					else {
						$lot_size = '';
					}
					if(isset($trimmed['property_type'])) {
						$property_type = sanitize($trimmed['property_type'], $dbc);
					}
					else {
						$property_type = '';
					}
					if(isset($trimmed['year_built'])) {
						$year_built = sanitize($trimmed['year_built'], $dbc);
					}
					else {
						$year_built = '';
					}
					if(isset($trimmed['community'])) {
						$community = sanitize($trimmed['community'], $dbc);
					}
					else {
						$community = '';
					}
					if(isset($trimmed['county'])) {
						$county = sanitize($trimmed['county'], $dbc);
					}
					else {
						$county = '';
					}
					if(isset($trimmed['mls'])) {
						$mls = (int) $dbc->real_escape_string($trimmed['mls']);
					}
					else {
						$mls = '';
					}
					
					
				 	$q = "
						UPDATE 
						$table  
						
						SET 
						beds = '$beds',  
						bath = '$bath', 
						sqft = '$sqft', 
						lot_size = '$lot_size', 
						property_type = '$property_type', 
						year_built = '$year_built', 
						community = '$community', 
						county= '$county', 
						mls = '$mls', 
						progress = 2 
						
						WHERE 
						id = $property 
						AND user_id = $user_id 
						
					"; 
					
					$r = @mysqli_query ($dbc, $q);
				
			}
			else {
				echo 'You have reached this page in error';
			}
			
		}// END PROPERTY INT CHECK
} // IN P ISSET CHECK
?>