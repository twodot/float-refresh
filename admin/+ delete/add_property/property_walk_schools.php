<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_login();
?>

<h3><span class="add_property_name">Neighborhood &amp; Schools</span><span class="add_property_icon">Icon</span></h3>
<section data-mode="async" data-url="/_content/add_property/get_walk_schools.php?p=<?php echo $property; ?>">
<h2 class="create_property_headline">What&rsquo;s There to do Around Here?</h2>

</section>