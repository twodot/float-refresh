<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_login();
?>
<h3><span class="add_property_name">Details</span><span class="add_property_icon">Icon</span></h3>
<section>
	<script>
	$( document ).ready(function() {
		$('#custom_message_input').keyup(function(){
			if($('#custom_status_message_holder').not('form_checked')) {
				$('#custom_status_message_holder.status_click').click();
			}
		})
	})
	</script>
	<?php
		echo '<div id="property_details" class="clear">';
			echo '<h2 class="create_property_headline">Interesting. Tell Us More.</h2>';
			?>
            	<form id="property_status_form">
                    <ul>
                        <?php if(!$rental) { ?>
                            <li class="status_click radio_switch <?php if ($sales_status == 0) {echo ' form_checked'; } ?>" ><input type="radio" name="sales_status" value="0" id="status_for_sale" <?php if ($sales_status == 0) {echo 'checked="checked"'; } ?> /> For Sale</li>
                            <li class="status_click radio_switch <?php if ($sales_status == 1) {echo ' form_checked'; } ?>" ><input type="radio" name="sales_status" value="1" id="status_pending" <?php if ($sales_status == 1) {echo 'checked="checked"'; } ?> /> Pending</li>
                            <li class="status_click radio_switch <?php if ($sales_status == 2) {echo ' form_checked'; } ?>" ><input type="radio" name="sales_status" value="2" id="status_sold" <?php if ($sales_status == 2) {echo 'checked="checked"'; } ?> /> Sold</li>
                             <li id="custom_status_message_holder" class="status_click radio_switch <?php if ($sales_status == 5) {echo ' form_checked'; } ?>" ><input type="radio" name="sales_status" value="5" id="status_custom" <?php if ($sales_status == 5) {echo 'checked="checked"'; } ?> /> Custom </li>
                             <li><input type="text" name="sales_status_message" maxlength="20" <?php if (!empty($status_message)) {echo 'value="' . $status_message . '"'; } ?> placeholder="<?php if (!empty($status_message)) {echo 'value="' . $status_message . '"'; } else {echo 'Custom Message'; } ?>" id="custom_message_input"/></li>
                        <?php 
                        } // END RENTAL CHECK 
                        else {
                            ?>
                            <li>Available <input type="radio" name="sales_status" value="3" id="status_available" <?php if ($sales_status == 3) {echo 'checked="checked"'; } ?> /></li>
                            <li>Rented <input type="radio" name="sales_status" value="4" id="status_rented" <?php if ($sales_status == 4) {echo 'checked="checked"'; } ?> /></li>
                        <?php 
                        } // END RENTAL ELSE
                        ?>
                    </ul>
                    <input type="hidden" name="p" id="sales_status_property_id" value="<?php echo $property; ?>" />
                </form><p></p>
                <form id="info_update_1">
                    <table>
                        <tr><td>Beds </td><td><input type="text" name="bed" value="<?php echo $beds; ?>"  /></td></tr>
                        <tr><td>Bath </td><td><input type="text" name="bath" value="<?php echo $bath; ?>" /></td></tr>
                        <tr><td>SqFt </td><td><input type="text" name="sqft" value="<?php echo $sqft; ?>" /></td></tr>
                        <tr><td>Lot Size </td><td><input type="text" name="lot_size" value="<?php echo $lot_size; ?>" /></td></tr>
                        <tr><td>Property Type </td><td><input type="text" name="property_type" value="<?php echo $property_type; ?>" /></td></tr>
                        <tr><td>Year Built </td><td><input type="text" name="year_built" value="<?php echo $year_built; ?>" /></td></tr>
                        <tr><td>Neighborhood </td><td><input type="text" name="community" id="neighborhood" value="<?php echo $community; ?>" /></td></tr>
                        <tr><td>County </td><td><input type="text" name="county" id="county" value="<?php echo $county; ?>" /></td></tr>
                        <tr><td>MLS# </td><td><input type="text" name="mls" value="<?php echo $mls; ?>" /></td></tr>
                    </table>
                    <input type="hidden" value="<?php echo $property; ?>" name="p" />
                </form>
			</div>
</section>