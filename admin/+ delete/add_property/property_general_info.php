<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_login();
?>
<h3><span class="add_property_name">Marketing</span><span class="add_property_icon">Icon</span></h3>
 <section>
    <h2 class="create_property_headline">Ok. You Have Our Attention.</h2>
        <h3 class="create_property_subhead">Let’s Hear the Pitch.</h3>
        <p>Add a pdf flyer below.</p>
    <script type="text/javascript">
	$(function() {
		
		Dropzone.options.flyerUploaderForm = false;
		
		var flyerUpload = new Dropzone("#flyer_uploader_form", {
			acceptedFiles: "application/pdf", 
			dictDefaultMessage: "Drag and drop a PDF flyer here.", 
			dictInvalidFileType: "The file must be a pdf.", 
			maxFiles: 1, 
		});
		 
		 
		flyerUpload.on("success", function(file, response) {
			$('#flyer_uploader_form').fadeOut(300);
			$("#file_uploaded").prepend( '<img src="/images/pdf_icon.png" alt="pdf icon" id="pdf_icon"/>' + response +'.pdf'); 
			$('#file_uploaded').fadeIn(500);
		});
	});
	</script>
    <div id="file_uploaded" <?php if ($flyer_id == 0) {echo ' class="hide_flyer" '; }?> >

		<?php
			if ($flyer_id != 0) {echo '<img src="/images/pdf_icon.png" alt="pdf icon" id="pdf_icon"/>' . $flyer_name; }
		?>
    	<a href="/delete_flyer?i=<?php echo $property; ?>" id="delete_flyer_button">
			<img src="/images/delete.png" alt="Delete flyer" title="Delete your flyer" class="delete_button">
		</a>
    </div>
    
    <div <?php if ($flyer_id != 0) {echo ' class="hide_flyer" '; } ?> >
    <form action="/_content/add_property/flyer_upload.php?p=<?php echo $property; ?>" class="dropzone " method="post" enctype="multipart/form-data" id="flyer_uploader_form" >
        <div class="fallback">
            <input type="file" multiple name="file" accept="application/pdf" capture="camera" >
            <div id="submit_holder">
                <img src="/images/loading.gif" class="loader" />
                <input type="submit" name="submit" value="Submit" id="upload_image" />
            </div>
        </div>
    </form>
    </div><?php // END HIDE FLYER DIV ?>
    <form id="info_update_2">
    
        <label for="property_title">Property Title</label>
        <input type="text" id="property_title" value="<?php echo $property_title; ?>" name="title" placeholder="Title" maxlength="50" />
        <label for="price">Price</label>
        <input type="text" id="price" value="<?php echo $price; ?>" name="price" placeholder="Price" />
        
        <?php // DESCRIPTION ?>
        <label for="description">Marketing Description</label>
        <textarea name="description" id="marketing_description_text_box"><?php echo $marketing_description; ?></textarea>
        
        <input type="hidden" name="p" value="<?php echo $property; ?>" />
        <input type="hidden" name="submitted" value="true" />
    
    </form>
    <?php // END DESCRIPTION ?>
</section>