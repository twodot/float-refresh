<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_login();
?>

<h3><span class="add_property_name">Float Tour</span><span class="add_property_icon">Icon</span></h3>
 <section>	
 
 <h2 class="create_property_headline">The Big Finale</h2>
 <p>Check it out! Your Beacons have been right here waiting for you. Put them to work by dragging one to the main property box first. This is all you need to get started. However, to truly become a Float master, more Beacons can be added to the features boxes to create a mind-blowing, room-to-room Float Tour in no time. Boom!</p>
	<?php	
	
	
    echo '<div class="holder one_half">' . "\n";
    echo '<ul id="b_con_' . $property_id . '" class="top_parent">';
    
        echo '<li id="li_' . $primary_info_id . '">
            <div id="info_' . $primary_info_id . '" class="property_info">' . "\n";
                
                    if (is_null($main_beacon_title)) {
                        echo '<div id="info_container_' . $primary_info_id . '" class="prop_info_holder property_info_child property_info_avail"> <h3 class="property_title">' . $property_title . '</h3>';
                    }
                    else {
                        echo '<div id="info_container_' . $primary_info_id . '" class="prop_info_holder property_info_child"><h4 class="info_title"> <h3 class="property_title">' . $property_title . '</h3>';
                    }
					
					if (is_null($main_beacon_title)) {
						echo '<div class="beacon_title_holder"><span id="prop_id_' . $primary_info_id . '"></span></div>';
					}
					else {
						echo '<div class="beacon_title_holder beacon_assigned"><span id="prop_id_' . $primary_info_id . '"> Beacon ' . $main_beacon_title . ' <a href="/remove_beacon?b=' . $main_beacon_id . '&p=' . $property_id . '&i=' . $primary_info_id . '" class="remove_beacon" ><img src="/images/delete.png" class="remove_beacon_image" alt="Remove beacon" title="Remove beacon from ' .  $row['info_title'] . '" /></a></span></div>';
					}
					
                echo '</h4>';
                ?>
                <?php 
        echo '</div>' . "\n";
            
        echo '</div><!-- END INFO -->' . "\n"; // END INFO		
        echo '</li>';
	
    
    $r = get_info($user_id, $dbc, $property);
	
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
        
		if ($row['primary_info'] != 1) {
        $info_id =  $row['info_id'];
        $beacon_title = $row['title'];
        echo '<li id="li_' . $row['info_id'] . '">
            <div id="info_' . $row['info_id'] . '" class="property_info">' . "\n";
                
                    if (is_null($beacon_title)) {
                        echo '<div id="info_container_' . $row['info_id'] . '" class="prop_info_holder property_info_child property_info_avail"><h4 class="info_title">' . $row['info_title'];
                    }
                    else {
                        echo '<div id="info_container_' . $row['info_id'] . '" class="prop_info_holder property_info_child"><h4 class="info_title">' . $row['info_title'];
                    }
					
					if (is_null($beacon_title)) {
						echo '<div class="beacon_title_holder"><span id="prop_id_' . $row['info_id'] . '"></span></div>';
					}
					else {
						echo '<div class="beacon_title_holder beacon_assigned"><span id="prop_id_' . $row['info_id'] . '"> Beacon ' . $beacon_title . ' <a href="/remove_beacon?b=' . $row['beacon_id'] . '&i=' . $row['info_id'] . '" class="remove_beacon" ><img src="/images/delete.png" class="remove_beacon_image" alt="Remove beacon" title="Remove beacon from ' .  $row['info_title'] . '" /></a></span></div>';
					}
					
                echo '</h4>';
                ?>
                <?php 
        echo '</div>' . "\n";
            
        echo '</div><!-- END INFO -->' . "\n"; // END INFO			
        } // END PRIMARY CHECK 
        echo '</li>';
    } // END GET INFORMATION 
    echo '</ul>';
	echo '</div><!-- END HOLDER -->' . "\n"; // END HOLDER
    
	
	echo '<div id="property_beacons" class="one_half">';
	echo '<p><a href="/remove_beacons/' . $property . '" id="remove_all" >Remove all beacons from this property</a></p>';
		echo '<div id="available_beacons" >';
		echo '<div id="location_beacons">';
			echo '<h3>Directional Beacons <a href="/remove_location_beacons?p=' . $property . '" ><img src="/images/delete.png" class="remove_beacon_image" alt="Remove all beacons" title="Remove location beacons from ' . $property_title . '" /></a></h3>';
			echo '<div id="location_beacons_holder_' . $property . '" class="location_beacon">';
				$r_location_beacons = get_location_beacons($user_id, $dbc, $property);
				while($row_location_beacons = mysqli_fetch_array($r_location_beacons, MYSQLI_ASSOC)) {
				
					if (!is_null($row_location_beacons['property_title'])) {
						$prop_title = $row_location_beacons['property_title'];
						$link = 'property';
						$link_id = $row_location_beacons['property_id']; 
					}
					elseif (!is_null($row_location_beacons['building_name'])) {
						$prop_title = $row_location_beacons['building_name'];
						$link = 'building';
						$link_id = $row_location_beacons['building_id']; 
					}
						echo '<div id="beacon-' . $row_location_beacons['id'] . '" class="active_beacon" title="Active in ' . $prop_title; if(!empty($row_location_beacons['info_title'])){echo ' / ' . $row_location_beacons['info_title']; }  echo' ">' . $row_location_beacons['title'] . '</div>';
			}
			echo '</div>'; // END LOCATION BEACONS HOLDER	
			echo '<div class="clear"></div>';	
		echo '</div>'; // END LOCATION BEACONS
		echo '<h3>Available Beacons</h3>';
			
		echo '<div class="holder">';
		$r = get_avail_beacons($user_id, $dbc);
		while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
			echo '<div id="beacon-' . $row['id'] . '" class="available_beacon">' . $row['title'] . '</div>';
		}
		
		echo '</div>'; // END HOLDER
		
		echo '<div class="clear"></div>';
		echo '<div class="active_beacons">';
			echo '<h3>Active Beacons</h3>';
			
			echo '<div class="holder_active_beacons">';
			$r = get_active_beacons($user_id, $dbc);
			echo $row['property_title'];
			echo $row['building_name'];
			while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
				
			if (!is_null($row['property_title'])) {
				$prop_title = $row['property_title'];
				$link = 'create_property';
				$link_id = $row['property_id']; 
			}
			elseif (!is_null($row['building_name'])) {
				$prop_title = $row['building_name'];
				$link = 'create_property';
				$link_id = $row['building_id']; 
			}
				
				echo '<a href="/' . $link . '/' .  $link_id . '?step=5'; if(!empty($row['info_title'])){echo '#info_container_' . $row['prop_info_id']; } echo '" ><div id="beacon-' . $row['id'] . '" class="active_beacon" title="Active in ' . $prop_title; if(!empty($row['info_title'])){echo ' / ' . $row['info_title']; }  echo' ">' . $row['title'] . '</div></a>';
			}
			echo '<div class="clear"></div>';
			echo '</div>'; // END ACTIVE HOLDER //
		echo '</div>';// END ACTIVE BEACONS

echo '</div>'; // END BEACONS
		echo '</div>'; // END PROPERTY BEACONS
	
	
	
	
	?>
</section>