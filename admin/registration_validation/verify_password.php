<?php

include('../../float_req/mysqli_connect.php');

if(isset($_GET['pass_confirmation'])) {
	
	$pass_confirmation = $_GET['pass_confirmation'];
	
	if (preg_match ('/^((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})$/', $pass_confirmation) ) {
		echo 'true';
	}
	else {
		echo '"Your password must be between 8 and 20 charactors, have at least one upper case letter, one lower case letter, and one number."';	
	}
}
else {
	echo 'false';
}
?>