<?php

require_login();
$page_title = $page_name = 'Control Panel';

include('includes/header.php');

$Properties = new Properties;
$Beacons = new Beacons;
$json = $Properties->get_properties_json();
$beacon_json = $Beacons->get_beacons_json();
?>
<section class="col-xs-12 col-sm-12">
	<h1>New Property</h1>
	<form id="newProperty" action="/ajax/new_property.php" method="post">
    	<input id="newAddress" name="address" type="text" placeholder="Address"  class="col-xs-8 col-sm-6">
        <input type="hidden" name="keepAddress" value="0" id="keepAddress" />
        <button form="newProperty" id="createProperty" type="submit"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></button>
    </form>
</section>

<section id="activeProperties" class="col-xs-12 col-sm-4">
	<h1>Active Properties</h1>
    <div class="col-xs-12 col-sm-12 no-padding">	
        <div class="col-xs-3 col-sm-3 no-padding"  data-bind="fadeVisible: activeProperties().length > 0">
	       <input type="checkbox" data-bind="checked: showActiveImages" id="showActiveImages" class="hidden"><label for="showActiveImages" class="hand"><i class="fa fa-toggle-on green" aria-hidden="true"  data-bind="visible: showActiveImages"></i><i class="fa fa-toggle-off red" aria-hidden="true"  data-bind="visible: !showActiveImages()"></i> <i class="fa fa-file-image-o" aria-hidden="true"></i></label>
    	</div>
    </div>   
    <div class="loading" data-bind="slideVisible: propertiesLoaded() == 0"><p>Loading</p><i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></div>
    <div id="activateProperty" class="col-xs-12 col-sm-12"  data-bind="droppable: { settings: droppableActivatePropertySettings }"><p><span class="glyphicon glyphicon-download"></span>  Drop Property Here To Activate</p></div>
    <div data-bind="template: { name: 'activePropertiesTemplate',  foreach: activeProperties, beforeRemove: animateSlideUpRemove, afterRender: animateSlideInDown }"></div>
</section>
<section id="inactivatedProperties" class="col-xs-12 col-sm-4">
	<h1>Inactive Properties</h1>
    
    <div data-bind="fadeVisible:  inactiveProperties().length > 0" class="col-xs-12 col-sm-12 no-padding">
    	<div class="col-xs-12 col-sm-12 no-padding">	
            <div class="col-xs-3 col-sm-3 no-padding"  data-bind="fadeVisible: inactiveProperties().length > 0">
                <input type="checkbox" data-bind="checked: showInactiveImages" id="showInactiveImages" class="hidden"><label for="showInactiveImages" class="hand"><i class="fa fa-toggle-on green" aria-hidden="true"  data-bind="visible: showInactiveImages"></i><i class="fa fa-toggle-off red" aria-hidden="true"  data-bind="visible: !showInactiveImages()"></i>  <i class="fa fa-file-image-o" aria-hidden="true"></i></label>
            </div>
            <div class="col-xs-4 col-sm-4"  data-bind="fadeVisible: inactiveProperties().length > 0">
                <input type="checkbox" data-bind="checked: showActivatable" id="showActivatableProperties" class="hidden"><label for="showActivatableProperties" class="hand"><i class="fa fa-toggle-on green" aria-hidden="true"  data-bind="visible: showActivatable"></i><i class="fa fa-toggle-off red" aria-hidden="true"  data-bind="visible: !showActivatable()"></i>  <i class="fa fa-rocket blue" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Show Activatable Properties Only."></i></label>
            </div>
        </div>
    </div>
   
    <div class="loading" data-bind="slideVisible: propertiesLoaded() == 0"><p>Loading</p><i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></div>
    <div id="deactivateProperty" class="col-xs-12 col-sm-12" data-bind="droppable: { settings: droppableDeactivatePropertySettings }"><p><span class="glyphicon glyphicon-download"></span> Drop Property Here To Deactivate</p></div>
    <div data-bind="template: { name: 'inactivePropertiesSetUp', foreach: inactiveProperties, beforeRemove: animateSlideUpRemove, afterRender: animateSlideInDown}"></div>
</section>
<section id="beacons" class="hidden-xs col-sm-4">
	<h1>Available Beacons</h1>
	<div data-bind="template: { name: 'inactiveBeaconsTemplate',  foreach: inactiveBeacons }, beforeRemove: fadeVisible" class="row" id="inactiveBeacons">
    </div>
    
    <div class="loading" data-bind="slideVisible: beaconsLoaded() == 0"><p>Loading</p><i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></div>
    
    <h1>Active Beacons</h1>
	<div data-bind="template: { name: 'activeBeaconsTemplate',  foreach: activeBeacons, afterAdd: fadeElementIn  }" id="activeBeacons" class="row">
    </div>
    
    <div class="loading" data-bind="slideVisible: beaconsLoaded() == 0"><p>Loading</p><i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></div>
    
</section>


<div class="dialog col-xs-10 col-sm-4 no-padding" id="multiple_properties_message">
	<div class="col-xs-12 col-sm-12 no-padding">
		<div class="propertyContent col-xs-12 col-sm-12 no-padding" data-bind="foreach: multipleAddresses">
        	<div class="multipleAddress col-xs-12 col-sm-12" data-bind="text: address"></div> 
		</div>
    </div>
</div>


<div class="dialog col-xs-10 col-sm-4 no-padding" id="property_add_error_message">
	<div class="col-xs-12 col-sm-12 no-padding">
        	<div class="col-xs-12 col-sm-12" data-bind="text: propertyErrorMessage"></div> 
    </div>
</div>


<div class="dialog col-xs-10 col-sm-4 no-padding" id="delete_property_message">
	<!-- ko if: deleteProperty() -->
	<div class="col-xs-12 col-sm-12 no-padding">
		<div class="col-xs-12 col-sm-12 no-padding dialogHeader">
			<p>Delete this property?</p>
		</div>
		<img class="dialogImage"  data-bind="attr: {src: deleteProperty().image}" />
		<div class="propertyContent col-xs-12 col-sm-12">
			<!-- ko if: deleteProperty().title != '' -->
				<div class="propertyTitle"><h4 data-bind="html: deleteProperty().title"></h4></div>
			<!-- /ko -->
			<!-- ko if: deleteProperty().title == '' -->
				<div class="propertyTitle"><h4 data-bind="html: deleteProperty().address"></h4></div>
			<!-- /ko -->
		</div>
    </div>
   <!-- /ko -->
</div>

<script type="text/html" id="inactivePropertiesSetUp">
	<!-- ko if: $data.primary_beacon() -->
		<div class="col-xs-12 col-sm-12 no-padding property" data-bind="css: { canActivate: primary_beacon(), needsBeaconToActivate: !primary_beacon() }, attr: {id: 'property_' + id()}, template: { name: 'inactivePropertiesTemplate'}">
		</div>
	<!-- /ko -->
	<!-- ko ifnot: $data.primary_beacon() -->
		<div class="col-xs-12 col-sm-12 no-padding property" data-bind="css: { canActivate: primary_beacon(), needsBeaconToActivate: !primary_beacon() }, attr: {id: 'property_' + id()}, droppable: { settings: $root.droppableBeaconSettings }, template: { name: 'inactivePropertiesTemplate'}, fadeVisible: !$root.showActivatable()">
		</div>
	<!-- /ko -->
</script>
<script type="text/html" id="inactivePropertiesTemplate">
	
		<div class="col-xs-12 col-sm-12 no-padding iconHeader">
			<div class="col-xs-3 col-sm-3 iconHolder">
				<a data-bind="attr: {href: '/property/' + id() }"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
			</div>
			<div class="col-xs-6 col-sm-6 icon">
				<!-- ko if: ! canActivate() -->
					<i class="fa fa-ban red" aria-hidden="true"  data-toggle="tooltip"  data-placement="top" title="This property needs a beacon assigned to it to activate it."></i>
				<!-- /ko -->
				<!-- ko if: canActivate() -->
					<i class="fa fa-rocket" data-bind="css: {blue: active() == 0, green: active() == 1 }" aria-hidden="true" data-toggle="tooltip"  data-placement="top" title="This property can be activated."></i>
				<!-- /ko -->
	
				&nbsp&nbsp&nbsp&nbsp<span data-bind="text: beacon_count"></span> <i class="fa fa-circle blue" aria-hidden="true"  data-toggle="tooltip" data-placement="top" title="Amout of beacons assigned to this property"></i>
			</div>
			<div class="col-xs-2 col-xs-offset-1 col-sm-2 col-sm-offset-1 icon">
				<i class="fa fa-minus-circle hand red" aria-hidden="true" data-toggle="tooltip" data-bind="click: $root.removeProperty" data-placement="top" title="Delete this property."></i>
			</div>
			
		</div>
		<img class="propertyImage"  data-bind="attr: {src: image}, fadeVisible: $root.showInactiveImages" />
		<div class="propertyContent col-xs-12 col-sm-12">
			<!-- ko if: title() != '' -->
				<div class="propertyTitle"><a data-bind="attr: {href: '/property/' + id() }"><h4 data-bind="html: title"></h4></a></div>
			<!-- /ko -->
			<!-- ko if: title() == '' -->
				<div class="propertyTitle"><a data-bind="attr: {href: '/property/' + id() }"><h4 data-bind="html: address"></h4></a></div>
			<!-- /ko -->
			
			<!-- ko if: beacon_count() > 0 -->
				<a href="#" class="showPropertyBeacons"><span class="glyphicon glyphicon-circle-arrow-down showList"></span> Beacon Details</a>
				<div class="propertyBeacons" data-bind="foreach: beacons" >
					<div class="col-xs-12 col-sm-12 no-padding">
						<div class="beacon col-xs-2 col-sm-2 no-padding">
							<div class="beaconTitle"  data-bind="text: beacon"></div>
						</div>
						<div class="beaconRoom col-xs-8 col-sm-8" data-bind="text: room_title"></div>
					</div>
				</div>
			<!-- /ko -->
		</div>
</script>


<script type="text/html" id="activePropertiesTemplate">
	
	<div class="col-xs-12 col-sm-12 no-padding property" data-bind="css: { isActive: active() }, attr: {id: 'property_' + id()}">
		<div class="col-xs-12 col-sm-12 no-padding iconHeader">
			<div class="col-xs-3 col-sm-3 iconHolder">
				<a data-bind="attr: {href: '/property/' + id() }"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
			</div>
			<div class="col-xs-6 col-sm-6 icon">
				<i class="fa fa-rocket" data-bind="css: {blue: active() == 0, green: active() == 1 }" aria-hidden="true" data-toggle="tooltip"  data-placement="top" title="This property is active."></i>
				
				&nbsp&nbsp&nbsp&nbsp<span data-bind="text: beacon_count"></span> <i class="fa fa-circle blue" aria-hidden="true"  data-toggle="tooltip" data-placement="top" title="Amout of beacons assigned to this property"></i>
			</div>
			<div class="col-xs-2 col-xs-offset-1 col-sm-2 col-sm-offset-1 icon">
				<i class="fa fa-minus-circle hand red" aria-hidden="true" data-toggle="tooltip" data-bind="click: $root.removeProperty" data-placement="top" title="Delete this property."></i>
			</div>
			
		</div>
		<img class="propertyImage"  data-bind="attr: {src: image}, fadeVisible: $root.showActiveImages" />
		<div class="propertyContent col-xs-12 col-sm-12">
			<!-- ko if: title() != '' -->
				<div class="propertyTitle"><a data-bind="attr: {href: '/property/' + id() }"><h4 data-bind="html: title"></h4></a></div>
			<!-- /ko -->
			<!-- ko if: title() == '' -->
				<div class="propertyTitle"><a data-bind="attr: {href: '/property/' + id() }"><h4 data-bind="html: address"></h4></a></div>
			<!-- /ko -->
			
			<!-- ko if: beacon_count() > 0 -->
				<a href="#" class="showPropertyBeacons"><span class="glyphicon glyphicon-circle-arrow-down showList"></span> Beacon Details</a>
				<div class="propertyBeacons" data-bind="foreach: beacons" >
					<div class="col-xs-12 col-sm-12 no-padding">
						<div class="beacon col-xs-2 col-sm-2 no-padding">
							<div class="beaconTitle"  data-bind="text: beacon"></div>
						</div>
						<div class="beaconRoom col-xs-8 col-sm-8" data-bind="text: room_title"></div>
					</div>
				</div>
			<!-- /ko -->
		</div>
    </div>
</script>


<script type="text/html" id="inactiveBeaconsTemplate">
	
	<div class="beacon inactiveBeacon" data-bind="attr: {id: 'beacon_' + id}">
		<div class="beaconTitle" ><h4 data-bind="html: title"></h4></div>
	</div>
</script>

<script type="text/html" id="activeBeaconsTemplate">
	
	<div class="beacon activeBeacon" data-bind="attr: {id: 'beacon_' + id}">
		<div class="beaconTitle" ><h4 data-bind="html: title"></h4></div>
	</div>
</script>


<script>

function Property(data) {
    var self = this;
    self.active = ko.observable(data.active);
	self.address = ko.observable(data.address);
	self.beacon_count = ko.observable(data.beacon_count);
	self.beacons = ko.observable(data.beacons);
	self.canActivate = ko.observable(data.canActivate);
	self.id = ko.observable(data.id);
	self.image = ko.observable(data.image);
	self.price = ko.observable(data.price);
	self.primary_beacon = ko.observable(data.primary_beacon);
	self.title = ko.observable(data.title);
	self.property_data_type = ko.observable(data.property_data_type);
	self.deleted = ko.observable(data.deleted);
}


function Beacon(data) {
    var self = this;
    self.active = ko.observable(data.active);
	self.address = ko.observable(data.address);
	self.id = ko.observable(data.id);
	self.title = ko.observable(data.title);
	self.property_info_title = ko.observable(data.property_info_title);
}

// Overall viewmodel for this screen, along with initial state
function PropertiesViewModel() {
    var self = this;
	
	self.properties =  ko.observableArray();
	
	self.showInactiveImages = ko.observable(false);
	self.showActiveImages = ko.observable(true);
	
	self.deleteProperty = ko.observable(false);
	
	self.beacons = ko.observableArray();
	self.beaconsLoaded = ko.observable(0);
	self.propertiesLoaded = ko.observable(0);
	
	self.showActivatable = ko.observable(0);
	
	self.multipleAddresses = ko.observableArray();
	self.propertyErrorMessage = ko.observable();
	self.inactiveProperties = ko.pureComputed(function() {
		return ko.utils.arrayFilter(self.properties(), function(Property) {
			if(Property().property_data_type() == 'property') {
				if(Property().deleted() != 1) {
					return Property().active() != 1;
				}
			}
		})
    });
	
	self.activeProperties = ko.pureComputed(function() {
		return ko.utils.arrayFilter(self.properties(), function(Property) {
			if(Property().property_data_type() == 'property') {
				if(Property().deleted() != 1) {
					return Property().active() == 1;
				}
			}
		})
    });
	
	self.buildings = ko.computed(function() {
		return ko.utils.arrayFilter(self.properties(), function(Property) {
			if(Property().property_data_type() == 'building') {
					return Property().deleted() != 1
			}
		})
    });
	
	self.activeBeacons = ko.computed(function() {
		return ko.utils.arrayFilter(self.beacons(), function(Beacon) {
			return Beacon().active() === 1;
		})
    });
	

	self.inactiveBeacons = ko.computed(function() {
		return ko.utils.arrayFilter(self.beacons(), function(Beacon) {
			return Beacon().active() === 0;
		})
    });
	
	$.getJSON("/ajax/get_properties.php", function(data) { 
		self.propertiesLoaded(1);
		$.each(JSON.parse(data), function( index, value ) {
			self.properties.push(ko.observable(new Property(value)));
		});
		$('[data-toggle="tooltip"]').tooltip();
	})
	
	
	$.getJSON("/ajax/get_beacons.php", function(data) { 
		self.beaconsLoaded(1);
		$.each(JSON.parse(data), function( index, value ) {
			self.beacons.push(ko.observable(new Beacon(value)));
		});
		$('[data-toggle="tooltip"]').tooltip();
	})
	
	self.fadeElementIn = function(element) {
		$(element).hide();
		$(element).fadeIn(1000);
	}
	
	self.removeProperty = function(property) { 
		
		self.deleteProperty(property);
		$.ui.dialog.prototype._focusTabbable = function(){};
		$( "#delete_property_message" ).dialog({
		  modal: true,
		  dialogClass: "deletePropertyAlert", 
		  draggable: false, 
		  resizable: false, 
		  buttons: [
			{
			  text: "Delete",
			  click: function() {
				$.ajax({
					url: "/ajax/delete_property.php",
					data: property,
					method:"POST", 
					cache: false,
				}).done(function(data) {
					data = jQuery.parseJSON(data);
					if(data.success) {
						 //self.properties.fadeOut(property);
						 property.deleted(1);
					}
					$( "#delete_property_message" ).dialog( "close" );	
				});	 // END AJAX 
			  }, 
			  class: "deleteButton"
			 }, 
			 {
			  text: "Cancel",
			  click: function() {
				$( this ).dialog( "close" );
			  }, 
			  class:"cancelButton"
			}
		  ]
			
		});
		
	}
	
	ko.bindingHandlers.fadeVisible = {
		init: function(element, valueAccessor) {
			// Initially set the element to be instantly visible/hidden depending on the value
			var value = valueAccessor();
			$(element).toggle(ko.utils.unwrapObservable(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
		},
		update: function(element, valueAccessor) {
			// Whenever the value subsequently changes, slowly fade the element in or out
			var value = valueAccessor();
			ko.utils.unwrapObservable(value) ? $(element).fadeIn() : $(element).fadeOut();
		}
	};
	
	ko.bindingHandlers.slideVisible = {
		init: function(element, valueAccessor) {
			// Initially set the element to be instantly visible/hidden depending on the value
			var value = valueAccessor();
			$(element).toggle(ko.utils.unwrapObservable(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
		},
		update: function(element, valueAccessor) {
			// Whenever the value subsequently changes, slowly fade the element in or out
			var value = valueAccessor();
			ko.utils.unwrapObservable(value) ? $(element).slideDown() : $(element).slideUp();
		}
	};
	
	self.animateRemove = function(element) {
		if (element.nodeType === 1) {
			    $(element).fadeOut(500, function() {
					$(element).remove();
				});
		}
	};
	
	self.animateSlideInDown = function(element) {
			$(element).hide(0, function() {
				$(element).slideDown(500, function() {});
			});
		
	};
	
	self.animateSlideUpRemove = function(element) {
			$(element).css({"opacity":0});
			
				$(element).slideUp(500, function() {
					$(element).remove();
				});
			
	};
	
	ko.bindingHandlers.droppable = {
        init: function (element, valueAccessor) {
			var $element = $(element),
                options = ko.unwrap(valueAccessor());

            $element.droppable(options.settings);
            // Handle disposal
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $element.droppable('destroy');
                $element = null;
            });
        },
        update: function (element, valueAccessor) {
            var $element = $(element),
                options = ko.unwrap(valueAccessor()),
				
                action = options.enable ? 'enable' : 'disable';
				

            //$element.droppable(action);
        }
    };
	
	self.droppableBeaconSettings = {
		drop: function (event, ui) {
			var contextTarget = ko.contextFor(this);
			//$( this ).addClass( "ui-state-highlight" )
			var contextBeacon= ko.dataFor(ui.draggable[0]);
			var createJSON = jQuery.parseJSON('{"beacon": ' + ko.toJSON(contextBeacon) + ', "property":' + ko.toJSON(contextTarget.$rawData()) + '}');
			
			$.ajax({
				url: "/ajax/add_beacon_to_property_from_panel.php",
				data: createJSON,
				method:"POST", 
				cache: false,
			}).done(function(data) {
				if(!data.error) {
					contextBeacon.active(data.beacon.active);
					contextBeacon.property_info_title(data.beacon.property_info_title);
					contextTarget.$rawData(new Property(data.property));
				}
				else {
					alert(data.error);
				}
			});	 // END AJAX 
			
			
			//contextTarget.$rawData(new Property(updateProperty));
			//console.log(contextTarget.$rawData());
			//
		},
		accept:".inactiveBeacon",
		activeClass: "dropBeaconHere",
		over: function () {
			// Handle over-event from jQuery droppable
		},
		tolerance: 'pointer'
		// For more settings, see http://api.jqueryui.com/droppable/
	},
	canDrop = ko.observable(true);

	self.droppableActivatePropertySettings = {
		drop: function (event, ui) {
			//var contextTarget = ko.contextFor(this);
			var contextProperty= ko.dataFor(ui.draggable[0]);
			var createJSON = jQuery.parseJSON('{"property":' + ko.toJSON(contextProperty) + '}');
			//console.log(contextProperty);
			 $.ajax({
				url: "/ajax/activate_property_from_panel.php",
				data: createJSON,
				method:"POST", 
				cache: false,
			}).done(function(data) {
			
				if(!data.error) {
					contextProperty.active(data.property.active);
				}
				else {
					alert(data.error);
				}
			});	 // END AJAX 
			
			
			//contextTarget.$rawData(new Property(updateProperty));
			//console.log(contextTarget.$rawData());
			//
		},
		accept:".canActivate",
		activeClass: "activatePropertyHere",
		over: function () {
			// Handle over-event from jQuery droppable
		},
		tolerance: 'pointer'
		// For more settings, see http://api.jqueryui.com/droppable/
	},
	canActivateProperty = ko.observable(true);
	
	self.droppableDeactivatePropertySettings = {
		drop: function (event, ui) {
			//var contextTarget = ko.contextFor(this);
			var contextProperty= ko.dataFor(ui.draggable[0]);
			var createJSON = jQuery.parseJSON('{"property":' + ko.toJSON(contextProperty) + '}');
			
			 $.ajax({
				url: "/ajax/deactivate_property_from_panel.php",
				data: createJSON,
				method:"POST", 
				cache: false,
			}).done(function(data) {
				if(!data.error) {
					contextProperty.active(data.property.active);
				}
				else {
					alert(data.error);
				}
			});	 // END AJAX 
			
			
			//contextTarget.$rawData(new Property(updateProperty));
			//console.log(contextTarget.$rawData());
			//
		},
		accept:".isActive",
		activeClass: "deactivatePropertyHere",
		over: function () {
			// Handle over-event from jQuery droppable
		},
		tolerance: 'pointer'
		// For more settings, see http://api.jqueryui.com/droppable/
	},
	canDeactivateProperty = ko.observable(true);

}

window.vm = new PropertiesViewModel();
ko.applyBindings(vm);

$(document).ready(function(){
	
	$('body').on('click', '.showPropertyBeacons', function(e) {
		e.preventDefault();
		$(this).find('span').toggleClass('showList hideList');
		$(this).parent().find('.propertyBeacons').stop().slideToggle(500);
	});
	
	
	$('#multiple_properties_message').on('click', '.multipleAddress', function(e) {
		data = ko.dataFor(this);
		if(data.address =='None, add the actual address') {
			$('#keepAddress').val(1);
			$( "#multiple_properties_message" ).dialog("close");
			$('#createProperty').click();
		}
		else {
			$('#newAddress').val(data.address);
			$( "#multiple_properties_message" ).dialog("close");
			$('#createProperty').click();
		}
	});
	
	$('#createProperty').click(function(e){
		e.preventDefault();
		
		var formData = $('#newProperty').serialize();
		 $.ajax({
			url: "/ajax/new_property.php",
			data:formData, 
			method:"POST", 
			cache: false,
		}).done(function(data) {
			window.vm.multipleAddresses([]);
			if (data.id) {
				window.location.href = "/property/"+ data.id;
			}
			else if(data.potential_addresses) {
				
				$.each(data.potential_addresses, function( index, value ) {
					window.vm.multipleAddresses.push(ko.observable(value));
					$.ui.dialog.prototype._focusTabbable = function(){};
					$( "#multiple_properties_message" ).dialog({
						  modal: true,
						  title: "Eeny, meeny, miny, mo. <br /><span class=\"popupSubTitle\">Choose the right one<br />and off we go.</span>", 
						  dialogClass: "multiplePropertiesAlert", 
						  draggable: false, 
						  resizable: false, 
					 });
				});
			}
			else if(data.error) {
				window.vm.propertyErrorMessage(data.error);
				$( "#property_add_error_message" ).dialog({
					  modal: true,
					  title: "Oh No!", 
					  dialogClass: "propertyErrorAlert", 
					  draggable: false, 
					  resizable: false, 
				 });
			}
			//console.log(data);
		});
	});
	
	$(document).on("mouseenter", '.inactiveBeacon', function(e){
		var item = $(this); 
		if (!item.is('.ui-draggable')) {
			item.draggable({
				revert: "invalid", 
				zIndex: 100, 
				scroll:true, 
				stop: function(event, ui) {
					//console.log(ko.dataFor(this));
				}
			});
		}
	}); 
	
	$(document).on("mouseenter", '.canActivate', function(e){
		var item = $(this); 
		if (!item.is('.ui-draggable')) {
			item.draggable({
				revert: "invalid", 
				zIndex: 100, 
				scroll:true, 
				stop: function(event, ui) {
					//console.log(ko.dataFor(this));
				}
			});
		}
	}); 
	
	$(document).on("mouseenter", '.isActive', function(e){
		var item = $(this); 
		if (!item.is('.ui-draggable')) {
			item.draggable({
				revert: "invalid", 
				zIndex: 100, 
				scroll:true, 
				stop: function(event, ui) {
					//console.log(ko.dataFor(this));
				}
			});
		}
	}); 
	
});
</script>
<?php
include('includes/footer.php');

?>