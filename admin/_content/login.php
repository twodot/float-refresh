<?php
$page_title = $page_name ='Login';
login_forward();

include('includes/header.php');

if (isset($_POST['submitted'])) {
	$errors = array();
	if (!empty($_POST['email'])){
		$e=$dbc->real_escape_string($_POST['email']);
	}
	else {
		$e=FALSE;
		
		$errors[] = '<p>You forgot to enter your email address!</p>';
	}
	
	if (!empty($_POST['pass'])) {
		$p = $dbc->real_escape_string($_POST['pass']);
	}
	else {
		$p = FALSE;
		
		$errors[] = '<p>You forgot to enter your password!</p>';
	}
	
	if ($e && $p) {
		$q = "SELECT user_id, first_name, last_name, user_level, email FROM users WHERE (email='$e' AND pass=SHA1('$p')) AND active IS NULL";
		
		$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
		
		if (@mysqli_num_rows($r) == 1 ) {
			$_SESSION['login'] = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			mysqli_free_result($r);
			
			mysqli_close($dbc);
			
			
			$url = BASE_URL . 'panel';
			
			ob_end_clean();
		
			header("Location: $url");
			
			exit();
		}
		else {
			$errors[] = '<p>Either the email address and password do not match those on file or you have not activated your account.</p><br />
				<p>Forgot your password?<br />Don&rsquo;t worry you can <a href="/forgot_password" class="body_link" title="Password Retrieval">retrieve it here.</a></p>';
		}
	}
	else {
		$errors[] = '<h3>Please try again</h3>
		<p>Forgot your password?<br />Don&rsquo;t worry you can <a href="/forgot_password" class="body_link" title="Password Retrieval">retrieve it here.</a></p>';
	}
	
	mysqli_close($dbc);
	
	if (!empty($errors)) {
		echo '<div class="access error col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3">';
			foreach($errors as $key => $val) {
				echo $val;
			}
		echo '</div>';
	}

	
}
?>
<section class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3">
        <div id="login"><h1>Login</h1>
    		<form action="login" method="post" id="loginForm">
                <input type="text" name="email" size="20" maxlength="80" <?php if(isset($e)) {echo 'value="' . $e . '"'; }?> placeholder="Email" id="emailLogin" class="col-xs-12 col-sm-12" />
               <input type="password" name="pass" size="20" maxlength="20" placeholder="password" id="passwordLogin" class="col-xs-12 col-sm-12" />
                <button class="submit" type="submit"  form="loginForm">Login</button>
                <input type="hidden" name="submitted" value="TRUE" />
            </form>
     </div> 
 </section>
 <?php
 include('includes/footer.php');
 ?>