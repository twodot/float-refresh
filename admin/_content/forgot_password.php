<?php
login_forward();

$page_title = $page_name ='Forgot Password';

echo'<div id="bumper">';

$sent = false;
if (isset($_POST['submitted'])) {
	
	$uid= FALSE;
	if (!empty($_POST['email'])) {
		$q = 'SELECT * FROM users WHERE email="' . $dbc->real_escape_string($_POST['email']) . '"';
		$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
		if (mysqli_num_rows($r) ==1) {
			list($uid) = mysqli_fetch_array($r, MYSQLI_NUM);
			$e = $_POST['email'];
		}
		else {
			include('includes/header.php');
			echo '<div class="error login_text">The submitted email address does not match those on file!</div>';
		}
	}
	else {
		include('includes/header.php');
		echo '<div class="error login_text">You forgot to enter your email address!</div>';
	}
	
	if ($uid) {
		$e = $dbc->real_escape_string($_POST['email']);
		$p = substr ( md5(uniqid(rand(), true)), 3, 10);
		$q = "UPDATE users SET pass=SHA1('$p') WHERE user_id=$uid LIMIT 1";
		$r = mysqli_query($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
		if (mysqli_affected_rows($dbc) == 1) {
			
				
				include('email_templates/forgot_password.php');
				$HTMLEmailoutput = ob_get_contents();
				$HTMLEmailoutput = str_replace('[password]', $p, $HTMLEmailoutput);
				ob_end_clean();
				
				
				
				//Create a new PHPMailer instance
				include(MAIL_WHOOPS);
				
				$mail_whoops->FromName = 'Whoops From Float';
				$mail_whoops->addAddress($e);     // Add a recipient
				$mail_whoops->addReplyTo('whoops@' . EMAIL_URL, 'Whoops From Float');
				
				$mail_whoops->WordWrap = 50;                                 // Set word wrap to 50 characters
				$mail_whoops->isHTML(true);                                  // Set email format to HTML
				
				$mail_whoops->Subject = 'Hey There Forgetful, Here\'s Your New Password';
				
				$mail_whoops->Body    = $HTMLEmailoutput;
				
				
				//send the message, check for errors
				echo'<div id="bumper">';
				include('includes/header.php');
				echo '<div id="forgot_password" class="col-xs-12 col-xs-offset-0 col-sm-4 col-sm-offset-4">';
					if (!$mail_whoops->send()) {
						echo '<p>We could not send you an email, please check your email address.</p>';
					} else {
						echo '<h1>Alright!</h1><p>You&rsquo;re almost ready. We&rsquo;ve sent you an email with your new password, please login and change it to something less forgetful. Don&rsquo;t forget to check your junk and spam folders. If you still didn&rsquo;t get an email, <a href="mailto:' . ADMIN . '">contact our administrator</a></p>';
						?>
                        <form action="login" method="post" id="loginForm">
                            <input type="text" name="email" size="20" maxlength="80" <?php if(isset($e)) {echo 'value="' . $e . '"'; }?> placeholder="Email" id="emailLogin" class="col-xs-12 col-sm-12" />
                           <input type="password" name="pass" size="20" maxlength="20" placeholder="password" id="passwordLogin" class="col-xs-12 col-sm-12" />
                            <button class="submit" type="submit"  form="loginForm">Login</button>
                            <input type="hidden" name="submitted" value="TRUE" />
                        </form>
                        <?php
					}
					$sent = true;
				echo '</div>'; // END FORGOT PASSWORD (AFTER MAIL IS SENT)
				
		}
		else {
			echo'<div class="error login_text">Your password could not be changed due to a system error. We appologize for any inconvienence.</div>';
		}
		
	die();
	}
	
	mysqli_close($dbc);
}
if(!$sent) {
	if (!isset($_POST['submitted'])) {
		include('includes/header.php');
	}
	echo '<div id="forgot_password" class="col-xs-12 col-xs-offset-0 col-sm-4 col-sm-offset-4"><h3>Forgot Password</h3>';
	?>
		<form action="forgot_password" id="forgotPasswordForm" method="post">
			<input type="email" name="email" size="20" maxlength="40" value="<?php if (isset($_POST['email'])) echo $_POST['email']; ?>" placeholder="Email" class="col-xs-12 col-sm-12" />
			<button class="submit" type="submit"  form="forgotPasswordForm">Reset My Password</button>
			<input type="hidden" name="submitted" value="TRUE" />
		</form>
	 <?php
	 echo '</div>'; // END #FORGOT PASSWORD
}
if (isset($_POST['submitted']) && !$uid) {
		echo'<div class="error login_text">Please try again.</div>';
	} 

echo '<div>'; // END BUMPER
 include('includes/footer.php');
 ?>