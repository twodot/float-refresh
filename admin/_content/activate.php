<?php

if(isset($_SESSION)) {
	$_SESSION = array();
}
$page_title = 'Activate Your Acount';

$page_name = 'Activation';
echo '<div id="bumper">';
include('includes/header.php');

require_once (MYSQL);
ob_start();
$x = $y = FALSE;

if (isset($_GET['x'])) {
	$x = str_replace('%40', '@', $_GET['x']);
}

if (isset($_GET['y']) && strlen($_GET['y']) == 32) {
	$y = $_GET['y'];
}

if ($x && $y) {
	
	
	$q = "UPDATE users SET active = NULL WHERE (email='" . mysqli_real_escape_string($dbc, $x) . "' AND active='" . mysqli_real_escape_string($dbc, $y) . "') LIMIT 1";
	
	$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
	
	if (mysqli_affected_rows($dbc) == 1) {
		echo '<p class="active_success">Your account is now active. you may log in.</p>
		<section class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3">
        <div id="login"><h1>Login</h1>
    
            <form action="login" method="post" id="loginForm">
            <input type="text" name="email" size="20" maxlength="80" ' . (isset($e)?'value="' . $e . '"':'')  . 'placeholder="Email" id="emailLogin" class="col-xs-12 col-sm-12" />
           <input type="password" name="pass" size="20" maxlength="20" placeholder="password" id="passwordLogin" class="col-xs-12 col-sm-12" />
            <button class="submit" type="submit"  form="loginForm">Login</button>
            <input type="hidden" name="submitted" value="TRUE" />
            </form>
     </div> 
 </section>
 </div>';
	}
	else {
		$admin = ADMIN;
		echo '<div class="error" style="width:342px; margin:auto;"><p>Your account could not be activated. <br />Please re-check the link or contact the <br /><a href ="mailto:' . $admin . '" class ="body_link"><u>system administrator</u></a>.</p></div></div>';
	}
	mysqli_close($dbc);
}
else {
	$url = BASE_URL;
	
	ob_end_clean();
	
	header("location: $url");
	
	exit();
}

?>