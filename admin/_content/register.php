
<?php

$_SESSION = array();

$page_title = $page_name = 'Register';
login_forward();

$regCheck = FALSE;
echo'<div id="bumper">';
if (isset($_POST['submitted'])) {
	
	$trimmed = array_map('trim', $_POST);
	
	$fn = $ln = $e = $p = FALSE;
	
	// FIRST NAME VALIDATION
	$errors = array();
	if (preg_match ('/^[A-Z \'.-]{2,20}$/i', $trimmed['first_name'])) { 
		$fn = $dbc->real_escape_string($trimmed['first_name']);
	}
	else {
		$errors[] = '<p>Please enter your first name!</p>';
	}
	
	// LAST NAME VALIDATION
	
	if (preg_match ('/^[A-Z \'.-]{2,40}$/i', $trimmed['last_name']))  { 
		$ln = $dbc->real_escape_string($trimmed['last_name']);
	}
	else {
		$errors[] = '<p>Please enter your last name!</p>';
	}
	
	// REFFERAL CODE
	$referral_code = '';
	if (isset($trimmed['referral_code']))  { 
		$referral_code = $dbc->real_escape_string($trimmed['referral_code']);
		
	}
	
	// EMAIL VALIDATION
	if (preg_match ('/^[\w.-]+@[\w.-]+\.[A-Za-z]{2,6}$/', $trimmed['email'])) {
		$e = $dbc->real_escape_string($trimmed['email']);
	}
	else {
		$errors[] = '<p>Please enter a valid email address!<p>';
	}
	
	// BOTH PASSWORD VALIDATION
	
	if (preg_match ('/^((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})$/', $trimmed['pass_confirmation']) ) {
		if ($trimmed['pass_confirmation'] == $trimmed['pass']){
			$p = $dbc->real_escape_string($trimmed['pass_confirmation']);
		}
		else { 
			$errors[] = '<p>Your password did not match the confirmed password!</p>';
		}
	}
	else {
		$errors[] = '<p>Please enter a valid password! (It must be between 8 and 20 charactors, at least one upper case letter, one lower case letter, and one number.)</p>';
	}
	
	if ($fn && $ln && $e && $p) {
		$q = "SELECT user_id FROM users WHERE email='$e'";
		$r = mysqli_query($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
		
		if (mysqli_num_rows($r) == 0) {
			
			// ADD MAIL CHIMP SECTION
			$data = '
			{
				"email_address": "' . $e . '",
				"status": "subscribed",
				"merge_fields": {
					"FNAME": "' . $fn . '",
					"LNAME": "' . $ln . '"
				}
			}
			';
			//SUBSCRIBE MEMBER 
			$mcSubscriberJson = mc_request('POST', 'lists/' . FLOAT_LIST_ID . '/members/', $data);
			$mcSubscriber = json_decode($mcSubscriberJson);
			
			// ADD MEMBER TO NO SUBSCRIPTION LIST
			$emailHash = md5(strtolower($e));
			$data ='{"interests": {"' . SUBSCRIPTION_TYPE_NONE . '": true}}';
			$url = '/lists/' . FLOAT_LIST_ID . '/members/' . $emailHash;
			$mcMemberJson = mc_request('PUT', $url, $data);
			$mcMember = json_decode($mcMemberJson);
			
			// END ADD MAIL CHIMP SECTION
			
			
			$a = md5(uniqid(rand(), true));
			
			$q_id = "SELECT user_id FROM users WHERE loyalty_id='$referral_code' LIMIT 1";
			$r_id = mysqli_query($dbc, $q_id);
			$row_id = mysqli_fetch_array($r_id, MYSQLI_ASSOC);
			$referral_id = $row_id['user_id'];
			$q = "INSERT INTO users (email, pass, first_name, last_name, active, registration_date, referred_by, referred_loyalty_code) VALUES ('$e', SHA1('$p'), '$fn', '$ln', '$a', NOW(), '$referral_id', '$referral_code')";
			$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL ERROR: " . mysqli_error($dbc));
			
			
			if (mysqli_affected_rows($dbc) == 1) {
				
				$insert_id = mysqli_insert_id($dbc);
				$hash = substr($emailHash, 0, 3);
				$name = $fn .  substr($ln, 0, 1);
				$loyalty_id =  $name .  $insert_id  . $hash;
				$q_u = "UPDATE users SET loyalty_id = '$loyalty_id' WHERE user_id = $insert_id";
				$r = mysqli_query($dbc, $q_u);
				
				$activete_url = '<a href="' . BASE_URL . 'activate?x=' . urlencode($e) . '&y=' . $a . '">activate</a>';
				
				include('email_templates/activate_account.php');
				$HTMLEmailoutput = ob_get_contents();
				$HTMLEmailoutput = str_replace('[ACTIVATE]', $activete_url, $HTMLEmailoutput);
				ob_end_clean();
				
				
				$HTMLEmailoutput_notification = file_get_contents('email_templates/activate_account_notification.php');
				$HTMLEmailoutput_notification = str_replace('[FIRST_NAME]', $fn, str_replace('[LAST_NAME]', $ln, str_replace('[EMAIL]', $e ,$HTMLEmailoutput_notification)));
				
				
				include(MAIL_HELLO);

				$mail_hello->From = 'hello@' . EMAIL_URL;
				$mail_hello->FromName = 'Hello From Float';
				$mail_hello->addAddress($e, $fn . ' ' . $ln);    // Add a recipient
				$mail_hello->addReplyTo('hello@' . EMAIL_URL, 'Hello From Float');
				
				$mail_hello->WordWrap = 50;                                 // Set word wrap to 50 characters
				$mail_hello->isHTML(true);                                  // Set email format to HTML
				
				$mail_hello->Subject = 'Hey There, Thanks For Registering';
				
				$mail_hello->Body    = $HTMLEmailoutput;

				//send the message, check for errors
				echo'<div id="bumper">';
				include('includes/header.php');
				if (!$mail_hello->send()) {
					echo '<div id="reg_welcome"><p>We could not send you an email, but you have been registered. Please contact  <a href="mailto:' . ADMIN . '">an administrator</a> to activate your account.</p></div>';
				} else {
					echo '<div id="reg_welcome"><h1>Alright!</h1><p>You&rsquo;re almost ready. We&rsquo;ve sent you an email to activate your account. Please click the link. You&rsquo;ll be off and running soon.</p><p>If you don&rsquo;t receive an email soon, please check your junk and spam folders. Otherwise, contact our <a href="mailto:' . ADMIN . '">administrator</a></div>';
				}
				
				include(MAIL_SALES);
				
				$mail_sales->FromName = 'Float';
				$mail_sales->addAddress('info@' . EMAIL_URL, 'Float');    // Add a recipient
				$mail_sales->addReplyTo('sales@' . EMAIL_URL, 'Float');
				
				$mail_sales->WordWrap = 50;                                 // Set word wrap to 50 characters
				$mail_sales->isHTML(true);                                  // Set email format to HTML
				
				$mail_sales->Subject = 'New Float Registrant';
				
				$mail_sales->Body = $HTMLEmailoutput_notification;
				
				$mail_sales->send();
				
				$regCheck = true;
				
			}
			else {
				$errors[] = '<p>You could not be registered due to a system error. We apologize for any inconvienence.</p>';
			}
			
		}
		else {
			$errors[] = '<p>That email address has already been registered. If you have forgotten your password, please go <a href="/forgot_password" class="body_link">here</a> to have your password sent to you.</p>';
		}
	}
	else {
		$errors[] = '<p>Please re-enter your passwords and try again.</p>';
	}
	
	mysqli_close($dbc);
}

if (!$regCheck) {

include('includes/header.php');
?>

<style>
	.error.checked {
		background: url("../images/check.png") no-repeat 0px 0px;
		float: right;
		position: relative;
		width: 20px;
		z-index: 20000;
		margin: -30px 9px 0px 0px;
	}
</style>

<?php
if (!empty($errors)) {
	echo '<div class="access error">';
		foreach($errors as $key => $val) {
			echo $val;
		}
	echo '</div>';
}

?>
<div id="log_register"><p>Already a member? <a href="/login" class="body_link">Login!</a></p></div>

<section class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3">
<div id="login">
    <h1>Register</h1>
        <form action="/register" method="post" id="registerForm" data-toggle="validator" role="form">
           <div class="form-group has-feedback clearfix"><input type="text" name="first_name" size="20" id="first_name" maxlength="20" value="<?php if (isset($trimmed['first_name'])) echo $trimmed['first_name']; ?>" placeholder="First Name"  class="col-xs-12 col-sm-12 form-control" required /></div>
           <div class="form-group has-feedback clearfix"><input type="text" name="last_name" size="20" maxlength="40" value="<?php if (isset($trimmed['last_name'])) echo $trimmed['last_name']; ?>" placeholder="Last Name"  class="col-xs-12 col-sm-12 form-control" required /></div>
            <div class="form-group has-feedback clearfix">
	            <input type="email" name="email" size="30" maxlength="80" value="<?php if (isset($trimmed['email'])) echo $trimmed['email']; ?>" placeholder="Email" data-validation="email"  class="col-xs-12 col-sm-12 form-control" data-error="The email address is invalid" required />
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<div class="help-block with-errors"></div>
            </div>
           <div class="form-group has-feedback clearfix">
           		<input type="password" name="pass" size="20" maxlength="20" class="col-xs-12 col-sm-12 form-control" data-minlength="8" id="inputPassword" placeholder="Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$" required data-error="Your password must have a minimum of 8 characters and have at least one uppercase letter one lowercase letter and one number" />
           		<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<div class="help-block with-errors"></div>
           </div>
           <div class="form-group has-feedback clearfix">
           		<input type="password" class="col-xs-12 col-sm-12 form-control" id="inputPasswordConfirm" data-match="#inputPassword" name="pass_confirmation" data-minlength="8" data-match-error="Whoops, these don't match" placeholder="Confirm" required>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<div class="help-block with-errors"></div>
           </div>
            
           <div class="form-group has-feedback clearfix"><input type="text" name="referral_code" size="20" maxlength="20" value="<?php if (isset($trimmed['referral_code'])) echo $trimmed['referral_code']; ?>" placeholder="Referral Code"  class="col-xs-12 col-sm-12" /></div>
            
            
            <button class="submit btn btn-primary" type="submit"  form="registerForm">Login</button>
            <input type="hidden" name="submitted" value="TRUE" />
         </form>
    </div>
</section>
 </div> 
 
<?php
} // END REG CHECK 

include('includes/footer.php');

?>