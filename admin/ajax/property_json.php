<?php
	include("../includes/config.inc.php");
	//require_login();
	$property_id='';
	$pro='';
	if(isset($_GET["property"]))
		$property_id=(int)$_GET["property"];
	users_property($property_id);
	try{
		$pro=new Property($property_id, $dbc);
		// header('Content-Type: application/json');
	} catch(Exception $e){
		echo "Caught exception: ".$e->getMessage()."<br>";
	}
	
	$json_data = $_POST['data'];
	
	$data=json_decode(json_decode($json_data));
	
	//var_dump($data);
	// fields never changed
	unset($data->building);
	unset($data->building_sort);
	unset($data->updated);
	unset($data->media);
	unset($data->beacon_list);
	
	$pro->set_title($data->property_title);
	unset($data->property_title);
	$pro->set_description($data->description);
	unset($data->description);
	$thePrice = (int) str_replace(',', '', substr($data->price, 1));
	$pro->set_price($thePrice);
	unset($data->price);
	// active
	if($data->active)
		$pro->activate();
	else
		$pro->deactivate();
	unset($data->active);
	$pro->set_address($data->address);
	unset($data->address);
	$pro->set_city($data->city);
	unset($data->city);
	$pro->set_state($data->state);
	unset($data->state);
	$pro->set_zip($data->zip);
	unset($data->zip);
	$pro->set_latitude($data->latitude);
	unset($data->latitude);
	$pro->set_longitude($data->longitude);
	unset($data->longitude);
	$pro->set_status($data->status);
	unset($data->status);
	$pro->set_status_message($data->status_message);
	unset($data->status_message);
	$pro->set_co_agent($data->co_agent);
	unset($data->co_agent);
	$pro->set_beds($data->beds);
	unset($data->beds);
	$pro->set_bath($data->bath);
	unset($data->bath);
	
	
	$pro->set_rooms($data->rooms);
	unset($data->rooms);
	$pro->set_number_of_floors($data->numberOfFloors);
	unset($data->numberOfFloors);
	$pro->set_basement($data->basement);
	unset($data->basement);
	$pro->set_parking_type($data->parkingType);
	unset($data->parkingType);
	$pro->set_heating_sources($data->heatingSources);
	unset($data->heatingSources);
	$pro->set_heating_system($data->heatingSystem);
	unset($data->heatingSystem);
	$pro->set_appliances($data->appliances);
	unset($data->appliances);
	$pro->set_year_updated($data->yearUpdated);
	unset($data->yearUpdated);
	$pro->set_zillow_disclaimer($data->zillow_disclaimer);
	unset($data->zillow_disclaimer);
	$pro->set_sqft($data->sqft);
	unset($data->sqft);
	$pro->set_lot_size($data->lot_size);
	unset($data->lot_size);
	$pro->set_property_type($data->property_type);
	unset($data->property_type);
	$pro->set_style($data->property_style);
	unset($data->property_style);
	$pro->set_year_built($data->year_built);
	unset($data->year_built);
	$pro->set_community($data->community);
	unset($data->community);
	$pro->set_county($data->county);
	unset($data->county);
	$pro->set_mls($data->mls);
	unset($data->mls);
	$pro->set_walkscore($data->walkscore);
	unset($data->walkscore);
	$pro->set_walkscore_link($data->walkscore_link);
	unset($data->walkscore_link);
	// show_walksore
	if($data->show_walkscore)
		$pro->set_show_walkscore();
	else
		$pro->unset_show_walkscore();
	unset($data->show_walkscore);
	// show_greatschools
	if($data->show_greatschools)
		$pro->set_show_greatschools();
	else
		$pro->unset_show_greatschools();
	unset($data->show_greatschools);
	// collapse_schools
	if($data->collapse_schools)
		$pro->set_collapse_schools();
	else
		$pro->unset_collapse_schools();
	unset($data->collapse_schools);
	$pro->set_short_url_hash($data->short_url_hash);
	unset($data->short_url_hash);
	$pro->set_facebook_share($data->facebook_share);
	unset($data->facebook_share);
	$pro->set_twitter_share($data->twitter_share);
	unset($data->twitter_share);
	// deleted: in case needed
/*
	if($data->deleted)
		$pro->set_deleted();
	else
		$pro->unset_deleted();
*/
	unset($data->deleted);
	$pro->set_property_data_type($data->property_data_type);
	unset($data->property_data_type);
	$pro->set_progress($data->progress);
	unset($data->progress);
	// completed
	if($data->completed)
		$pro->set_completed();
	else
		$pro->unset_completed();
	unset($data->completed);
	$pro->set_amenities($data->amenities);
	unset($data->amenities);
	$pro->set_crm($data->crm);
	unset($data->crm);
	$pro->set_crm_field_1_value($data->crm_field_1_value);
	unset($data->crm_field_1_value);
	$pro->set_crm_field_2_value($data->crm_field_2_value);
	unset($data->crm_field_2_value);
	$pro->set_crm_field_3_value($data->crm_field_3_value);
	unset($data->crm_field_3_value);
	$pro->set_crm_field_4_value($data->crm_field_4_value);
	unset($data->crm_field_4_value);
	$pro->set_crm_field_5_value($data->crm_field_5_value);
	unset($data->crm_field_5_value);
	$pro->set_crm_field_6_value($data->crm_field_6_value);
	unset($data->crm_field_6_value);
	
	/* info_list */
	// First iteration: change and add
	foreach($data->info_list as $new_info){
		$exist=false;
		// info exists
		foreach($pro->get_info_list() as $exist_info){
			if($new_info->info_id==$exist_info->get_id()){
				$exist=true;
				$exist_info->keep=true;
				// not needed
				unset($new_info->properties_id);
				unset($new_info->building_id);
				unset($new_info->location);
				
				/* subinfo_list */
				if(isset($new_info->subinfo_list)){
					// First iteration: change and add
					foreach($new_info->subinfo_list as $new_subinfo){
						$subinfo_exist=false;
						// info exists
						foreach($exist_info->get_subinfo_list() as $exist_subinfo){
							if($new_subinfo->info_id==$exist_subinfo->get_id()){
								$subinfo_exist=true;
								$exist_subinfo->keep=true;
								// not needed
								unset($new_subinfo->properties_id);
								unset($new_subinfo->building_id);
								unset($new_subinfo->location);
								unset($new_subinfo->has_360_image);
								unset($new_subinfo->link_360_image);
								unset($new_subinfo->media);
								unset($new_subinfo->link_beacon);
								unset($new_subinfo->link_beacon_text);
								unset($new_subinfo->link_beacon_link);
								unset($new_subinfo->beacon_title);
								unset($new_subinfo->beacon_id);
								// modify info
								$exist_subinfo->set_title($new_subinfo->title);
								unset($new_subinfo->title);
								$exist_subinfo->set_text($new_subinfo->text);
								unset($new_subinfo->text);
								$exist_subinfo->set_notes($new_subinfo->notes);
								unset($new_subinfo->notes);
								$exist_subinfo->set_child_of($new_subinfo->child_of);
								unset($new_subinfo->child_of);
								$exist_subinfo->set_sort_order($new_subinfo->sort_order);
								unset($new_subinfo->sort_order);
								$exist_subinfo->set_serial_num($new_subinfo->serial_num);
								unset($new_subinfo->serial_num);
							}
						}
						// info does not exist in DB and need to be added
						if(!$subinfo_exist){
							$exist_info->add_subinfo(get_object_vars($new_subinfo));
							//echo "Some subinfo needs to be added<br>";
						}
					}
					// Second iteration: delete
					foreach($exist_info->get_subinfo_list() as $exist_subinfo){
						if(!isset($exist_subinfo->keep))
							//echo("Some subinfo needs to be deleted<br>");
							$pro->remove_property_info($exist_info->get_id());
						unset($exist_subinfo->keep);
					}
				}
				unset($new_info->subinfo_list);
				
				// modify info
				// $exist_info->set_beacon_id($new_info->beacon_id);
				unset($new_info->beacon_title);
				// unset($new_info->beacon_id);
				$exist_info->set_title($new_info->title);
				unset($new_info->title);
				$exist_info->set_text($new_info->text);
				unset($new_info->text);
				$exist_info->set_notes($new_info->notes);
				unset($new_info->notes);
				$exist_info->set_child_of($new_info->child_of);
				unset($new_info->child_of);
				$exist_info->set_sort_order($new_info->sort_order);
				unset($new_info->sort_order);
				$exist_info->set_serial_num($new_info->serial_num);
				unset($new_info->serial_num);
				
				if(is_array($new_info->media) && !empty($new_info->media)) {
					$new_info->media = implode(", ", $new_info->media);
				}
				else {
					$new_info->media = '';	
				}
				$exist_info->set_media_string($new_info->media);
				unset($new_info->media);
				if($new_info->link_beacon)
					$exist_info->set_link_beacon();
				else
					$exist_info->unset_link_beacon();
				unset($new_info->link_beacon);
				$exist_info->set_link_beacon_text($new_info->link_beacon_text);
				unset($new_info->link_beacon_text);
				$exist_info->set_link_beacon_link($new_info->link_beacon_link);
				unset($new_info->link_beacon_link);
				if($new_info->has_360_image)
					$exist_info->set_360_image();
				else
					$exist_info->unset_360_image();
				unset($new_info->has_360_image);
				$exist_info->set_link_360_image($new_info->link_360_image);
				unset($new_info->link_360_image);
			}
		}
		// info does not exist in DB and need to be added
		if(!$exist){
			$exist_info->add_subinfo(get_object_vars($new_info));
			//echo "Some info needs to be added<br>";
		}
	}
	// Second iteration: delete
	foreach($pro->get_info_list() as $exist_info){
		if(!isset($exist_info->keep))
			//echo("Some info needs to be deleted<br>");
			$pro->remove_property_info($exist_info->get_id());
		unset($exist_info->keep);
	}
	unset($data->info_list);
	
	/* flyers */
	unset($data->flyer_id);
	unset($data->flyer_name);
	unset($data->fileData);
	
	$pro->update_DB();
	
	//var_dump($data);
	
	function mediaCat(array &$arr){
		$str='';
		foreach($arr as $value)
			$str.=', '.$value;
		return substr($str, 1);
	}
?>