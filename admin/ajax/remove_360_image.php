<?php

require_once('../includes/config.inc.php');
require_login();

$jsonPostData =  json_decode($_POST['data']);
$postData = json_decode($jsonPostData);
$errors = array();


if (isset($postData->info_id) && isset($postData->propertyid)) {
	$p = (int) $postData->propertyid;
	$i = (int) $postData->info_id;
	$q = "SELECT user_id FROM properties WHERE id = $p";
	$r = mysqli_query($dbc, $q);
	if(mysqli_num_rows($r) == 1) {
	
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		if($row['user_id'] == $user_id) {
			$q = "UPDATE property_info SET has_360_image = 0, link_360_image ='' WHERE id = $i AND properties_id = $p LIMIT 1";
			$r = mysqli_query($dbc, $q);
			header('Content-Type: application/json');
			echo '{"success":true}';
		} // END USER ID CHECK
		else {
			header('Content-Type: application/json');
			echo '{"success":false}';
			exit();
		}
	} // END NUM ROWS CHECK
	else {
		header('Content-Type: application/json');
		echo '{"success":false}';
		exit();
	}
}// END POST PROPERTY CHECK
else {
	header('Content-Type: application/json');
	echo '{"success":false}';
	exit();
}
