<?php

require_once('../includes/config.inc.php');
	
	$jsonPostData =  json_decode($_POST['data']);
	$postData = json_decode($jsonPostData);
	$errors = array();
	
	if(isset($postData->address) && isset($postData->city) && isset($postData->state)) {
		$url = 'http://www.zillow.com/webservice/GetDeepSearchResults.htm?zws-id=' . ZILLOW . '&address=' . urlencode($postData->address) . '&citystatezip=' . urlencode($postData->city) . '+' . urlencode($postData->state);
		
		
		$ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
		
        curl_close($ch); 
		libxml_use_internal_errors(true);
		$xml = simplexml_load_string($output);
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);
		
		if(isset($array['message']['code']) && $array['message']['code'] ==0) {
			if(isset($array["response"]['results']['result']['zpid'])) {
				
				$url = 'http://www.zillow.com/webservice/GetUpdatedPropertyDetails.htm?zws-id=' . ZILLOW . '&zpid=' . urlencode($array["response"]['results']['result']['zpid']);
	
				$ch2 = curl_init(); 
				curl_setopt($ch2, CURLOPT_URL, $url); 
				curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1); 
				$outputDeep = curl_exec($ch2); 
				
				curl_close($ch2); 
				$xmlDeep = simplexml_load_string($outputDeep);
				$jsonDeep = json_encode($xmlDeep);
				$json= '{"setup": ' . $json . ', "updated": '. json_encode($xmlDeep) . '}';
				echo $json;
				exit();
				$return = true;
			}
			else {
				$errors[] = $array['message']['text'];
				$return = false;
			}
		}
		else if(isset($array['message']['code']) && $array['message']['code'] != 0) {
			$return = false;
			$errors[] = $array['message']['text'];
		}
		else {
			$errors[] = 'No Data Found';
			$return = false;
		}
		
	}
	else {
		$errors[] = 'No Data Found';
		$return = false;
	}
	
	if($return) {
		$data = $json;
	}
	else {
		$data = json_encode($errors);
	}
	
	header('Content-Type: application/json');
	 echo  json_encode($data);
?>