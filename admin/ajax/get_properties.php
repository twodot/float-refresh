<?php

require_once('../includes/config.inc.php');

if(isset($user_id) && isset($dbc)) {
	$UserInfo = new user($user_id, $dbc);
	$Properties = new Properties();
	$data = $Properties->get_properties_json();
	header('Content-Type: application/json');
	echo json_encode($data);
}
?>