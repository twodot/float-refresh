<?php

require_once('../includes/config.inc.php');

if(!empty($_POST)) {
	
	if(isset($user_id) && isset($dbc)) {
		$UserInfo = new user($user_id, $dbc);
	
		$user_id = $UserInfo->get_user_id();
		
		$address = urlencode($_POST['address']);
		
		$keepAddress = $_POST['keepAddress'] = (bool) $_POST['keepAddress'];
		
		if($keepAddress) {
			
			
			$address = array();
			
			$address['street_name'] = false;
			$address['city'] = false;
			$address['county'] = false;
			$address['state'] = false;
			$address['zip_code'] = false;
			$address['lat'] = 0;
			$address['lng'] = 0;
			$address['formatted_address'] = sanatize_address($_POST['address']);
			$check = create_property($address, $user_id);
			
			if(is_int($check)) {
				header('Content-Type: application/json');
				echo '{"id":' . json_encode($check) . '}';
			}
			else {
				header('Content-Type: application/json');
				echo '{"error":' . json_encode($check) , '}';
			}
		}
		else {
		
			
			$url = "http://maps.google.com/maps/api/geocode/json?address=" . $address . "&sensor=false";
			$response = file_get_contents($url);
			$response = json_decode($response, true);
			if(isset($response['results'])) {
				$addresses = array();
				foreach($response['results'] as $result) {
					$address = array();
					foreach($result['address_components'] as $component) {
						if(isset($component['types'][0]) && $component['types'][0] == 'route') {
							$key = 'street_name';
						}
						else if(isset($component['types'][0]) && $component['types'][0] == 'locality') {
							$key = 'city';
						}
						else if(isset($component['types'][0]) && $component['types'][0] == 'administrative_area_level_2') {
							$key = 'county';
						}
						else if(isset($component['types'][0]) && $component['types'][0] == 'administrative_area_level_1') {
							$key = 'state';
						}
						else if(isset($component['types'][0]) && $component['types'][0] == 'postal_code') {
							$key = 'zip_code';
						}
						else if(isset($component['types'][0])) {
							$key = $component['types'][0];
						}
						$val = (isset($component['short_name'])?$component['short_name']:false);
						
						$address[$key] = $val; 
						
					} // END FOR EACH
					
					$address['formatted_address'] = (isset($result['formatted_address'])?$result['formatted_address']:false);
					$address['lat'] = (isset($result['geometry']['location']['lat'])?$result['geometry']['location']['lat']:false);
					$address['lng'] = (isset($result['geometry']['location']['lng'])?$result['geometry']['location']['lng']:false);
					$addresses[] = $address;
				}// END FOR EACH ADDRESS RESULTS
				if(count($addresses) == 0) {
					header('Content-Type: application/json');
					echo '{"error":"No property found"}';
				}
				else if(count($addresses) == 1) { 
					$check = create_property($addresses[0], $user_id);
					if(is_int($check)) {
						
						header('Content-Type: application/json');
						echo '{"id":' . json_encode($check) . '}';
					}
					else {
						header('Content-Type: application/json');
						echo '{"error":' . json_encode($check) , '}';
					}
				}
				else {
					$formattedAddresses = array();
					foreach($addresses as $theAddress) {
						if(isset($theAddress['formatted_address']) && !empty($theAddress['formatted_address'])) {
							$formattedAddresses[]=array('address'=>$theAddress['formatted_address']);
						}
					}
					$formattedAddresses[]=array('address'=>"None, add the actual address");
					header('Content-Type: application/json');
					echo '{"potential_addresses":' . json_encode($formattedAddresses) . '}';
					
				} // END ADDRESSES IS GREATER THAN 1
				
			} else {
				$address = json_encode($_POST['address']);
				header('Content-Type: application/json');
				echo '{"no_address":' . $address . '}';
			}// END ISSET $response['results'] GOOGLE
		}
		//$xml = simplexml_load_file('test.xml');
		//print_r($xml);
	} // END USER INFO CHECK

}// END POST
else {
	//panel_direct();
}

?>