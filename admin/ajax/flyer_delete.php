<?php
require_once('../includes/config.inc.php');
require '/home/ec2-user/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

require_login();


$jsonPostData =  json_decode($_POST['data']);
$postData = json_decode($jsonPostData);
$errors = array();

if(isset($postData->flyer_id)) {
	
	
	$prop_id = (int) $postData->flyer_id;
	
	if($prop_id != 0) {
		// echo $info_id;
		$q = "
			SELECT 
			*, 
			COUNT(*) as count_files 
			 
			FROM 
			flyers   
			
			WHERE 
			id =  $prop_id 
			AND user_id = $user_id  
		";
		
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		if ($row['count_files'] == 1) {
			$flyer_id = $row['id'];
			$q = "
			DELETE  
			FROM 
			
			flyers   
			
			WHERE 
			user_id = $user_id  
			AND id = $flyer_id 
			
			"; 
			
			$pdf = FLYERS . $flyer_id . '.pdf';
			$s3pdf = $flyer_id . '.pdf';
			$bucket = 'images.listfloat.com/flyers';
			$S3 = S3Client::factory(array('region' => 'us-west-2'));
			
			$result = $S3->deleteObject(array(
				'Bucket' => $bucket,
				'Key'    => $s3pdf
			));  
			
			if (isset($pdf) && file_exists($pdf) && is_file($pdf)){
				unlink($pdf);
			}
			
			$r = @mysqli_query ($dbc, $q);
			
			header('Content-Type: application/json');
			echo '{"success":true}';
				
		} // END FILES CHECK
		else {
			header('Content-Type: application/json');
			echo '{"success":false, "error": "There was no flyer to delete."}';
		}
	} // P = 0 CHECK
	else {
		header('Content-Type: application/json');
		echo '{"success":false, "error": "There was an error with removing the flyer."}';
	}
	
}// ISSET I CHECK
else {
	header('Content-Type: application/json');
	echo '{"success":false, "error": "No data available."}';
}
?>

