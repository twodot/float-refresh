<?php
require_once('../includes/config.inc.php');
require_login();

if(isset($_POST['property']['id'])) {
	$property_id = (int) $_POST['property']['id'];
	if($property_id != 0) {
		
		$q = "
		
		SELECT 
		* 
		
		FROM 
		properties 
				
		WHERE 
		properties.id = $property_id  
		AND properties.user_id = $user_id  
		";
		
		$r = @mysqli_query ($dbc, $q);
		$rows = mysqli_num_rows($r);
		if($rows == 1) {
		
			$q = "
			
			UPDATE 
			properties 
			
			SET 
			active = 0 
			
			WHERE 
			id = $property_id 
			
			";
			$r = @mysqli_query ($dbc, $q);
			$rows = mysqli_affected_rows($dbc);
			if($rows == 1) {
				
				$_POST['property']['active'] = 0;
				
				$propertyArray = $_POST['property'];
				 $property =  '"property": ' . json_encode($propertyArray) . '';
				ob_end_clean();
				header('Content-Type: application/json');
				echo '{' . $property . '}';	
				exit();
				 
			}
			else { // NO NO PROPERTY INFO UPDATE
				ob_end_clean();
				header('Content-Type: application/json');
				echo '{"error":"No Property Data Found"}';
			} 
		
		
		}
		else { // NO USER FOUND IN DATABASE
			ob_end_clean();
			header('Content-Type: application/json');
			echo '{"error":"No Data Found"}';
		} 
	}
	else { // PROPERTY ID OR BEACON ID WAS 0
		ob_end_clean();
		header('Content-Type: application/json');
		echo '{"error":"No Data Found"}';
	}
}
else { // NO POST FOUND
	ob_end_clean();
	header('Content-Type: application/json');
	echo '{"error":"No Data Found"}';
}

/* array(1) {
  ["property"]=>array("active"=>"0", "address"=> "2327 N 156th Pl Shoreline, WA", "beacon_count"=>"1", "beacons"=>array(
    }"canActivate"=>4) "true""id"=>3) "155""image"=>85) "https://s3-us-west-2.amazonaws.com/images.listfloat.com/phone/please_add_an_image.jpg""price"=>0) """primary_beacon"=>1) "2""title"=>0) """property_data_type"=>8) "property""deleted"=>1) "0"
  }
}


*/


?>