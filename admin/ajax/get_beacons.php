<?php

require_once('../includes/config.inc.php');

if(isset($user_id) && isset($dbc)) {
	$UserInfo = new user($user_id, $dbc);
	$Beacons = new Beacons();
	$data = $Beacons->get_beacons_json();
	header('Content-Type: application/json');
	echo json_encode($data);
}
?>