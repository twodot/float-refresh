<?php

require_once('../includes/config.inc.php');
require_login();

$jsonPostData =  json_decode($_POST['data']);
$postData = json_decode($jsonPostData);
$errors = array();


if(isset($postData->id)) {
	
	$info_id = (int) $postData->id;
	
	if($info_id != 0) {
		// echo $info_id;
		$q = "
			SELECT 
			*, 
			COUNT(*) as count_images 
			 
			FROM 
			media   
			
			WHERE 
			id =  $info_id 
			AND user_id = $user_id  
		";
		
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		if ($row['count_images'] == 1) {
			$property_id = $row['property_id'];
			$q = "
			UPDATE
			media   
			
			SET 
			deleted = 1 
			
			WHERE 
			user_id = $user_id  
			AND id = $info_id 
			
		"; 
		
		$r = @mysqli_query ($dbc, $q);
		
			header('Content-Type: application/json');
			echo '{"success":true}';
		}
		else {
			header('Content-Type: application/json');
			echo '{"success":false, "error": "There was no flyer to delete."}';
			echo '<p>Could not delete image.</p>';
		}
	} // P = 0 CHECK
	else {
		echo '<p>Could not find the image.</p>';
	}
}// ISSET I CHECK
else {
	echo '<p>There was an error.</p>';
}
?>

