<?php
require_once('../includes/config.inc.php');
require_login();

if(isset($_POST['beacon']['id']) && isset($_POST['property']['id'])) {
	$beacon_id = (int) $_POST['beacon']['id'];
	$property_id = (int) $_POST['property']['id'];
	if($beacon_id != 0 && $property_id != 0) {
		
		$q = "
		
		SELECT 
		* 
		
		FROM 
		beacons, 
		properties 
				
		WHERE 
		
		beacons.user_id = $user_id 
		AND beacons.id = $beacon_id  
		
		AND properties.id = $property_id  
		AND properties.user_id = $user_id  
		";
		
		$r = @mysqli_query ($dbc, $q);
		$rows = mysqli_num_rows($r);
		if($rows == 1) {
		
			$q = "
			
			UPDATE 
			property_info 
			
			SET 
			beacon_id = $beacon_id 
			
			WHERE 
			properties_id = $property_id 
			AND primary_info = 1 
			
			";
			$r = @mysqli_query ($dbc, $q);
			$rows = mysqli_affected_rows($dbc);
			if($rows == 1) {
				$q = "
				
				UPDATE 
				beacons 
				
				SET 
				property_info_id = (SELECT property_info.id FROM property_info WHERE property_info.beacon_id = $beacon_id) , 
				beacon_type = 'Property' 
				
				WHERE 
				id = $beacon_id 
				AND user_id = $user_id 
				
				
				";
				$r = @mysqli_query ($dbc, $q);
				$rows = mysqli_affected_rows($dbc);
				if($rows == 1) {
					
					
					$q_beacons = "SELECT * FROM beacons where id = $beacon_id AND user_id = $user_id";
					
					$r_beacons = @mysqli_query ($dbc, $q_beacons);
					$rows_beacons = mysqli_fetch_array($r_beacons, MYSQLI_ASSOC);
					
					
					$q_property = "SELECT properties.*, property_info.title as property_info_title, property_info.beacon_id FROM properties, property_info  WHERE properties.id = $property_id AND user_id = $user_id AND property_info.primary_info = 1 AND property_info.properties_id = $property_id";
					$r_property = @mysqli_query ($dbc, $q_property);
					$rows_property = mysqli_fetch_array($r_property, MYSQLI_ASSOC);
					
					$beacon_active = ($rows_beacons['property_info_id'] > 0?1:0);
					$beacon_id = $rows_beacons['id'];
					$beacon_title = $rows_beacons['title'];
					$canActivate = ($rows_property['beacon_id'] !=0?1:0);
					$property_info_title = (empty($rows_property['property_info_title'])?'Primary':$rows_property['property_info_title']);
					
					$beaconArray = array( "active"=>$beacon_active, "id"=>$beacon_id, "title"=>$beacon_title, "property_info_title"=>$property_info_title);
					$beacon ='"beacon": ' .  json_encode( $beaconArray) . '';
					
					$propertyArray = array("active"=> $_POST['property']['active'], 
						"address"=> $_POST['property']['address'], 
						"beacon_count"=> $_POST['property']['beacon_count'] + 1, 
						"canActivate"=> $canActivate, 
						"id"=>$property_id, 
						"image"=>$_POST['property']['image'], 
						"price"=> $_POST['property']['price'], 
						"primary_beacon"=>$beacon_id, 
						"title"=> $_POST['property']['title'], 
						"property_data_type"=>$_POST['property']['property_data_type'], 
						"deleted"=>0);
					 $property =  '"property": ' . json_encode($propertyArray) . '';
					ob_end_clean();
					header('Content-Type: application/json');
					echo '{' . $beacon . ', ' . $property . '}';	
					exit();
					
				}
				else { // NO BEACON 
					ob_end_clean();
					header('Content-Type: application/json');
					echo '{"error":" No Beacon Data Found"}';
				} 
			}
			else { // NO NO PROPERTY INFO UPDATE
				ob_end_clean();
				header('Content-Type: application/json');
				echo '{"error":"No Property Data Found"}';
			} 
		
		
		}
		else { // NO USER FOUND IN DATABASE
			ob_end_clean();
			header('Content-Type: application/json');
			echo '{"error":"No Data Found"}';
		} 
	}
	else { // PROPERTY ID OR BEACON ID WAS 0
		ob_end_clean();
		header('Content-Type: application/json');
		echo '{"error":"No Data Found"}';
	}
}
else { // NO POST FOUND
	ob_end_clean();
	header('Content-Type: application/json');
	echo '{"error":"No Data Found"}';
}


?>