<?php
require '/home/ec2-user/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

require_once('../includes/config.inc.php');
require_login();



if (isset($_GET['p'])) {
	$p = (int) $_GET['p'];
	
	if($p != 0) {
		$q="SELECT COUNT(*) as properties_count FROM properties WHERE id = $p AND user_id = $user_id";
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		if($row['properties_count'] == 1) {
			$q = "SELECT COUNT(*) as flyers_total FROM flyers WHERE property_id = '$p'";
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			if($row['flyers_total'] == 0) {
				$ext = substr($_FILES['file']['name'], -4);
				
				if($ext == '.pdf' || $ext == '.PDF') {
					$file = $_FILES['file'];
					if (is_uploaded_file ($file['tmp_name'])){
						$temp = FLYERS . md5($file['name']);
						if (move_uploaded_file($file['tmp_name'], $temp)){
							$name = $file['name'];
							$q="INSERT INTO flyers SET property_id = $p, name='$name', user_id = $user_id";
							$r = @mysqli_query ($dbc, $q);
							
							$flyer_id = mysqli_insert_id($dbc);
							
							rename ($temp, FLYERS . "$flyer_id" . '.pdf');
							
							$flyer_location = FLYERS . "$flyer_id" . '.pdf';
							
							$flyerBucket = 'images.listfloat.com/flyers';
							$flyerS3 = S3Client::factory(array('region' => 'us-west-2'));
							
							try {
								$upload = $flyerS3->upload($flyerBucket, $flyer_id . '.pdf', fopen($flyer_location, 'rb'), 'public-read');
							} catch(Exception $e) { 
							}					
							
							if (isset($flyer_location) && file_exists($flyer_location) && is_file($flyer_location)){
								unlink($flyer_location);
							} // DELETE FLYER FROM TEMP SECTION 
							
							if (isset($temp) && file_exists($temp) && is_file($temp)){
								unlink($temp);
							}// END TEMP FILE CHECK
							header('Content-Type: application/json');
							echo '{"name": "' . $name . '", "id":' . $flyer_id . '}';
							exit();
						} // END IF MOVE FILE
						else {
							header('Content-Type: application/json');
							echo '{"error": "The file could not be sent"}';
							exit();
						}
					}// END IF IS UPLOADED
					else {
						header('Content-Type: application/json');
						echo '{"error": "The file could not be uploaded"}';
						exit();
					}
				} // END IF PDF				
				else {
					header('Content-Type: application/json');
					echo '{"error": "The file is not a PDF"}';
					exit();
				}
			}// END CHECK HASH
			else {
				header('Content-Type: application/json');
				echo '{"error": "There is already a flyer added"}';
				exit();
			}
		} // END USER/PROPERTY CHECK
		else {		
			$url = '/panel' ;
					
			ob_end_clean();
			
			header("Location: $url");
		}
		
	} // END P CHECK
	
} // END POST P CHECK
else {		
	$url = '/panel' ;
			
	ob_end_clean();
	
	header("Location: $url");
}
	