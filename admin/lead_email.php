<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $propertytitle; ?></title>
</head>

<body style="text-align:center">

<table cellpadding="0" cellspacing="0" border="0" style="font-family:Helvetica, Arial, sans-serif; max-width:700px; text-align:left; margin:auto; padding:20px;">
	<tr>
    	<td>
        	<table width="100%">
        		<tr>
                	<td style="padding-right:20px; width:20%;">
                		<img src="[BASE_URL_ADMIN]images/logo-126.jpg"  style="width:100%; max-width:126px; min-width:50px; height:auto" />
                    </td>
                   
               </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
        	<p>[MESSAGE_TO_CLIENT]</p>
        </td>
	</tr>
    <tr>
    	<td>
        	<table width="100%">
        		<tr>
                    <td style="padding-right:20px; padding-left:20px; color:#FFF; background:#01aef0; max-width:100%;">
                        <h2>Share   :   <a href="[facebook_share]" target="_blank" style="color:#FFF;">Facebook</a>   |   <a href="[twitter_share]" target="_blank" style="color:#FFF;">Twitter</a></h2>
                    </td>
               </tr>
            </table>
        </td>
    </tr>
    
</table>
</body>
</html>
