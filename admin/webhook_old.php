<?php
	// $customer = 'cus_4isndMlWV4VkHT'; // REMOVE
	require_once('includes/config.inc.php');

	Stripe::setApiKey(STRIPE_SECRET);
	
	date_default_timezone_set("UTC"); 
	
	// Retrieve the request's body and parse it as JSON
	$body = @file_get_contents('php://input');
	$event_json = json_decode($body);
if (!empty($event_json)) {
	$stripe_vars = get_object_vars ( $event_json );
	
	$type = $stripe_vars['type'];
	$event_id = $stripe_vars['id'];
	
	if($type == 'charge.succeeded') {
		$customer = $event_json->data->object->customer;	
		$created = $event_json->data->object->created;
		$amount = $event_json->data->object->amount;
		
	}
	
	else if ($type == 'invoice.payment_succeeded') {
		
		$customer = $stripe_vars['data']->object->customer;	
		
		$created = $stripe_vars['created'];
		$amount = $stripe_vars['data']->object->lines->data[0]->plan->amount;
		
		$start = $stripe_vars['data']->object->period_start;
		$end = $stripe_vars['data']->object->period_end;
		
			
		$q = "SELECT user_id FROM users WHERE stripe_id = '$customer'";
		$r = @mysqli_query($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$user_id = $row['user_id'];
		
	 	$q= "UPDATE 
			subscription 
			
			SET 
			current_period_start = FROM_UNIXTIME($start), 
			current_period_end = FROM_UNIXTIME($end),
			active = 1
			
			WHERE 
			user_id = $user_id";
			
			$r = @mysqli_query ($dbc, $q);
		
	}
	else if ($type == 'invoice.updated') {
		
		$customer = $stripe_vars['data']->object->customer;	
		
		$created = $stripe_vars['created'];
		$amount = $stripe_vars['data']->object->lines->data[0]->plan->amount;
		
		$start = $stripe_vars['data']->object->period_start;
		$end = $stripe_vars['data']->object->period_end;
			
		$q = "SELECT user_id FROM users WHERE stripe_id = '$customer'";
		$r = @mysqli_query($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$user_id = $row['user_id'];
		
	 	$q= "UPDATE 
			subscription 
			
			SET 
			current_period_start = FROM_UNIXTIME($start), 
			current_period_end = FROM_UNIXTIME($end),
			active = 1
			
			WHERE 
			user_id = $user_id";
			
			$r = @mysqli_query ($dbc, $q);
		
	}
	else if ($type == 'invoice.payment_failed') {
		
		$customer = $stripe_vars['data']->object->customer;	
		$created = $stripe_vars['created'];
		$amount = $stripe_vars['data']->object->lines->data[0]->plan->amount;
		
	}
	else if ($type == 'customer.subscription.updated') {
		
		$customer = $stripe_vars['data']->object->customer;	
		
		$created = $stripe_vars['created'];
		$amount = $stripe_vars['data']->object->plan->amount;
		
		$start = $stripe_vars['data']->object->current_period_start;
		$end = $stripe_vars['data']->object->current_period_end;
		
		$q = "SELECT user_id FROM users WHERE stripe_id = '$customer'";
		$r = @mysqli_query($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$user_id = $row['user_id'];
		
		
		$q= "UPDATE 
			subscription 
			
			SET 
			current_period_start = FROM_UNIXTIME($start), 
			current_period_end = FROM_UNIXTIME($end),
			active = 1
			
			WHERE 
			user_id = $user_id";
		$r = @mysqli_query ($dbc, $q);
	}
	
	else if ($type == 'customer.subscription.deleted') {
		$customer = $stripe_vars['data']->object->customer;	
		$created = $stripe_vars['created'];
		$amount = $stripe_vars['data']->object->plan->amount;
	}
	
	if(isset($customer)) {
		$q= "SELECT users.user_id, subscription.subscribe_level FROM users, subscription WHERE stripe_id = '$customer' AND users.user_id = subscription.user_id";
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$cust_id = $row['user_id'];
		$subscribe_level = $row['subscribe_level'];
		
		$q ="SELECT COUNT(*) as event_ids FROM past_subscriptions WHERE event_id = '$event_id'";
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		if ($row['event_ids'] == 0) {
			$q = "INSERT INTO past_subscriptions SET user_id = '$cust_id', invoice_date = FROM_UNIXTIME($created), properties = '$subscribe_level', subscribe_level = '$subscribe_level', amount =  '$amount', event_type = '$type', event_id = '$event_id'";
			$r = @mysqli_query ($dbc, $q);
		}
	
	}
	if ($type == 'customer.subscription.trial_will_end') {
		$customer = $stripe_vars['data']->object->customer;
		
		
		
		$q= "SELECT users.user_id, users.email, subscription.subscribe_level, TIMESTAMPDIFF(DAY, NOW(), trial_end) + 1 as days_left FROM users, subscription WHERE stripe_id = '$customer' AND users.user_id = subscription.user_id";
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$cust_id = $row['user_id'];
		$date_end = $row['days_left'];
		$e = $row['email'];
		
		
		
		include('email_trial_end.php');
				$HTMLEmailoutput = ob_get_contents();
				$HTMLEmailoutput = str_replace('[DAYS_LEFT]', $date_end, $HTMLEmailoutput);
				ob_end_clean();
				
				
				//Create a new PHPMailer instance
				$mail = new PHPMailer();
				//Set who the message is to be sent from
				$mail->setFrom('info@' . EMAIL_URL, 'FYI From Float');
				//Set an alternative reply-to address
				$mail->addReplyTo('info@' . EMAIL_URL, 'FYI From Float');
				//Set who the message is to be sent to
				$mail->addAddress($e);
				//Set the subject line
				$mail->Subject = 'Your Trial Period Will End in ' . $date_end . ' Days';
				//Read an HTML message body from an external file, convert referenced images to embedded,
				//convert HTML into a basic plain-text alternative body
				$mail->msgHTML($HTMLEmailoutput);
				//Replace the plain text body with one created manually
				
				//send the message, check for errors
				
				$mail->send();
		
			
	}
	
	else if ($type == 'invoice.payment_failed') {
		
		
		
		$q= "SELECT users.user_id, users.email, subscription.subscribe_level, TIMESTAMPDIFF(DAY, NOW(), trial_end) + 1 as days_left FROM users, subscription WHERE stripe_id = '$customer' AND users.user_id = subscription.user_id";
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$e = $row['email'];
		
		include('email_failed_payment.php');
				$HTMLEmailoutput = ob_get_contents();
				ob_end_clean();
				
				
				//Create a new PHPMailer instance
				$mail = new PHPMailer();
				//Set who the message is to be sent from
				$mail->setFrom('info@' . EMAIL_URL, 'FYI From Float');
				//Set an alternative reply-to address
				$mail->addReplyTo('info@' . EMAIL_URL, 'FYI From Float');
				//Set who the message is to be sent to
				$mail->addAddress($e);
				//Set the subject line
				$mail->Subject = 'Houston, We Have a Problem';
				//Read an HTML message body from an external file, convert referenced images to embedded,
				//convert HTML into a basic plain-text alternative body
				$mail->msgHTML($HTMLEmailoutput);
				//Replace the plain text body with one created manually
				
				//send the message, check for errors
				
				$mail->send();
	}
	
	
} // END CHECK FOR STRIPE VAR OBJECT
	
?>

