<?php
include('../float_req/mysqli_connect.php');
$image = FALSE;

if (isset($_GET['i']) && is_numeric($_GET['i']) ) {
	$prop = (int) $_GET['i'];
	
	$q= "SELECT * FROM flyers WHERE property_id = $prop";
	$r = @mysqli_query ($dbc, $q);	
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	
	$name = $row['name'];
	$id = $row['id'];
	$name = (!empty($name)) ? $name : 'print image';
	
	$image = '../float_req/flyers/' . $id . '.pdf';
	
	if (!file_exists ($image) || (!is_file($image))) {
		$image = FALSE;
	}
	
}

if (!$image) {

	$image = '../float_req/images/' . 'unavailable.png';
	
	$name = 'unavailable.png';
}

$info = getimagesize($image);

$fs = filesize($image);

header ("Content-Type: {$info['mime']}\n");

header ("Content-Disposition: inline; filename=\"$name\"\n");

header ("Content-Length: $fs\n");

readfile ($image);

?>