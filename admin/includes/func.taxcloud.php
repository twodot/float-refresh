<?php
/**
 * TaxCloud v1.2
 * @license https://taxcloud.net/ftpsl.pdf
 */
require("classes.php");
require("taxCloudConfig.php");
define('WSDL_URL','https://api.taxcloud.net/1.0/TaxCloud.asmx?wsdl');
$client = new SoapClient(WSDL_URL);
/**
 * Verify an address using TaxCloud service
 * @param $address
 * @param $err
 */
function func_verify_address_taxcloud($address, &$err) {
	global $client;
	// Verify the address through the TaxCloud verify address service
	$params = array( "uspsUserID" => USPS_ID,
				 "address1" => $address->getAddress1(),
				 "address2" => $address->getAddress2(),
				 "city" => $address->getCity(),
				 "state" => $address->getState(),
				 "zip5" => $address->getZip5(),
				 "zip4" => $address->getZip4());
				 
				 
	try {
		$verifyaddressresponse = $client->verifyAddress( $params );
	} catch (Exception $e) {
		//retry
		try {
			$verifyaddressresponse = $client->verifyAddress( $params );
		} catch (Exception $e) {
			$err[] = "Error encountered while verifying address " . $e->getMessage();
			//irreparable, return
			return null;
		}
	}
	if($verifyaddressresponse->{'VerifyAddressResult'}->ErrNumber == 0) {
		// Use the verified address values
		$address->setAddress1($verifyaddressresponse->{'VerifyAddressResult'}->Address1);
		//$address->setAddress2($verifyaddressresponse->{'VerifyAddressResult'}->Address2);
		$address->setCity($verifyaddressresponse->{'VerifyAddressResult'}->City);
		$address->setState($verifyaddressresponse->{'VerifyAddressResult'}->State);
		$address->setZip5($verifyaddressresponse->{'VerifyAddressResult'}->Zip5);
		$address->setZip4($verifyaddressresponse->{'VerifyAddressResult'}->Zip4);
	} else {
		$err[] = "Error encountered while verifying address ".$verifyaddressresponse->VerifyAddressResult->{'ErrDescription'};
		return null;
	}
	return $address;
}
/**
 * Returns the customer's address
 * @param $customer
 */
function func_get_customer_address_taxcloud($customer) {
	// Customer's address
	$destination = new Address();
	$address = $customer['address']['S'];
	
	$destination->setAddress1($address['address']);
	$destination->setAddress2('');
	$destination->setCity($address['city']);
	$destination->setState($address['state']); // Two character state appreviation
	$destination->setZip5($address['zipcode']);
	$destination->setZip4('');
	return $destination;
}
/**
 * Returns the store's address
 */
function func_get_store_address_taxcloud() {
	$origin = new Address();
	$origin->setAddress1(OUR_ADDRESS);
	$origin->setAddress2(OUR_SUITE);
	$origin->setCity(OUR_CITY);
	$origin->setState(OUR_STATE);
	$origin->setZip5(STORE_ZIP);		
	return $origin;
}
/**
 * Retrieves the exemption certificate stored in the session.
 * @param $customer
 */
function func_get_exemption_cert_taxcloud($customer) {		
	// Look for a single purchase certificate
	$orig = $_SESSION['taxcloudExemptionCertificate'];
	
	if ( isset($orig) ) {
		$exemptionCertificate = unserialize($orig);
		return $exemptionCertificate;
	} else {
		$selectedCertID = $_SESSION['taxCloudSelectedCert'];		
		if ( isset($selectedCertID) ) { 
			$selectedCert = new ExemptionCertificate();
			$selectedCert ->setCertificateID($selectedCertID);
			return $selectedCert;
		}
	}
	return null;
}
/**
 * Retrieves a product's TIC ID.
 * @param $product_id
 */
function func_get_tic_taxcloud($product_id) {	
	$dbc = @mysqli_connect (DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	
	if (!$dbc) {
		trigger_error ('Could not connect to MySQL: ' . mysqli_connect_error() );
	}
	$query = "SELECT tic FROM products WHERE sku = ".$product_id;	
	$result = mysqli_query($dbc, $query);
	$row_tic = mysqli_fetch_array($result, MYSQLI_ASSOC);	
	$tic = $row_tic['tic'];
	$i = preg_match("/^(\d+)/", $tic);
	if($i != 0)
		return $tic;
	else 
		return $i;			
}
/**
 * Look up tax amount using the TaxCloud web services
 * @param $product
 * @param $customer
 * @param $errMsg
 * @param $shipping
 */
function func_lookup_tax_taxcloud($products,$customer,&$errMsg,$shipping) { 
	global $client, $userinfo, $tax_service, $cart; 
	
	$states_to_tax = array();
	
	if ( TAXCLOUD_ENABLED != 'on' ) {
		$errMsg[] = "TAXCLOUD_NOT_ENABLED";
		return -1;
	}
			
	$origin = func_get_store_address_taxcloud($customer);	
	$origin = func_verify_address_taxcloud($origin, $errMsg);
	
	//Verify Customer's destination address
	$destination = func_get_customer_address_taxcloud($customer);	
	$destination = func_verify_address_taxcloud($destination, $errMsg);
	$customers_state = $customer['address']['S']['state'];
	
	$states_to_tax[] = 'WA';
	$states_to_tax[] = 'CA';
	$states_to_tax[] = 'OR';
	
	if(!is_null($origin) && !is_null($destination)) {
		foreach ($states_to_tax as $state_to_tax) {
			
			if($state_to_tax == $customers_state) {
				
				$cartItems = Array();
				$index = 0;
				//total cost of cartItems
				$items_cost = 0;
				
				foreach ($products as $k => $product) {
					$cartItem = new CartItem();
								
					preg_match("/^(\d+)/", $product['id'], $match);			
					$cartItem->setItemID($match[0]);
					$cartItem->setIndex($index); // Each cart item must have a unique index
					$tic = func_get_tic_taxcloud($match[0]);
					if(!$tic) {
						//no TIC has been assigned to this product, use default TIC
						$tic = '00000';
					}
					
					$cartItem->setTIC($tic);
					$cartItem->setPrice($product['price']); // Price of each item
					$cartItem->setQty($product['qty']); // Quantity
					$cartItems[$index] = $cartItem;
					$index++;
					$items_cost += $cartItem->getPrice() * $cartItem->getQty();
				}
				//Shipping as a cart item
				$cartItem = new CartItem();
				$cartItem->setItemID('shipping');
				$cartItem->setIndex($index);
				$cartItem->setTIC(11010);
				$cartItem->setPrice($shipping);
				$cartItem->setQty(1);
				$cartItems[] = $cartItem;
				$items_cost += $shipping;
				
				$exemptCert = func_get_exemption_cert_taxcloud($customer);
				
				$params = array( "apiLoginID" => API_ID,
						 "apiKey" => API_KEY,
						 "customerID" => $customer['id'],
						 "cartID" => $_SESSION['cart']['id'],
						 "cartItems" => $cartItems,
						 "origin" => $origin,
						 "destination" => $destination,
						 "deliveredBySeller" => false,
						 "exemptCert" => $exemptCert
				);
				
				//Call the TaxCloud web service
				try {
					$lookupResponse = $client->lookup( $params );
				} catch (Exception $e) {
					//retry
					try {
						$lookupResponse = $client->lookup( $params );
					} catch (Exception $e) {
						$errMsg[] = "Error encountered looking up tax amount ".$e->getMessage();
			
						//irreparable, return
						return -1;
					}
				}
				$lookupResult = $lookupResponse->{'LookupResult'};
				
				if($lookupResult->ResponseType == 'OK' || $lookupResult->ResponseType == 'Informational') {
					$cartItemsResponse = $lookupResult->{'CartItemsResponse'};
					$cartItemResponse = $cartItemsResponse->{'CartItemResponse'};
					$taxTotal = 0;
					
					//response may be an array
					if ( is_array($cartItemResponse) ) {
						foreach ($cartItemResponse as $c) {
							$amount = ($c->TaxAmount);
							$taxTotal = $taxTotal + $amount;
						}
					} else {
						$amount = ($cartItemResponse->TaxAmount);
						$taxTotal = $amount;
					}
					$TAX_NAME = 'Sales Tax';
					
					$_SESSION['taxcloudTaxes'] = $taxTotal;
					return array('name' => $TAX_NAME, 'rate' => round(($taxTotal*100/$items_cost),2), 'tax' => $taxTotal, 'params'=>$params);
					
				} else {
					$errMsgs = $lookupResult->{'Messages'};
					foreach($errMsgs as $err) {
						$errMsg[] = "Error encountered looking up tax amount ".$err->{'Message'};				
					}
					
					return -1;
				}
			} // END CUSTOMERS STATE CHECK
		}// END FOR EACH STATE TO TAX 
	} // END ORIGIN AND DESTINATION 
	else {
		return -1;
	}
}
/**
 * Authorized with Capture to complete the transaction
 * @param $customerID
 * @param $orderID
 * @param $errMsg
 */
function func_authorized_with_capture_taxcloud($customerID, $cartID, $orderID, &$errMsg) {
	global $client;
	
	if ( TAXCLOUD_ENABLED != 'on' ) {
		return 1;  //skip processing
	}
	$result = 0;
	
	$dup = "This purchase has already been marked as authorized";
	
	// Current date - example of format: '2010-09-08T00:00:00';
	$dateAuthorized = date("Y-m-d");
	$dateAuthorized = $dateAuthorized . "T00:00:00";
	
	$params = array( "apiLoginID" => API_ID,
					 "apiKey" => API_KEY,
					 "customerID" => $customerID,
					 "cartID" => $cartID,
					 "orderID" => $orderID,
					 "dateAuthorized" => $dateAuthorized,
					 "dateCaptured" => $dateAuthorized);
	// The authorizedResponse array contains the response verification (Error, OK, ...)
	$authorizedResponse = null;
	try {
		$authorizedResponse = $client->authorizedWithCapture( $params );							
	} catch (Exception $e) {
		//infrastructure error, try again	
		try {
			$authorizedResponse = $client->authorizedWithCapture( $params );
			$authorizedResult = $authorizedResponse->{'AuthorizedWithCaptureResult'};		
			if ($authorizedResult->ResponseType != 'OK') {
				$msgs = $authorizedResult->{'Messages'};
				$respMsg = $msgs->{'ResponseMessage'};
		
				//duplicate means the the previous called was good. Therefore, consider this to be good
				if (trim ($respMsg->Message) == $dup) {
					return 1;
				}
			} else {
				return 1;
			}
			
		} catch (Exception $e) {
			//give up
			$errMsg[] = $e->getMessage();
			return 0;
		}
	}
	$authorizedResult = $authorizedResponse->{'AuthorizedWithCaptureResult'}; 
	if ($authorizedResult->ResponseType == 'OK') {
		$_SESSION['taxcloudExemptionCertificate'] = null;
	 	$_SESSION['taxCloudSelectedCert'] = null;
		return 1;
	} else {
		$msgs = $authorizedResult->{'Messages'};
		$respMsg = $msgs->{'ResponseMessage'};		
		
		//duplicate means the the previous called was good. Therefore, consider this to be good
		if (trim ($respMsg->Message) == $dup) {
			return 1;
		} else {
		
			$errMsg [] = $respMsg->Message;
			return 0;
		}
	}
			
	/* unregister cartID from the session */
	unset($_SESSION['cartID']);
	unset($_SESSION['taxcloudTaxes']);
	return $result;	
}
function func_taxcloud_add_exemption_certificate($exemptionCertificate,$customerID) {		
	global $client;
	
	$params = array( "apiLoginID" => API_ID,
					 "apiKey" => API_KEY,
					 "customerID" => $customerID,
				 	 "exemptCert" => $exemptionCertificate 
				 	 );
	
	try {
		$addExemptionResponse = $client->addExemptCertificate( $params );		
		return $addExemptionResponse;
	} catch (Exception $e) {	
		return $e;
	}
}
function func_taxcloud_get_exemption_certificates($customerID) {
	global $client;
	
	$params = array( "apiLoginID" => API_ID,
					 "apiKey" => API_KEY,
					 "customerID" => $customerID
				 	 );
	
	try {
		$getExemptCertificatesResponse = $client->getExemptCertificates( $params );
		$getCertificatesRsp = $getExemptCertificatesResponse->{'GetExemptCertificatesResult'};
		$exemptCertificatesArray = $getCertificatesRsp->{'ExemptCertificates'};
		$exemptCertificates = $exemptCertificatesArray->{'ExemptionCertificate'};
		
		if (is_array($exemptCertificates)) {
			return $exemptCertificates;
		} else {
			return $exemptCertificatesArray;
		}
		
	} catch (Exception $e) {
		return Array();
	}
	return Array();
}
function func_taxcloud_delete_exemption_certificate($certID) {
	
	global $client;
	
	$params = array( 
	 	"apiLoginID" => API_ID,
		"apiKey" => API_KEY,
		"certificateID" => $certID
	);
	
	try {
		$deleteExemptCertificateResponse = $client->deleteExemptCertificate( $params );
	} catch (Exception $e) {
		return -1;
	}
}
?>