<?php 
	$admin_nav = array();
	$admin_nav[] = array('link'=>'http://www.listfloat.com', 'name'=>'<img src="/images/logo-small-25.png" alt="Float Logo" title="Float Logo" />', 'page_title'=>'Logo');
?>
<nav id="admin_nav">
    <div id="admin_nav_holder">
        <ul>
                <?php
                    
                    foreach($admin_nav as $key=>$val) {
                        
                            if ($val['page_title'] == $page_title) {
                                echo '<li class="current_page">';
                                echo '<p>' . $val['name'] . '</p>';
                            }
							else if ($val['page_title'] == 'Orders') {
								if($row_orders['unfullfilled'] > 0) {
									echo '<li class="orders_waiting">';
                               		echo '<a href="' . $val['link'] . '">' . $val['name'] . '</a>';
								}
								else {
									echo '<li>';
                                	echo '<a href="' . $val['link'] . '">' . $val['name'] . '</a>';
								}
							}
							else if ($val['page_title'] == 'Logo') {
								echo '<li>';
                                echo '<a href="' . $val['link'] . '" class="branding_link">' . $val['name'] . '</a>';
							}
                            else {
                                echo '<li>';
                                echo '<a href="' . $val['link'] . '">' . $val['name'] . '</a>';
                            }
                        echo '</li>';
                    }
                
                ?>
            </ul>
    </div>
    <div class="clear"></div>
</nav>
