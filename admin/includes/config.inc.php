<?php

/*

NEXUS' SET ON 

func.taxcloud.php ON LINE 145 FOR THE SUBSCRIPTION PAGE
AND functions.php ON LINE 1474 FOR THE CART PAGE


$states_to_tax[] = 'WA';
$states_to_tax[] = 'CA';
$states_to_tax[] = 'OR'; 

*/

define('__ROOT__', dirname(dirname(__FILE__))); 

define ('OUTSIDE', dirname(__ROOT__));

define ('UPLOADS', OUTSIDE . '/float_req/images/');

define ('FLYERS', OUTSIDE . '/float_req/flyers/');

define('EMAIL', 'josh@twodotmarketing.com');

define('ADMIN', 'josh@twodotmarketing.com');

define('SALES', 'josh@twodotmarketing.com');

define('BASE_URL', 'https://refresh.listfloat.com/');
define('PUBLIC_BASE_URL', 'https://listfloat.com/');
define('EMAIL_URL', 'listfloat.com');

define('MYSQL', OUTSIDE . '/float_req/mysqli_connect.php');

define('MAILER',  OUTSIDE . '/float_req/PHPMailerAutoload.php');

define('IMAGES', '/images/');

define('FUNCTIONS', 'functions.php');

define('BEACON_PRICE', '30');

define('SMTP_MAILER', OUTSIDE . '/float_req/smtp_mailer_info.php');

define('MAIL_INFO', OUTSIDE . '/float_req/smtp_mailer_info.php');
define('MAIL_SALES', OUTSIDE . '/float_req/smtp_mailer_sales.php');
define('MAIL_HELLO', OUTSIDE . '/float_req/smtp_mailer_hello.php');
define('MAIL_WHOOPS', OUTSIDE . '/float_req/smtp_mailer_whoops.php');
define('MAIL_REQUESTS', OUTSIDE . '/float_req/smtp_mailer_requests.php');
define('MAIL_LEADS', OUTSIDE . '/float_req/smtp_mailer_leads.php');

define('POST_CRM_HASH', '4abdb2e47700934d99a315b6344aa3c7');

define('SUPPORT_EMAIL', 'support@' . EMAIL_URL);

define('SUPPORT_NUMBER', '(970) 773-5628 or (970) 77FLOAT'); // JOSH GOOGLE VOICE ACCOUNT

define('SURVEY_LINK', '');

// API SECTION 

// MAIL CHIMP API SETTINGS

define('FLOAT_LIST_ID', 'b24b8dd0ca');

// SUBSCRIPTION TYPE
define('SUBSCRIPTION_TYPE_ACTIVE', 'e831b07e4c');
define('SUBSCRIPTION_TYPE_CANCELED', '91471dc168');
define('SUBSCRIPTION_TYPE_FREE', '5b714a9e92');
define('SUBSCRIPTION_TYPE_NONE', 'eb782ba130');

// SUBCRIPTION LEVEL
define('SUBSCRIPTION_LEVEL_GENERAL_REAL_ESTATE', 'aaa6a65b3c');
define('SUBSCRIPTION_LEVEL_PROPERTY_MANAGEMENT', 'bb9119d06a');


define('WALKSCORE', 'af1fd77f6fe4b1c7551c4ed6da8dca75');
// CHANGE THIS IN config.inc.php (FOR API TOO)

define('BITLY_USER', 'twodot');
define('BITLY', 'R_1aa7b473fb52463a8846b2c17502ac29');

define('BITLY_APP_CLIENT', '0ca2f45f5ec3badb0f91c7ce2f1161dffc2ad1c1');
define('BITLY_APP_SECRET', '671de565fc6692c4c7c5e294cb5fd46692bb3d43');

define('BITLY_APP_GENERIC_ACCESS', 'fc66777f6e7ad1be86957de29f956cd592d181e6');

define('BITLY_BASE_URL', 'http://bit.ly/');

define('GREATSCHOOLS', 'bx3h01yfmo2lf9ctafxhpqxx');
// CHANGE THIS IN config.inc.php (FOR API TOO)


define('ZILLOW', 'X1-ZWz19nbzcao0zv_1u0pu');


//  LIVE KEYS
define('STRIPE_API', 'pk_live_s0ikl8aoszyK9LHTezQgX83x'); // THIS IS A LIVE API KEY
// CHANGE THIS IN THE WEBHOOK INCLUDE ALL TOO
define('STRIPE_SECRET', 'sk_live_lTitZxqV6bOzc3nrZ0ZM9yHQ'); // THIS IS A LIVE API KEY

//TEST KEYS
//define('STRIPE_API', 'pk_test_MJaxenafaPdN4ozpqOA0KtrU'); // THIS IS A TEST API KEY
// CHANGE THIS IN THE WEBHOOK INCLUDE ALL TOO
//define('STRIPE_SECRET', 'sk_test_yrydvs3W8J0FoSKMsXOYXQeU'); // THIS IS A TEST API KEY


define('WHITE_PAGES', 'a106376e2430ca905b4b0ba97fb81f14');

define('BEACON_API', 'fCSAZwDDsqsUhcBoLRrRtPMwOcmkSeOd');

define('GIMBAL_API', '417ead4d8563f352e156867817dfd64e');

define('MAILCHIMP_STANDARD_API', 'd80bb8f7aaff60462caca67f2107fbf7-us11');

define('USPS_USER', '680FLOAT7314');

define('USPS_PASS', '153TR20WM936');

define('OUR_NAME', 'Float Technologies, LLC');

define('OUR_ADDRESS', '5607 Keystone Place N.');

define('OUR_SUITE', 'Suite WA');

define('OUR_CITY', 'Seattle');

define('OUR_STATE', 'WA');

define('OUR_ZIP', '98103');

// CANGE ADDRESS IN WEBHOOK INCLUDE ALL TOO

define('FULL_ADDRESS', '5607 Keystone Place N.<br />Suite C<br />Seattle WA, 98103');

require_once (MYSQL);

require_once (MAILER);

require_once(__ROOT__ . '/lib/Stripe.php');

require_once (__ROOT__ . '/includes/load.inc.php');

$site = 'Float - Real Estate iBeacon Tours';

// IMAGE SIZES
define('THUMBNAIL_IMAGE_MAX_WIDTH', 150);
define('THUMBNAIL_IMAGE_MAX_HEIGHT', 150);

define('PHONE_IMAGE_MAX_WIDTH', 960);
define('PHONE_IMAGE_MAX_HEIGHT', 640);


define('TABLET_IMAGE_MAX_WIDTH', 1024);
define('TABLET_IMAGE_MAX_HEIGHT', 768);



date_default_timezone_set ('America/Los_Angeles');

define('LIVE', FALSE);



function my_error_handler ($e_number, $e_message, $e_file, $e_line, $e_vars) {
	$message = "<p>An error occurred in the script '$e_file' on line $e_line: $e_message\n<br />"; 
	
	$message .="Date/Time: " . date ('n-j-y H:i:s') . "\n<br />";
	
	$message .="<pre>" . print_r ($e_vars, 1) . "</pre>\n</p>";

	if (!LIVE) {
			
		ini_set("display_errors", 1);
		ini_set("track_errors", 1);
		ini_set("html_errors", 1);
		error_reporting(E_ALL);

		echo '<div class="error">' . $message . '</div><br />';
	}
	else {
		mail(EMAIL, 'Site Error!', $message, 'From: josh@twodotmarketing.com');
		
		if ($e_number != E_NOTICE) {
			echo '<div class="error">A system error occurred. We apologize for the inconvience.</div><br />';
		}
	}
}

error_reporting(E_ALL);
ini_set('display_errors', 1);

set_error_handler ('my_error_handler');
ob_start();

?>