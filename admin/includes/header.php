<!DOCTYPE html>
<?php if(isset($user_id) && isset($dbc)) { $UserInfo = new user($user_id, $dbc); } ?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<script type="text/javascript" src="/js/jquery-1.11.0.min.js" ></script>
<script type="text/javascript" src="/js/jquery-ui-1.10.4.custom.min.js" ></script>
<script type='text/javascript' src='/js/knockout-3.4.0.js'></script>
<script type='text/javascript' src='/js/ko.mapping.js'></script>
<script type='text/javascript' src='/js/validator.js'></script>
<script type="text/javascript" src="/js/jquery.steps.min.js"></script>
<script type="text/javascript" src="/js/jquery.tmpl.min.js"></script>
<!-- 
<script type="text/javascript" src="/js/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="/js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="/js/intro.js"></script> -->

<script type="text/javascript" src="https://checkout.stripe.com/checkout.js"></script>



<title><?php echo $page_title . ' | ' . $site; ?></title>

<link rel="icon" type="image/png" href="/favicon.ico" />

<link rel="apple-touch-icon-precomposed" href="/ios-icon.png" />
<link rel="apple-touch-icon" href="/ios-icon.png" />
<link rel="apple-touch-icon" sizes="76x76" href="/ios-icon-ipad.png" />
<link rel="apple-touch-icon" sizes="120x120" href="/ios-icon-iphone-retina.png" />
<link rel="apple-touch-icon" sizes="152x152" href="/ios-icon-ipad-retina.png" />


<meta name="viewport" content="initial-scale=1, maximum-scale=1" />

<link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="/css/introjs.css" rel="stylesheet" type="text/css" />
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

<!-- STEPS SECTION -->
<link href="/css/steps.css" rel="stylesheet" type="text/css" />
<link href="/css/steps.main.css" rel="stylesheet" type="text/css" />
<link href="/css/steps.normalize.css" rel="stylesheet" type="text/css" />
<link href="/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />

<link href="/css/styles.css" rel="stylesheet" type="text/css" />


<!-- 
<link href="/css/loaders.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="/css/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<link href="/css/mobile.css" rel="stylesheet" type="text/css" />
-->
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52545347-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body id="<?php echo strtolower(str_replace(' ', '_', $page_title) . '_page');?>">
<?php 
	if(isset($user_id)) {
		require_once('includes/admin_nav.php');	
	}
	else {
		require_once('includes/login_nav.php');	
	}
	
?>

<div id="contain_all">


<div id="content">

