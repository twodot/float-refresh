<?php 
	$admin_nav = array();
	$admin_nav[] = array('link'=>'#', 'name'=>'<img src="/images/hamburger.png" alt="Float Mobile Nav Icon" title="Float Mobile Nav Icon" />', 'page_title'=>'Mobile');
	$admin_nav[] = array('link'=>'/', 'name'=>'<img src="/images/logo-small-25.png" alt="Float Logo" title="Float Logo" />', 'page_title'=>'Logo');
	$admin_nav[] = array('link'=>'/panel', 'name'=>'My Properties', 'page_title'=>'Control Panel');
	$admin_nav[] = array('link'=>'/multi-family', 'name'=>'My Multi-family', 'page_title'=>'Multi-Family');
	$admin_nav[] = array('link'=>'/account', 'name'=>'My Account', 'page_title'=>'My Account');
	$admin_nav[] = array('link'=>'/beacons', 'name'=>'My Beacons', 'page_title'=>'Beacons');
	echo $UserInfo->get_sub_subscribe_level();
	if($UserInfo->get_sub_subscribe_level() && $UserInfo->get_sub_subscribe_level() == 1) {
		
		$q_orders = "SELECT COUNT(*) as unfullfilled FROM beacon_orders WHERE order_fullfilled = 0";
		$r_orders = mysqli_query($dbc, $q_orders);
		$row_orders = mysqli_fetch_array($r_orders, MYSQLI_ASSOC);
		
		$admin_nav[] = array('link'=>'/orders', 'name'=>'Orders', 'page_title'=>'Orders');
	}
	else {
		if(isset($_SESSION['cart'])) {
			$admin_nav[] = array('link'=>'/view_cart', 'name'=>'Cart', 'page_title'=>'View Cart', 'class'=>'orders_waiting');
		}
		else {
			$admin_nav[] = array('link'=>'/view_cart', 'name'=>'Cart', 'page_title'=>'View Cart');
		}
	}
	if(isset($_SESSION['login']['user_level']) && $_SESSION['login']['user_level'] == 1  || isset($_SESSION['pseudo_user'])) {
		$admin_nav[] = array('link'=>'/users', 'name'=>'Users', 'page_title'=>'Users');
	}
	
	$admin_nav[] = array('link'=>'/logout', 'name'=>'Logout', 'page_title'=>'logout');
?><nav id="admin_nav" class="navbar navbar-default">
    <div id="admin_nav_holder" class="">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
        </button>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                    <?php
                        
                        foreach($admin_nav as $key=>$val) {
                                $class_name = strtolower(str_replace(' ', '_', $val['page_title'])) . '_link';
                                if ($val['page_title'] == $page_title) {
                                    echo '<li class="current_page ' . $class_name . '">';
                                    echo '<p>' . $val['name'] . '</p>';
                                }
                                else if ($val['page_title'] == 'Orders') {
                                    if($row_orders['unfullfilled'] > 0) {
                                        echo '<li class="orders_waiting ' . $class_name . '">';
                                        echo '<a href="' . $val['link'] . '">' . $val['name'] . '</a>';
                                    }
                                    else {
                                        echo '<li class="' . $class_name . '">';
                                        echo '<a href="' . $val['link'] . '">' . $val['name'] . '</a>';
                                    }
                                }
                                else if ($val['page_title'] == 'Logo') {
                                    echo '<li class="branding_link_holder ' . $class_name . '">';
                                    echo '<a href="' . $val['link'] . '" class="branding_link">' . $val['name'] . '</a>';
                                }
                                else if ($val['page_title'] == 'Mobile') {
                                    echo '<li class="' . $class_name . '">';
                                    echo '<a href="' . $val['link'] . '" id="hamburger" >' . $val['name'] . '</a>';
                                }
                                else {
                                    echo '<li class="' . $class_name; if(!empty($val['class'])) { echo ' ' . $val['class']; } echo '">';
                                    echo '<a href="' . $val['link'] . '">' . $val['name'] . '</a>';
                                }
                            echo '</li>';
                           
                        }
                    
                    
                    ?>
                    
                </ul>
                <ul class="nav navbar-nav navbar-right">
      			<li>
            <?php
      
        if( $UserInfo->has_subscription()) {
           
			$stripe_id = $UserInfo->get_sub_allowed_prop();
			
			$total_properties = $UserInfo->get_sub_allowed_prop();
			if ($total_properties == 0) {
				$total_properties = 'Unlimited';
			}
			if($UserInfo->is_sub_free() ==1) {
				$total_properties = 'FREE';
				if($Subscription->is_sub_active() == 1) {
					echo '<div id="current_subscription"><h4>' . $UserInfo->get_first_name() . '&rsquo;s Subscription</h4>'. $total_properties . '</div>';
				}
				else {
					echo '<div id="current_subscription"><h4>' . $UserInfo->get_first_name() . '&rsquo;s Subscription</h4>'. $total_properties . '</div>';
				}
			}
			else {
				if($UserInfo->is_sub_active() == 1) {
					echo '<div id="current_subscription"><h4>' . $UserInfo->get_first_name() . '&rsquo;s Subscription</h4><span class="glyphicon glyphicon-ok-circle"></span> '. $total_properties . ' Properties</div>';
				}
			}
			
           
        } // END HAS SUBSCRIPTION CHECK
        else {
            echo '<div id="current_subscription"><h4>' . $UserInfo->get_first_name() . ' ' . $UserInfo->get_last_name() . '</h4><span class="glyphicon glyphicon-remove-circle"></span> Subscription</div>';
        }
    ?></li>
    </ul>
            </div>
    </div>
    <div class="clear"></div>
</nav>
<?php
if(isset($_SESSION['login']['user_level']) && $_SESSION['login']['user_level'] == 1) {
	if(isset($_SESSION['pseudo_user']['id'])) {
		echo '<div id="pseudo_error">You&rsquo;re using the panel as another user <a href="/reset_pseudo_user">Switch back</a></div>';
	}
}
?>

<?php 

//$show_hints = show_hints($user_id, $dbc); 
// $rental = rental($user_id, $dbc);

?>