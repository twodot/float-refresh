<?php
if(!isset($_SESSION)) {
     session_start();
}

function panel_direct() {
	$url = '/panel';
	ob_end_clean();
	header("Location: $url");
	exit();
}

function require_login() {
	
	if (!isset($_SESSION['login']['first_name'])) {
		
		echo $url = BASE_URL . 'login' ;
		
		ob_end_clean();
		
		header("Location: $url");
		
		exit();
	}
	if (isset($_SESSION['login']['user_level'])) {
		if ($_SESSION['login']['user_level'] != 1) {
			$url = 'https://admin.listfloat.com/panel' ;
	
			ob_end_clean();
			
			header("Location: $url");
			
			exit();
		}
	}
}

function login_forward() {
	
	if (isset($_SESSION['login']['first_name'])) {
		$url = BASE_URL . 'panel' ;
		ob_end_clean();
		header("Location: $url");
		exit();
	}
}

function require_admin_login() {
	
	if (!isset($_SESSION['login']['first_name'])) {
		
		$url = BASE_URL . 'login' ;
		
		ob_end_clean();
		
		header("Location: $url");
		
		exit();
	}
	if (isset($_SESSION['login']['user_level'])) {
		if ($_SESSION['login']['user_level'] != 1) {
			$url = BASE_URL . 'panel' ;
	
			ob_end_clean();
			
			header("Location: $url");
			
			exit();
		}
	}
}


// USER LOGIN SECTION //
if(isset($_SESSION['login'])) {
	$user_id = $_SESSION['login']['user_id'];
	
	$user_email = $_SESSION['login']['email'];
	
	//$user_name = $_SESSION['login']['first_name'] . ' ' . $_SESSION['login']['last_name'];
}


// Get the properties id and creating a property_id variable
$property_id = 0;
function get_property_id() {
	global $property;
	global $property_id;
	if(isset($_GET['property']) || isset($property)) {
		if(isset($_GET['property']) ) {
			$property_id = (int) $_GET['property'];
		}
		else if(isset($property)) {
			$property_id = (int) $property;
		}
	}
	else {
		$property_id = 0;
	}
		
	if($property_id == 0) {
		header('Location: /panel');
	}
}

// prevent others from accessing this user's property

function users_property(){
	global $user_id;
	global $dbc;
	global $property_id;
	$query="SELECT user_id FROM properties WHERE id=".$property_id." AND user_id=".$user_id;
	$res=mysqli_query($dbc, $query);
	if(!$res || mysqli_num_rows($res)==0)
		die();
}


function mc_request( $type, $target, $data = false )
{
	//MAILCHIMP 3.0 API
	
	$api = array
	(
		'login' => 'User',
		'key'   => 'd80bb8f7aaff60462caca67f2107fbf7-us11',
		'url'   => 'https://us11.api.mailchimp.com/3.0/'
	);
	
	$ch = curl_init( $api['url'] . $target );
 
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array
	(
		'Content-Type: application/json', 
		'Authorization: ' . $api['login'] . ' ' . $api['key'],
//		'X-HTTP-Method-Override: ' . $type,
	) );
 
//	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $type );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_USERAGENT, 'YOUR-USER-AGENT' );
 
	if( $data )
		curl_setopt( $ch, CURLOPT_POSTFIELDS,  $data  );
 
	$response = curl_exec( $ch );
	curl_close( $ch );
 
	return $response;
}


function sanatize_address($address = '') {
	$options = array(
		'options' => array(
			'default' => false
		),
		'flags' => FILTER_FLAG_STRIP_HIGH,
	);
	$var = filter_var($address, FILTER_SANITIZE_STRING, $options);
	return $var;
}

function create_property($address, $user_id) {
	require_once('bitly.php');
	global $dbc;
	$street_number = (isset($address['street_number']) && $address['street_number'] ?$address['street_number']:'');
	$street_name = (isset($address['street_name']) && $address['street_name'] ?$address['street_name']:'');
	$neighborhood = (isset($address['neighborhood']) && $address['neighborhood']?$address['neighborhood']:'');
	$city = (isset($address['city']) && $address['city']?$address['city']:'');
	$county = (isset($address['county'])&& $address['county']?$address['county']:'');
	$state = (isset($address['state'])&&$address['state']?$address['state']:'');
	$zip = (isset($address['zip_code'])&&$address['zip_code']?$address['zip_code']:'');
	$lat  = (isset($address['lat'])?$address['lat']:'');
	$lng = (isset($address['lng'])?$address['lng']:'');
	
	if($street_number == '' && $street_name  == '' && $neighborhood == '' && $city  == '' && $county  == '' && $state  == '' && $zip  == '' && $lat  == 0 && $lng  == 0) {
		// IF ADDRESS ISNT FOUND IN GOOGLE BUT USER STILL WANTS TO UPLOAD IT
		$address = (isset($address['formatted_address'])?$address['formatted_address']:'');
	}
	else {
		// CORRECTLY FORMATTED ADDRESS FROM GOOGLE
		$address = $street_number . ' ' . $street_name;
	}
	
	$q_check = "SELECT id from properties WHERE address = '$address' AND user_id = '$user_id'";
	$r_check = @mysqli_query ($dbc, $q_check);
	
	if (mysqli_num_rows($r_check) > 0) {
		$row_check = mysqli_fetch_array($r_check);
		$id = $row_check['id'];
		$q_check = "UPDATE properties SET deleted = 0 WHERE address = '$address' AND user_id = '$user_id' AND id = $id";
		$r_check = @mysqli_query ($dbc, $q_check);
		if(mysqli_affected_rows($dbc) == 1) {
			return (int) $id;
		}
		else {
			return 'This Property Already Exists';
		}
	}
	else { // NUM ROWS
	
		$q = "
				INSERT INTO 
				
				properties 
				
				(user_id, address, city, state, zip, community, county, latitude, longitude, property_data_type)
				
				VALUES 
				
				(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				
			$stmt= mysqli_prepare($dbc, $q);
			
			$type = 'property';
			mysqli_stmt_bind_param($stmt, 'isssissdds', $user_id, $address, $city, $state, $zip, $neighborhood, $county, $lat, $lng, $type);
			
			mysqli_stmt_execute($stmt);
			
			if (mysqli_stmt_affected_rows($stmt) ==1){
				
				$ri = mysqli_stmt_insert_id($stmt);
				/* if($rental == 1) {
					$q_status = "UPDATE properties SET status = 3 WHERE id = $ri";
					$r_status = @mysqli_query($dbc, $q_status);
				} */
				$results = bitly_v3_shorten(PUBLIC_BASE_URL . '/view_property/' . $ri, BITLY_APP_GENERIC_ACCESS, 'bit.ly');
				$short_url = $results['hash'];
				
				$fb_results = bitly_v3_shorten(PUBLIC_BASE_URL . '/view_property/' . $ri . '?utm_source=facebook&utm_medium=share_panel&utm_campaign=property', BITLY_APP_GENERIC_ACCESS, 'bit.ly');
				$fb_short_url = $fb_results['hash'];
				
				$tw_results = bitly_v3_shorten(PUBLIC_BASE_URL . '/view_property/' . $ri . '?utm_source=twitter&utm_medium=share_panel&utm_campaign=property', BITLY_APP_GENERIC_ACCESS, 'bit.ly');
				$tw_short_url = $tw_results['hash'];
				
				$q = "UPDATE properties SET short_url_hash = '$short_url', facebook_share = '$fb_short_url', twitter_share = '$tw_short_url' WHERE id = $ri";
				$r = @mysqli_query($dbc, $q);
				
				$q = "
				INSERT INTO 
				property_info 
				
				SET 
				properties_id = $ri, 
				title = 'Primary',  
				primary_info = 1";
				$r = mysqli_query($dbc, $q);
				$q = "
				INSERT INTO 
				property_info 
				
				SET 
				properties_id = $ri, 
				title = 'Location',  
				location = 1";
				
				$r = mysqli_query($dbc, $q);
				return (int) $ri;
			}
	}
	return 'The property could not be added';
}

function get_map_styles() {
	?>
    
 
    var usRoadMapType = new google.maps.StyledMapType([

           
            {
              featureType: 'landscape',
              elementType: 'geometry',
              stylers: [
                {color: '#eeeeee'},
                {saturation: 0}, 
                {visibility: 'simplified'}
              ]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [
                {color: '#656268'}, 
                {saturation: -70}, 
                {lightness: -10}
              ]
            }, {
              featureType: 'road.arterial',
              elementType: 'geometry',
              stylers: [
                {color: '#eeeeee'},
                {visibility: 'simplified'}
              ]
            },
              {
              featureType: 'road.arterial',
              elementType: 'labels',
              stylers: [
                {hue: '#00AEC7'},
                {lightness: 0}, 
                {visibility: 'simplified'}
              ]
            }, {
              featureType: 'road.local',
              elementType: 'geometry',
              stylers: [
                {color: '#ffffff'},
                {visibility: 'simplified'}
              ]
            },
            {
              featureType: 'road.local',
              elementType: 'labels',
              stylers: [
                {hue: '#30353b'}
              ]
            },
             {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [
                {color: '#00AEC7'}, 
                {lightness: 0}, 
                {saturation: 0}
              ]
            }, 
             {
              featureType: 'administrative.neighborhood',
              elementType: 'labels',
              stylers: [
                {hue: '#00AEC7'},
                {saturation: 0},
                {lightness: 0}
              ]
            },
             {
              featureType: 'administrative.neighborhood',
              elementType: 'labels.text.fill',
              stylers: [
               { "visibility": "off" }
              ]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels',
              stylers: [
                {hue: '#00AEC7'},
                {saturation: 0},
                {lightness: 0}, 
                
              ]
            }, {
              featureType: 'administrative.locality',
              elementType: 'labels',
              stylers: [
                {hue: '#00AEC7'}, 
                {saturation: 0}, 
                
              ]
            }, {
              featureType: 'administrative.area_level_1',
              elementType: 'labels',
              stylers: [
                {hue: '#00AEC7'}, 
                {saturation: 0}, 
                
              ]
            }, {
              featureType: 'transit.line',
              elementType: 'geometry',
              stylers: [
                {hue: '#DFC221'},
                {visibility: 'on'},
                {lightness: -30}
              ]
            }, 
            {
              featureType: 'poi',
             elementType: 'geometry',
              stylers: [
                {color: '#cccccc'},
                {lightness: 0},
                {saturation: 0},  
                {visibility: 'simplified'}
              ]
            }, 
            {
              featureType: 'poi',
              elementType: 'labels',
              stylers: [
                 {hue: '#30353b'},
                {saturation: -100},
                {lightness:-10}, 
                
              ]
            }, 
             {
              featureType: 'poi.business',
              elementType: 'labels.icon',
              stylers: [
                {hue:'#fff'}, 
                {saturation: 10}, 
                {lightness:10}
              ]
            }
          ], {name: 'US Road Atlas'});
        
        function getMiles(i) {
             return i*0.000621371192;
        }
    
    <?php
}
// IMAGE RESIZE


function generate_image_thumbnail($source_image_path, $thumbnail_image_path, $id)
{
	list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
	switch ($source_image_type) {
		case IMAGETYPE_GIF:
			$source_gd_image = imagecreatefromgif($source_image_path);
			break;
		case IMAGETYPE_JPEG:
			$source_gd_image = imagecreatefromjpeg($source_image_path);
			break;
		case IMAGETYPE_PNG:
			$source_gd_image = imagecreatefrompng($source_image_path);
			break;
	}
	if ($source_gd_image === false) {
		return false;
	}
	$source_aspect_ratio = $source_image_width / $source_image_height;
	$thumbnail_aspect_ratio = THUMBNAIL_IMAGE_MAX_WIDTH / THUMBNAIL_IMAGE_MAX_HEIGHT;
	if ($source_image_width <= THUMBNAIL_IMAGE_MAX_WIDTH && $source_image_height <= THUMBNAIL_IMAGE_MAX_HEIGHT) {
		$thumbnail_image_width = $source_image_width;
		$thumbnail_image_height = $source_image_height;
	} elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
		$thumbnail_image_width = (int) (THUMBNAIL_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
		$thumbnail_image_height = THUMBNAIL_IMAGE_MAX_HEIGHT;
	} else {
		$thumbnail_image_width = THUMBNAIL_IMAGE_MAX_WIDTH;
		$thumbnail_image_height = (int) (THUMBNAIL_IMAGE_MAX_WIDTH / $source_aspect_ratio);
	}
	$thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
	imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);
	imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 90);
	imagedestroy($source_gd_image);
	imagedestroy($thumbnail_gd_image);
	
	return $thumbnail_image_path;
}



function generate_image_phone($source_image_path, $phone_image_path, $id)
{
	list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
	switch ($source_image_type) {
		case IMAGETYPE_GIF:
			$source_gd_image = imagecreatefromgif($source_image_path);
			break;
		case IMAGETYPE_JPEG:
			$source_gd_image = imagecreatefromjpeg($source_image_path);
			break;
		case IMAGETYPE_PNG:
			$source_gd_image = imagecreatefrompng($source_image_path);
			break;
	}
	if ($source_gd_image === false) {
		return false;
	}
	$source_aspect_ratio = $source_image_width / $source_image_height;
	$phone_aspect_ratio = PHONE_IMAGE_MAX_WIDTH / PHONE_IMAGE_MAX_HEIGHT;
	if ($source_image_width <= PHONE_IMAGE_MAX_WIDTH && $source_image_height <= PHONE_IMAGE_MAX_HEIGHT) {
		$phone_image_width = $source_image_width;
		$phone_image_height = $source_image_height;
	} elseif ($phone_aspect_ratio > $source_aspect_ratio) {
		$phone_image_width = (int) (PHONE_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
		$phone_image_height = PHONE_IMAGE_MAX_HEIGHT;
	} else {
		$phone_image_width = PHONE_IMAGE_MAX_WIDTH;
		$phone_image_height = (int) (PHONE_IMAGE_MAX_WIDTH / $source_aspect_ratio);
	}
	$phone_gd_image = imagecreatetruecolor($phone_image_width, $phone_image_height);
	imagecopyresampled($phone_gd_image, $source_gd_image, 0, 0, 0, 0, $phone_image_width, $phone_image_height, $source_image_width, $source_image_height);
	imagejpeg($phone_gd_image, $phone_image_path, 90);
	imagedestroy($source_gd_image);
	imagedestroy($phone_gd_image);
	
	return $phone_image_path;
}




function generate_image_tablet($source_image_path, $tablet_image_path, $id)
{
	list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
	switch ($source_image_type) {
		case IMAGETYPE_GIF:
			$source_gd_image = imagecreatefromgif($source_image_path);
			break;
		case IMAGETYPE_JPEG:
			$source_gd_image = imagecreatefromjpeg($source_image_path);
			break;
		case IMAGETYPE_PNG:
			$source_gd_image = imagecreatefrompng($source_image_path);
			break;
	}
	if ($source_gd_image === false) {
		return false;
	}
	$source_aspect_ratio = $source_image_width / $source_image_height;
	$tablet_aspect_ratio = TABLET_IMAGE_MAX_WIDTH / TABLET_IMAGE_MAX_HEIGHT;
	if ($source_image_width <= TABLET_IMAGE_MAX_WIDTH && $source_image_height <= TABLET_IMAGE_MAX_HEIGHT) {
		$tablet_image_width = $source_image_width;
		$tablet_image_height = $source_image_height;
	} elseif ($tablet_aspect_ratio > $source_aspect_ratio) {
		$tablet_image_width = (int) (TABLET_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
		$tablet_image_height = TABLET_IMAGE_MAX_HEIGHT;
	} else {
		$tablet_image_width = TABLET_IMAGE_MAX_WIDTH;
		$tablet_image_height = (int) (TABLET_IMAGE_MAX_WIDTH / $source_aspect_ratio);
	}
	$tablet_gd_image = imagecreatetruecolor($tablet_image_width, $tablet_image_height);
	imagecopyresampled($tablet_gd_image, $source_gd_image, 0, 0, 0, 0, $tablet_image_width, $tablet_image_height, $source_image_width, $source_image_height);
	imagejpeg($tablet_gd_image, $tablet_image_path, 90);
	imagedestroy($source_gd_image);
	imagedestroy($tablet_gd_image);
	
	return $tablet_image_path;
}

function check_property_info($property_id) {
	global $dbc;
	$userID = $_SESSION['login']['user_id'];
	$q="SELECT * FROM properties WHERE user_id = $userID AND id = $property_id";
	$res=mysqli_query($dbc, $q);
	return (bool) mysqli_num_rows($res);
}

?>