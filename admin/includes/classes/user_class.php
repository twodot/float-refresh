<?php
/*
	DEFINE ('DB_USER', 'thefloat');
	DEFINE ('DB_PASSWORD', 'Garbanz0beans!');
	DEFINE ('DB_HOST', 'float-admin-dev.cxyioikpwrih.us-west-2.rds.amazonaws.com');
	DEFINE ('DB_NAME', 'float_admin');
	$dbc = @mysqli_connect (DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
*/
	
	class User{
		private $dbc;
		private $user_id;
		private $users;
		private $sub;
		private $changed=false;
		
		/**
		* constructs an instance with info of specified user_id in DB table users & subscription
		* @param int $user_id User id used to construct the instance with info in DB
		* @param mysqli $dbc DB link
		* @return void
		*/
		public function __construct($id, mysqli $dbc){
			global $dbc;
			$this->dbc=$dbc;
			
			if(is_int($id+0)){
				$this->user_id=(int)$id;
				
				// query-users
				$q_users = "SELECT * FROM users WHERE user_id=".$id;
				$r_users = mysqli_query($this->dbc, $q_users);
				if($r_users && $row_users = mysqli_fetch_assoc($r_users)){
					// get info from DB
					$this->users["first_name"]=$row_users["first_name"];
					$this->users["last_name"]=$row_users["last_name"];
					$this->users["email"]=$row_users["email"];
					$this->users["pass"]=$row_users["pass"];
					$this->users["user_level"]=$row_users["user_level"];
					$this->users["active"]=$row_users["active"];
					$this->users["registration_date"]=$row_users["registration_date"];
					$this->users["ph_num"]=$row_users["ph_num"];
					$this->users["test"]=$row_users["test"];
					$this->users["stripe_id"]=$row_users["stripe_id"];
					$this->users["show_hints"]=$row_users["show_hints"];
					$this->users["listing_name"]=$row_users["listing_name"];
					$this->users["listing_email"]=$row_users["listing_email"];
					$this->users["listing_phone"]=$row_users["listing_phone"];
					$this->users["listing_brokerage"]=$row_users["listing_brokerage"];
					$this->users["broker_website"]=$row_users["broker_website"];
					$this->users["show_tour"]=$row_users["show_tour"];
					$this->users["loyalty_id"]=$row_users["loyalty_id"];
					$this->users["sent_loyalty"]=$row_users["sent_loyalty"];
					$this->users["referred_by"]=$row_users["referred_by"];
					$this->users["referred_loyalty_code"]=$row_users["referred_loyalty_code"];
					$this->users["referred_credited"]=$row_users["referred_credited"];
					$this->users["charge_failed"]=$row_users["charge_failed"];
				} else
					throw new Exception("User Does Not Exist.");
					
				//query-sub
				$q_sub = "SELECT * FROM subscription WHERE user_id=".$id;
				$r_sub = mysqli_query($this->dbc, $q_sub);
				if($row_sub = mysqli_fetch_assoc($r_sub)){
					// get info from DB
					$this->sub["has_subscription"] = true;
					$this->sub["subscribe_level"]=$row_sub["subscribe_level"];
					$this->sub["allowed_prop"]=$row_sub["allowed_prop"];
					$this->sub["amount"]=$row_sub["amount"];
					$this->sub["active"]=$row_sub["active"];
					$this->sub["deactivate_in"]=$row_sub["deactivate_in"];
					$this->sub["current_period_start"]=$row_sub["current_period_start"];
					$this->sub["current_period_end"]=$row_sub["current_period_end"];
					$this->sub["trial_start"]=$row_sub["trial_start"];
					$this->sub["trial_end"]=$row_sub["trial_end"];
					$this->sub["rental"]=$row_sub["rental"];
					$this->sub["free"]=$row_sub["free"];
				} else
					$this->sub["has_subscription"] = false;
					$this->sub["subscribe_level"]= false;
					$this->sub["allowed_prop"]= false;
					$this->sub["amount"]= false;
					$this->sub["active"]= false;
					$this->sub["deactivate_in"]= false;
					$this->sub["current_period_start"]= false;
					$this->sub["current_period_end"]= false;
					$this->sub["trial_start"]= false;
					$this->sub["trial_end"]= false;
					$this->sub["rental"]= false;
					$this->sub["free"]= false;

				//query-billing
				$q_bill = "SELECT * FROM billing WHERE user_id=".$id;
				$r_bill = mysqli_query($this->dbc, $q_bill);
				if($row_bill = mysqli_fetch_assoc($r_bill)){
					// get info from DB
					$this->users["has_billing"] = true;
					$this->users["billing_street"] = $row_bill['street'];
					$this->users["billing_street_cont"] = $row_bill['street_cont'];
					$this->users["billing_apt_number"] = $row_bill['apt_number'];
					$this->users["billing_city"] = $row_bill['city'];
					$this->users["billing_state"] = $row_bill['state_name'];
					$this->users["billing_zip"] = $row_bill['zip'];
				} else
					$this->users["has_billing"] = false;
					$this->users["billing_street"] = false;
					$this->users["billing_street_cont"] = false;
					$this->users["billing_apt_number"] = false;
					$this->users["billing_city"] = false;
					$this->users["billing_state"] = false;
					$this->users["billing_zip"] = false;
					
					//query-shipping
				$q_ship = "SELECT * FROM billing WHERE user_id=".$id;
				$r_ship = mysqli_query($this->dbc, $q_ship);
				if($row_ship = mysqli_fetch_assoc($r_ship)){
					// get info from DB
					$this->users["has_shipping"] = true;
					$this->users["shipping_street"] = $row_ship['street'];
					$this->users["shipping_street_cont"] = $row_ship['street_cont'];
					$this->users["shipping_apt_number"] = $row_ship['apt_number'];
					$this->users["shipping_city"] = $row_ship['city'];
					$this->users["shipping_state"] = $row_ship['state_name'];
					$this->users["shipping_zip"] = $row_ship['zip'];
				} else
					$this->users["has_shipping"] = false;
					$this->users["shipping_street"] = false;
					$this->users["shipping_street_cont"] = false;
					$this->users["shipping_apt_number"] = false;
					$this->users["shipping_city"] = false;
					$this->users["shipping_state"] = false;
					$this->users["shipping_zip"] = false;

			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Get the user id of the user
		* @param void
		* @return int $user_id User id of the user
		*/
		function get_user_id(){
			return $this->user_id;
		}
		
		/**
		* Get the first name of the user
		* @param void
		* @return string $first_name First name of the user
		*/
		function get_first_name(){
			return $this->users["first_name"];
		}
		
		/**
		* Change the first name of the user to the given string
		* @param string $first_name
		* @return void
		*/
		function set_first_name($first_name){
			if(is_string($first_name)){
				if($this->users["first_name"]!=$first_name){
					$this->users["first_name"]=$first_name;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Get the last name of the user
		* @param void
		* @return string $last_name Last name of the user
		*/
		function get_last_name(){
			return $this->users["last_name"];
		}
		
		/**
		* Change the last name of the user to the given string
		* @param string $last_name
		* @return void
		*/
		function set_last_name($last_name){
			if(is_string($last_name)){
				if($this->users["last_name"]!=$last_name){
					$this->users["last_name"]=$last_name;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Get the email address of the user
		* @param void
		* @return string $email Email address of the user
		*/
		function get_email(){
			return $this->users["email"];
		}
		
		/**
		* Change the email address of the user to the given string
		* @param string $email
		* @return void
		*/
		function set_email($email){
			if(is_string($email)){
				if($this->users["email"]!=$email){
					$this->users["email"]=$email;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Change the password of the user to the given string if
		* there is only one passward passed to the function or
		* the two password passed are the same. PLEASE CHECK WHETHER
		* THE TWO PASSWORDS ARE THE SAME IF TWO PASSWORDS ARE PASSED.
		* @param string $pass1
		* @param string $pass2 optional
		* @return bool $status True if set the password successfully and false if not
		*/
		function set_pass($pass1, $pass2=NULL){
			if(is_string($pass1) && (is_string($pass2) || $pass2===NULL)){
				if(($pass2===NULL && $pass1!="") || ($pass2!==NULL && $pass1!="" && $pass1===$pass2)){
					if($this->users["pass"]!=$pass1){
						$this->users["pass"]=$pass1;
						$this->changed=true;
					}
					return true;
				}
				return false;
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* returns whether or not the user is admin
		* @param void
		* @return bool $is_admin True if the user is admin and false otherwise
		*/
		function is_admin(){
			return $this->users["user_level"];
		}
		
		/**
		* Change the user level to input (admin or not)
		* @param bool $admin
		* @return void
		*/
		function set_admin($admin){
			$admin=(bool)$admin;
			if($this->users["user_level"]!=$admin){
				$this->users["user_level"]=$admin;
				$this->changed=true;
			}
		}
		
		/**
		* returns whether or not the user is activated
		* @param void
		* @return bool $is_active True if the user is activated and false otherwise
		*/
		function is_active(){
			return ($this->users["active"])? 0: 1;
		}
		
		/**
		* Activate the user account
		* @param void
		* @return void
		*/
		function activate(){
			if($this->users["active"]){
				$this->users["active"]=null;
				$this->changed=true;
			}
		}
		
		/**
		* Get the registration date of the user
		* @param void
		* @return string $registration_date Registration date of the user
		*/
		function get_registration_date(){
			return $this->users["registration_date"];
		}
		
		/**
		* Get the phone number of the user
		* @param void
		* @return string $ph_num Phone number of the user
		*/
		function get_ph_num(){
			return $this->users["ph_num"];
		}
		
		/**
		* Change the user's phone number
		* @param string $ph_num phone number to be changed to
		* @return void
		*/
		function set_ph_num($ph_num){
			if(is_string($ph_num)){
				if($this->users["ph_num"]!=$ph_num){
					$this->users["ph_num"]=$ph_num;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Get the stripe id of the user
		* @param void
		* @return string $stripe_id Stripe id of the user
		*/
		function get_stripe_id(){
			return $this->users["stripe_id"];
		}
		
		/**
		* Returns whether the user needs to show hints
		* @param void
		* @return bool $show_hints True if the user needs to show hints and false otherwise
		*/
		function need_show_hints(){
			return $this->users["show_hints"];
		}
		
		/**
		* Get the listing info of the user
		* @param void
		* @return array $listing_info{
		*	@string string listing_name
		*	@string string listing_email
		*	@string string listing_phone
		*	@string string listing_brokerage
		*	@string string broker_website
		* }
		*/
		function get_listing_info(){
			return array("listing_name"=>$this->users["listing_name"], "listing_email"=>$this->users["listing_email"], 
					"listing_phone"=>$this->users["listing_phone"], "listing_brokerage"=>$this->users["listing_brokerage"], 
					"broker_website"=>$this->users["broker_website"]);
		}
		
		
		/**
		* Get the billing info of the user
		* @param void
		* @return array $billing_address{
		*	@string string street
		*	@string string street_cont
		*	@string string apt_number
		*	@string string city
		*	@string string state
		*	@string string zip
		* }
		*/
		function get_billing_address(){
			return array("street"=>$this->users["billing_street"], "street_cont"=>$this->users["billing_street_cont"], 
					"apt_number"=>$this->users["billing_apt_number"], "city"=>$this->users["billing_city"], 
					"state"=>$this->users["billing_state"], "zip"=>$this->users["billing_zip"]);
		}
		
		/**
		* Get the billing info of the user
		* @param void
		* @return array $billing_address{
		*	@string string street
		*	@string string street_cont
		*	@string string apt_number
		*	@string string city
		*	@string string state
		*	@string string zip
		* }
		*/
		function get_shipping_address(){
			return array("street"=>$this->users["shipping_street"], "street_cont"=>$this->users["shipping_street_cont"], 
					"apt_number"=>$this->users["shipping_apt_number"], "city"=>$this->users["shipping_city"], 
					"state"=>$this->users["shipping_state"], "zip"=>$this->users["shipping_zip"]);
		}
		
		/**
		* Change the user's listing info to the informations in the given array
		* @param array $listing_info listing information to be changed to
		* @return void
		*/
		function set_listing_info(array $listing_info){
			if($this->users["listing_name"]!=$listing_info["listing_name"] ||
					$this->users["listing_email"]!=$listing_info["listing_email"] ||
					$this->users["listing_phone"]!=$listing_info["listing_phone"] ||
					$this->users["listing_brokerage"]!=$listing_info["listing_brokerage"] ||
					$this->users["broker_website"]!=$listing_info["broker_website"])
			{
				$this->users["listing_name"]=$listing_info["listing_name"];
				$this->users["listing_email"]=$listing_info["listing_email"];
				$this->users["listing_phone"]=$listing_info["listing_phone"];
				$this->users["listing_brokerage"]=$listing_info["listing_brokerage"];
				$this->users["broker_website"]=$listing_info["broker_website"];
				$this->changed=true;
			}
		}
		
		/**
		* Change the user's listing name
		* @param string $listing_name listing name to be changed to
		* @return void
		*/
		function set_listing_name($listing_name){
			if(is_string($listing_name)){
				if($this->users["listing_name"]!=$listing_name){
					$this->users["listing_name"]=$listing_name;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Change the user's listing email
		* @param string $listing_email listing email to be changed to
		* @return void
		*/
		function set_listing_email($listing_email){
			if(is_string($listing_email)){
				if($this->users["listing_email"]!=$listing_email){
					$this->users["listing_email"]=$listing_email;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Change the user's listing phone number
		* @param string $listing_phone listing phone number to be changed to
		* @return void
		*/
		function set_listing_phone($listing_phone){
			if(is_string($listing_phone)){
				if($this->users["listing_phone"]!=$listing_phone){
					$this->users["listing_phone"]=$listing_phone;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Change the user's listing brokerage
		* @param string $listing_brokerage listing brokerage to be changed to
		* @return void
		*/
		function set_listing_brokerage($listing_brokerage){
			if(is_string($listing_brokerage)){
				if($this->users["listing_brokerage"]!=$listing_brokerage){
					$this->users["listing_brokerage"]=$listing_brokerage;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Change the user's brokerage website
		* @param string $broker_website brokerage website to be changed to
		* @return void
		*/
		function set_broker_website($broker_website){
			if(is_string($broker_website)){
				if($this->users["broker_website"]!=$broker_website){
					$this->users["broker_website"]=$broker_website;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Returns whether the user want get a tour on log in
		* @param void
		* @return bool $show_tour True if the user need to get a tour on log in and false otherwise 
		*/
		function need_show_tour(){
			return $this->users["show_tour"];
		}
				
		/**
		* Get the loyalty id of the user
		* @param void
		* @return string $loyalty_id Loyalty id of the user
		*/
		function get_loyalty_id(){
			return $this->users["loyalty_id"];
		}
		
		/**
		* Returns whether the user has sent loyalty
		* @param void
		* @return bool $sent_loyalty True if the user has sent loyalty and false otherwise 
		*/
		function has_sent_loyalty(){
			return $this->users["sent_loyalty"];
		}
		
		/**
		* Returns by whom the user is referred
		* @param void
		* @return int $referred_by By whom the user is referred
		*/
		function get_referred_by(){
			return $this->users["referred_by"];
		}
		
		/**
		* Get the referred loyal code of the user
		* @param void
		* @return string $referred_loyal_code The referred loyal code of the user
		*/
		function get_referred_loyal_code(){
			return $this->users["referred_loyal_code"];
		}
		
		/**
		* Check for user subscription
		* @param void
		* @return bool 
		*/
		function has_subscription(){
			return $this->sub["has_subscription"];
		}
		
		
		/**
		* Get subscribe level of the user
		* @param void
		* @return int $subscribe_level Subscribe level of the user
		*/
		function get_sub_subscribe_level(){
			return $this->sub["subscribe_level"];
		}
		
		/**
		* Get allowed property of the user
		* @param void
		* @return int $allowed_prop Allowed property of the user
		*/
		function get_sub_allowed_prop(){
			return $this->sub["allowed_prop"];
		}
		
		/**
		* Get the amount in cents the user pay per month
		* @param void
		* @return int $amount The amount in cents the user pay per month
		*/
		function get_sub_amount(){
			return $this->sub["amount"];
		}
		
		/**
		* Returns whether the user has active subscription
		* @param void
		* @return bool $user_id True if the user has active subscription and false otherwise
		*/
		function is_sub_active(){
			return $this->sub["active"];
		}
		
		/**
		* Add or remove user's active subscription
		* @param bool $active True if want to activate user's subscription and false if want to deactivate it
		* @return void
		*/
		function set_sub_active($active){
			$active=(bool)$active;
			if($this->sub["active"]!=$active){
				$this->sub["active"]=$active? 1: 0;
				$this->changed=true;
			}
		}
		
		/**
		* Returns when the current subscription period of the user starts
		* @param void
		* @return string $current_period_start The time the current subscription period of the user starts as integer
		*/
		function get_sub_current_period_start(){
			return $this->sub["current_period_start"];
		}
		
		/**
		* Returns when the current subscription period of the user ends
		* @param void
		* @return string $current_period_end The time the current subscription period of the user ends as integer
		*/
		function get_sub_current_period_end(){
			return $this->sub["current_period_end"];
		}
		
		/**
		* Returns when the subscription free trial of the user starts
		* @param void
		* @return string $trial_start The time the subscription free trial of the user starts as integer
		*/
		function get_sub_trial_start(){
			return $this->sub["trial_start"];
		}
		
		/**
		* Returns when the subscription free trial of the user ends
		* @param void
		* @return string $trial_end The time the subscription free trial of the user ends as integer
		*/
		function get_sub_trial_end(){
			return $this->sub["trial_end"];
		}
		
		/**
		* Returns whether the user is in subscription trial
		* @param void
		* @return bool $is_trialing True if the user is in subscription trial and false otherwise
		*/
		function is_sub_trialing(){
			return (time() > strtotime($this->sub["trial_start"])) &&
					(time() < strtotime($this->sub["trial_end"]))? 1: 0;
		}
		
		/**
		* Returns whether the user has a rental subscription
		* @param void
		* @return bool $is_rental True if the user has a rental subscription and false otherwise
		*/
		function is_sub_rental(){
			return $this->sub["rental"];
		}
		
		/**
		* Returns whether the user has a free subscription
		* @param void
		* @return bool $is_free True if the user has a free subscription and false otherwise
		*/
		function is_sub_free(){
			return $this->sub["free"];
		}
		
		
		/**
		* Add or remove user's free subscription
		* @param bool $free True if want to add subscription to the user and false if want to remove it
		* @return void
		*/
		function set_sub_free($free){
			$free=(bool)$free;
			if($this->sub["free"]!=$free){
				$this->sub["free"]=$free? 1:0;
				$this->changed=true;
			}
		}
		
		/**
		* Update table users in DB. Will update the DB only if anything field changed.
		* @param void
		* @return string $msg Empty string if success, error message if fails
		*/
		function update_DB(){
			$msg="";
			if($this->changed){
				// update table users
				$q_users='UPDATE users SET ';
				$q_temp='';
				foreach($this->users as $key=>$value)
					//$q_temp=$q_temp.", ".$key."='".$value."'";
					$q_temp=$q_temp.", ".$key."=?";
				$q_temp=substr($q_temp, 2);
				$q_users=$q_users.$q_temp." WHERE user_id=".$this->user_id;
				
				// query
				$stmt = mysqli_prepare($this->dbc, $q_users);
				$temp_param=array(str_repeat("s", count($this->users)));
				foreach($this->users as $key=>$value)
					$temp_param[]=&$this->users[$key];
				
				call_user_func_array(array($stmt, "bind_param"), $temp_param);
				// execute prepared statement
				if(!mysqli_stmt_execute($stmt))
					$msg.="Update table users failed: ".mysqli_stmt_error($stmt);
				// close statement and connection
				mysqli_stmt_close($stmt);
				
				//update table subscription
				$q_sub='UPDATE subscription SET ';
				$q_temp='';
				foreach($this->sub as $key=>$value)
					//$q_temp=$q_temp.", ".$key."='".$value."'";
					$q_temp=$q_temp.", ".$key."=?";
				$q_temp=substr($q_temp, 2);
				$q_sub=$q_sub.$q_temp." WHERE user_id=".$this->user_id;
				
				// query
				$stmt = mysqli_prepare($this->dbc, $q_sub);
				$temp_param=array(str_repeat("s", count($this->sub)));
				foreach($this->sub as $key=>$value)
					$temp_param[]=&$this->sub[$key];
				
				call_user_func_array(array($stmt, "bind_param"), $temp_param);
				// execute prepared statement
				if(!mysqli_stmt_execute($stmt))
					$msg.="Update table subscription failed: ".mysqli_stmt_error($stmt);
				else
					$this->changed=false;
				// close statement and connection
				mysqli_stmt_close($stmt);
			}
			return $msg;
		}
		
		/**
		* Reload info in DB to object
		* @param void
		* @return void
		*/
		function reload_DB(){
			// query-users
			$q_users = "SELECT * FROM users WHERE user_id=".$this->user_id;
			$r_users = mysqli_query($this->dbc, $q_users);
			if($row_users = mysqli_fetch_assoc($r_users)){
				// get info from DB
				$this->users["first_name"]=$row_users["first_name"];
				$this->users["last_name"]=$row_users["last_name"];
				$this->users["email"]=$row_users["email"];
				$this->users["pass"]=$row_users["pass"];
				$this->users["user_level"]=$row_users["user_level"];
				$this->users["active"]=$row_users["active"];
				$this->users["registration_date"]=$row_users["registration_date"];
				$this->users["ph_num"]=$row_users["ph_num"];
				$this->users["test"]=$row_users["test"];
				$this->users["stripe_id"]=$row_users["stripe_id"];
				$this->users["show_hints"]=$row_users["show_hints"];
				$this->users["listing_name"]=$row_users["listing_name"];
				$this->users["listing_email"]=$row_users["listing_email"];
				$this->users["listing_phone"]=$row_users["listing_phone"];
				$this->users["listing_brokerage"]=$row_users["listing_brokerage"];
				$this->users["broker_website"]=$row_users["broker_website"];
				$this->users["show_tour"]=$row_users["show_tour"];
				$this->users["loyalty_id"]=$row_users["loyalty_id"];
				$this->users["sent_loyalty"]=$row_users["sent_loyalty"];
				$this->users["referred_by"]=$row_users["referred_by"];
				$this->users["referred_loyalty_code"]=$row_users["referred_loyalty_code"];
				$this->users["referred_credited"]=$row_users["referred_credited"];
				$this->users["charge_failed"]=$row_users["charge_failed"];
			}
			
			// query-users
			$q_sub = "SELECT * FROM subscription WHERE user_id=".$this->user_id;
			$r_sub = mysqli_query($this->dbc, $q_sub);
			if($row_sub = mysqli_fetch_assoc($r_sub)){
				// get info from DB
				$this->sub["has_subscription"] = true;
				$this->sub["subscribe_level"]=$row_sub["subscribe_level"];
				$this->sub["allowed_prop"]=$row_sub["allowed_prop"];
				$this->sub["amount"]=$row_sub["amount"];
				$this->sub["active"]=$row_sub["active"];
				$this->sub["deactivate_in"]=$row_sub["deactivate_in"];
				$this->sub["current_period_start"]=$row_sub["current_period_start"];
				$this->sub["current_period_end"]=$row_sub["current_period_end"];
				$this->sub["trial_start"]=$row_sub["trial_start"];
				$this->sub["trial_end"]=$row_sub["trial_end"];
				$this->sub["rental"]=$row_sub["rental"];
				$this->sub["free"]=$row_sub["free"];
			}
		}
	}

?>