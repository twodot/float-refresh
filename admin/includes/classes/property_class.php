<?php
	/**
	* Keeps track of infomations of the property
	*/
	class Property{
		private $dbc;
		private $property_id;
		private $property;
		private $info_list=array();
		private $property_beacon=false;
		private $location_info_id;
		private $primary_info_id;
		private $changed=false;
		private $flyer;
		private $media;
		
		/**
		* constructs an instance of property with property infos of specified property_id in DB
		* @param int $id Property ID
		* @param mysqli $dbc DB link
		* @return void
		*/
		public function __construct($id, mysqli $dbc){
			$this->property_id=$id;
			$this->dbc=$dbc;
			
			// query-properties
			$query="SELECT properties.*, flyers.id as flyer_id, flyers.name as flyer_name 
						from properties left join flyers on properties.id=flyers.property_id 
						where properties.id=".$this->property_id;
			// $query="SELECT * FROM properties WHERE id=".$this->property_id;
			$res=mysqli_query($this->dbc, $query);
			if($res && $row=mysqli_fetch_assoc($res)){
				// get primary info and location info id
				$q_info="SELECT * FROM property_info WHERE child_of=0 AND properties_id=".$this->property_id." ORDER BY sort_order, id ASC";
				$res_info=mysqli_query($this->dbc, $q_info);
				while($res_info && $row_info=mysqli_fetch_assoc($res_info)){
					if($row_info["primary_info"]){
						$this->primary_info_id=$row_info["id"];
						if($row_info["beacon_id"])
							$this->property_beacon=new Beacon($row_info["beacon_id"], $this->dbc);
					}
					else if($row_info["location"])
						$this->location_info_id=$row_info["id"];
					else
						$this->info_list[$row_info["id"]]=new Property_info($row_info, $this->dbc, $this->primary_info_id);
				}
				// construct info list
				$q_info="SELECT * FROM property_info WHERE child_of=".$this->primary_info_id." AND properties_id=".$this->property_id." ORDER BY sort_order, id ASC";
				$res_info=mysqli_query($this->dbc, $q_info);
				while($res_info && $row_info=mysqli_fetch_assoc($res_info))
					$this->info_list[$row_info["id"]]=new Property_info($row_info, $this->dbc, $this->primary_info_id);
				
				// MEDIA
				
				$q_media = "SELECT * FROM media WHERE deleted = 0 AND property_id = ".$this->property_id . " ORDER BY main_gallery_order";
				$res_media=mysqli_query($this->dbc, $q_media);
				while($res_media && $row_media=mysqli_fetch_assoc($res_media))
					$this->media[] = array('id'=>$row_media['id'], 'name' => $row_media['title']);
				// initialize other vars
				$this->property["user_id"]=(int)$row["user_id"];
				$this->property['primary_info_id'] = (int) $this->primary_info_id;
				$this->property['location_info_id'] = (int) $this->location_info_id;
				$this->property["building_id"]=$row["building_id"];
				$this->property["building_sort"]=$row["building_sort"];
				$this->property["title"]=$row["title"];
				$this->property["description"]=$row["description"];
				$this->property["price"]=$row["price"];
				$this->property["active"]=(int)$row["active"];
				$this->property["views"]=(int)$row["views"];
				$this->property["address"]=$row["address"];
				$this->property["city"]=$row["city"];
				$this->property["state"]=$row["state"];
				$this->property["zip"]=$row["zip"];
				$this->property["latitude"]=$row["latitude"];
				$this->property["longitude"]=$row["longitude"];
				$this->property["status"]=(int)$row["status"];
				$this->property["status_message"]=$row["status_message"];
				$this->property["co_agent"]=(int)$row["co_agent"];
				$this->property["beds"]=$row["beds"];
				$this->property["bath"]=$row["bath"];
				$this->property["sqft"]=$row["sqft"];
				$this->property["rooms"]=$row["rooms"];
				$this->property["lot_size"]=$row["lot_size"];
				$this->property["number_of_floors"]=(int)$row["number_of_floors"];
				$this->property["property_type"]=$row["property_type"];
				$this->property["basement"]=(int)$row["basement"];
				$this->property["parking_type"]=$row["parking_type"];
				$this->property["heating_sources"]=$row["heating_sources"];
				$this->property["heating_system"]=$row["heating_system"];
				$this->property["appliances"]=$row["appliances"];
				$this->property["style"]=$row["style"];
				$this->property["year_built"]=$row["year_built"];
				$this->property["year_updated"]=$row["year_updated"];
				$this->property["community"]=$row["community"];
				$this->property["county"]=$row["county"];
				$this->property["mls"]=$row["mls"];
				$this->property["zillow_disclaimer"]=$row["zillow_disclaimer"];
				$this->property["walkscore"]=(int)$row["walkscore"];
				$this->property["walkscore_link"]=$row["walkscore_link"];
				$this->property["show_walkscore"]=(int)$row["show_walkscore"];
				$this->property["show_greatschools"]=(int)$row["show_greatschools"];
				$this->property["collapse_schools"]=(int)$row["collapse_schools"];
				$this->property["gs_elm"]=$row["gs_elm"];
				$this->property["gs_jh"]=$row["gs_jh"];
				$this->property["gs_hs"]=$row["gs_hs"];
				$this->property["short_url_hash"]=$row["short_url_hash"];
				$this->property["facebook_share"]=$row["facebook_share"];
				$this->property["twitter_share"]=$row["twitter_share"];
				$this->property["updated"]=(int)$row["updated"];
				$this->property["deleted"]=(int)$row["deleted"];
				$this->property["property_data_type"]=$row["property_data_type"];
				$this->property["progress"]=(int)$row["progress"];
				$this->property["completed"]=(int)$row["completed"];
				$this->property["amenities"]=$row["amenities"];
				$this->property["crm"]=$row["crm"];
				$this->property["crm_field_1_value"]=$row["crm_field_1_value"];
				$this->property["crm_field_2_value"]=$row["crm_field_2_value"];
				$this->property["crm_field_3_value"]=$row["crm_field_3_value"];
				$this->property["crm_field_4_value"]=$row["crm_field_4_value"];
				$this->property["crm_field_5_value"]=$row["crm_field_5_value"];
				$this->property["crm_field_6_value"]=$row["crm_field_6_value"];
				$this->property["zpid"]=$row["zpid"];
				$this->flyer["flyer_id"]=(int)$row["flyer_id"];
				$this->flyer["flyer_name"]=$row["flyer_name"];
			} else
				throw new Exception("Property Does Not Exist.");
		}
		
		/**
		* Returns a json encoded string of all infomation of this property
		* @param void
		* @return string A json encoded string of all infomation of this property
		*/
		public function get_json(){
			if($this->is_deleted()){
				$prop["error"]=1;
				$prop["error_message"]="This property has already been deleted";
			} else{
				$prop=$this->property;
				foreach($this->info_list as $value)
					$prop["info_list"][]=$value->get_info_arr();
				$prop["user"]=$prop["user_id"];
				unset($prop["user_id"]);
				$prop["building"]=$prop["building_id"];
				unset($prop["building_id"]);
				// add property beacon
				if($this->property_beacon)
					$prop["property_beacon"]=$this->property_beacon->get_title();
				// add flyer
				$prop["flyer_id"]=$this->flyer["flyer_id"];
				$prop["flyer_name"]=$this->flyer["flyer_name"];
				$prop["error"]=0;
				$prop["error_message"]="";
				// media
				$prop["media"]=$this->media;
			}
			
			return json_encode($prop, JSON_HEX_APOS);
		}
		
		/**
		* Get the properties id of the property
		* @param void
		* @return int $properties_id Properties id of this property
		*/
		public function get_properties_id(){
			return $this->property_id;
		}
		
		/**
		* Get ID of the user to whom the property belongs 
		* @param void
		* @return int $user_id ID of the user to whom the property belongs
		*/
		public function get_user_id(){
			return $this->property["user_id"];
		}
		
		/**
		* Get the building id of the property
		* @param void
		* @return int $building_id Building id of this property
		*/
		public function get_building_id(){
			return $this->property["building_id"];
		}
		
		/**
		* Get the order of buildings listed on the webpage
		* @param void
		* @return int $building_sort The order of buildings listed on the webpage
		*/
		public function get_building_sort(){
			return $this->property["building_sort"];
		}
		
		/**
		* Get the title of the property
		* @param void
		* @return string $title Title of the property
		*/
		public function get_title(){
			return $this->property["title"];
		}
		
		/**
		* Get description of the property
		* @param void
		* @return string $description description of the property
		*/
		public function get_description(){
			return $this->property["description"];
		}
		
		/**
		* Get price of the property
		* @param void
		* @return int $price Price of the property
		*/
		public function get_price(){
			return (int)($this->property["price"]);
		}
		
		/**
		* Returns whether the property is active
		* @param void
		* @return bool $active True if the property is active and False otherwise
		*/
		public function is_active(){
			return $this->property["active"];
		}
		
		/**
		* Returns how many times this property have been viewed in the app
		* @param void
		* @return int $views Number of times this property have been viewed in the app
		*/
		public function get_views(){
			return $this->property["views"];
		}
		
		/**
		* Get the address of the property
		* @param void
		* @return string $address Address of the property
		*/
		public function get_address(){
			return $this->property["address"];
		}
		
		/**
		* Returns the city in which the property is located
		* @param void
		* @return string $city The city in which the property is located
		*/
		public function get_city(){
			return $this->property["city"];
		}
		
		/**
		* Returns the state in which the property is located
		* @param void
		* @return string $state The state in which the property is located
		*/
		public function get_state(){
			return $this->property["state"];
		}
		
		/**
		* Get zip of the property
		* @param void
		* @return int $zip zip of the property
		*/
		public function get_zip(){
			return $this->property["zip"];
		}
		
		/** 
		* Get the latitude of the property as decimal
		* @param void
		* @return float $latitude Latitude of the property as decimal
		*/
		public function get_latitude(){
			return $this->property["latitude"];
		}
		
		/** 
		* Get the longitude of the property as decimal
		* @param void
		* @return float $longitude Longitude of the property as decimal
		*/
		public function get_longitude(){
			return $this->property["longitude"];
		}
		
		/**
		* Get current tag status shown on webpage
		* @param void
		* @return int $status Current tag status shown on webpage
		*/
		public function get_status(){
			return $this->property["status"];
		}
		
		/**
		* Get the status message shown on webpage
		* @param void
		* @return string $status_message Status message shown on webpage
		*/
		public function get_status_message(){
			return $this->property["status_message"];
		}
		
		/**
		* Get the id from the co_agent table
		* @param void
		* @return int $co_agent ID from the co_agent table
		*/
		public function get_co_agent(){
			return $this->property["co_agent"];
		}
		
		/**
		* Get the number of bedrooms in the property
		* @param void
		* @return int $beds Number of bedrooms in the property
		*/
		public function get_beds(){
			return $this->property["beds"];
		}
		
		/**
		* Get the number of bath in the property
		* @param void
		* @return float $bath Number of bath in the property
		*/
		public function get_bath(){
			return $this->property["bath"];
		}
		
		/**
		* Get the area of the property in sqft
		* @param void
		* @return int $sqft Area of the property in sqft
		*/
		public function get_sqft(){
			return $this->property["sqft"];
		}
		
		/**
		* Get the lot size of the property
		* @param void
		* @return int $lot_size Lot size of the property
		*/
		public function get_lot_size(){
			return $this->property["lot_size"];
		}
		
		
		/**
		* Get the rooms of a property
		* @param void
		* @return string $rooms of the property
		*/
		public function get_rooms(){
			return $this->property["rooms"];
		}
		
		
		/**
		* Get the number of floors
		* @param void
		* @return int $number of floors of the property
		*/
		public function get_number_of_floors(){
			return $this->property["number_of_floors"];
		}
		
		/**
		* Get the type of the property input by the user
		* @param void
		* @return string $type Type of this property input by the user
		*/
		public function get_property_type(){
			return $this->property["property_type"];
		}
		
		/**
		* Get if house has a basement
		* @param void
		* @return int $basement of the property
		*/
		public function get_basement(){
			return $this->property["basement"];
		}
		
		/**
		* Get the parking type of the property
		* @param void
		* @return string $parking type of the property
		*/
		public function get_parking_type(){
			return $this->property["parking_type"];
		}
		
		/**
		* Get the heating soursec of the property
		* @param void
		* @return string $heating_sources of the property
		*/
		public function get_heating_sources(){
			return $this->property["heating_sources"];
		}
		
		/**
		* Get the heating system of the property
		* @param void
		* @return string $heating_system of the property
		*/
		public function get_heating_system(){
			return $this->property["heating_system"];
		}
		
		/**
		* Get the appliances included with the property
		* @param void
		* @return string $appliances of the property
		*/
		public function get_appliances(){
			return $this->property["appliances"];
		}
		
		/**
		* Get the year the property was updated
		* @param void
		* @return int $year_updated of the property
		*/
		public function get_year_updated(){
			return $this->property["year_updated"];
		}
		
		/**
		* Get the style of the property
		* @param void
		* @return string $style Style of the property
		*/
		public function get_style(){
			return $this->property["style"];
		}
		
		/**
		* Get age of the property in years
		* @param void
		* @return int $year Age of the property in years in years
		*/
		public function get_year_built(){
			return $this->property["year_built"];
		}
		
		/**
		* Get the community the property is located
		* @param void
		* @return string $community The community the property is located
		*/
		public function get_community(){
			return $this->property["community"];
		}
		
		/**
		* Get the county the property is located
		* @param void
		* @return string $county The county the property is located
		*/
		public function get_county(){
			return $this->property["county"];
		}
		
		/**
		* Get the mls id of the property
		* @param void
		* @return int $mls mls id of the property
		*/
		public function get_mls(){
			return $this->property["mls"];
		}
		
			/**
		* Get if user has zillow_disclaimer
		* @param void
		* @return int $zillow_disclaimer zillow_disclaimer of the property
		*/
		public function get_zillow_disclaimer(){
			return $this->property["zillow_disclaimer"];
		}
		
		
		
		/**
		* Get walkscore of the property
		* @param void
		* @return int $walkscore walkscore of this property
		*/
		public function get_walkscore(){
			return $this->property["walkscore"];
		}
		
		/**
		* Get the link to walkscore for the property
		* @param void
		* @return string $walkscore_link Link to walkscore for the property
		*/
		public function get_walkscore_link(){
			return $this->property["walkscore_link"];
		}
		
		/**
		* Returns whether or not show the walkscore of the property
		* @param void
		* @return bool $show_walkscore True if need to show the walkscore of the property and False otherwise
		*/
		public function show_walkscore(){
			return $this->property["show_walkscore"];
		}
		
		/**
		* Returns whether or not show the great school near the property in app
		* @param void
		* @return bool $show_greatschools True if need to show the great schools near the property and False otherwise
		*/
		public function show_greatschools(){
			return $this->property["show_greatschools"];
		}
		
		/**
		* Returns whether or not collapse the details of great schools near the property in app
		* @param void
		* @return bool $collapse_schools True if need to collapse the details great schools near the property and False otherwise
		*/
		public function collapse_schools(){
			return $this->property["collapse_schools"];
		}
		
		/**
		* Get short url hash of the property
		* @param void
		* @return string $short_url_hash Short url hash of the property
		*/
		public function get_short_url_hash(){
			return $this->property["short_url_hash"];
		}
		
		/**
		* Get the facebook share link of the property
		* @param void
		* @return string $facebook_share Facebook share link of the property
		*/
		public function get_facebook_share(){
			return $this->property["facebook_share"];
		}
		
		/**
		* Get the twitter share link of the property
		* @param void
		* @return string $twitter_share Twitter share link of the property
		*/
		public function get_twitter_share(){
			return $this->property["twitter_share"];
		}
		
		/**
		* Get the timestamp of the last time the property was changed
		* @param void
		* @return string $time_updated Timestamp of the last time the property was changed
		*/
		public function get_time_updated(){
			return $this->property["updated"];
		}
		
		/**
		* Return whether or not the property has been deleted
		* @param void
		* @return bool $deleted True if the property has been deleted and false otherwise
		*/
		public function is_deleted(){
			return $this->property["deleted"];
		}
		
		/**
		* Get the type of the property (property, building, etc.)
		* @param void
		* @return string $property_data_type The type of the property (property, building, etc.)
		*/
		public function get_property_data_type(){
			return $this->property["property_data_type"];
		}
		
		/**
		* Get the progress of tutorial the user is at for this property, range [0, 100]
		* @param void
		* @return int $progress Progress of tutorial, range [0, 100]
		*/
		public function get_progress(){
			return $this->property["progress"];
		}
		
		/**
		* Return whether or not the user has completed the tutorial
		* @param void
		* @return bool $completed True if the user has finished the tutorial and false otherwise
		*/
		public function is_completed(){
			return $this->property["completed"];
		}
		
		/**
		* Get the amenities of the property
		* @param void
		* @return int $amenities Amenities of this property
		*/
		public function get_amenities(){
			return $this->property["amenities"];
		}
		
		/**
		* Get an array of the media 
		* @param void
		* @return array $media of the property
		*/
		public function get_media(){
			return $this->media;
		}
		
		
		/**
		* Get ID of the flyer for the property 
		* @param void
		* @return int $flyer_id ID of the flyer for the property
		*/
		public function get_flyer_id(){
			return $this->flyer["flyer_id"];
		}
		
		/**
		* Get the Name of the flyer for the property 
		* @param void
		* @return string $flyer_name name of the flyer for the property
		*/
		public function get_flyer_name(){
			return $this->flyer["flyer_name"];
		}
		
		/**
		* Get crm id of the property
		* @param void
		* @return int $crm_id crm id of this property
		*/
		public function get_crm(){
			return $this->property["crm"];
		}
		
		/**
		* Get crm field values
		* @param void
		* @return array $crm_field_values{
		* 	@string string crm_field_1_value
		* 	@string string crm_field_2_value
		* 	@string string crm_field_3_value
		* 	@string string crm_field_4_value
		* 	@string string crm_field_5_value
		* 	@string string crm_field_6_value
		* }
		*/
		public function get_crm_field_values(){
			return array(
				$this->property["crm_field_1_value"], 
				$this->property["crm_field_2_value"], 
				$this->property["crm_field_3_value"],
				$this->property["crm_field_4_value"],
				$this->property["crm_field_5_value"],
				$this->property["crm_field_6_value"]);
		}
		
		/**
		* Return whether or not this property has a property beacon
		* @param void
		* @return bool $has_property_beacon True if this property has a property beacon and false otherwise
		*/	
		
		/**
		* Get zillow id id of the property
		* @param void
		* @return int $zpid Zillow id of this property
		*/
		public function get_zpid(){
			return $this->property["zpid"];
		}
			
		public function has_property_beacon(){
			return $this->property_beacon? 1: 0;
		}
		
		/**
		* Returns the property beacon of this property info
		* @param void
		* @return Beacon $property_beacon The property beacon of this property info
		*/	
		public function get_property_beacon(){
			return $this->property_beacon;
		}
		
		/**
		* Get all root property infos (child_of==0) of the property as a list
		* @param void
		* @return array $info_list{
		*	@Property_info Property_info
		*	...
		* }
		*/
		public function get_info_list(){
			return $this->info_list;
		}
		
		/**
		* Get the id of location info of this property
		* @param void
		* @return int $location_info_id ID of location info of this property
		*/
		public function get_location_info_id(){
			return $this->location_info_id;
		}
		
		/**
		* Get the id of the primary info of this property
		* @param void
		* @return int $primary_info_id ID of the primary info of this property
		*/
		public function get_primary_info_id(){
			return $this->primary_info_id;
		}
		
		// modifier
		/**
		* Changes the title of the property
		* @param string $title Property title 
		* @return void
		*/
		public function set_title($title){
			if(is_string($title)){
				if($this->property["title"]!=$title){
					$this->property["title"]=$title;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the description of the property
		* @param string $description Property description 
		* @return void
		*/
		public function set_description($description){
			if(is_string($description)){
				if($this->property["description"]!=$description){
					$this->property["description"]=$description;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the price of the property
		* @param string $price Property price, string of a number required
		* @return void
		*/
		public function set_price($price){
			if(is_numeric($price)){
				if($this->property["price"]!=$price){
					$this->property["price"]=$price;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Activate the property
		* @param void
		* @return void
		*/
		public function activate(){
			if($this->property["active"]!=1){
				$this->property["active"]=1;
				$this->changed=true;
			}
		}
		
		/**
		* Deactivate the property
		* @param void
		* @return void
		*/
		public function deactivate(){
			if($this->property["active"]!=0){
				$this->property["active"]=0;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the address of the property
		* @param string $address Property address 
		* @return void
		*/
		public function set_address($address){
			if(is_string($address)){
				if($this->property["address"]!=$address){
					$this->property["address"]=$address;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		
		/**
		* Changes the city in which the property is located
		* @param string $city Scity in which the property is located 
		* @return void
		*/
		public function set_city($city){
			if(is_string($city)){
				if($this->property["city"]!=$city){
					$this->property["city"]=$city;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		
		
		/**
		* Changes the state in which the property is located
		* @param string $state State in which the property is located 
		* @return void
		*/
		public function set_state($state){
			if(is_string($state)){
				if($this->property["state"]!=$state){
					$this->property["state"]=$state;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the zip of the property
		* @param int $zip zip of the property 
		* @return void
		*/
		public function set_zip($zip){
			$zip=(int)$zip;
			if($this->property["zip"]!=$zip){
				$this->property["zip"]=$zip;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the latitude of the address of property
		* Assume $latitude have already been trimed if it is a string
		* @param mixed $latitude Latitude of the property address , as float or string
		* @return void
		*/
		public function set_latitude($latitude){
			if(is_numeric($latitude)){
				if($this->property["latitude"]!=$latitude){
					$this->property["latitude"]=$latitude;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the longitude of the address of property
		* Assume $longitude have already been trimed if it is a string
		* @param mixed $longitude Longitude of the property address , as float or string
		* @return void
		*/
		public function set_longitude($longitude){
			if(is_numeric($longitude)){
				if($this->property["longitude"]!=$longitude){
					$this->property["longitude"]=$longitude;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the current tag status shown on webpage
		* @param int $status Current tag status 
		* @return void
		*/
		public function set_status($status){
			$status=(int)$status;
			if($this->property["status"]!=$status){
				$this->property["status"]=$status;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the status message shown on webpage
		* @param string $status_message Status message 
		* @return void
		*/
		public function set_status_message($status_message){
			if(is_string($status_message)){
				if($this->property["status_message"]!=$status_message){
					$this->property["status_message"]=$status_message;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes co_agent id in the co_agent table of this property
		* @param int $co_agent ID in the co_agent table 
		* @return void
		*/
		public function set_co_agent($co_agent){
			$co_agent=(int)$co_agent;
			if($this->property["co_agent"]!=$co_agent){
				$this->property["co_agent"]=$co_agent;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the number of bedrooms in the property
		* @param int $beds The number of bedrooms in the property 
		* @return void
		*/
		public function set_beds($beds){
			$beds=(int)$beds;
			if($this->property["beds"]!=$beds){
				$this->property["beds"]=$beds;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the number of bath in the property
		* @param float $bath The number of bath in the property 
		* @return void
		*/
		public function set_bath($bath){
			$bath=floatval($bath);
			if($this->property["bath"]!=$bath){
				$this->property["bath"]=$bath;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the area of the property in sqft
		* @param int $title Area of the property in sqft 
		* @return void
		*/
		public function set_sqft($sqft){
			$sqft=(int)$sqft;
			if($this->property["sqft"]!=$sqft){
				$this->property["sqft"]=$sqft;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the rooms of the property
		* @param mixed $rooms The rooms of the property
		* @return void
		*/
		public function set_rooms($rooms){
			
			if(is_string($rooms)){
				if($this->property["rooms"]!=$rooms){
					$this->property["rooms"]=$rooms;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the number_of_floors of the property
		* @param mixed $number_of_floors The number_of_floors of the property
		* @return void
		*/
		public function set_number_of_floors($number_of_floors){
			$number_of_floors = (int) $number_of_floors;
			if($this->property["number_of_floors"]!=$number_of_floors){
				$this->property["number_of_floors"]=$number_of_floors;
				$this->changed=true;
			}
	
		}
		
		/**
		* Changes the basement of the property
		* @param mixed $basement The basement of the property
		* @return void
		*/
		public function set_basement($basement){
			$basement = (int) $basement;
			if($this->property["basement"]!=$basement){
				$this->property["basement"]=$basement;
				$this->changed=true;
			}
	
		}
		
		
		// Assume $parking_type have already been trimed if it is a string
		/**
		* Changes the parking_type of the property
		* @param mixed $parking_type The parking_type of the property
		* @return void
		*/
		public function set_parking_type($parking_type){
			if(is_string($parking_type) || is_null($parking_type)){
				if($this->property["parking_type"]!=$parking_type){
					$this->property["parking_type"]=$parking_type;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		
		// Assume $heating_sources have already been trimed if it is a string
		/**
		* Changes the heating_sources of the property
		* @param mixed $heating_sources The heating_sources of the property
		* @return void
		*/
		public function set_heating_sources($heating_sources){
			if(is_string($heating_sources) || is_null($heating_sources)){
				if($this->property["heating_sources"]!=$heating_sources){
					$this->property["heating_sources"]=$heating_sources;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		
		// Assume $heating_system have already been trimed if it is a string
		/**
		* Changes the heating_system of the property
		* @param mixed $heating_system The heating_system of the property
		* @return void
		*/
		public function set_heating_system($heating_system){
			if(is_string($heating_system) || is_null($heating_system)){
				if($this->property["heating_system"]!=$heating_system){
					$this->property["heating_system"]=$heating_system;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		
		// Assume $appliances have already been trimed if it is a string
		/**
		* Changes the appliances of the property
		* @param mixed $appliances The appliances of the property
		* @return void
		*/
		public function set_appliances($appliances){
			if(is_string($appliances) || is_null($appliances)){
				if($this->property["appliances"]!=$appliances){
					$this->property["appliances"]=$appliances;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		
		// Assume $year_updated have already been trimed if it is a string
		/**
		* Changes the year_updated of the property
		* @param mixed $year_updated The year_updated of the property
		* @return void
		*/
		public function set_year_updated($year_updated){
			$year_updated = (int) $year_updated;
			if($this->property["year_updated"]!=$year_updated){
				$this->property["year_updated"]=$year_updated;
				$this->changed=true;
			}
		}
		
		// Assume $lot_size have already been trimed if it is a string
		/**
		* Changes the lot size the property
		* @param mixed $lot_size The lot size in the property
		* @return void
		*/
		public function set_lot_size($lot_size){
			if(is_string($lot_size) || is_null($lot_size)){
				if($this->property["lot_size"]!=$lot_size){
					$this->property["lot_size"]=$lot_size;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the type of the property input by the user
		* @param string $property_type The type of the property input by user
		* @return void
		*/
		public function set_property_type($property_type){
			if(is_string($property_type)){
				if($this->property["property_type"]!=$property_type){
					$this->property["property_type"]=$property_type;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the style of the property
		* @param string $style Property style 
		* @return void
		*/
		public function set_style($style){
			if(is_string($style)){
				if($this->property["style"]!=$style){
					$this->property["style"]=$style;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the age of the property in years
		* @param string $year_built The age of the property 
		* @return void
		*/
		public function set_year_built($year_built){
			$year_built=(int)$year_built;
			if($this->property["year_built"]!=$year_built){
				$this->property["year_built"]=$year_built;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the community where the property is located
		* @param string $community The community where the property is located
		* @return void
		*/
		public function set_community($community){
			if(is_string($community)){
				if($this->property["community"]!=$community){
					$this->property["community"]=$community;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the county where the property is located
		* @param string $county The county where the property is located
		* @return void
		*/
		public function set_county($county){
			if(is_string($county)){
				if($this->property["county"]!=$county){
					$this->property["county"]=$county;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the mls id of the property
		* @param int $mls The mls id of the property 
		* @return void
		*/
		public function set_mls($mls){
			$mls=(int)$mls;
			if($this->property["mls"]!=$mls){
				$this->property["mls"]=$mls;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the zillow_disclaimer of the property
		* @param int $zillow_disclaimer if user has zillow_disclaimer
		* @return void
		*/
		public function set_zillow_disclaimer($zillow_disclaimer){
			if($zillow_disclaimer == 'true') {
				$zillow_disclaimer = (int)1;
			}
			else {
				$zillow_disclaimer = (int)0;
			}
			if($this->property["zillow_disclaimer"]!=$zillow_disclaimer){
				$this->property["zillow_disclaimer"]=$zillow_disclaimer;
				$this->changed=true;
			}
		}
		
		
		
		/**
		* Changes the walk score of the property
		* @param int $walkscore The walk score of the property
		* @return void
		*/
		public function set_walkscore($walkscore){
			$walkscore=(int)$walkscore;
			if($this->property["walkscore"]!=$walkscore){
				$this->property["walkscore"]=$walkscore;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the link to walkscore of this property
		* @param string $walkscore_link The link to walkscore of this property
		* @return void
		*/
		public function set_walkscore_link($walkscore_link){
			if(is_string($walkscore_link)){
				if($this->property["walkscore_link"]!=$walkscore_link){
					$this->property["walkscore_link"]=$walkscore_link;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* To show the walkscore of the property on the webpage
		* Changes show_walkscore column in DB to 1
		* @param void
		* @return void
		*/
		public function set_show_walkscore(){
			if($this->property["show_walkscore"]!=1){
				$this->property["show_walkscore"]=1;
				$this->changed=true;
			}
		}
		
		/**
		* To hide the walkscore of the property on the webpage
		* Changes show_walkscore column in DB to 0
		* @param void
		* @return void
		*/
		public function unset_show_walkscore(){
			if($this->property["show_walkscore"]!=0){
				$this->property["show_walkscore"]=0;
				$this->changed=true;
			}
		}
		
		/**
		* To show the great schools near the property on the webpage
		* Changes show_greatschools column in DB to 1
		* @param void
		* @return void
		*/
		public function set_show_greatschools(){
			if($this->property["show_greatschools"]!=1){
				$this->property["show_greatschools"]=1;
				$this->changed=true;
			}
		}
		
		/**
		* To hide the great schools near the property on the webpage
		* Changes show_greatschools column in DB to 0
		* @param void
		* @return void
		*/
		public function unset_show_greatschools(){
			if($this->property["show_greatschools"]!=0){
				$this->property["show_greatschools"]=0;
				$this->changed=true;
			}
		}
		
		/**
		* To collapse details of the great schools near the property on the webpage
		* Changes collapse_schools column in DB to 1
		* @param void
		* @return void
		*/
		public function set_collapse_schools(){
			if($this->property["collapse_schools"]!=1){
				$this->property["collapse_schools"]=1;
				$this->changed=true;
			}
		}
		
		/**
		* To expand details of the great schools near the property on the webpage
		* Changes collapse_schools column in DB to 0
		* @param void
		* @return void
		*/
		public function unset_collapse_schools(){
			if($this->property["collapse_schools"]!=0){
				$this->property["collapse_schools"]=0;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the short url hash value of the property
		* @param string $short_url_hash The short url hash value of the property
		* @return void
		*/
		public function set_short_url_hash($short_url_hash){
			if(is_string($short_url_hash)){
				if($this->property["short_url_hash"]!=$short_url_hash){
					$this->property["short_url_hash"]=$short_url_hash;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the facebook share link of the property
		* @param string $facebook_share Facebook share link of the property
		* @return void
		*/
		public function set_facebook_share($facebook_share){
			if(is_string($facebook_share)){
				if($this->property["facebook_share"]!=$facebook_share){
					$this->property["facebook_share"]=$facebook_share;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the twitter share link of the property
		* @param string $twitter_share Twitter share link of the property
		* @return void
		*/
		public function set_twitter_share($twitter_share){
			if(is_string($twitter_share)){
				if($this->property["twitter_share"]!=$twitter_share){
					$this->property["twitter_share"]=$twitter_share;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Marks this property as deleted
		* @param void
		* @return void
		*/
		public function set_deleted(){
			if($this->property["deleted"]!=1){
				$this->property["deleted"]=1;
				$this->changed=true;
			}
		}
		
		/**
		* Unsets the deleted mark of the property
		* @param void
		* @return void
		*/
		public function unset_deleted(){
			if($this->property["deleted"]!=0){
				$this->property["deleted"]=0;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the type of the property
		* @param string $property_data_type Type of the property
		* @return void
		*/
		public function set_property_data_type($property_data_type){
			if(is_string($property_data_type)){
				if($this->property["property_data_type"]!=$property_data_type){
					$this->property["property_data_type"]=$property_data_type;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the progress of tutorial the user is at for this property, range [0, 100]
		* @param int $progress Progress of tutorial, range [0, 100]
		* @return void
		*/
		public function set_progress($progress){
			$progress=(int)$progress;
			if($this->property["progress"]!=$progress){
				$this->property["progress"]=$progress;
				$this->changed=true;
			}
		}
		
		/**
		* Mark the user as has completed the tutorial
		* Changes column completed to 1
		* @param void
		* @return void
		*/
		public function set_completed(){
			if($this->property["completed"]!=1){
				$this->property["completed"]=1;
				$this->changed=true;
			}
		}
		
		/**
		* Unmark the user from having completed the tutorial
		* Changes column completed to 0
		* @param void
		* @return void
		*/
		public function unset_completed(){
			if($this->property["completed"]!=0){
				$this->property["completed"]=0;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the amenities of the property
		* @param int $amenities Amenities of the property 
		* @return void
		*/
		public function set_amenities($amenities){
			if(!$amenities)
				$amenities=(int)$amenities;
			if($this->property["amenities"]!=$amenities){
				$this->property["amenities"]=$amenities;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the ID of the flyer for the property
		* @param int $flyer_id the name of the flyer for the property
		* @return void
		*/
		public function set_flyer_id($flyer_id){
			$flyer_id = (int) $flyer_id;
			if($this->flyer["flyer_id"]!=$flyer_id){
				$this->flyer["flyer_id"]=$flyer_id;
				$this->changed=true;
			}
		}
		
		
		/**
		* Changes the name of the flyer for the property
		* @param string $flyer_name the name of the flyer for the property
		* @return void
		*/
		public function set_flyer_name($flyer_name){
			if(is_string($sflyer_name)){
				if($this->flyer["flyer_name"]!=$flyer_name){
					$this->flyer["flyer_name"]=$flyer_name;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the crm id of the property
		* @param int $crm The crm id of the property
		* @return void
		*/
		public function set_crm($crm){
			$crm=(int)$crm;
			if($this->property["crm"]!=$crm){
				$this->property["crm"]=$crm;
				$this->changed=true;
			}
		}
		
		/**
		* Change values in crm fields of the property
		* @param array $crm_field_values{
		* 	@string string crm_field_1_value
		* 	@string string crm_field_2_value
		* 	@string string crm_field_3_value
		* 	@string string crm_field_4_value
		* 	@string string crm_field_5_value
		* 	@string string crm_field_6_value
		* } Values in crm fields of the property as an array
		* @return void
		*/
		public function set_crm_field_values(array $crm_field_values){
				if($this->property["crm_field_1_value"]!=$crm_field_values["crm_field_1_value"] ||
				$this->property["crm_field_2_value"]!=$crm_field_values["crm_field_2_value"] ||
				$this->property["crm_field_3_value"]!=$crm_field_values["crm_field_3_value"] ||
				$this->property["crm_field_4_value"]!=$crm_field_values["crm_field_4_value"] ||
				$this->property["crm_field_5_value"]!=$crm_field_values["crm_field_5_value"] ||
				$this->property["crm_field_6_value"]!=$crm_field_values["crm_field_6_value"])
				{
					$this->property["crm_field_1_value"]=$crm_field_values["crm_field_1_value"];
					$this->property["crm_field_2_value"]=$crm_field_values["crm_field_2_value"];
					$this->property["crm_field_3_value"]=$crm_field_values["crm_field_3_value"];
					$this->property["crm_field_4_value"]=$crm_field_values["crm_field_4_value"];
					$this->property["crm_field_5_value"]=$crm_field_values["crm_field_5_value"];
					$this->property["crm_field_6_value"]=$crm_field_values["crm_field_6_value"];
					$this->changed=true;
				}
		}
		
		/**
		* Changes value in crm field 1 of the property
		* @param string $crm_field_1_value Value in crm field 1 of the property
		* @return void
		*/
		public function set_crm_field_1_value($crm_field_1_value){
			if(!$crm_field_1_value || is_string($crm_field_1_value)){
				if($this->property["crm_field_1_value"]!=$crm_field_1_value){
					$this->property["crm_field_1_value"]=$crm_field_1_value;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes value in crm field 2 of the property
		* @param string $crm_field_2_value Value in crm field 2 of the property
		* @return void
		*/
		public function set_crm_field_2_value($crm_field_2_value){
			if(!$crm_field_2_value || is_string($crm_field_2_value)){
				if($this->property["crm_field_2_value"]!=$crm_field_2_value){
					$this->property["crm_field_2_value"]=$crm_field_2_value;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes value in crm field 3 of the property
		* @param string $crm_field_3_value Value in crm field 3 of the property
		* @return void
		*/
		public function set_crm_field_3_value($crm_field_3_value){
			if(!$crm_field_3_value || is_string($crm_field_3_value)){
				if($this->property["crm_field_3_value"]!=$crm_field_3_value){
					$this->property["crm_field_3_value"]=$crm_field_3_value;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes value in crm field 4 of the property
		* @param string $crm_field_4_value Value in crm field 4 of the property
		* @return void
		*/
		public function set_crm_field_4_value($crm_field_4_value){
			if(!$crm_field_4_value || is_string($crm_field_4_value)){
				if($this->property["crm_field_4_value"]!=$crm_field_4_value){
					$this->property["crm_field_4_value"]=$crm_field_4_value;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes value in crm field 5 of the property
		* @param string $crm_field_5_value Value in crm field 5 of the property
		* @return void
		*/
		public function set_crm_field_5_value($crm_field_5_value){
			if(!$crm_field_5_value || is_string($crm_field_5_value)){
				if($this->property["crm_field_5_value"]!=$crm_field_5_value){
					$this->property["crm_field_5_value"]=$crm_field_5_value;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes value in crm field 6 of the property
		* @param string $crm_field_6_value Value in crm field 6 of the property
		* @return void
		*/
		public function set_crm_field_6_value($crm_field_6_value){
			if(!$crm_field_6_value || is_string($crm_field_6_value)){
				if($this->property["crm_field_6_value"]!=$crm_field_6_value){
					$this->property["crm_field_6_value"]=$crm_field_6_value;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the zpid id of the property
		* @param int $zpid The Zillow id of the property
		* @return void
		*/
		public function set_zpid($zpid){
			$zpid=(int)$zpid;
			if($this->property["zpid"]!=$zpid){
				$this->property["zpid"]=$zpid;
				$this->changed=true;
			}
		}
		
		// ADD REMOVE
		/**
		* Add a property (root) info to this property
		* Changes both the database and the info list if success
		* @param array{
		* 	@int int id
		* 	@int int properties_id
		* 	@int int building_id
		* 	@int int beacon_id
		* 	@string string title
		* 	@string string text
		* 	@string string notes
		* 	@int int child_of
		* 	@int int primary_info
		* 	@int int location
		* 	@int int sort_order
		* 	@string string serial_num
		* 	@array array media
		* 	@bool bool link_beacon
		* 	@string string link_beacon_text
		* 	@string string link_beacon_link
		* 	@bool bool has_360_image
		* 	@string string link_360_image
		* } Reference to a array containing all the columns of the info to be added
		* @return string Empty string if success and error message if not
		*/
		public function add_property_info(array &$info){
			// remove info id, because of auto increment
			unset($info["id"]);
			
			// check properties id
			if($info["properties_id"] != $this->property_id)
				return "Failed to add info: properties id does not match.";
			
			// check parent
			if($info["child_of"] !==$this->primary_info_id)
				return "Failed to add info: is not a root info.";
			
			// add to DB
			$msg="";
			// update table property_info
			$q_info='INSERT INTO property_info';
			$q_key='';
			$q_value='';
			foreach($info as $key=>$value){
				$q_key=$q_key.", ".$key;
				$q_value=$q_value.", ?";
			}
			$q_key=" (".substr($q_key, 2).") ";
			$q_value=" (".substr($q_value, 2).") ";
			$q_info=$q_info.$q_key."VALUES".$q_value;
			
			// query
			$stmt = mysqli_prepare($this->dbc, $q_info);
			$temp_param=array(str_repeat("s", count($info)));
			foreach($info as $key=>$value)
				$temp_param[]=&$info[$key];
			
			call_user_func_array(array($stmt, "bind_param"), $temp_param);
			// execute prepared statement
			if(!mysqli_stmt_execute($stmt))
				$msg.="Insert into table property_info failed: ".mysqli_stmt_error($stmt);
			else
				// add to subinfo list only if inserted successfully
				$this->info_list[$info["id"]]=new Property_info($info, $this->dbc, $this->primary_info_id);
			
			// close statement and connection
			mysqli_stmt_close($stmt);			
			return $msg;
		}
		
		/**
		* Remove the property info specified by the info id from both the info list and the DB
		* @param int $info_id ID of the property info to be removed. 
		* @return string Empty string if success and error message if not
		*/
		public function remove_property_info($info_id){
			$msg='';
			// remove from DB
			$info_id=(int)$info_id;
			// check if the info exists in info list
			if($this->info_list[$info_id]){
				// checks if the info exists in DB
				$q_info="SELECT * FROM property_info WHERE id=".$info_id;
				$res_info=mysqli_query($this->dbc, $q_info);
				if(mysqli_num_rows($res_info)){
					$q_info="DELETE FROM property_info WHERE id=".$info_id;
					if(mysqli_query($this->dbc, $q_info))
						// remove from subinfo list
						unset($this->info_list[$info_id]);
					else
						$msg="Remove Failed: ".mysqli_error($this->dbc);
				} else
					// remove from info list if info does not exists in DB
					unset($this->info_list[$info_id]);
			} else
				$msg="Info Does Not Exists Or Is Not A Info Of This Property.";
			return $msg;
		}
		
		/**
		* Attaches a beacon to this property as a property beacon
		* @param Beacon $new_beacon The beacon to be attached to this property
		* @return string Empty string if success and error message if not
		*/
		public function attach_property_beacon(Beacon $new_beacon){
			$msg='';
			if($this->property_beacon)
				$mst="Attaching property beacon failed: This property already has one property beacon";
			else{
				$q_beacon="UPDATE beacons SET beacon_type='".$this->property["property_data_type"]."' property_info_id=".$this->primary_info_id.
							" WHERE id=".$new_beacon->get_id();
				if(!mysqli_query($this->dbc, $q_beacon))
					$msg="Attaching property beacon failed: ".mysqli_error($this->dbc);
				else{
					$q_info="UPDATE property_info SET beacon_id=".$new_beacon->get_id()." WHERE id=".$this->primary_info_id;
					if(!mysql_query($this->dbc))
						$msg="Attaching property beacon failed: ".mysqli_error($this->dbc);
					// changes this property only if all of the queries succeed. 
					else
						$this->property_beacon=$new_beacon;
				}
			}
			return $msg;
		}
		
		/**
		* Detaches the property beacon of this property
		* @param void
		* @return string Empty string is success and error messgae of not
		*/
		public function detach_property_beacon(){
			$msg='';
			if($this->property_beacon){
				$q_beacon='UPDATE beacons SET property_info_id=0, beacon_type="" WHERE id='.$this->property_beacon->get_id();
				if(!mysqli_query($this->dbc, $q_beacon))
					$msg="Detaching property beacon failed: ".mysqli_error($this->dbc);
				else{
					$q_info="UPDATE property_info SET beacon_id=0 WHERE beacon_id=".$this->property_beacon->get_id();
					if(!mysqli_query($this->dbc, $q_info))
						$msg="Detaching property beacon failed: ".mysqli_error($this->dbc);
					// changes this property only if all of the queries succeed. 
					else
						unset($this->property_beacon);
				}
			} else
				$msg="Detaching property beacon failed: This property does not have a beacon attached with it.";
			return $msg;
		}
		
		// DB Operations
		/**
		* Update only this property in DB. Will update the DB only if anything field changed.
		* @param void
		* @return string $msg Empty string if success, error message if fails
		*/
		public function update_property_DB(){
			// store time
			date_default_timezone_set('US/Pacific');
			$temp_time=$this->property["updated"];
			
			$msg="";
			if($this->changed){
				// Delete updated
				unset($this->property["updated"]);
				// update table properties
				$query='UPDATE properties SET ';
				$q_temp='';
				foreach($this->property as $key=>$value)
					//$q_temp=$q_temp.", ".$key."='".$value."'";
					$q_temp=$q_temp.", ".$key."=?";
				$q_temp=substr($q_temp, 2);
				$query=$query.$q_temp." WHERE id=".$this->get_properties_id();
				
				// query
				$stmt = mysqli_prepare($this->dbc, $query);
				$temp_param=array(str_repeat("s", count($this->property)));
				foreach($this->property as $key=>$value)
					$temp_param[]=&$this->property[$key];
				
				call_user_func_array(array($stmt, "bind_param"), $temp_param);
				// execute prepared statement and restore timestamp
				if(!mysqli_stmt_execute($stmt)){
					$msg.="Update table property_info failed: ".mysqli_stmt_error($stmt);
					$this->property["updated"]=$temp_time;
				} else
					$this->property["updated"]=date('Y-m-d H:i:s');
				// close statement and connection
				mysqli_stmt_close($stmt);
			}
			return $msg;
		}
		
		/**
		* Update this property with all its perperty info in DB. Will update the DB only if anything field changed.
		* @param void
		* @return string $msg Empty string if success, error message if fails
		*/
		public function update_DB(){
			// store time
			date_default_timezone_set('US/Pacific');
			$temp_time=$this->property["updated"];
			
			$msg="";
			
			// update info list
			// subinfo also updated when update_DB() is called
			foreach($this->info_list as $value)
				$msg.=$value->update_DB();
			
			// update this property
			if($this->changed){
				// Delete updated
				unset($this->property["updated"]);
				// update table properties
				$query='UPDATE properties SET ';
				$q_temp='';
				foreach($this->property as $key=>$value)
					//$q_temp=$q_temp.", ".$key."='".$value."'";
					$q_temp=$q_temp.", ".$key."=?";
				$q_temp=substr($q_temp, 2);
				$query=$query.$q_temp." WHERE id=".$this->get_properties_id();
				
				// query
				$stmt = mysqli_prepare($this->dbc, $query);
				$temp_param=array(str_repeat("s", count($this->property)));
				foreach($this->property as $key=>$value)
					$temp_param[]=&$this->property[$key];
				
				call_user_func_array(array($stmt, "bind_param"), $temp_param);
				// execute prepared statement and restore timestamp
				if(!mysqli_stmt_execute($stmt)){
					$msg.="Update table property_info failed: ".mysqli_stmt_error($stmt);
					$this->property["updated"]=$temp_time;
				} else
					$this->property["updated"]=date('Y-m-d H:i:s');
				// close statement and connection
				mysqli_stmt_close($stmt);
			}
			return $msg;
		}
		
		/**
		* Reload info in DB to object
		* @param void
		* @return void
		*/
		public function reload_DB(){
			// clear the info list
			if($this->info_list)
				unset($this->info_list);
			$this->info_list=array();
			// query-properties
			$query="properties.*, flyers.id as flyer_id, flyers.name as flyer_name 
						from properties left join flyers on properties.id=flyers.property_id 
						where properties.id=".$this->property_id;
			$res=mysqli_query($this->dbc, $query);
			if($res && $row=mysqli_fetch_assoc($res)){
				// get primary info and location info id
				$q_info="SELECT * FROM property_info WHERE child_of=0 AND properties_id=".$this->property_id." ORDER BY sort_order, id ASC";
				$res_info=mysqli_query($this->dbc, $q_info);
				while($res_info && $row_info=mysqli_fetch_assoc($res_info)){
					if($row_info["primary_info"]){
						$this->primary_info_id=$row_info["id"];
						if($row_info["beacon_id"])
							$this->property_beacon=true;
					}
					else if($row_info["location"])
						$this->location_info_id=$row_info["id"];
					else
						$this->info_list[$row_info["id"]]=new Property_info($row_info, $this->dbc, $this->primary_info_id);
				}
				// construct info list
				$q_info="SELECT * FROM property_info WHERE child_of=".$this->primary_info_id." AND properties_id=".$this->property_id." ORDER BY sort_order, id ASC";
				$res_info=mysqli_query($this->dbc, $q_info);
				while($res_info && $row_info=mysqli_fetch_assoc($res_info))
					$this->info_list[$row_info["id"]]=new Property_info($row_info, $this->dbc, $this->primary_info_id);
				// MEDIA
				
				$q_media = "SELECT * FROM media WHERE deleted = 0 AND property_id = ".$this->property_id . " ORDER BY main_gallery_order";
				$res_media=mysqli_query($this->dbc, $q_media);
				while($res_media && $row_media=mysqli_fetch_assoc($res_media))
					$this->media[] = array('id'=>$row_media['id'], 'name' => $row_media['title']);
				// initialize other vars
				$this->property["user_id"]=$row["user_id"];
				$this->property["building_id"]=$row["building_id"];
				$this->property["building_sort"]=$row["building_sort"];
				$this->property["title"]=$row["title"];
				$this->property["description"]=$row["description"];
				$this->property["price"]=$row["price"];
				$this->property["active"]=$row["active"];
				$this->property["views"]=$row["views"];
				$this->property["address"]=$row["address"];
				$this->property["city"]=$row["city"];
				$this->property["state"]=$row["state"];
				$this->property["zip"]=$row["zip"];
				$this->property["latitude"]=$row["latitude"];
				$this->property["longitude"]=$row["longitude"];
				$this->property["status"]=$row["status"];
				$this->property["status_message"]=$row["status_message"];
				$this->property["co_agent"]=$row["co_agent"];
				$this->property["beds"]=$row["beds"];
				$this->property["bath"]=$row["bath"];
				$this->property["sqft"]=$row["sqft"];
				$this->property["rooms"]=$row["rooms"];
				$this->property["lot_size"]=$row["lot_size"];
				$this->property["number_of_floors"]=$row["number_of_floors"];
				$this->property["property_type"]=$row["property_type"];
				$this->property["basement"]=$row["basement"];
				$this->property["parking_type"]=$row["parking_type"];
				$this->property["heating_sources"]=$row["heating_sources"];
				$this->property["heating_system"]=$row["heating_system"];
				$this->property["appliances"]=$row["appliances"];
				$this->property["style"]=$row["style"];
				$this->property["year_built"]=$row["year_built"];
				$this->property["year_updated"]=$row["year_updated"];
				$this->property["community"]=$row["community"];
				$this->property["county"]=$row["county"];
				$this->property["mls"]=$row["mls"];
				$this->property["zillow_disclaimer"]=$row["zillow_disclaimer"];
				$this->property["walkscore"]=$row["walkscore"];
				$this->property["walkscore_link"]=$row["walkscore_link"];
				$this->property["show_walkscore"]=$row["show_walkscore"];
				$this->property["show_greatschools"]=$row["show_greatschools"];
				$this->property["collapse_schools"]=$row["collapse_schools"];
				$this->property["gs_elm"]=$row["gs_elm"];
				$this->property["gs_jh"]=$row["gs_jh"];
				$this->property["gs_hs"]=$row["gs_hs"];
				$this->property["short_url_hash"]=$row["short_url_hash"];
				$this->property["facebook_share"]=$row["facebook_share"];
				$this->property["twitter_share"]=$row["twitter_share"];
				$this->property["updated"]=$row["updated"];
				$this->property["deleted"]=$row["deleted"];
				$this->property["property_data_type"]=$row["property_data_type"];
				$this->property["progress"]=$row["progress"];
				$this->property["completed"]=$row["completed"];
				$this->property["amenities"]=$row["amenities"];
				$this->property["crm"]=$row["crm"];
				$this->property["crm_field_1_value"]=$row["crm_field_1_value"];
				$this->property["crm_field_2_value"]=$row["crm_field_2_value"];
				$this->property["crm_field_3_value"]=$row["crm_field_3_value"];
				$this->property["crm_field_4_value"]=$row["crm_field_4_value"];
				$this->property["crm_field_5_value"]=$row["crm_field_5_value"];
				$this->property["crm_field_6_value"]=$row["crm_field_6_value"];
				$this->property["zpid"]=$row["zpid"];
				$this->flyer["flyer_id"]=(int)$row["flyer_id"];
				$this->flyer["flyer_name"]=$row["flyer_name"];
			} else
				throw new Exception("Property Does Not Exist.");
		}
	}
	
	/**
	* Keeps track of property infos
	*/
	class Property_info{
		private $dbc;
		private $info_id;
		private $primary_info_id;
		private $info;
		private $beacon;
		private $subinfo_list=null;
		private $is_subinfo=true;
		private $changed=false;
		
		/**
		* constructs an instance of property_info with its subinfos if any of specified property_info id in DB
		* @param array{
		* 	@int int id
		* 	@int int properties_id
		* 	@int int building_id
		* 	@int int beacon_id
		* 	@string string title
		* 	@string string text
		* 	@string string notes
		* 	@int int child_of
		* 	@int int sort_order
		* 	@string string serial_num
		* 	@array array media
		* 	@bool bool link_beacon
		* 	@string string link_beacon_text
		* 	@string string link_beacon_link
		* 	@bool bool has_360_image
		* 	@string string link_360_image
		* }Reference to the returned array from mysqli_fetch_assoc() upon query to all columns of a row
		* @param mysqli $dbc DB link
		* @param int $primary ID of the primary info
		* @return void
		*/
		public function __construct(array &$info, $dbc, $primary){
			$this->primary_info_id=$primary;
			$this->info_id=$info["id"];
			$this->dbc=$dbc;
			
			// construct subinfo list
			
			if($info["child_of"]==$this->primary_info_id){
				$this->is_subinfo=false;
				$this->subinfo_list=array();
				
				$q_info="SELECT * FROM property_info WHERE child_of=".$this->info_id." ORDER BY sort_order, id ASC";
				$res_info=mysqli_query($this->dbc, $q_info);
				while($res_info && $row_info=mysqli_fetch_assoc($res_info))
					$this->subinfo_list[$row_info["id"]]=new Property_info($row_info, $this->dbc, $this->primary_info_id);
			}
			
			// initialize other vars
			$this->info["properties_id"]=$info["properties_id"];
			$this->info["building_id"]=$info["building_id"];
			$this->info["beacon_id"]=$info["beacon_id"];
			if($info["beacon_id"])
				$this->beacon=new Beacon($info["beacon_id"], $this->dbc);
			$this->info["title"]=$info["title"];
			$this->info["text"]=$info["text"];
			$this->info["notes"]=$info["notes"];
			$this->info["child_of"]=$info["child_of"];
			$this->info["location"]=$info["location"];
			$this->info["sort_order"]=$info["sort_order"];
			$this->info["serial_num"]=$info["serial_num"];
			if(!empty($info["media"])) {
				$this->info["media"]=preg_split("/(, |,)/", str_replace(' ', '', $info["media"]));
			}
			else {
				$this->info["media"] = NULL;
			}
			$this->info["link_beacon"]=(int)$info["link_beacon"];
			$this->info["link_beacon_text"]=$info["link_beacon_text"];
			$this->info["link_beacon_link"]=$info["link_beacon_link"];
			$this->info["has_360_image"]=(int) $info["has_360_image"];
			$this->info["link_360_image"]=$info["link_360_image"];
		}
		
		/**
		* Returns a json encoded string of all infomation of this property info
		* @param void
		* @return string A json encoded string of all infomation of this property info
		*/
		public function get_json(){
			return json_encode($this->get_info_arr(), JSON_HEX_APOS);
		}
		
		/**
		* Mainly for function get_json
		* Returns all the fields of this property info if it is a subinfo
		* Returns all the fields of this property info with all the fields of its subinfo 
		* as a list stored in member subinfo_list if it is a root info
		* @param void
		* @return array{
		* 	@mixed mixed
		* 	@mixed ...
		* } All the fields of this property info if it is a subinfo, OR all the fields 
		* of this property info with all the fields of its subinfo as a list stored in 
		* member subinfo_list if it is a root info
		*/
		public function get_info_arr(){
			if($this->is_subinfo)
				return array("info_id"=>$this->info_id)+$this->info;
			else{
				$info_arr=$this->info;
				$info_arr["info_id"]=$this->info_id;
				unset($info_arr["beacon_id"]);
				if($this->beacon)
					$info_arr["beacon_title"]=$this->beacon->get_title();
				foreach($this->subinfo_list as $value)
					$info_arr["subinfo_list"][]=$value->get_info_arr();
				return $info_arr;
			}
		}
		
		/**
		* Get the info id of this property info
		* @param void
		* @return int $info_id Info id of this property info
		*/
		public function get_id(){
			return $this->info_id;
		}
		
		/**
		* Get the property id this property info belongs to
		* @param void
		* @return int $property_id The property id this property info belongs to
		*/
		public function get_properties_id(){
			return $this->info["properties_id"];
		}
		
		/**
		* Get the building id of this property info
		* @param void
		* @return int $building_id Building id of this property info
		*/
		public function get_building_id(){
			return $this->info["building_id"];
		}
		
		/**
		* Get the beacon id of this property info
		* @param void
		* @return int $beacon_id Beacon id of this property info
		*/
		public function get_beacon_id(){
			return $this->info["beacon_id"];
		}
		
		/**
		* Get the title of this property info
		* @param void
		* @return string $title Title of this property info
		*/
		public function get_title(){
			return $this->info["title"];
		}
		
		/**
		* Get the description of this property info
		* @param void
		* @return string $text Description of this property info
		*/
		public function get_text(){
			return $this->info["text"];
		}
		
		/**
		* Get the notes of this property info
		* @param void
		* @return string $notes Notes of this property info
		*/
		public function get_notes(){
			return $this->info["notes"];
		}
		
		/**
		* Get info id of parent of this property info
		* @param void
		* @return int $child_of info id of parent of this property info
		*/
		public function get_parent(){
			return $this->info["child_of"];
		}
		
		/**
		* Get info id of parent of this property info
		* @param void
		* @return int $child_of info id of parent of this property info
		*/
		public function get_child_of(){
			return $this->info["child_of"];
		}
		
		/**
		* Get the sort order by which this property info should be shown on the webpage
		* @param void
		* @return int $sort_order The sort order of this property info
		*/
		public function get_sort_order(){
			return $this->info["sort_order"];
		}
		
		/**
		* Get the serial number of this property info
		* @param void
		* @return string $serial_num The serial number of this property info
		*/
		public function get_serial_num(){
			return $this->info["serial_num"];
		}
		
		/**
		* Get a list of media ids of this property info, each of the media id is a string of a number
		* @param void
		* @return array{
		* 	@string string
		*	...
		* } media ids of this property info, each of the media id is a string of a number
		*/
		public function get_media(){
			return explode(", ", $this->info["media"]);
		}
		
		/**
		* Returns whether or not the beacon attached with this property info is a link beacon
		* @param void
		* @return bool $link_beacon True if the beacon attached with this property info is a link beacon and False otherwise
		*/
		public function is_link_beacon(){
			return $this->info["link_beacon"];
		}
		
		/**
		* Get the description of the link beacon
		* @param void
		* @return string $link_beacon_text The description of the link beacon
		*/
		public function get_link_beacon_text(){
			return $this->info["link_beacon_text"];
		}
		
		/**
		* Get the link to the link beacon
		* @param void
		* @return string $link_beacon_link The link to the link beacon
		*/
		public function get_link_beacon_link(){
			return $this->info["link_beacon_link"];
		}
		
		/**
		* Returns whether or not the property info has a 360 image
		* @param void
		* @return bool $has_360_image True if the property info has a 360 image and False otherwise
		*/
		public function has_360_image(){
			return $this->info["has_360_image"];
		}
		
		/**
		* Get the link to the 360 image of property info if it has one
		* @param void
		* @return string $link_360_image The link to the 360 image of property info
		*/
		public function get_link_360_image(){
			return $this->info["link_360_image"];
		}
		
		/**
		* Returns whether or not this property info is a subinfo
		* @param void
		* @return bool $is_subinfo True if the property info is a subinfo and False otherwise
		*/
		public function is_subinfo(){
			return $this->is_subinfo;
		}
		
		/**
		* Get a array containing all the subinfos in it, null if itself is a subinfo
		* @param void
		* @return array{
		* 	@Property_info Property_info
		* 	...
		* }
		*/
		public function get_subinfo_list(){
			return $this->subinfo_list;
		}
		
		// modifier
		/**
		* Changes the beacon id of the property info
		* @param int $beacon_id Beacon id of the property info 
		* @return void
		*/
		public function set_beacon_id($beacon_id){
			$beacon_id=(int)$beacon_id;
			if($this->info["beacon_id"]!=$beacon_id){
				$this->info["beacon_id"]=$beacon_id;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the title of the property info
		* @param string $title Title of the property info 
		* @return void
		*/
		public function set_title($title){
			if(is_string($title)){
				if($this->info["title"]!=$title){
					$this->info["title"]=$title;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the description of the property info
		* @param string $text description of the property info 
		* @return void
		*/
		public function set_text($text){
			if(is_string($text)){
				if($this->info["text"]!=$text){
					$this->info["text"]=$text;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the notes of the property info
		* @param string $notes Notes of the property info 
		* @return void
		*/
		public function set_notes($notes){
			if(is_string($notes)){
				if($this->info["notes"]!=$notes){
					$this->info["notes"]=$notes;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the parent of the property info
		* @param int $child_of Parent of the property info 
		* @return void
		*/
		public function set_parent($child_of){
			$child_of=(int)$child_of;
			if($this->info["child_of"]!=$child_of){
				$this->info["child_of"]=$child_of;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the parent of the property info
		* @param int $child_of Parent of the property info 
		* @return void
		*/
		public function set_child_of($child_of){
			$child_of=(int)$child_of;
			if($this->info["child_of"]!=$child_of){
				$this->info["child_of"]=$child_of;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the sort order by which this property info should be shown on the webpage
		* @param int $sort_order The sort order of this property info
		* @return void
		*/
		public function set_sort_order($sort_order){
			$sort_order=(int)$sort_order;
			if($this->info["sort_order"]!=$sort_order){
				$this->info["sort_order"]=$sort_order;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the serial number of the property info
		* @param string $serial_num Serial number of the property info 
		* @return void
		*/
		public function set_serial_num($serial_num){
			if(is_string($serial_num)){
				if($this->info["serial_num"]!=$serial_num){
					$this->info["serial_num"]=$serial_num;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the media ids of the property info to the media ids in a array, in which each of the media id is a number or string of a number
		* @param array{
		* 	@mixed int/string
		*	...
		* } media ids of this property info, each of the media id is a number or string of a number
		* @return void
		*/
		public function set_media(array &$media){
			foreach($media as $value)
				$this->info["media"]=($this->info["media"]).", ".((int)$value);
			$this->info["media"]=substr($this->info["media"], 2);
		}
		
		/**
		* Changes the media ids of the property info to the media ids in a string, in which media ids are separated by a comma followed by a space
		* @param string
		* @return void
		*/
		public function set_media_string($media){
			
			if(is_string($media)){
				if($this->info["media"]!=$media){
					$this->info["media"]=$media;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the beacon to be a link beacon
		* @param void
		* @return void
		*/
		public function set_link_beacon(){
			if($this->info["link_beacon"]!=1){
				$this->info["link_beacon"]=1;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the beacon from being a link beacon
		* @param void
		* @return void
		*/
		public function unset_link_beacon(){
			if($this->info["link_beacon"]!=0){
				$this->info["link_beacon"]=0;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the description of the link beacon if it is
		* @param string $link_beacon_text Description of the link beacon
		* @return void
		*/
		public function set_link_beacon_text($link_beacon_text){
			if(is_string($link_beacon_text)){
				if($this->info["link_beacon_text"]!=$link_beacon_text){
					$this->info["link_beacon_text"]=$link_beacon_text;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Changes the link to the link beacon if it is
		* @param string $link_beacon_link Link to the link beacon
		* @return void
		*/
		public function set_link_beacon_link($link_beacon_link){
			if(is_string($link_beacon_link)){
				if ($link_beacon_link != '' && preg_match("#https?://#", $link_beacon_link) === 0) {
					$link_beacon_link = 'http://'.$link_beacon_link;
				}
				if($this->info["link_beacon_link"]!=$link_beacon_link){
					$this->info["link_beacon_link"]=$link_beacon_link;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Mark this property info as has a 360 image
		* Sets column has_360_image to 1
		* @param void
		* @return void
		*/
		public function set_360_image(){
			if($this->info["has_360_image"]!=1){
				$this->info["has_360_image"]=1;
				$this->changed=true;
			}
		}
		
		/**
		* Mark this property info as does not have a 360 image
		* Sets column has_360_image to 0
		* @param void
		* @return void
		*/
		public function unset_360_image(){
			if($this->info["has_360_image"]!=0){
				$this->info["has_360_image"]=0;
				$this->changed=true;
			}
		}
		
		/**
		* Changes the link to the 360 image of this property info
		* @param string $link Link to the 360 image of this property info 
		* @return void
		*/
		public function set_link_360_image($link_360_image){
			if(is_string($link_360_image)){
				if($this->info["link_360_image"]!=$link_360_image){
					$this->info["link_360_image"]=$link_360_image;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		// ADD REMOVE
		/**
		* Add a subinfo to this property info
		* Changes both the database and the subinfo list if success
		* @param array{
		* 	@int int id
		* 	@int int properties_id
		* 	@int int building_id
		* 	@int int beacon_id
		* 	@string string title
		* 	@string string text
		* 	@string string notes
		* 	@int int child_of
		* 	@int int primary_info
		* 	@int int location
		* 	@int int sort_order
		* 	@string string serial_num
		* 	@array array media
		* 	@bool bool link_beacon
		* 	@string string link_beacon_text
		* 	@string string link_beacon_link
		* 	@bool bool has_360_image
		* 	@string string link_360_image
		* } Reference to a array containing all the columns of the info to be added
		* @return string Empty string if success and error message if not
		*/
		public function add_sub_info(array &$info){
			// remove info id, because of auto increment
			unset($info["id"]);
			
			// check properties id
			if($info["properties_id"] != $this->info["properties_id"])
				return "Failed to add subinfo: properties id does not match.";
			
			if($info["child_of"] != $this->info_id)
				return "Failed to add subinfo: parent info id does not match info id of this property info.";
			
			// add to DB
			$msg="";
			// update table property_info
			$q_info='INSERT INTO property_info';
			$q_key='';
			$q_value='';
			foreach($info as $key=>$value){
				$q_key=$q_key.", ".$key;
				$q_value=$q_value.", ?";
			}
			$q_key=" (".substr($q_key, 2).") ";
			$q_value=" (".substr($q_value, 2).") ";
			$q_info=$q_info.$q_key."VALUES".$q_value;
			
			// query
			$stmt = mysqli_prepare($this->dbc, $q_info);
			$temp_param=array(str_repeat("s", count($info)));
			foreach($info as $key=>$value)
				$temp_param[]=&$info[$key];
			
			call_user_func_array(array($stmt, "bind_param"), $temp_param);
			// execute prepared statement
			if(!mysqli_stmt_execute($stmt))
				$msg.="Insert into table property_info failed: ".mysqli_stmt_error($stmt);
			else
				// add to subinfo list only if inserted successfully
				$this->subinfo_list[$info["id"]]=new Property_info($info, $this->dbc, $this->primary_info_id);
			
			// close statement and connection
			mysqli_stmt_close($stmt);			
			return $msg;
		}
		
		/**
		* Change an existing property info to the subinfo list of this property info, ignore if it already is.
		* Changes both the database and the subinfo list if success
		* @param Property_info Reference to an existing property info
		* @return string Empty string if success and error message if not
		*/
		public function add_existing_sub_info(Property_info &$info){
			$msg="";
			// check properties id
			if($info->info["properties_id"] != $this->info["properties_id"] ||
				$info->info["child_of"] != $this->info_id)
			{
				$info->info["properties_id"] = $this->info["properties_id"];
				$info->info["child_of"] = $this->info_id;
				$info->changed=true;
				$msg.=$info->update_info_DB();
			}
			return $msg;
		}
		
		/**
		* Remove the property info specified by the info id from both the subinfo list and the DB
		* @param int $info_id ID of the property info to be removed. 
		* @return string Empty string if success and error message if not
		*/
		public function remove_sub_info($info_id){
			$msg='';
			// remove from DB
			$info_id=(int)$info_id;
			// check if the subinfo exists in subinfo list
			if($this->subinfo_list[$info_id]){
				// checks if the subinfo exists in DB
				$q_info="SELECT * FROM property_info WHERE id=".$info_id;
				$res_info=mysqli_query($this->dbc, $q_info);
				if(mysqli_num_rows($res_info)){
					$q_info="DELETE FROM property_info WHERE id=".$info_id;
					if(mysqli_query($this->dbc, $q_info))
						// remove from subinfo list
						unset($this->subinfo_list[$info_id]);
					else
						$msg="Remove Failed: ".mysqli_error($this->dbc);
				} else
					// remove from subinfo list if subinfo does not exists in DB
					unset($this->subinfo_list[$info_id]);
			} else
				$msg="Subinfo Does Not Exists Or Is Not A Subinfo Of This Property Info.";
			return $msg;
		}
		
		/**
		* Attaches a beacon to this property info
		* The property info is required not to be a subinfo
		* @param Beacon $new_beacon The beacon to be attached to this property info
		* @return string Empty string if success and error message if not
		*/
		public function attach_beacon(Beacon $new_beacon){
			$msg='';
			if($this->beacon)
				$mst="Attaching beacon failed: This property already has one property beacon";
			else{
				$q_beacon="UPDATE beacons SET beacon_type='".($this->info["location"]? "location": "information")."' property_info_id=".$this->info_id.
							" WHERE id=".$new_beacon->get_id();
				if(!mysqli_query($this->dbc, $q_beacon))
					$msg="Attaching beacon failed: ".mysqli_error($this->dbc);
				else{
					$q_info="UPDATE property_info SET beacon_id=".$new_beacon->get_id()." WHERE id=".$this->info_id;
					if(!mysql_query($this->dbc))
						$msg="Attaching beacon failed: ".mysqli_error($this->dbc);
					// changes this property only if all of the queries succeed. 
					else
						$this->beacon=$new_beacon;
				}
			}
			return $msg;
		}
		
		/**
		* Detaches the attached beacon to this property info
		* @param void
		* @return string Empty string is success and error messgae of not
		*/
		public function detach_beacon(){
			$msg='';
			if($this->beacon){
				$q_beacon='UPDATE beacons SET property_info_id=0, beacon_type="" WHERE id='.$this->info["beacon_id"];
				if(!mysqli_query($this->dbc, $q_beacon))
					$msg="Detaching beacon failed: ".mysqli_error($this->dbc);
				else{
					$q_info="UPDATE property_info SET beacon_id=0 WHERE beacon_id=".$this->info["beacon_id"];
					if(!mysqli_query($this->dbc, $q_info))
						$msg="Detaching property beacon failed: ".mysqli_error($this->dbc);
					// changes this property info only if all of the queries succeed. 
					else{
						unset($this->beacon);
						$this->info["beacon_id"]=0;
					}
				}
			} else
				$msg="Detaching beacon failed: This property info does not have a beacon attached with it.";
			return $msg;
		}
		
		// DB Operations
		/**
		* Update only this property_info in DB. Will update the DB only if anything field changed.
		* @param void
		* @return string $msg Empty string if success, error message if fails
		*/
		public function update_info_DB(){
			$msg="";
			if($this->changed){
				// update table property_info
				$q_info='UPDATE property_info SET ';
				$q_temp='';
				foreach($this->info as $key=>$value)
					//$q_temp=$q_temp.", ".$key."='".$value."'";
					$q_temp=$q_temp.", ".$key."=?";
				$q_temp=substr($q_temp, 2);
				$q_info=$q_info.$q_temp." WHERE id=".$this->info_id;
				
				// query
				$stmt = mysqli_prepare($this->dbc, $q_info);
				$temp_param=array(str_repeat("s", count($this->info)));
				foreach($this->info as $key=>$value)
					$temp_param[]=&$this->info[$key];
				
				call_user_func_array(array($stmt, "bind_param"), $temp_param);
				// execute prepared statement
				if(!mysqli_stmt_execute($stmt))
					$msg.="Update table property_info failed: ".mysqli_stmt_error($stmt);
				// close statement and connection
				mysqli_stmt_close($stmt);
			}
			return $msg;
		}
		
		/**
		* Update this property_info with all its subinfos if exist in DB. Will update the DB only if anything field changed.
		* @param void
		* @return string $msg Empty string if success, error message if fails
		*/
		public function update_DB(){
			$msg="";
			
			// update subinfos
			if(!$this->is_subinfo())
				foreach($this->subinfo_list as $value)
					$msg.=$value->update_DB();
			
			// update this info
			if($this->changed){
				// update table property_info
				$q_info='UPDATE property_info SET ';
				$q_temp='';
				foreach($this->info as $key=>$value)
					//$q_temp=$q_temp.", ".$key."='".$value."'";
					$q_temp=$q_temp.", ".$key."=?";
				$q_temp=substr($q_temp, 2);
				$q_info=$q_info.$q_temp." WHERE id=".$this->info_id;
				
				// query
				$stmt = mysqli_prepare($this->dbc, $q_info);
				$temp_param=array(str_repeat("s", count($this->info)));
				foreach($this->info as $key=>$value)
					$temp_param[]=&$this->info[$key];
				
				call_user_func_array(array($stmt, "bind_param"), $temp_param);
				// execute prepared statement
				if(!mysqli_stmt_execute($stmt))
					$msg.="Update table property_info failed: ".mysqli_stmt_error($stmt);
				// close statement and connection
				mysqli_stmt_close($stmt);
			}
			return $msg;
		}
		
		/**
		* Reload info in DB to object
		* @param void
		* @return void
		*/
		public function reload_DB(){
			// clear subinfo list
			if($this->subinfo_list)
				unset($this->subinfo_list);
			
			// initialize this property info
			$q_info="SELECT * FROM property_info WHERE id=".$this->info_id;
			$res_info=mysqli_query($this->dbc, $q_info);
			if($res_info && $info=mysqli_fetch_assoc($res_info)){
				// initialize other vars
				$this->info["properties_id"]=$info["properties_id"];
				$this->info["building_id"]=$info["building_id"];
				$this->info["beacon_id"]=$info["beacon_id"];
				if($info["beacon_id"])
					$this->beacon=new Beacon($info["beacon_id"], $this->dbc);
				$this->info["title"]=$info["title"];
				$this->info["text"]=$info["text"];
				$this->info["notes"]=$info["notes"];
				$this->info["child_of"]=$info["child_of"];
				$this->info["location"]=$info["location"];
				$this->info["sort_order"]=$info["sort_order"];
				$this->info["serial_num"]=$info["serial_num"];
				$this->info["media"]=$info["media"];
				$this->info["link_beacon"]=$info["link_beacon"];
				$this->info["link_beacon_text"]=$info["link_beacon_text"];
				$this->info["link_beacon_link"]=$info["link_beacon_link"];
				$this->info["has_360_image"]=$info["has_360_image"];
				$this->info["link_360_image"]=$info["link_360_image"];
			}
			
			// construct subinfo list
			if($info["child_of"]==$this->primary_info_id){
				$this->is_subinfo=false;
				$this->subinfo_list=array();
				
				$q_info="SELECT * FROM property_info WHERE child_of=".$this->info_id." ORDER BY sort_order, id ASC";
				$res_info=mysqli_query($this->dbc, $q_info);
				while($res_info && $row_info=mysqli_fetch_assoc($res_info))
					$this->subinfo_list[$row_info["id"]]=new Property_info($row_info, $this->dbc, $this->primary_info_id);
			}
			
		}
	}
	
	
	/**
	* Keeps track of beacons information
	*/
	class Beacon{
		private $id;
		private $info;
		private $dbc;
		private $changed=false;
		
		/**
		* constructs an beacon instance with information of specified beacon ID in DB table beacons
		* @param int $id ID of this beacon
		* @param mysqli $dbc DB link
		* @return void
		*/
		public function __construct($id, $dbc){
			$this->dbc=$dbc;
			$this->id=$id;
			
			// get info from DB
			$q_info="SELECT * FROM beacons WHERE id=".$this->id;
			$res_info=mysqli_query($this->dbc, $q_info);
			if($res_info && $row_info = mysqli_fetch_assoc($res_info)){
				$this->info["beacon_type"]=$row_info["beacon_type"];
				$this->info["title"]=$row_info["title"];
				$this->info["k_proximity"]=$row_info["k_proximity"];
				$this->info["k_major"]=$row_info["k_major"];
				$this->info["k_minor"]=$row_info["k_minor"];
				$this->info["k_uniqueId"]=$row_info["k_uniqueId"];
				$this->info["k_txPower"]=$row_info["k_txPower"];
				$this->info["k_interval"]=$row_info["k_interval"];
				$this->info["battery_level"]=$row_info["battery_level"];
				$this->info["battery_level_updated"]=$row_info["battery_level_updated"];
			} else
				throw new Exception("Beacon Does Not Exit.");
		}
		
		/**
		* Returns ID of this beacon
		* @param void
		* @return int $beacon_id ID of this beacon
		*/
		public function get_id(){
			return $this->id;
		}
		
		/**
		* Get the type of this beacon (Property, Building, Information, etc.)
		* @param void
		* @return string $beacon_type Type of this beacon
		*/
		public function get_beacon_type(){
			return $this->info["beacon_type"];
		}
		
		/**
		* Get the title of this beacon
		* @param void
		* @return int $beacon_title Title of this beacon
		*/
		public function get_title(){
			return $this->info["title"];
		}
		
		/**
		* Get the beacon proximity value of this beacon
		* @param void
		* @return int $proximity Beacon proximity value
		*/
		public function get_k_proximity(){
			return $this->info["k_proximity"];
		}
		
		/**
		* Get the beacon major value of this beacon
		* @param void
		* @return int $major Beacon major value
		*/
		public function get_k_major(){
			return $this->info["k_major"];
		}
		
		/**
		* Get the beacon minor value of this beacon
		* @param void
		* @return int $minor Beacon minor value
		*/
		public function get_k_minor(){
			return $this->info["k_minor"];
		}
		
		/**
		* Get the unique ID of this beacon
		* @param void
		* @return int $uniqueId The unique ID of this beacon
		*/
		public function get_k_uniqueId(){
			return $this->info["k_uniqueId"];
		}
		
		/**
		* Get the beacon txPower value of this beacon
		* @param void
		* @return int $txPower Beacon txPower value
		*/
		public function get_k_txPower(){
			return $this->info["k_txPower"];
		}
		
		/**
		* Get the beacon interval value of this beacon
		* @param void
		* @return int $interval Beacon interval value
		*/
		public function get_k_interval(){
			return $this->info["k_interval"];
		}
		
		/**
		* Get the battery level of this beacon, range [0, 100]
		* @param void
		* @return int $battery_level Battery level of this beacon
		*/
		public function get_battery_level(){
			return $this->info["battery_level"];
		}
		
		/**
		* Returns the timestamp of the last time when the battery level information of this beacon is updated
		* @param void
		* @return string $battery_level_updated The timestamp of the last time when the battery level info is updated
		*/
		public function get_battery_level_updated(){
			return $this->info["battery_level_updated"];
		}
		
		/**
		* Sets the type of this beacon (to Property, Building, Information, etc.)
		* @param $type The type of this beacon
		* @return void
		*/
		public function set_beacon_type($type){
			if(is_string($type)){
				if($this->info["beacon_type"]!=$type){
					$this->info["beacon_type"]=$type;
					$this->changed=true;
				}
			} else
				throw new Exception("Wrong Parameter Type.");
		}
		
		/**
		* Update information of this beacon in DB. Will update the DB only if any field has changed.
		* @param void
		* @return string $msg Empty string if success, error message if fails
		*/
		public function update_DB(){
			$msg="";
			
			// update beacon informations in the database
			if($this->changed){
				// update table property_info
				$q_info='UPDATE beacons SET ';
				$q_temp='';
				foreach($this->info as $key=>$value)
					//$q_temp=$q_temp.", ".$key."='".$value."'";
					$q_temp=$q_temp.", ".$key."=?";
				$q_temp=substr($q_temp, 2);
				$q_info=$q_info.$q_temp." WHERE id=".$this->id;
				
				// query
				$stmt = mysqli_prepare($this->dbc, $q_info);
				$temp_param=array(str_repeat("s", count($this->info)));
				foreach($this->info as $key=>$value)
					$temp_param[]=&$this->info[$key];
				
				call_user_func_array(array($stmt, "bind_param"), $temp_param);
				// execute prepared statement
				if(!mysqli_stmt_execute($stmt))
					$msg.="Update table property_info failed: ".mysqli_stmt_error($stmt);
				// close statement and connection
				mysqli_stmt_close($stmt);
			}
			return $msg;
		}
		
		/**
		* Reload info in DB to object
		* @param void
		* @return void
		*/
		public function reload_DB(){
			$q_info="SELECT * FROM beacons WHERE id=".$this->id;
			$res_info=mysqli_query($this->dbc, $q_info);
			if($res_info && $row_info = mysqli_fetch_assoc($res_info)){
				$this->info["beacon_type"]=$row_info["beacon_type"];
				$this->info["title"]=$row_info["title"];
				$this->info["k_proximity"]=$row_info["k_proximity"];
				$this->info["k_major"]=$row_info["k_major"];
				$this->info["k_minor"]=$row_info["k_minor"];
				$this->info["k_uniqueId"]=$row_info["k_uniqueId"];
				$this->info["k_txPower"]=$row_info["k_txPower"];
				$this->info["k_interval"]=$row_info["k_interval"];
				$this->info["battery_level"]=$row_info["battery_level"];
				$this->info["battery_level_updated"]=$row_info["battery_level_updated"];
			}
		}
		
	}
	
?>