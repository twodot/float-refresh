<?php
	/**
	* Keeps track of properties of a user
	*/
	class Properties {
		private $dbc;
		private $properties;
	
		/**
		* constructs an instance of properties with user id in DB
		* @param null
		* @return void
		*/
		public function __construct(){
			global $UserInfo;
			$user_id = $UserInfo->get_user_id();
			global $dbc;
			$this->user__id=$user_id;
			$this->dbc=$dbc;
			
			$total_properties = 0;
			
			// query-properties
			$query="SELECT 
				*
				
				FROM properties 
				
				WHERE 
				properties.user_id=".$user_id . " 
				ORDER BY id DESC";
			// $query="SELECT * FROM properties WHERE id=".$this->property_id;
			$res=mysqli_query($this->dbc, $query);
			$this->properties = array();
			while($res && $row=mysqli_fetch_assoc($res)){
				
				$propertyId = $row['id'];
				$propertyTitle = $row['title'];
				$active = $row['active'];
				($active?$total_properties ++:'');
				$address = $row['address'];
				$price = $row['price'];
				$property_data_type = $row['property_data_type'];
				$deleted = $row['deleted'];
				
				$q_beacons="SELECT 
					property_info.title, 
					property_info.primary_info, 
					property_info.location, 
					beacons.title as beacon_title
					
					
					FROM 
					property_info, 
					beacons 
					
					WHERE
					(property_info.beacon_id != 0 || property_info.location != 0) 
					AND beacons.property_info_id = property_info.id 
					AND property_info.properties_id =" . $propertyId . " 
					ORDER BY property_info.id";
				
				$res_beacons=mysqli_query($this->dbc, $q_beacons);
				$primary_beacon = false;
				$beacons = array();
				$locationBeacons = array();
				$canActivate = false;
				while($res_beacons && $row_beacons=mysqli_fetch_assoc($res_beacons)){
					if($row_beacons['primary_info'] == 1) {
						$primary_beacon = $row_beacons['beacon_title'];
						$title = 'Primary';
						$canActivate = true;
					}
					elseif($row_beacons['location'] == 1) {
							$title = $row_beacons['title'];
							$locationBeacons[] = array('beacon'=>$row_beacons['beacon_title'], 'room_title'=>$row_beacons['title']);
					}
					else {
						$title = $row_beacons['title'];
					}
					$beacons[] = array('beacon'=>$row_beacons['beacon_title'], 'room_title'=>$title);
				}
				
				$beaconCount  = ($row_beacons['beacon_count']?$row_beacons['beacon_count']:0);
				
				$q_image="SELECT * FROM media WHERE main_gallery_order = 1 AND property_id =" . $propertyId;
				
				$res_image=mysqli_query($this->dbc, $q_image);
				
				if(mysqli_num_rows($res_image) == 0) {
					$q_image="SELECT * FROM media WHERE property_id =" . $propertyId . " ORDER BY id LIMIT 1";
					$res_image=mysqli_query($this->dbc, $q_image);
				}
				
				$primary_image = false;
				if(mysqli_num_rows($res_image) != 0) {
					while($res_image && $row_image=mysqli_fetch_assoc($res_image)){
						$primary_image = 'https://s3-us-west-2.amazonaws.com/images.listfloat.com/phone/' . $row_image['id'] . '.jpg';
					}
				}
				else {
					$primary_image = 'https://s3-us-west-2.amazonaws.com/images.listfloat.com/phone/please_add_an_image.jpg';
				}
				
				$this->properties[] = array('id'=>$propertyId, 'title'=>$propertyTitle, 'active'=>$active, 'address'=>$address, 'price'=>$price, 'primary_beacon'=>$primary_beacon, 'beacon_count'=>count($beacons), 'image'=>$primary_image, 'beacons'=>$beacons, 'property_data_type'=>$property_data_type, 'canActivate'=>$canActivate, 'deleted'=>$deleted);
				
				$this->total_properties = $total_properties ;
				
			} // END WHILE 
		} // END CONSTRUCT
		/**
		* Get crm id of the property
		* @param void
		* @return int $crm_id crm id of this property
		*/
		public function get_properties(){
			return $this->properties;
		}
		
		public function get_properties_json(){
			return json_encode($this->properties);
		}
		public function get_total_active_properties(){
			return $this->total_properties;
		}
		
		
	}
	
	
?>