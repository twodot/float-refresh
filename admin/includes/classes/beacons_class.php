<?php
	/**
	* Keeps track of properties of a user
	*/
	class Beacons {
		private $dbc;
		private $beacons;
	
		/**
		* constructs an instance of beacons with user id in DB
		* @param null
		* @return void
		*/
		public function __construct(){
			global $UserInfo;
			$user_id = $UserInfo->get_user_id();
			global $dbc;
			$this->user__id=$user_id;
			$this->dbc=$dbc;
			
		// query-properties
			$query="SELECT 
				beacons.*, 
				property_info.title as property_info_title, 
				properties.address as property_address 
				
				FROM 
				beacons
							
				LEFT JOIN property_info 
				ON beacons.property_info_id = property_info.id
				
				LEFT JOIN properties 
				ON property_info.properties_id = properties.id 
				AND beacons.property_info_id = property_info.id
				
				WHERE 
				beacons.user_id= ".$user_id . "  
				ORDER BY title ASC";
			// $query="SELECT * FROM properties WHERE id=".$this->property_id;
			$res=mysqli_query($this->dbc, $query);
			$this->beacons = array();
			while($res && $row=mysqli_fetch_assoc($res)){
				$beacon_id = $row['id'];
				$property_info_id = $row['property_info_id'];
				$beacon_title = $row['title'];
				$owner_id = $row['owner_id'];
				$property_info_title = $row['property_info_title'];
				$property_address = $row['property_address'];
				$active = ($property_info_id != 0?1:0);
				
				if($user_id != $owner_id) {
					
					$query_vendor="SELECT 
					*
					FROM vendors
					
					WHERE 
					id=".$owner_id . " 
					ORDER BY id DESC";
					$res_vendor=mysqli_query($this->dbc, $query_vendor);
					while($res_vendor && $row_vendor=mysqli_fetch_assoc($res_vendors)){
						$vendor = $row_vendor['name'];
					}
				} // END GET VENDOR 
				else {
					$vendor = false;
				}
		
				
				$this->beacons[] = array('id'=>$beacon_id , 'title'=>$beacon_title, 'active'=>$active, 'property_address'=>$property_address, 'property_info_title'=>$property_info_title);
				
			} // END WHILE 
		} // END CONSTRUCT
		
		public function get_beacons(){
			return $this->beacons;
		}
		
		public function get_beacons_json(){
			return json_encode($this->beacons);
		}
		
	}
	
	
?>