<?php
require_once('includes/config.inc.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Thank You For Registering</title>
</head>

<body>

<table cellpadding="0" cellspacing="0" border="0" style="font-family:Helvetica, Arial, sans-serif;">
	<tr>
    	<td align="center">
        	<img src="<?php echo BASE_URL; ?>images/logo-126.jpg" />
        </td>
    </tr>
    <tr>
    	<td>
        	<h1 style="color:#00AEC7;">You&rsquo;re Fantastic, thank&rsquo;s for registering</h1>
            <p>Your first step is to [ACTIVATE] your account.</p>
            <p>Then you can <a href="<?php echo BASE_URL; ?>login.php">log in and enjoy</a></p>
        </td>
	</tr>
</table>
</body>
</html>
