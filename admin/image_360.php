<?php
if(isset($_GET['name'])) { 
require_once('includes/Mobile_Detect.php');
$detect = new Mobile_Detect;

$name = 'jdh_living_room';
$ogShow = 1;
if(isset($_GET['name'])) {
	if(!empty($_GET['name'])) {
		$name = $_GET['name'];
		$ogShow = 0;
	}
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Float 360 Image</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <?php if($ogShow) { ?>
            <meta property="og:image" content="https://admin.listfloat.com/images/<?php echo $name ?>.jpg">
            <meta property="og:image:type" content="image/jpeg">
            <meta property="og:image:width" content="1024">
            <meta property="og:image:height" content="767">
        <?php } ?>
		<style>
			@font-face {
			  font-family: 'FontAwesome';
			  src: url('/fontawesome-fonts/fontawesome-webfont.eot?v=4.5.0');
			  src: url('/fontawesome-fonts/fontawesome-webfont.eot?#iefix&v=4.5.0') format('embedded-opentype'), url('/fontawesome-fonts/fontawesome-webfont.woff2?v=4.5.0') format('woff2'), url('/fontawesome-fonts/fontawesome-webfont.woff?v=4.5.0') format('woff'), url('/fontawesome-fonts/fontawesome-webfont.ttf?v=4.5.0') format('truetype'), url('/fontawesome-fonts/fontawesome-webfont.svg?v=4.5.0#fontawesomeregular') format('svg');
			  font-weight: normal;
			  font-style: normal;
			}
			
		
			html, body {
				margin:0;
				padding:0;
				-webkit-text-size-adjust: 100%;
			}
			
			#container {
				position:absolute;	
			}
			
			
			.fa {
			  display: inline-block;
			  font: normal normal normal 14px/1 FontAwesome;
			  font-size: 2em;
			  text-rendering: auto;
			  -webkit-font-smoothing: antialiased;
			  -moz-osx-font-smoothing: grayscale;
			  color:#00AEC7;
				margin:0px 10px;
			}
			.fa-refresh:before {
			  content: "\f021";
			}
			
			.fa-eye:before {
			  content: "\f06e";
			}
			.fa-eye-slash:before {
			  content: "\f070";
			}
			.fa-hand-o-up:before {
			  content: "\f0a6";
			}
			#iconBar {
				position:fixed;
				bottom:0;
				z-index:2000;
				width:100%;
				text-align:center;
			}
			
			#icons {
				background:rgba( 255, 255, 255, 0.8);
				display:inline-block;
				padding:10px;
				margin:auto;
				text-align:center;
				list-style:none;
			}
			
			#icons li {
				display:inline;
				color:#000;
			}

			a {
				color: #ff8800;
			}
		</style>

	</head>
	<body>
		<script src="/js/jquery-1.11.0.min.js"></script>
        <script src="/js/three.min.js"></script>
        <script src="/js/threex.windowresize.js"></script>
        <script src="/js/StereoEffect.js"></script>
        <script src="/js/DeviceOrientationControls.js"></script>
        <script src="/js/OrbitControls.js"></script>
		
        
		<script>
				 $( document ).ready(function() {
					
					var container, camera, scene, renderer, controls, geometry, mesh, effect;
	
					var animate = function(){

						
						window.requestAnimationFrame( animate );
						onWindowResize();
						controls.update();
						renderer.render(scene, camera);
						if(effect) {
							effect.render( scene, camera );
						}
					};
					
					function onWindowResize() {
						
						camera.aspect = window.innerWidth / window.innerHeight;
						camera.updateProjectionMatrix();
						
						renderer.setSize( window.innerWidth, window.innerHeight );
						
						if(window.innerWidth > window.innerHeight) {
							$('#goggles').show();
						}
						else {
							$('#goggles').hide();
							effect = null;
						}
						
						
					}
					
					function addSterio() {
						effect = new THREE.StereoEffect( renderer );
						effect.eyeSeparation = 10;
						effect.setSize( window.innerWidth, window.innerHeight );
					}
					
					
					function init() {
						
		
						container = document.getElementById( 'container' );
						camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1100);
						
						scene = new THREE.Scene();
						<?php if ( $detect->isMobile() ) { ?>
						controls = new THREE.DeviceOrientationControls( camera );
						<?php }?>
						
						var geometry = new THREE.SphereGeometry( 500, 16, 8 );
						geometry.applyMatrix( new THREE.Matrix4().makeScale( -1, 1, 1 ) );

						var material = new THREE.MeshBasicMaterial( {
							map: THREE.ImageUtils.loadTexture( 'https://admin.listfloat.com/show_image_aws.php?name=<?php echo $name; ?>' )
						} );
						
						
						var mesh = new THREE.Mesh( geometry, material );
						scene.add( mesh );

						renderer = new THREE.WebGLRenderer();
						renderer.setPixelRatio( window.devicePixelRatio );
						renderer.setSize(window.innerWidth, window.innerHeight);
						renderer.domElement.style.position = 'absolute';
						renderer.domElement.style.top = 0;
						container.appendChild(renderer.domElement);
						
						$('#goggles').on('click',function(){
							if(effect) {
								$(this).removeClass('fa-eye-slash');
								$(this).addClass('fa-eye');
								effect = null;
							}
							else {
								$(this).removeClass('fa-eye');
								$(this).addClass('fa-eye-slash');
								addSterio();
							}
						})
						
						
						<?php if ( !$detect->isMobile() ) { ?>
							$('#controlChange').remove();
							controls = new THREE.OrbitControls(camera, renderer.domElement);
							controls.target.set(
								camera.position.x + 0.15,
								camera.position.y,
								camera.position.z
							);
							controls.noPan = true;
							controls.noZoom = true;
						<?php }?>
						
						$('#controlChange').on('click',function(){
							
							if($(this).hasClass('device')) {
								$(this).removeClass('device');
								$(this).removeClass('fa-hand-o-up');
								$(this).addClass('manual')
								$(this).addClass('fa-refresh');
								controls = new THREE.OrbitControls(camera, renderer.domElement);
								controls.target.set(
									camera.position.x + 0.15,
									camera.position.y,
									camera.position.z
								);
								controls.noPan = true;
								controls.noZoom = true;
							}
							else {
								$(this).removeClass('manual');
								$(this).removeClass('fa-refresh');
								$(this).addClass('device');
								$(this).addClass('fa-hand-o-up');
								controls = new THREE.DeviceOrientationControls( camera );
							}
						});
						
						
						window.addEventListener('deviceorientation', setOrientationControls, true);
						function setOrientationControls(e) { // CHECK FOR DEVICE ORIENTATION AND GIVE MANUAL CONTROL IF NOT AVAILABLE.
							if (!e.alpha) {
								$('#controlChange').remove();
								controls = new THREE.OrbitControls(camera, renderer.domElement);
								controls.target.set(
									camera.position.x + 0.15,
									camera.position.y,
									camera.position.z
								);
								controls.noPan = true;
								controls.noZoom = true;
								return;
							}
							
						}
						
						window.addEventListener( 'resize', onWindowResize, false );
						
						animate();
								
					}
					init();
			 });
		</script>
        <div id="iconBar">
			<ul id="icons">
            	<li><i id="goggles" class="fa fa-eye"></i></li>
    	    	<li><i id="controlChange" class="device fa fa-hand-o-up"></i></li>
            </ul>
        </div>
		<div id="container"></div>
	</body>
</html>
<?php
} // END GET NAME CHECK
?>