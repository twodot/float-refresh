<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Leads - <?php echo $propertytitle; ?></title>
</head>

<body style="text-align:center">

<table cellpadding="0" cellspacing="0" border="0" style="font-family:Helvetica, Arial, sans-serif; max-width:700px; text-align:left; margin:auto; padding:20px;">
	<tr>
    	<td>
        	<table width="100%">
        		<tr>
                	<td style="padding-right:20px; width:20%;">
                		<img src="<?php echo BASE_URL_ADMIN; ?>images/logo-126.jpg"  style="width:100%; max-width:126px; min-width:50px; height:auto" />
                    </td>
                   
               </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
        	<p>Hi, <?php echo $agent_first_name . ' ' . $agent_last_name; ?></p>
        	<p><?php echo $message; ?></p>
            <p>Thanks,<br /><?php echo $first_name . ' ' . $last_name; ?><br /><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
        </td>
	</tr>
    
    
</table>
</body>
</html>
