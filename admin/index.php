<?php
/*
ini_set("display_errors", 1);
ini_set("track_errors", 1);
ini_set("html_errors", 1);
error_reporting(E_ALL);
*/
session_start();

$url = $_SERVER['REQUEST_URI'];

$qs = $_SERVER['QUERY_STRING'];

$dirs = explode('/', $url);

$dir_chain = '_content/';
foreach($dirs as $key => $dir) {
	 if($dir == 'ajax') {
		 $dir_chain = $dir . '/';
	 }
}


$inc_file = end($dirs);

$inc_file = str_replace('?', '', str_replace($qs, '', $inc_file));

if((int) $inc_file > 0) {
	$p = $inc_file;
	$inc_file = prev($dirs)	;
}

if(empty($inc_file)) {
	if (!isset($_SESSION['login']['first_name'])) {
		$inc_file = 'login';
	}
	else {
		$inc_file = 'panel';
	}	
}

if ($inc_file == 'admin') {
	$inc_file = 'panel';
}

$inc_file = str_replace('.php', '', $inc_file);

$login_required = FALSE;

$base = $_SERVER["DOCUMENT_ROOT"];

$base = str_replace('www', '', $base);

require_once(dirname(__FILE__) . '/includes/config.inc.php');

if ($inc_file == 'webhook') {
	include('webhook.php');
	exit();
}

if ($inc_file == 'webhook_test') {
	include('webhook_test.php');
	exit();
}


if (file_exists($dir_chain . $inc_file . '.php')) {
	include($dir_chain . $inc_file . '.php');
}
else {
	include('_content/home.php');
}


?>