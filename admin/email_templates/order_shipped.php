<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body style="margin-top:0px; margin-left:0p; margin-right:0px; padding-left:0px; padding-top:0px; padding-right:0px; background-color:#EEEEEE;">
<table cellpadding="0" cellspacing="0" border="0" width="100%" >
	<tr>
    	<td>
			<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#FFFFFF">
				<tr>
                	<td bgcolor="#2b363e">
                    	<table cellpadding="0" cellspacing="0" border="0" width="100%" >
                        	<tr>
                            	<td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                            	<td width="5%">&nbsp;</td>
                            	<td width="45%"><a href="https://listfloat.com" target="_blank"><img src="https://admin.listfloat.com/images/logo-small-25.png" width="86" height="25" alt="Float Logo" border="0" /></a></td>
                                <td width="22%" align="right"><a href="https://admin.listfloat.com/account" target="_blank" style="color:#ffffff; text-decoration:none; font-family:Helvetica, Arial, sans-serif;">MY ACCOUNT</a></td>
                                <td width="3%" align="center" style="color:#ffffff; font-weight:bold;" class="hidden">&nbsp;&nbsp;|&nbsp;&nbsp;</td>
                                <td width="22%" align="left"><a href="https://listfloat.com/contact" target="_blank" style="color:#ffffff; text-decoration:none; font-family:Helvetica, Arial, sans-serif;">CONTACT US</a></td>
                            </tr>
                            <tr>
                            	<td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr> <!-- END HEADER -->
                <tr> <!-- START RED LINE -->
                	<td bgcolor="#fa0102">
                    	<table cellpadding="0" cellspacing="0" border="0" width="100%" >
                        	<tr>
                            	<td style="line-height:5px; height:5px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr> <!-- END RED LINE -->
                
                
                <tr> <!-- START BODY TOP PADDING -->
                	<td>&nbsp;</td>
                </tr><!-- END BODY TOP PADDING -->
                
                <tr> <!-- START BODY -->
                	<td>
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td width="5%">&nbsp;</td>
                                <td>
                                <p style="font-family:Helvetica, Arial, sans-serif; font-size:16px; color:#2b363e;">
                                Hey <?php echo $first_name; ?>, <br /><br />
                                Your Order has shipped!.</p>
                                <h3 style="color:#2b363e; font-family:Helvetica, Arial, sans-serif;">Tracking # <a href="https://tools.usps.com/go/TrackConfirmAction.action?tRef=fullpage&tLc=1&text28777=&tLabels=<?php echo $tracking_number; ?>" target="_blank"><?php echo $tracking_number; ?></a></h3>
                                </td>
                                <td width="5%">&nbsp;</td>
                            </tr>
                        </table>
                                <table cellpadding="0" cellspacing="0" border="0" width="100%" >
                                	<tr>
                                    	<td bgcolor="#00acc8" width="5%" valign="top">&nbsp;</td><td bgcolor="#00acc8" width="29%" valign="top">&nbsp;</td><td bgcolor="#ffffff" width="1px" valign="top">&nbsp;</td><td bgcolor="#00acc8" width="2%" valign="top">&nbsp;</td><td bgcolor="#00acc8" width="28%" valign="top"><td bgcolor="#ffffff" width="1px" valign="top">&nbsp;</td><td bgcolor="#00acc8" width="2%" valign="top">&nbsp;</td><td bgcolor="#00acc8" width="25%" valign="top"><td bgcolor="#00acc8" width="5%" valign="top">&nbsp;</td>
                                    </tr>
                                    <tr>
                                    	<td bgcolor="#00acc8" width="5%" valign="top">&nbsp;</td>
                                    	<td bgcolor="#00acc8" width="29%" valign="top">
                                        	<p style="color:#ffffff; font-family:Helvetica, Arial, sans-serif;"><strong>Shipping</strong></p>
                                        </td>
                                        <td bgcolor="#ffffff" width="1px" valign="top">&nbsp;</td>
                                        <td bgcolor="#00acc8" width="2%" valign="top">&nbsp;</td>
                                    	<td bgcolor="#00acc8" width="28%" valign="top">
                                        	<p style="color:#ffffff; font-family:Helvetica, Arial, sans-serif;"><strong><?php echo $first_name . ' ' . $last_name; ?><br />
                                            <?php echo $shipping_address; ?></p>
                                        </td>
                                        <td bgcolor="#ffffff" width="1px" valign="top">&nbsp;</td>
                                        <td bgcolor="#00acc8" width="2%" valign="top">&nbsp;</td>
                                    	<td bgcolor="#00acc8" width="28%" valign="top">
                                        	<p style="color:#ffffff; font-family:Helvetica, Arial, sans-serif;"><strong>Shipping Method</strong><br />Standard Ground</p>
                                        </td>
                                        <td bgcolor="#00acc8" width="5%" valign="top">&nbsp;</td>
                                    </tr>
                                    <tr>
                                    	<td bgcolor="#00acc8" width="5%" valign="top">&nbsp;</td><td bgcolor="#00acc8" width="29%" valign="top">&nbsp;</td><td bgcolor="#ffffff" width="1px" valign="top">&nbsp;</td><td bgcolor="#00acc8" width="5%" valign="top">&nbsp;</td><td bgcolor="#00acc8" width="25%" valign="top"><td bgcolor="#ffffff" width="1px" valign="top">&nbsp;</td><td bgcolor="#00acc8" width="5%" valign="top">&nbsp;</td><td bgcolor="#00acc8" width="25%" valign="top"><td bgcolor="#00acc8" width="5%" valign="top">&nbsp;</td>
                                    </tr>
                                </table>
                                
                                
                                
                                
                    </td>
                </tr><!-- END BODY -->
                
                
                 <tr> <!-- START BODY BOTTOM PADDING -->
                	<td>&nbsp;</td>
                </tr><!-- END BODY BOTTOM PADDING -->
                
                
                <tr> <!-- START FOOTER -->
                	<td bgcolor="#2b363e">
                    	<table cellpadding="0" cellspacing="0" border="0" width="100%" >
                        	<tr>
                            	<td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                            	<td width="5%">&nbsp;</td>
                                
                                



                        
                            	<td ><p style="color:#FFFFFF; font-size:10px; font-family:Helvetica, Arial, sans-serif">This email was sent by: <?php echo OUR_NAME; ?><br />
<?php echo OUR_ADDRESS . ' ' . OUR_SUITE; ?>, <?php echo OUR_CITY; ?>, <?php echo OUR_STATE; ?> <?php echo OUR_ZIP; ?></p></td>
                                <td width="20">&nbsp;</td>
                                <td width="84" align="right"><a href="https://itunes.apple.com/us/app/real-estate-by-float-beacon/id1006537937?ls=1&mt=8" target="_blank"><img src="https://admin.listfloat.com/images/apple.png" border="0" /></a></td>
                                <td width="20">&nbsp;</td>
                                <td width="25" align="right"><a href="https://plus.google.com/+Listfloat/posts" target="_blank"><img src="https://admin.listfloat.com/images/google_plus_email.png" border="0" /></a></td>
                                <td width="20">&nbsp;</td>
                                <td width="25" align="right"><a href="https://twitter.com/listfloat" target="_blank"><img src="https://admin.listfloat.com/images/twitter_email.png" border="0" /></a></td>
                                <td width="20">&nbsp;</td>
                                <td width="25" align="right"><a href="https://www.facebook.com/listfloat" target="_blank"><img src="https://admin.listfloat.com/images/facebook_email.png" border="0" /></a></td>
                                <td width="20">&nbsp;</td>
                                <td width="25" align="right"><a href="https://www.linkedin.com/company/float-technologies" target="_blank"><img src="https://admin.listfloat.com/images/linked_in_email.png" border="0" /></a></td>
                            	<td width="30">&nbsp;</td>
                            </tr>
                            <tr>
                            	<td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr> <!-- END FOOTER -->
                
                
                
                
			</table><!-- END MAIN TABLE -->
		</td>
    </tr>
</table><!-- END TABLE HOLDER -->
</body>
</html>
