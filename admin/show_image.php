<?php

$image = FALSE;

$name = (!empty($_GET['name'])) ? $_GET['name'] : 'print image';

if (isset($_GET['id']) && is_numeric($_GET['id']) ) {
	$image = '../float_req/images/tablet/' . (int) $_GET['id'] . '.jpg';
	
	if (!file_exists ($image) || (!is_file($image))) {
		$image = FALSE;
	}
	
}

if (!$image) {

	$image = '../float_req/images/tablet/unavailable.jpg';
	
	$name = 'unavailable.jpg';
}

$info = getimagesize($image);

$fs = filesize($image);

header ("Content-Type: {$info['mime']}\n");

header ("Content-Disposition: inline; filename=\"$name\"\n");

header ("Content-Length: $fs\n");

readfile ($image);

?>