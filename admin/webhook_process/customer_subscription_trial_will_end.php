<?php

$customer = $event_json->data->object->customer;	
$created = $event_json->created;
$amount = $event_json->data->object->plan->amount;

$q= "SELECT users.user_id, users.email, subscription.subscribe_level, TIMESTAMPDIFF(DAY, NOW(), trial_end) + 1 as days_left FROM users, subscription WHERE stripe_id = '$customer' AND users.user_id = subscription.user_id";
$r = @mysqli_query ($dbc, $q);

if(mysqli_num_rows($r) != 1) {
	$to      = 'jwhite@listfloat.com';
	$subject = 'webhook error';
	$message = $q;
	$headers = 'From: info@listfloat.com' . "\r\n" .
		'Reply-To: info@listfloat.com' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();
	
	mail($to, $subject, $message, $headers);
}


$row = mysqli_fetch_array($r, MYSQLI_ASSOC);

$cust_id = $row['user_id'];
$date_end = $row['days_left'];
$user_email = $row['email'];

include('email_templates/trial_will_end.php');
$HTMLEmailoutput = ob_get_contents();


if($date_end == 1) {
	$date_end_replace = '1 day';
}
else {
	$date_end_replace = $date_end . ' days';
}

$HTMLEmailoutput = str_replace('[DAYS_LEFT]', $date_end_replace, $HTMLEmailoutput);

ob_end_clean();

//Create a new PHPMailer instance
include(SMTP_MAILER);

$mail->From = 'info@' . EMAIL_URL;
$mail->FromName = 'FYI From Float';
$mail->addAddress($user_email);     // Add a recipient
$mail->addReplyTo('info@' . EMAIL_URL, 'FYI From Float');

$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Your Trial Period Will End in ' . $date_end . ' Days';

$mail->Body    = $HTMLEmailoutput;


//send the message, check for errors

if($mail->send()) {
	header("HTTP/1.1 200 OK");
	echo 'Customer Trial Will End Processed Properly';
	exit();
}
else {
	header("HTTP/1.0 503 Service Unavailable", true, 503 );
	echo $q;
	exit();
}
?>