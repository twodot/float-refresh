<?php
session_start();
function require_login() {
	
	if (!isset($_SESSION['login']['first_name'])) {
		
		$url = BASE_URL . 'login' ;
		
		ob_end_clean();
		
		header("Location: $url");
		
		exit();
	}
	if (isset($_SESSION['login']['user_level'])) {
		if ($_SESSION['login']['user_level'] != 1) {
			$url = 'https://admin.listfloat.com/panel' ;
	
			ob_end_clean();
			
			header("Location: $url");
			
			exit();
		}
	}
}

function login_forward() {
	
	if (isset($_SESSION['login']['first_name'])) {
		$url = BASE_URL . 'panel' ;
		ob_end_clean();
		header("Location: $url");
		exit();
	}
}

function require_admin_login() {
	
	if (!isset($_SESSION['login']['first_name'])) {
		
		$url = BASE_URL . 'login' ;
		
		ob_end_clean();
		
		header("Location: $url");
		
		exit();
	}
	if (isset($_SESSION['login']['user_level'])) {
		if ($_SESSION['login']['user_level'] != 1) {
			$url = BASE_URL . 'panel' ;
	
			ob_end_clean();
			
			header("Location: $url");
			
			exit();
		}
	}
}


// USER LOGIN SECTION //
if(isset($_SESSION['login'])) {
	$user_id = $_SESSION['login']['user_id'];
	
	$user_email = $_SESSION['login']['email'];
	
	//$user_name = $_SESSION['login']['first_name'] . ' ' . $_SESSION['login']['last_name'];
}


// Get the properties id and creating a property_id variable
$property_id = 0;
function get_property_id() {
	global $property_id;
	if(isset($_GET['property']) || isset($p)) {
		if(isset($_GET['property']) ) {
			$property_id = (int) $_GET['property'];
		}
		else if(isset($p)) {
			$property_id = (int) $p;
		}
	}
	else {
		$property_id = 0;
	}
		
	if($property_id == 0) {
		header('Location: /panel');
	}
}

// prevent others from accessing this user's property

function users_property(){
	global $user_id;
	global $dbc;
	global $property_id;
	$query="SELECT user_id FROM properties WHERE id=".$property_id." AND user_id=".$user_id;
	$res=mysqli_query($dbc, $query);
	if(!$res || mysqli_num_rows($res)==0)
		die();
}

?>