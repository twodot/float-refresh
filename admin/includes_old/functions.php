<?php
function get_active($user_id, $dbc) {

	$q = "
	SELECT 
	
	properties.id as property_id, 
	properties.title as property_title, 
	properties.user_id as user_id, 
	properties.active as active, 
	properties.price as price, 
	properties.deleted, 
	
	property_info.id as info_id, 
	property_info.properties_id as properties_id, 
	property_info.title as info_title, 
	property_info.text, 
	property_info.notes,   
	property_info.beacon_id as beacon_id, 
	
	(
	
		SELECT 
		COUNT(*) 
		
		FROM 
		
		beacons 
		
		WHERE 
		
		property_info_id = info_id 
	
	) as total_beacons, 
	
	beacons.id as beacon_id, 
	beacons.user_id as beacon_user, 
	beacons.title as beacon_title, 
	beacons.property_info_id as beacon_info_id  
	
	FROM 
	properties, 
	property_info, 
	beacons 
	
	WHERE 
	
	properties.user_id = $user_id 
	AND property_info.primary_info = 1  
	AND properties.active = 1 
	AND property_info.properties_id = properties.id 
	AND beacons.user_id = $user_id 
	AND beacons.property_info_id = property_info.id 
	AND properties.deleted = 0 
	AND property_data_type = 'property' 
	
	ORDER BY 
	
	properties.id 
	
	DESC
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	
	return $r;
}

function sanitize($data, $dbc) {
	
	$data = htmlentities($data, ENT_QUOTES);
	$data = trim($dbc->real_escape_string($data));
	return $data;
}


function get_property($user_id, $dbc, $property) {

	$q = "
	SELECT 
	
	properties.id as property_id, 
	properties.title as property_title, 
	properties.user_id as user_id, 
	properties.active as active,
	properties.price as price,
	properties.progress as progress, 
	properties.completed as completed, 
	properties.status as sold_status, 
	properties.status_message, 
	properties.description, 
	properties.address, 
	properties.latitude,
	properties.longitude, 
	properties.beds,
	properties.bath,
	properties.sqft,
	properties.lot_size,
	properties.property_type,
	properties.style,
	properties.year_built,
	properties.community,
	properties.county,
	properties.mls, 
	properties.walkscore,
	properties.show_walkscore,
	properties.show_greatschools, 
	properties.active, 
	properties.walkscore, 
	properties.collapse_details, 
	properties.collapse_schools,  
	properties.facebook_share, 
	properties.twitter_share, 
	properties.deleted, 
	properties.property_data_type, 
	properties.progress, 
	properties.crm, 
	properties.crm_field_1_value, 
	properties.crm_field_2_value, 
	properties.crm_field_3_value, 
	properties.crm_field_4_value, 
	properties.crm_field_5_value, 
	properties.crm_field_6_value, 
	
	property_info.id as info_id, 
	property_info.properties_id as properties_id, 
	property_info.title as info_title, 
	property_info.text, 
	property_info.notes,  
	property_info.beacon_id as beacon_id, 
	properties.gs_elm, 
	properties.gs_jh, 
	properties.gs_hs, 
	properties.short_url_hash, 
	
	(
	
		SELECT 
		COUNT(*) 
		
		FROM 
		property_info, 
		beacons 
		
		WHERE 
		
		beacons.property_info_id = property_info.id 
		AND property_info.beacon_id = beacons.id 
		AND beacons.user_id = $user_id 
		AND property_info.properties_id = properties.id  
	
	) as total_beacons, 
	
	(
	
		SELECT 
		COUNT(*) 
		
		FROM 
		media  
		
		WHERE 
		media.property_id = $property AND media.deleted != 1  
	
	) as total_media, 
	
	(
	
		SELECT 
		id 
		
		FROM 
		flyers  
		
		WHERE 
		flyers.property_id = $property  
	
	) as flyer_id, 
	
	(
	
		SELECT 
		name 
		
		FROM 
		flyers  
		
		WHERE 
		flyers.property_id = $property  
	
	) as flyer_name, 
	
	beacons.id as beacon_id, 
	beacons.title as beacon_title 
	
	
	
	FROM 
	properties, 
	property_info    
	
	LEFT JOIN beacons 
	ON property_info.id = beacons.property_info_id 
	
	WHERE 
	
	properties.user_id = $user_id 
	AND properties.id = $property 
	AND property_info.primary_info = 1  
	AND property_info.properties_id = properties.id 
	AND property_info.id NOT IN (SELECT property_info_id FROM beacons) 
	AND properties.deleted = 0 
	AND properties.property_data_type = 'property' 
	
	ORDER BY 
	
	properties.id 
	
	DESC
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	$status = 'inactive';
	
	$num_rows = mysqli_num_rows($r);
	
	if( mysqli_num_rows($r) == 0) {
		
	$q = "

	SELECT 
	
	properties.id as property_id, 
	properties.title as property_title, 
	properties.user_id as user_id, 
	properties.active as active, 
	properties.price as price, 
	properties.progress as progress, 
	properties.completed as completed, 
	properties.status as sold_status, 
	properties.status_message, 
	properties.address, 
	properties.description, 
	properties.latitude,
	properties.longitude,
	properties.beds,
	properties.bath,
	properties.sqft,
	properties.lot_size,
	properties.property_type,
	properties.style,
	properties.year_built,
	properties.community,
	properties.county,
	properties.mls, 
	properties.walkscore,
	properties.show_walkscore,
	properties.show_greatschools, 
	properties.active, 
	properties.walkscore, 
	properties.gs_elm, 
	properties.gs_jh, 
	properties.gs_hs, 
	properties.short_url_hash, 
	properties.collapse_details, 
	properties.collapse_schools, 
	properties.facebook_share, 
	properties.twitter_share, 
	properties.deleted, 
	properties.property_data_type, 
	properties.progress, 
	properties.crm, 
	properties.crm_field_1_value, 
	properties.crm_field_2_value, 
	properties.crm_field_3_value, 
	properties.crm_field_4_value, 
	properties.crm_field_5_value, 
	properties.crm_field_6_value, 
	
	property_info.id as info_id, 
	property_info.properties_id as properties_id, 
	property_info.title as info_title, 
	property_info.text,
	property_info.notes,  
	property_info.beacon_id, 
	
	(
	
		SELECT 
		COUNT(*) 
		
		FROM 
		property_info, 
		beacons 
		
		WHERE 
		
		beacons.property_info_id = property_info.id 
		AND property_info.beacon_id = beacons.id 
		AND beacons.user_id = $user_id 
		AND property_info.properties_id = properties.id 
	
	) as total_beacons, 
	
	beacons.id as beacon_id, 
	beacons.title as beacon_title, 
	
	(
	
		SELECT 
		COUNT(*) 
		
		FROM 
		media  
		
		WHERE 
		media.property_id = $property and media.deleted != 1 
	
	) as total_media, 
	
	(
	
		SELECT 
		id 
		
		FROM 
		flyers  
		
		WHERE 
		flyers.property_id = $property  
	
	) as flyer_id, 
	
	(
	
		SELECT 
		name 
		
		FROM 
		flyers  
		
		WHERE 
		flyers.property_id = $property  
	
	) as flyer_name
	
	FROM 
	properties, 
	property_info, 
	beacons  
	
	WHERE 
	
	properties.user_id = $user_id 
	AND properties.id = $property 
	AND property_info.primary_info = 1 
	AND property_info.properties_id = properties.id 
	AND beacons.id = property_info.beacon_id 
	AND properties.deleted = 0 
	AND properties.property_data_type = 'property' 
	
	ORDER BY 
	
	properties.id 
	
	DESC
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	$status = 'active';
	}
	return array('r' =>$r, 'status'=>$status);
}


function get_non_building_properties($user_id, $dbc) {
	
	$q= "
	SELECT *
	
	FROM 
	properties
	
	WHERE 
	user_id = $user_id 
	AND building_id = 0 
	AND deleted = 0 
	AND properties.property_data_type != 'building' 
	
	ORDER BY 
	
	id 
	
	DESC";
	$r = @mysqli_query ($dbc, $q);	
	return $r;
	
}


function get_buildings($user_id, $dbc) {
	
	$q = "
	SELECT 
	
	properties.id as building_id, 
	properties.title as building_name, 
	properties.description, 
	properties.amenities, 
	properties.views, 
	properties.address, 
	properties.latitude, 
	properties.longitude, 
	properties.status, 
	properties.co_agent, 
	properties.community, 
	properties.county, 
	properties.walkscore, 
	properties.walkscore_link, 
	properties.show_walkscore, 
	properties.show_greatschools, 
	properties.gs_elm, 
	properties.gs_jh, 
	properties.gs_hs, 
	properties.deleted,  
	
	property_info.id as info_id, 
	property_info.properties_id, 
	property_info.title as info_title, 
	property_info.text, 
	property_info.notes,   
	property_info.beacon_id as beacon_id, 
	
	(
	
		SELECT 
		COUNT(*) 
		
		FROM 
		
		beacons 
		
		WHERE 
		
		property_info_id = info_id 
	
	) as total_beacons, 
	
	beacons.id as beacon_id, 
	beacons.user_id as beacon_user, 
	beacons.title as beacon_title, 
	beacons.property_info_id as beacon_info_id 
	
	FROM 
	properties, 
	property_info 
	
	LEFT JOIN beacons 
	ON (beacons.property_info_id = property_info.id)
	
	WHERE 
	
	properties.user_id = $user_id 
	AND property_info.primary_info = 1 
	AND property_info.properties_id = properties.id 
	AND properties.deleted = 0 
	AND properties.property_data_type = 'Building' 
	
	ORDER BY 
	
	properties.id 
	
	DESC
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	return $r;
}



function get_building($user_id, $dbc, $building) {
	
	$q = "
	SELECT 
	
	properties.id as property_id, 
	properties.title as property_title, 
	properties.description, 
	properties.amenities, 
	properties.views, 
	properties.address, 
	properties.latitude, 
	properties.longitude, 
	properties.status, 
	properties.co_agent, 
	properties.community, 
	properties.county, 
	properties.walkscore, 
	properties.walkscore_link, 
	properties.show_walkscore, 
	properties.show_greatschools, 
	properties.gs_elm, 
	properties.gs_jh, 
	properties.gs_hs,  
	properties.short_url_hash, 
	properties.collapse_details, 
	properties.collapse_schools,  
	properties.deleted, 
	properties.progress as progress, 
	properties.completed as completed, 
	properties.status as sold_status, 
	properties.status_message as status_message, 
	properties.year_built, 
	properties.active, 
	
	property_info.id as info_id, 
	property_info.properties_id as properties_id, 
	property_info.title as info_title, 
	property_info.text, 
	property_info.notes,   
	property_info.beacon_id as beacon_id, 
	
	(
	
		SELECT 
		COUNT(*) 
		
		FROM 
		
		beacons 
		
		WHERE 
		
		property_info_id = info_id 
	
	) as total_beacons, 
	
	(
	
		SELECT 
		COUNT(*) 
		
		FROM 
		media  
		
		WHERE 
		media.building_id = $building and media.deleted != 1 
	
	) as total_media, 
	
	beacons.id as beacon_id, 
	beacons.user_id as beacon_user, 
	beacons.title as beacon_title, 
	beacons.property_info_id as beacon_info_id 
	
	FROM 
	properties, 
	property_info 
	
	LEFT JOIN beacons 
	ON (beacons.property_info_id = property_info.id)
	
	WHERE 
	
	properties.user_id = $user_id 
	AND property_info.primary_info = 1 
	AND property_info.properties_id = properties.id 
	AND properties.id = $building 
	AND properties.deleted = 0 
	AND property_data_type = 'building' 
	
	ORDER BY 
	
	properties.id 
	
	DESC
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	return $r;
}


function get_building_properties($user_id, $dbc, $building) {

	$q = "
	SELECT *
	
	FROM 
	properties
	
	WHERE 
	user_id = $user_id 
	AND building_id = $building 
	AND properties.deleted = 0 
	
	ORDER BY 
	
	building_sort, id
	
	ASC";
	$r = @mysqli_query ($dbc, $q);	
	return $r;
	
}


function get_inactive($user_id, $dbc) {
	$q = "

	SELECT 
	beacons.* , 
	properties.id as property_id, 
	properties.title as property_title, 
	properties.user_id as user_id, 
	properties.active as active, 
	properties.price as price, 
	properties.deleted, 
	
	property_info.id as info_id, 
	property_info.properties_id as properties_id, 
	property_info.title as info_title, 
	property_info.text, 
	property_info.notes,  
	property_info.beacon_id as beacon_id,  
	
	(
	
		SELECT 
		COUNT(*) 
		
		FROM 
		property_info, 
		beacons 
		
		WHERE 
		
		beacons.property_info_id = property_info.id 
		AND property_info.beacon_id = beacons.id 
		AND beacons.user_id = $user_id 
		AND property_info.properties_id = properties.id 
	
	) as total_beacons 
	
	
	FROM 
	properties, 
	property_info
	
	LEFT JOIN 
	(beacons) 
	
	ON 
	( property_info.beacon_id = beacons.id   
	 )
	
	WHERE 
	
	properties.user_id = $user_id 
	AND properties.active = 0 
	AND property_info.properties_id = properties.id 
	AND property_info.primary_info = 1 
	AND property_info.child_of = 0  
	AND properties.deleted = 0 
	AND properties.property_data_type = 'property' 
	
	ORDER BY 
	
	properties.id 
	
	DESC
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	
	return $r;
}

function get_inactive_assigned($user_id, $dbc) {
	$q = "

	SELECT 
	
	properties.id as property_id, 
	properties.title as property_title, 
	properties.user_id as user_id, 
	properties.active as active, 
	properties.price as price, 
	properties.deleted, 
	
	property_info.id as info_id, 
	property_info.properties_id as properties_id, 
	property_info.title as info_title, 
	property_info.text, 
	property_info.notes,  
	property_info.beacon_id, 
	
	(
	
		SELECT 
		COUNT(*) 
		
		FROM 
		
		beacons 
		
		WHERE 
		
		property_info_id = info_id 
	
	) as total_beacons, 
	
	beacons.id as beacon_id, 
	beacons.title as beacon_title 
	
	FROM 
	properties, 
	property_info, 
	beacons 
	
	WHERE 
	
	properties.user_id = $user_id 
	AND properties.active = 0 
	AND property_info.properties_id = properties.id 
	AND beacons.id = property_info.beacon_id 
	AND properties.deleted = 0
	
	ORDER BY 
	
	properties.id 
	
	DESC
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	
	return $r;
}


function get_all_beacons($user_id, $dbc) {
	$q = "
	

	SELECT 
	beacons.id as beacon_id, 
	beacons.property_info_id,  
	beacons.title as beacon_title, 
	beacons.beacon_type, 
	beacons.k_major, 
	beacons.k_minor, 
	beacons.uuid, 
	
	 
	properties.title as property_name, 
	properties.title as building_name, 
	property_info.id, 
	property_info.primary_info    
	 
	
	FROM 
	beacons 
	LEFT JOIN property_info ON property_info.id = beacons.property_info_id
	
	LEFT JOIN properties ON property_info.properties_id = properties.id
	
	WHERE 
	beacons.user_id = $user_id  
	
	ORDER BY beacons.title + 0 
	ASC 
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	
	return $r;
}

function get_beacon_info($uuid, $major, $minor) {
	  	$ch = curl_init(); 

        // set url 
		
		$header = array("Api-Key: " . BEACON_API , "Accept: application/vnd.com.kontakt+json; version=2");
		
		curl_setopt($ch, CURLOPT_URL, "https://api.kontakt.io/beacon?proximity=" . $uuid  . "&major=" . $major . "&minor=" . $minor); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch); 

        // close curl resource to free up system resources 
        curl_close($ch); 
		
		return $output; 
}

function get_beacon_by_uid($uid) {
	  	$ch = curl_init(); 

        // set url 
		
		$header = array("Api-Key: " . BEACON_API , "Accept: application/vnd.com.kontakt+json; version=2");
		
		curl_setopt($ch, CURLOPT_URL, "https://api.kontakt.io/beacon/" . $uid); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch); 

        // close curl resource to free up system resources 
        curl_close($ch); 
		
		return $output; 
}

function get_pseudo() {
	require_admin_login();
	if(!isset($_SESSION['pseudo_login']['id'])) {
		$url = BASE_URL . 'panel' ;
		ob_end_clean();
		header("Location: $url");
		exit();
	}
	
	else if((int)$_SESSION['pseudo_login']['id'] == 0) {
		$url = BASE_URL . 'panel' ;
		ob_end_clean();
		header("Location: $url");
		exit();
	}
}


function get_avail_beacons($user_id, $dbc) {
	$q = "

	SELECT 
	beacons.title, 
	beacons.id 
	
	FROM 
	beacons 
	
	WHERE 
	beacons.user_id = $user_id 
	AND beacons.property_info_id = 0 
	AND beacons.id NOT IN (SELECT beacon_id FROM property_info)
	
	ORDER BY title + 0 
	ASC 
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	
	return $r;
}


function get_active_beacons($user_id, $dbc) {
	$q = "

	SELECT 
	beacons.title, 
	beacons.id, 
	beacons.property_info_id, 

	properties.title as property_title, 
	properties.id as property_id, 

	property_info.title as info_title, 
	property_info.properties_id,
	property_info.properties_id, 
	property_info.beacon_id, 
	property_info.id as prop_info_id
	
	FROM 
	beacons, 
	property_info 

LEFT JOIN properties ON (properties.id = property_info.properties_id AND (properties.property_data_type = 'property' || properties.property_data_type = 'building' )) 

	
	WHERE 
	beacons.user_id = $user_id  
	AND beacons.property_info_id != 0 
	AND beacons.property_info_id = property_info.id 
	
	
	
	ORDER BY beacons.title + 0 
	ASC 
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	
	return $r;
}



function get_location_beacons($user_id, $dbc, $property) {
	$q = "

	SELECT 
	beacons.title, 
	beacons.id, 
	beacons.property_info_id, 

	properties.title as property_title, 
	properties.id as property_id, 

	property_info.title as info_title, 
	property_info.properties_id,
	property_info.properties_id, 
	property_info.beacon_id, 
	property_info.id as prop_info_id
	
	FROM 
	beacons, 
	property_info 

LEFT JOIN properties ON (properties.id = property_info.properties_id) 

	
	WHERE 
	beacons.user_id = $user_id  
	AND beacons.property_info_id != 0 
	AND beacons.property_info_id = property_info.id 
	AND property_info.location = 1
	AND properties.id = $property
	
	
	ORDER BY beacons.title + 0 
	ASC 
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	
	return $r;
}


function get_shipping($user_id, $dbc) {
	$q="
	
	SELECT 
	addresses.address_id, 
	addresses.user_id, 
	addresses.billing_id, 
	addresses.shipping_id, 
	
	shipping.shipping_id, 
	shipping.user_id, 
	shipping.street, 
	shipping.street_cont, 
	shipping.apt_number, 
	shipping.city, 
	shipping.state_name as state, 
	shipping.zip 
	
	FROM 
	addresses, 
	shipping
	
	WHERE 
	addresses.user_id = $user_id 
	AND shipping.user_id = $user_id 
	AND addresses.shipping_id = shipping.shipping_id 
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	return $r;
}

function get_billing($user_id, $dbc) {
	$q="
	
	SELECT 
	addresses.address_id, 
	addresses.user_id, 
	addresses.billing_id, 
	addresses.shipping_id, 
	
	billing.billing_id, 
	billing.user_id, 
	billing.street, 
	billing.street_cont, 
	billing.apt_number, 
	billing.city, 
	billing.state_name as state, 
	billing.zip
	
	FROM 
	addresses, 
	billing 
	
	WHERE 
	addresses.user_id = $user_id 
	AND billing.user_id = $user_id 
	AND addresses.billing_id = billing.billing_id 
	
	";
	
	$r = @mysqli_query ($dbc, $q);	
	return $r;
}


function get_info($user_id, $dbc, $property) {
	
	$q="
	SELECT 
	id  
	FROM 
	property_info 
	
	WHERE 
	properties_id = $property 
	AND primary_info = 1";
	$r = @mysqli_query ($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	$primary_info = $row['id'];
	
	$q="
	SELECT 
	property_info.id as info_id, 
	property_info.properties_id, 
	property_info.beacon_id, 
	property_info.title as info_title, 
	property_info.text as info_text, 
	
	property_info.link_beacon, 
	property_info.link_beacon_text, 
	property_info.link_beacon_link,  
	
	property_info.has_360_image, 
	property_info.link_360_image, 
	
	property_info.notes as info_notes, 
	property_info.child_of, 
	property_info.primary_info,
	property_info.sort_order, 
	CHAR_LENGTH(property_info.media) - CHAR_LENGTH(REPLACE(property_info.media, ',', '')) + 1 as total_media, 
	
	beacons.id as beacon_id, 
	beacons.user_id, 
	beacons.property_info_id, 
	beacons.title
	 
	FROM 
	property_info 
	
	LEFT JOIN 
	(beacons) 
	
	ON 
	( beacons.property_info_id = property_info.id 
	 )
	 
	
	
	WHERE 
	properties_id = $property 
	AND child_of = $primary_info 
	
	ORDER BY property_info.sort_order 
	
	ASC
	";
	
	$r = @mysqli_query ($dbc, $q);	
	return $r;
	
}


function get_child_info($user_id, $dbc, $property, $primary_info) {
	$q="
	SELECT 
	* 
	FROM 
	property_info 
	
	WHERE 
	properties_id = $property 
	AND child_of = $primary_info 
	
	ORDER BY property_info.sort_order 
	
	ASC";
	
	$r = @mysqli_query ($dbc, $q);	
	return $r;
	
}


function add_addresses($user_id, $dbc, $street, $street_cont, $apt_number, $city, $state_name, $zip) {
	$q = "SELECT COUNT(*) as shipping_count FROM shipping WHERE user_id = $user_id";
	$r = @mysqli_query ($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	$ship_count = $row['shipping_count'];
	if($row['shipping_count'] ==0 ) {
		$q = "
			INSERT INTO 
			shipping 
			
			(user_id, street, street_cont, apt_number, city, state_name, zip) VALUES ($user_id, '$street', '$street_cont', '$apt_number', '$city', '$state_name', '$zip')";
		$r = @mysqli_query ($dbc, $q);	
		$s_id = mysqli_insert_id($dbc);
	}
	$q = "SELECT COUNT(*) as biling_count FROM billing WHERE user_id = $user_id";
	$r = @mysqli_query ($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	$bill_count = $row['biling_count'];
	if($row['biling_count'] ==0 ) {
		$q = "
		INSERT INTO 
		billing 
		
		(user_id, street, street_cont, apt_number, city, state_name, zip) VALUES ($user_id, '$street', '$street_cont', '$apt_number', '$city', '$state_name', '$zip')";
			
		$r = @mysqli_query ($dbc, $q);	
		$b_id = mysqli_insert_id($dbc);
	}
	
	if (isset($b_id) && isset($s_id)) {
		$q = "SELECT * FROM addresses WHERE user_id = $user_id";
		$r = @mysqli_query ($dbc, $q);
		
		if (mysqli_num_rows($r) == 0) {
			$q = " INSERT INTO addresses (user_id, billing_id, shipping_id) VALUES ($user_id, '$b_id', '$s_id')";
			$r = @mysqli_query ($dbc, $q);
		}
	}
}

function add_single_address($table, $user_id, $dbc, $street, $street_cont, $apt_number, $city, $state_name, $zip) {
	$q = "SELECT COUNT(*) as add_count FROM $table WHERE user_id = $user_id";
	$r = @mysqli_query ($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	if($row['add_count'] ==0 ) {
		$q = "
			INSERT INTO 
			$table  
			
			(user_id, street, street_cont, apt_number, city, state_name, zip) VALUES ($user_id, '$street', '$street_cont', '$apt_number', '$city', '$state_name', '$zip'); 
			
		";
		$r = @mysqli_query ($dbc, $q);	
		
		if ($table == 'shipping') {
			$s_id = mysqli_insert_id($dbc);
		}
		if ($table =='billing') {
			$b_id = mysqli_insert_id($dbc);
		}
		
		
		$q = "SELECT * FROM addresses WHERE user_id = $user_id";
		$r = @mysqli_query ($dbc, $q);
		
		if (mysqli_num_rows($r) == 0) {
			
			if (isset($b_id)) {
				$q = " INSERT INTO addresses (user_id, billing_id) VALUES ($user_id, '$b_id')";
				$r = @mysqli_query ($dbc, $q);
			}
			
			if (isset($s_id)) {
				$q = " INSERT INTO addresses (user_id, shipping_id) VALUES ($user_id, '$s_id')";
				$r = @mysqli_query ($dbc, $q);
			}
		}
		else {
			$q = "SELECT * FROM addresses WHERE user_id = $user_id";
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			$address_id = $row['address_id'];
			
			if (isset($b_id)) {
				$q = " UPDATE addresses SET billing_id = '$b_id' WHERE address_id = $address_id AND user_id = $user_id";
				$r = @mysqli_query ($dbc, $q);
			}
			
			if (isset($s_id)) {
				$q = " UPDATE addresses SET shipping_id = '$s_id' WHERE address_id = $address_id AND user_id = $user_id";
				$r = @mysqli_query ($dbc, $q);
			}		
		}
	}
}

function get_card ($user_id, $dbc) {
	$q="
		SELECT 
		* , 
		COUNT(*) as total 
		
		FROM 
		cards 
		
		WHERE 
		user_id = $user_id 
		
		ORDER BY id DESC
		
		LIMIT 1 
		
	";	
	$r = @mysqli_query ($dbc, $q);
	
	return $r;
}


function rental($user_id, $dbc) {
	$q="
	SELECT 
	rental 
	
	FROM 
	subscription
	
	WHERE 
	user_id = $user_id";
	
	$r = @mysqli_query ($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	
	return $row['rental'];
}

function show_hints($user_id, $dbc) {
	$q="
		SELECT 
		show_hints
		
		FROM 
		users 
		
		WHERE 
		user_id = $user_id 
		
		LIMIT 1
	";	
	$r = @mysqli_query ($dbc, $q);
	
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	
	if($row['show_hints'] == 1) {
		$show_hints = true;
	}
	else {
		$show_hints = false;
	}
	
	return $show_hints;
}


function get_cart_tax($user_id, $dbc, $shippingCost, $UserInfo) {

	if(!isset($_SESSION['cart']['id'])) {
		$_SESSION['cart']['id'] = md5(uniqid(rand(), true));
	}
	$cartID = $_SESSION['cart']['id'];
	$_SESSION['taxcloudExemptionCertificate'] = null;
	$_SESSION['taxCloudSelectedCert'] = null;
	
	$products = Array();
	$product = Array();
	$i = 0;
	foreach($_SESSION['cart'] as $sku => $quantity) {
		
		$q_price = "SELECT price, tic FROM products WHERE sku = '$sku'";
		$r_price = mysqli_query($dbc, $q_price);
		if (mysqli_num_rows($r_price) != 0) {
			$row_price = mysqli_fetch_array($r_price, MYSQLI_ASSOC);
			
			$price = $row_price['price'];
			$tic = $row_price['tic'];
			$qty = $quantity['quantity'];
			
			$product['id'] = $sku; //Your system's product ID
			$product['price'] = $price; //product price
			$product['qty'] = $qty; //product quantity
			$product['tic'] = $tic; //product TIC
			
			$products[$i] = $product; //repeat this for each item in the shopping cart
			
			$i++;
		} // END NUM ROWS CHECK
	}
		
		

	$origin = new Address();
	$origin->setAddress1(OUR_ADDRESS);
	$origin->setAddress2(OUR_SUITE);
	$origin->setCity(OUR_CITY);
	$origin->setState(OUR_STATE);
	$origin->setZip5(OUR_ZIP);
	
	$shipping = $shippingCost; 
	
	$errMsg = Array();
	
	$customer = array();
	$customer['address']['S']['address'] = $UserInfo->ship_street;
	$customer['address']['S']['city'] = $UserInfo->ship_city;
	$customer['address']['S']['state'] = $UserInfo->ship_state;
	$customer['address']['S']['zipcode'] = $UserInfo->ship_zip;
	$customer['id'] = $user_id;
	$cust = func_get_customer_address_taxcloud($customer);
	
	$taxes = func_lookup_tax_taxcloud($products,$customer,$errMsg,$shipping);
	return $taxes;
	//print_r($taxes);
	
}


function get_subscription_tax($amount, $user_id, $dbc) {
	
	if(!isset($_SESSION['cart']['id'])) {
		$_SESSION['cart']['id'] = md5(uniqid(rand(), true));
	}
	
	$customerID = $user_id;
	$cartID = $_SESSION['cart']['id'];
	$_SESSION['taxcloudExemptionCertificate'] = null;
	$_SESSION['taxCloudSelectedCert'] = null;
	
	$q_address = "
		SELECT 
		shipping.shipping_id as id, 
		shipping.street as ship_street, 
		shipping.city as ship_city, 
		shipping.state_name as ship_state, 
		shipping.zip as ship_zip
		
		FROM 
		shipping 
		
		WHERE 
		shipping.user_id = $user_id 
		
		ORDER BY shipping.shipping_id DESC
		
		LIMIT 1 
		";
		
	$r_address = @mysqli_query ($dbc, $q_address);
	$row_address = mysqli_fetch_array($r_address, MYSQLI_ASSOC);
	
	$products = Array();
		
	$product = Array();
	
		$q_sub = "SELECT amount, subscribe_level FROM subscription WHERE user_id = '$user_id'";
		$r_sub = mysqli_query($dbc, $q_sub);
		if (mysqli_num_rows($r_sub) != 0) {
			$row_sub = mysqli_fetch_array($r_sub, MYSQLI_ASSOC);
			$dollarAmount = $amount / 100;
			
			$price = number_format($dollarAmount, 2, '.', '');
			$tic = 30070;
			$qty = 1;
			$sku = $row_sub['subscribe_level'];
			
			$product['id'] = $sku; //Your system's product ID
			$product['price'] = $price; //product price
			$product['qty'] = $qty; //product quantity
			$product['tic'] = $tic; //product TIC
			
			$products[] = $product; //repeat this for each item in the shopping cart
			
		} // END NUM ROWS CHECK
	
	
	$origin = new Address();
	$origin->setAddress1(OUR_ADDRESS);
	$origin->setAddress2(OUR_SUITE);
	$origin->setCity(OUR_CITY);
	$origin->setState(OUR_STATE);
	$origin->setZip5(OUR_ZIP);
	
	$dest = new Address();
	$dest->setAddress1($row_address['ship_street']);
	$dest->setAddress2(null);
	$dest->setCity($row_address['ship_city']);
	$dest->setState($row_address['ship_state']);
	$dest->setZip5($row_address['ship_zip']);
	
	$shipping = 0; 
	
	$errMsg = Array();
	
	$customer = array();
	$customer['address']['S']['address'] = $row_address['ship_street'];
	$customer['address']['S']['city'] = $row_address['ship_city'];
	$customer['address']['S']['state'] = $row_address['ship_state'];
	$customer['address']['S']['zipcode'] = $row_address['ship_zip'];
	$customer['id'] = $user_id;
	
	//$cust = func_get_customer_address_taxcloud($customer);
	
	// CHECK NEXUS SECTION 
	$taxes = '0';
	$customers_state = strtoupper($row_address['ship_state']);
	
	$states_to_tax[] = 'WA';
	$states_to_tax[] = 'CA';
	$states_to_tax[] = 'OR';
	
	foreach ($states_to_tax as $state_to_tax) {
		if($state_to_tax == $customers_state) {
			$taxes = func_lookup_tax_taxcloud($products,$customer,$errMsg,$shipping);
		}
	}
	// END CHECK NEXUS SECTION 
	
	return array('taxes' =>$taxes, 'cart_id' => $cartID, 'errors'=>$errMsg);
	
}


// IMAGE RESIZE

define('THUMBNAIL_IMAGE_MAX_WIDTH', 150);
define('THUMBNAIL_IMAGE_MAX_HEIGHT', 150);

define('PHONE_IMAGE_MAX_WIDTH', 960);
define('PHONE_IMAGE_MAX_HEIGHT', 640);


define('TABLET_IMAGE_MAX_WIDTH', 1024);
define('TABLET_IMAGE_MAX_HEIGHT', 768);

function generate_image_thumbnail($source_image_path, $thumbnail_image_path, $id)
{
	list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
	switch ($source_image_type) {
		case IMAGETYPE_GIF:
			$source_gd_image = imagecreatefromgif($source_image_path);
			break;
		case IMAGETYPE_JPEG:
			$source_gd_image = imagecreatefromjpeg($source_image_path);
			break;
		case IMAGETYPE_PNG:
			$source_gd_image = imagecreatefrompng($source_image_path);
			break;
	}
	if ($source_gd_image === false) {
		return false;
	}
	$source_aspect_ratio = $source_image_width / $source_image_height;
	$thumbnail_aspect_ratio = THUMBNAIL_IMAGE_MAX_WIDTH / THUMBNAIL_IMAGE_MAX_HEIGHT;
	if ($source_image_width <= THUMBNAIL_IMAGE_MAX_WIDTH && $source_image_height <= THUMBNAIL_IMAGE_MAX_HEIGHT) {
		$thumbnail_image_width = $source_image_width;
		$thumbnail_image_height = $source_image_height;
	} elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
		$thumbnail_image_width = (int) (THUMBNAIL_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
		$thumbnail_image_height = THUMBNAIL_IMAGE_MAX_HEIGHT;
	} else {
		$thumbnail_image_width = THUMBNAIL_IMAGE_MAX_WIDTH;
		$thumbnail_image_height = (int) (THUMBNAIL_IMAGE_MAX_WIDTH / $source_aspect_ratio);
	}
	$thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
	imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);
	imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 90);
	imagedestroy($source_gd_image);
	imagedestroy($thumbnail_gd_image);
	
	return $thumbnail_image_path;
}



function generate_image_phone($source_image_path, $phone_image_path, $id)
{
	list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
	switch ($source_image_type) {
		case IMAGETYPE_GIF:
			$source_gd_image = imagecreatefromgif($source_image_path);
			break;
		case IMAGETYPE_JPEG:
			$source_gd_image = imagecreatefromjpeg($source_image_path);
			break;
		case IMAGETYPE_PNG:
			$source_gd_image = imagecreatefrompng($source_image_path);
			break;
	}
	if ($source_gd_image === false) {
		return false;
	}
	$source_aspect_ratio = $source_image_width / $source_image_height;
	$phone_aspect_ratio = PHONE_IMAGE_MAX_WIDTH / PHONE_IMAGE_MAX_HEIGHT;
	if ($source_image_width <= PHONE_IMAGE_MAX_WIDTH && $source_image_height <= PHONE_IMAGE_MAX_HEIGHT) {
		$phone_image_width = $source_image_width;
		$phone_image_height = $source_image_height;
	} elseif ($phone_aspect_ratio > $source_aspect_ratio) {
		$phone_image_width = (int) (PHONE_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
		$phone_image_height = PHONE_IMAGE_MAX_HEIGHT;
	} else {
		$phone_image_width = PHONE_IMAGE_MAX_WIDTH;
		$phone_image_height = (int) (PHONE_IMAGE_MAX_WIDTH / $source_aspect_ratio);
	}
	$phone_gd_image = imagecreatetruecolor($phone_image_width, $phone_image_height);
	imagecopyresampled($phone_gd_image, $source_gd_image, 0, 0, 0, 0, $phone_image_width, $phone_image_height, $source_image_width, $source_image_height);
	imagejpeg($phone_gd_image, $phone_image_path, 90);
	imagedestroy($source_gd_image);
	imagedestroy($phone_gd_image);
	
	return $phone_image_path;
}




function generate_image_tablet($source_image_path, $tablet_image_path, $id)
{
	list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
	switch ($source_image_type) {
		case IMAGETYPE_GIF:
			$source_gd_image = imagecreatefromgif($source_image_path);
			break;
		case IMAGETYPE_JPEG:
			$source_gd_image = imagecreatefromjpeg($source_image_path);
			break;
		case IMAGETYPE_PNG:
			$source_gd_image = imagecreatefrompng($source_image_path);
			break;
	}
	if ($source_gd_image === false) {
		return false;
	}
	$source_aspect_ratio = $source_image_width / $source_image_height;
	$tablet_aspect_ratio = TABLET_IMAGE_MAX_WIDTH / TABLET_IMAGE_MAX_HEIGHT;
	if ($source_image_width <= TABLET_IMAGE_MAX_WIDTH && $source_image_height <= TABLET_IMAGE_MAX_HEIGHT) {
		$tablet_image_width = $source_image_width;
		$tablet_image_height = $source_image_height;
	} elseif ($tablet_aspect_ratio > $source_aspect_ratio) {
		$tablet_image_width = (int) (TABLET_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
		$tablet_image_height = TABLET_IMAGE_MAX_HEIGHT;
	} else {
		$tablet_image_width = TABLET_IMAGE_MAX_WIDTH;
		$tablet_image_height = (int) (TABLET_IMAGE_MAX_WIDTH / $source_aspect_ratio);
	}
	$tablet_gd_image = imagecreatetruecolor($tablet_image_width, $tablet_image_height);
	imagecopyresampled($tablet_gd_image, $source_gd_image, 0, 0, 0, 0, $tablet_image_width, $tablet_image_height, $source_image_width, $source_image_height);
	imagejpeg($tablet_gd_image, $tablet_image_path, 90);
	imagedestroy($source_gd_image);
	imagedestroy($tablet_gd_image);
	
	return $tablet_image_path;
}


/*
class property {
	public $id = '';
	public $message = '';
	public $message_desc = '';
	public $title = '';
	public $price = '';
	public $description = '';
	public $address = '';
	public $latitude = '';
	public $longitude = '';
	
	//DETAILS 
	public $beds = '';
	public $baths = '';
	public $sqft = '';
	public $lot_size = '';
	public $property_type = '';
	public $year_built = '';
	public $neighborhood = '';
	public $county = '';
	public $mls = '';
	
	// WALKSCORE / SCHOOLS
	public $schools = array();
	public $walkscore = array();
	
	// PROPERTY HIGHLIGHTS (ROOMS)
	public $info = array();
	public $sub_info = array();
	
	// LISTING INFO
	public $listing_info = array();
	public $co_listed = array();
	
	// ACTIVATION
	public $facebook = '';
	public $twitter = '';
	public $short_url = '';
	
	// LOCATION BEACONS
	public $location_beacons;
	
	// MEDIA
	public $flyer = '';
	public $images = array();
	
	// CONSTRUCTOR
	public function __construct($id='') {
		$this->update_class($id);
	} // END CONSTRUCTOR 
	
	// GET ADDRESS
	public function get_address() {
		update_class($this->id);
		$the_address = array('address' => $this->address, 'latitude' => $this->latitude, 'longitude' => $this->longitude);
		return $the_address;
	} // END GET ADDRESS
	
	
	//UPDATE CLASS 
	public function update_class($id = '') {
		$this->id = $id;
	}// END UPDATE CLASS
	
	//GET PROPERTY DETAILS
	public function get_property_details() {
		$this->update_class($this->id); // GET UPDATED INFO FROM DATABASE
		
		// RETURN UPDATED CLASS INFORMATION FROM DATABASE
		$property_details = array();
		$property_details['id'] = $this->id;
		return $property_details;	
		
	} // END GET PROPERTY DETAILS
	
}
*/



class subscription {
	
	protected $dbc;
	private $q;
	private $r;
	private $row;
	
	public $num_rows;
	
	public $user_id;
	public $subscribe_level;
    public $allowed_prop;
    public $amount;
    public $active;
    public $deactivate_in;
    public $current_period_start;
    public $current_period_end;
    public $trial_start;
    public $trial_end;
    public $rental;
    public $free;
    public $days_left;
    public $days;
    public $name;
    public $stripe_id;
    public $stripe_trial;
    public $total_properties;
    public $stripe_amount;
    public $stripe_interval;
    public $stripe_name;
	
	
	
	function __construct($user_id, $dbc) {
		$this->user_id = $user_id;
		$this->dbc = $dbc;
		
		$q ="SELECT 
			subscription.*, 
			TIMESTAMPDIFF(DAY, NOW(), subscription.current_period_end) + 1 as days_left, 
			TIMESTAMPDIFF(DAY, subscription.current_period_start, current_period_end) as days, 
			
			subscription_types.id, 
			subscription_types.name, 
			subscription_types.alias, 
			subscription_types.stripe_id, 
			subscription_types.stripe_trial, 
			subscription_types.total_properties, 
			subscription_types.stripe_amount, 
			subscription_types.stripe_interval, 
			subscription_types.stripe_name, 
			subscription_types.rental, 
			subscription_types.updated, 
			subscription_types.deactivated, 
			subscription_types.free as subscription_type_free
			
			FROM 
			subscription 
			
			LEFT JOIN subscription_types 
			ON subscription_types.stripe_id = subscription.subscribe_level 
			
			WHERE 
			subscription.user_id = $user_id";
			
		$r = @mysqli_query ($dbc, $q);
		
		if(mysqli_num_rows($r) == 0) {
			$this->num_rows = 0;
		}
		else {
			$this->num_rows = mysqli_num_rows($r);
		}
		$this->row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$this->subscribe_level = $this->row['subscribe_level'];
    	$this->allowed_prop = $this->row['allowed_prop'];
    	$this->amount = $this->row['amount'];
    	$this->active = $this->row['active'];
    	$this->deactivate_in = $this->row['deactivate_in'];
    	$this->current_period_start = $this->row['current_period_start'];
    	$this->current_period_end = $this->row['current_period_end'];
    	$this->trial_start = $this->row['trial_start'];
    	$this->trial_end = $this->row['trial_end'];
    	$this->rental = $this->row['rental'];
    	$this->free = $this->row['free'];
    	$this->days_left = $this->row['days_left'];
    	$this->days = $this->row['days'];
    	$this->name = $this->row['name'];
    	$this->stripe_id = $this->row['stripe_id'];
    	$this->stripe_trial = $this->row['stripe_trial'];
    	$this->total_properties = $this->row['total_properties'];
    	$this->stripe_amount = $this->row['stripe_amount'];
    	$this->stripe_interval = $this->row['stripe_interval'];
    	$this->stripe_name = $this->row['stripe_name'];
    	
		if($this->free!=1) {
			$current_period_end = strtotime($this->current_period_end);
			$now = strtotime(date('Y-m-d H:i:s'));
			$activated_time = $current_period_end - $now;
			if ($activated_time <= 0) { 
				$q_deactivate = "UPDATE subscription SET active = 0 WHERE user_id = $user_id";
				$r_deactivate = @mysqli_query($dbc, $q_deactivate);
				$this->free = 0;
			}
		}
			
	}
	
	function get_query() {
		return $this->row;
	}
	function get_user_id() {
		return $this->user_id;
	}
	
	
}



/*
class user {
	
	protected $dbc;
	private $q;
	private $r;
	private $row;
	
	public $user_id;
	public $first_name;
	public $last_name;
	public $email;
	public $user_level;
	public $stripe_id;
	public $listing_name;
	public $listing_email;
	public $listing_phone;
	public $listing_brokerage;
	public $broker_website;
	public $auto_email;
	public $show_hints;
	public $registration_date;
	public $show_tour;
	
	public $ship_street;
	public $ship_city;
	public $ship_state;
	public $ship_zip;
	
	public $bill_street;
	public $bill_city;
	public $bill_state;
	public $bill_zip;
	
	public $address_count;
	
	public $card_id; 
	public $card; 
	public $card_type;
	public $exp_month; 
	public $exp_year;
	public $stripe_card_id;
	
	
   
	function __construct($user_id, $dbc) {
		$this->user_id = $user_id;
		$this->dbc = $dbc;
		
		$q ="SELECT users.*, 
			
			users.user_id, 
		
			shipping.street as ship_street, 
			shipping.city as ship_city, 
			shipping.state_name as ship_state, 
			shipping.zip as ship_zip, 
			shipping.user_id,  
			
			billing.street as bill_street, 
			billing.city as bill_city, 
			billing.state_name as bill_state, 
			billing.zip as bill_zip, 
			billing.user_id, 
			
			MAX(cards.id) as card_id, 
			cards.card, 
			cards.card_type, 
			cards.exp_month, 
			cards.exp_year, 
			cards.stripe_card_id, 
			
			COUNT(*) as addresses, 
			addresses.user_id 
			
			FROM 
			users, 
			addresses 
			
			LEFT JOIN shipping 
			ON addresses.user_id = $user_id and addresses.shipping_id = shipping.shipping_id AND shipping.user_id = $user_id 
			
			LEFT JOIN billing 
			ON addresses.user_id = $user_id and addresses.billing_id = billing.billing_id AND billing.user_id = $user_id 
			
			LEFT JOIN cards 
			ON cards.user_id = $user_id
			 
			WHERE 
			users.user_id = $user_id 
			AND addresses.user_id = $user_id";
			
		$r = @mysqli_query ($dbc, $q);
		
		$this->row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$this->user_id = $this->row['user_id'];
		$this->first_name = $this->row['first_name'];
		$this->last_name = $this->row['last_name'];
		$this->email = $this->row['email'];
		$this->user_level = $this->row['user_level'];
		$this->stripe_id = $this->row['stripe_id'];
		$this->listing_name = $this->row['listing_name'];
		$this->listing_email = $this->row['listing_email'];
		$this->listing_phone = $this->row['listing_phone'];
		$this->listing_brokerage = $this->row['listing_brokerage'];
		$this->broker_website = $this->row['broker_website'];
		$this->auto_email = $this->row['auto_email'];
		$this->show_hints = $this->row['show_hints'];
		$this->registration_date = $this->row['registration_date'];
		$this->show_tour = $this->row['show_tour'];
		
		// ADDRESSSES
		
		$this->ship_street = $this->row['ship_street']; 
		$this->ship_city = $this->row['ship_city'];
        $this->ship_state = $this->row['ship_state'];
        $this->ship_zip = $this->row['ship_zip']; 
  
        $this->bill_street = $this->row['bill_street']; 
        $this->bill_city = $this->row['bill_city'];
        $this->bill_state = $this->row['bill_state'];
        $this->bill_zip = $this->row['bill_zip']; 
		
		$this->address_count = $this->row['addresses'];
		
		$this->card_id = $this->row['card_id']; 
		$this->card = $this->row['card'];
		$this->card_type = $this->row['card_type'];
		$this->exp_month = $this->row['exp_month'];
		$this->exp_year = $this->row['exp_year'];
		$this->stripe_card_id = $this->row['stripe_card_id'];
			
	}
	
	
	
	function get_query() {
		return $this->row;
	}
	function get_user_id() {
		return $this->user_id;
	}
	function get_first_name() {
		return $this->first_name;
	}
	function get_last_name() {
		return $this->last_name;
	}
	function get_full_name() {
		return $this->first_name . ' ' . $this->last_name;
	}
	function get_email() {
		return $this->email;
	}
	function get_user_level() {
		return $this->user_level;
	}
	function get_stripe_id() {
		return $this->stripe_id;
	}
	function get_listing_name() {
		return $this->listing_name;
	}
	function get_listing_email() {
		return $this->listing_email;
	}
	function get_listing_phone() {
		return $this->listing_phone;
	}
	function get_listing_brokerage() {
		return $this->listing_brokerage;
	}
	function get_broker_website() {
		return $this->broker_website;
	}
	function get_auto_email() {
		return $this->auto_email;
	}
	function get_show_hints() {
		return $this->show_hints;
	}
	function show_hints() {
		return $this->show_hints;
	}
	function get_registration_date() {
		return $this->registration_date;
	}
	
	function get_card() {
		$card = array('card_id'=>$this->card_id, 'card'=>$this->card, 'card_type'=>$this->card_type, 'exp_month'=>$this->exp_month, 'exp_year'=>$this->exp_year, 'stripe_card_id'=>$this->stripe_card_id);
		return $card; 
	}
	
}*/

function mc_request( $type, $target, $data = false )
{
	//MAILCHIMP 3.0 API
	
	$api = array
	(
		'login' => 'User',
		'key'   => 'd80bb8f7aaff60462caca67f2107fbf7-us11',
		'url'   => 'https://us11.api.mailchimp.com/3.0/'
	);
	
	$ch = curl_init( $api['url'] . $target );
 
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array
	(
		'Content-Type: application/json', 
		'Authorization: ' . $api['login'] . ' ' . $api['key'],
//		'X-HTTP-Method-Override: ' . $type,
	) );
 
//	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $type );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_USERAGENT, 'YOUR-USER-AGENT' );
 
	if( $data )
		curl_setopt( $ch, CURLOPT_POSTFIELDS,  $data  );
 
	$response = curl_exec( $ch );
	curl_close( $ch );
 
	return $response;
}

?>