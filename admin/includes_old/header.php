<?php

if(isset($user_id) && isset($dbc)) {
	$Subscription = new subscription($user_id, $dbc);
	$UserInfo = new user($user_id, $dbc);
}

var_dump($UserInfo);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<script type="text/javascript" src="/js/jquery-1.11.0.min.js" ></script>
<script type="text/javascript" src="/js/jquery-ui-1.10.4.custom.min.js" ></script>
<script type='text/javascript' src='/js/knockout-3.4.0.js'></script>
<script type="text/javascript" src="/js/jquery.sticky.js"></script>
<script type="text/javascript" src="/js/jquery.steps.min.js"></script>
<script type="text/javascript" src="/js/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="/js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="/js/intro.js"></script>

<script type="text/javascript" src="https://checkout.stripe.com/checkout.js"></script>



<title><?php echo $page_title . ' | ' . $site; ?></title>

<link rel="icon" type="image/png" href="/favicon.ico" />

<link rel="apple-touch-icon-precomposed" href="/ios-icon.png" />
<link rel="apple-touch-icon" href="/ios-icon.png" />
<link rel="apple-touch-icon" sizes="76x76" href="/ios-icon-ipad.png" />
<link rel="apple-touch-icon" sizes="120x120" href="/ios-icon-iphone-retina.png" />
<link rel="apple-touch-icon" sizes="152x152" href="/ios-icon-ipad-retina.png" />


<meta name="viewport" content="initial-scale=1, maximum-scale=1" />


<link href="/css/introjs.css" rel="stylesheet" type="text/css" />
<link href="/css/bootstrap.css" rel="stylesheet" type="text/css" />

<link href="/css/styles.css" rel="stylesheet" type="text/css" />


<!-- STEPS SECTION -->
<link href="/css/steps.css" rel="stylesheet" type="text/css" />
<link href="/css/steps.main.css" rel="stylesheet" type="text/css" />
<link href="/css/steps.normalize.css" rel="stylesheet" type="text/css" />


<link href="/css/loaders.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="/css/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<link href="/css/mobile.css" rel="stylesheet" type="text/css" />

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52545347-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>
<?php 
	if(isset($user_id)) {
		require_once('includes/admin_nav.php');	
	}
	else {
		require_once('includes/login_nav.php');	
	}
	if(isset($user_id)) {
		
			if($Subscription->num_rows == 1) {
				if($Subscription->active == 1) {
					if ($Subscription->deactivate_in == 1 &&  $Subscription->free != 1) {
						echo '<div id="active_inactive" class="account_inactive">';
							echo 'Your account is active, but will deactivate in ' . $Subscription->days_left . ' days, <div id="activate">Reactivate your account</div>';
						echo '</div>'; // END ACTIVE INACTIVE
					}
					else {
						echo '<div id="active_inactive" >';
						echo '</div>'; // END ACTIVE INACTIVE
						if($UserInfo->need_show_tour() == 1) {
							if($page_title == 'Control Panel') {
								include('_content/take_tour_modal.php');
							}
						}
					}
					
				}
				if($Subscription->active == 0 &&  $Subscription->free != 1) {
					echo '<div id="active_inactive" class="account_inactive">';
						echo 'Your account is inactive, <div id="activate">Activate your account</div>';
					echo '</div>'; // END ACTIVE INACTIVE
				
				}
			}
			else {
				
				if($page_title == 'Select a Subscription') {
					
					/* echo '<div id="active_inactive" class="account_inactive">';
						echo '<div id="select_subscription">Please Choose a Subscription Level</div>';
					echo '</div>'; // END ACTIVE INACTIVE 
					*/
				}
				else {
					if( $Subscription->free != 1) {
						if($page_title != 'Control Panel') {
							echo '<div id="active_inactive" class="account_inactive">';
								echo '<div id="select_subscription">You do not have a subscription please <a href="/admin/select_subscription">start your subscription</a></div>';
							echo '</div>'; // END ACTIVE INACTIVE
						}
						else { // CONTROL PANEL MODAL POPUP
							include('_content/subscription_modal.php');
						}
					}
				}
			}
		
	} // END USER ID CHECK
	
?>

<script type="text/javascript">
 $(window).load(function(){
  $("#property_beacons").sticky({ topSpacing: 0});
   
   $('input, textarea').placeholder();
   
    function checkWidth() {
        var windowsize = $(window).width();
		
        if (windowsize < 1025) {
            
			$( '.tooltip' ).tooltip( "disable" );
			$( '.edit_info_button img' ).tooltip( "disable" );
			$( '.add_child_info_show' ).tooltip( "disable" );
			$( '.add_info_show' ).tooltip( "disable" );
			$( '.delete_info_button img' ).tooltip( "disable" );
			$( '.active_beacon' ).tooltip( "disable" );
			$(".caution").tooltip( "disable" );
			$('.remove_beacon_image').tooltip( "disable" );
				
        }
		
    }
    // Execute on load
    checkWidth();
    // Bind event listener
    $(window).resize(checkWidth);
  
});
</script>

<div id="contain_all">


<div id="content">

