<?php
	// $customer = 'cus_4isndMlWV4VkHT'; // REMOVE
	require_once('includes/config.inc.php');
	
	Stripe::setApiKey(STRIPE_SECRET);
	
	date_default_timezone_set("UTC"); 
	
	// Retrieve the request's body and parse it as JSON
	$body = @file_get_contents('php://input');
	$event_json = json_decode($body);
if (!empty($event_json)) {
	$stripe_vars = get_object_vars ( $event_json );
	
	$type = $stripe_vars['type'];
	$event_id = $stripe_vars['id'];
	
	if($type == 'charge.succeeded') {
		include('webhook_process/charge_succeeded.php');
	}
	else if ($type == 'invoice.payment_succeeded') {
		include('webhook_process/invoice_payment_succeeded.php');
	}
	else if ($type == 'invoice.payment_failed') {
		include('webhook_process/invoice_payment_failed.php');
	}
	else if ($type == 'customer.subscription.updated') {
		include('webhook_process/customer_subscription_updated.php');
	}
	else if ($type == 'customer.subscription.deleted') {
		include('webhook_process/customer_subscription_deleted.php');
	}
	else if ($type == 'customer.subscription.trial_will_end') {
		include('webhook_process/customer_subscription_trial_will_end.php');	
	}
	else if ($type == 'invoice.created') {
		include('webhook_process/invoice_created.php');	
	}
	
} // END CHECK FOR STRIPE VAR OBJECT
	
header("HTTP/1.1 200 OK");
exit();

?>

