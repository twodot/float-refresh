<?php
$page_title = $page_name ='Login';
login_forward();
echo'<div id="bumper">';
include('includes/header.php');

if (isset($_POST['submitted'])) {
	$errors = array();
	if (!empty($_POST['email'])){
		$e=$dbc->real_escape_string($_POST['email']);
	}
	else {
		$e=FALSE;
		
		$errors[] = '<p>You forgot to enter your email address!</p>';
	}
	
	if (!empty($_POST['pass'])) {
		$p = $dbc->real_escape_string($_POST['pass']);
	}
	else {
		$p = FALSE;
		
		$errors[] = '<p>You forgot to enter your password!</p>';
	}
	
	if ($e && $p) {
		$q = "SELECT user_id, first_name, last_name, user_level, email FROM users WHERE (email='$e' AND pass=SHA1('$p')) AND active IS NULL";
		
		$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
		
		if (@mysqli_num_rows($r) == 1 ) {
			session_start();
			$_SESSION['login'] = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			mysqli_free_result($r);
			
			mysqli_close($dbc);
			
			$url = BASE_URL . 'panel';
			
			ob_end_clean();
		
			header("Location: $url");
			
			exit();
		}
		else {
			$errors[] = '<p>Either the email address and password do not match those on file or you have not activated your account.</p><br />
				<p>Forgot your password? <a href="/forgot_password" class="body_link" title="Password Retrieval">Retrieve Your Password Here</a></p>';
		}
	}
	else {
		$errors[] = '<h3>Please try again</h3>
		<p>Forgot your password?<br /><a href="/forgot_password" class="body_link" title="Password Retrieval">Retrieve Your Password Here</a></p>';
	}
	
	mysqli_close($dbc);
	
	if (!empty($errors)) {
		echo '<div class="access error">';
			foreach($errors as $key => $val) {
				echo $val;
			}
		echo '</div>';
	}

	
}
?>

    <div id="login"><h3>Login</h3>

		<form action="login" method="post">
   	 	<p class="forms"><input type="text" name="email" size="20" maxlength="80" <?php if(isset($e)) {echo 'value="' . $e . '"'; }?> placeholder="Email" /> </p>
    	<p class="forms"><input type="password" name="pass" size="20" maxlength="20" placeholder="password" /> </p>
        <p class="forms"><input type="submit" name="submit" value="Login" /> </p>
    	<input type="hidden" name="submitted" value="TRUE" />
    	</form>
 </div> 
<?php echo '</div>'; // END BUMPER ?>
 <?php
 include('includes/footer.php');
 ?>