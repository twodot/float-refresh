<?php
require_admin_login();

if (isset($_GET['tracking'])) {
	if(isset($_SESSION['pseudo_login']['id'])) {
		$order_id = $_GET['order_id'];
		$customer_id = (int) $_SESSION['pseudo_login']['id'];
		$q = "SELECT id, tracking FROM beacon_orders WHERE user_id = $customer_id AND id = '$order_id'  LIMIT 1";
		$r = mysqli_query($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		if (empty($row['tracking']) ) {
			$order_id = $row['id'];
			$tracking_number = $_GET['tracking'];
			
			
				$q = "
				UPDATE 
				beacon_orders 
				
				SET 
					tracking = '$tracking_number', 
					date_shipped = NOW() 
				
				WHERE 
				id = $order_id
				
				";
				
				$r = mysqli_query($dbc, $q);
				
				$q_s_id ="
					SELECT 
					*,  
					users.user_id as user_id, 
					shipping.user_id, 
					
					shipping.street as ship_street, 
					shipping.city as ship_city, 
					shipping.state_name as ship_state, 
					shipping.zip as ship_zip, 
		
					billing.street as bill_street, 
					billing.city as bill_city, 
					billing.state_name as bill_state, 
					billing.zip as bill_zip
		
					
					FROM 
					users as users
					
					LEFT JOIN shipping ON shipping.user_id = $customer_id 
					LEFT JOIN billing ON billing.user_id = $customer_id 
					
					WHERE 
					users.user_id = $customer_id ";
					$r_s_id = @mysqli_query ($dbc, $q_s_id);
					$row_s_id = mysqli_fetch_array($r_s_id, MYSQLI_ASSOC);
					$user_email = $row_s_id['email'];
					$first_name = $row_s_id['first_name'];
					$last_name = $row_s_id['last_name'];
					
					$shipping_address = $row_s_id['ship_street'] . '<br />' . $row_s_id['ship_city'] . ', ' . $row_s_id['ship_state'] . ', ' . $row_s_id['ship_zip'];
					
					//$user_email = 'josh@twodotmarketing.com';
				
				
				include(MAIL_SALES);
				
				include('email_templates/order_shipped.php');
				$HTMLEmailoutput = ob_get_contents();
				ob_clean();

				$mail_sales->FromName = 'Order From Float';
				$mail_sales->addAddress($user_email);     // Add a recipient
				$mail_sales->addReplyTo('orders@' . EMAIL_URL, 'Order From Float');
				
				$mail_sales->WordWrap = 50;                                 // Set word wrap to 50 characters
				$mail_sales->isHTML(true);                                  // Set email format to HTML
				
				$mail_sales->Subject = 'Order #' . $order_id . ' Has Been Shipped';
				
				$mail_sales->Body    = $HTMLEmailoutput;

				$mail_sales->send();
				
				?>
				
				$('#tracking_number_added').html('<h3>Tracking number</h3><p><?php echo $tracking_number; ?></p>');
                $("#add_tracking_number").fadeOut(1000);
                $('#tracking_number_added').fadeIn(1000);
                
				<?php
				
		}// END BEACONS ORDERED COUNT
		
	}// END PSEUDO LOGIN
	else {
		$url = BASE_URL . 'orders';
		//header("Location: $url");
		exit();
	}
} // UID ID CHECK
else {
	$url = BASE_URL . 'panel';
	//header("Location: $url");
	exit();
}

?>

