<?php

require_once('includes/config.inc.php');
require_login();

if(isset($_GET['submitted'])) {
	
	$title = $dbc->real_escape_string($_GET['title']);
	$text = $dbc->real_escape_string($_GET['info']);
	
	if(isset($_GET['property_info_link'])) {
		if(!empty($_GET['property_info_link'])) {
			$link = preg_replace('/^(?!https?:\/\/)/', 'http://', $_GET['property_info_link']);
			$link=$dbc->real_escape_string($link);
		}
		else {
			$link = '';
		}
	}
	else {
		$link = '';
	}
	
	if (isset($_GET['property_info_link_text'])) {
		$link_text=$dbc->real_escape_string($_GET['property_info_link_text']);
	}
	else {
		$link_text = '';
	}
	
	if (isset($_GET['go_to_link_automatically']) && !empty($link)) {
		$automatic = (int) $_GET['go_to_link_automatically'];
	}
	else {
		$automatic = (int) 0;
	}
	if(isset($_GET['notes'])) {
		$notes = $dbc->real_escape_string($_GET['notes']);
	}
	else {
		$notes = '';
	}
	$property = $dbc->real_escape_string($_GET['p']);
	$info_id = $dbc->real_escape_string($_GET['i']); 
	
	$title = htmlspecialchars ($title, ENT_QUOTES);
	$text = htmlspecialchars ($text, ENT_QUOTES);
	
	
	
	$notes = htmlspecialchars ($notes, ENT_QUOTES);
	if($_GET['u'] == $user_id) {
		$q = "
		UPDATE 
		property_info 
		
		SET 
		title = '$title', 
		text = '$text', 
		notes = '$notes', 
		link_beacon = $automatic, 
		link_beacon_text = '$link_text', 
		link_beacon_link = '$link' 
		
		WHERE 
		id = $info_id 
		
		";
		if($r = @mysqli_query ($dbc, $q)) {
			
			?>
			
			var container = $("#<?php echo 'info_feature_' . $info_id; ?>");
			
            $(container).find(".info_title").html('<?php echo $title; ?>');
			$(container).find(".edit_info_title").hide(300);
            $(container).find(".info_title").show(300);
            $(container).find(".edit_info_title").val('<?php echo $title; ?>');
			
			$(container).find(".prop_revisions").show(300);
			
			$(container).find(".edit_media").show(300);
			
			$(container).find(".beacon_title_holder").show(300);
			
            $(container).find(".text p").html('<?php echo $text; ?>');
			$(container).find(".text p").show(300);
			$(container).find(".edit_info_box").hide(300);
            $(container).find(".edit_info_box").val('<?php echo $text; ?>');
			
			
            $(container).find(".notes_text").html('<?php echo $notes; ?>');
			$(container).find(".notes_text").show(300);
			$(container).find(".edit_notes_box").hide(300);
            $(container).find(".edit_notes_box").val('<?php echo $notes; ?>');
			
			$(container).find(".edit_save").hide(300);
			$(container).find(".edit_cancel").hide(300);
            
            
            $(container).find(".property_information_link_notification").show(300);
            
            <?php
				if($automatic == 1) {
					?> $(container).find(".link_beacon_automatic_notification").html('This link will automatically open in place of the property highlight screen'); <?php
				}
				else {
					?> $(container).find(".link_beacon_automatic_notification").html('This link will NOT automatically open in place of the property highlight screen'); <?php
				}
				if(empty($link)) {
					?>
                    
                    $(container).find(".link_beacon_automatic_notification").hide(300); 
                    $(container).find(".link_notification_warning").html('You have not added a link.');
            		$(container).find(".link_notification_warning").show(300);
                    
                    $(container).find(".property_info_link_input").val('');
                    $(container).find(".go_to_link_automatically").attr('checked', false);
           			$(container).find(".link_beacon_link_holder").html('');
                    
                    
					<?php
				}
				else {
					?>
					$(container).find(".link_beacon_automatic_notification").show(300); 
                    $(container).find(".link_notification_warning").html('');
		            $(container).find(".link_notification_warning").hide(300);
                    $(container).find(".property_info_link_input").val('<?php echo $link; ?>');
            		$(container).find(".link_beacon_link_holder").html('<a href="<?php echo htmlspecialchars($link,ENT_QUOTES); ?>" target="_blank"><?php echo $link_text; ?></a>');

                    <?php
				}
			?>
            
			$(container).find(".property_information_link_form").hide(300);
			
			<?php
		}//END INSERT CHECK 
		else {
			
		}
	}
	
} // END SUBMITTED CHECK

?>

