<?php

require_login();
$page_title = $page_name = 'Select a Subscription';

include('includes/header.php');
if(isset($_POST['stripeToken'])) {
	$token = $_POST['stripeToken'];
	
	
}

// SELECT FOR PROPERTY PRIMARY INFO

require_once('./lib/Stripe.php');
Stripe::setApiKey(STRIPE_SECRET);

$subs = Stripe_Plan::all();
$get_subs = $subs['data'];
//print_r($get_subs);
$stripe_plans = array();
foreach ($get_subs as $key ) {
	$key['id'];
	$id =  $key['id'];
	$amount = $key['amount'];
	$trial = $key['trial_period_days'];
	$interval = $key['interval'];
	
	$stripe_plans[$id] = array('id' => $id, 'amount' => $amount, 'trial' => "$trial", 'interval' => "$interval");
	
	$q= "UPDATE subscription_types SET stripe_trial = $trial, stripe_amount = $amount, stripe_interval = '$interval' WHERE stripe_id = $id";
	$r = @mysqli_query ($dbc, $q);
	
	
}

?>

<script>
	
  var handler = StripeCheckout.configure({
	key: '<?php echo STRIPE_API; ?>',
	image: '<?php echo IMAGES; ?>logo-126.jpg',
	token: function(token, args) {
		
		var dataString = JSON.stringify(args);
		 
		 $.ajax({
			type: 'POST',
			url: "/save_addresses", 
			data: { address: dataString } , 
			cache: false,
		}).done(function(data) {
			ga('send', 'event', 'Users', 'Subscription', 'User Subscribe Completed', '0');
			window.location.replace('/activate_subscription?t='+token.id + '&s=' + subscription);
		});
		
	}, 
	name: 'Float',
	email: '<?php echo $user_email; ?>', 
	allowRememberMe: false, 
	panelLabel: "Subscribe", 
	currency: "USD", 
	billingAddress: true
  });
  
</script>

<div id="error">
</div>
<div class="get_started_guide">
    <h2>Let&rsquo;s Get Started</h2>
    <h3>Step 1: Select a subscription</h3>
</div>
<?php
	$q ="SELECT COUNT(*) as subscription FROM subscription WHERE user_id = $user_id";
	$r = @mysqli_query ($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	if($row['subscription'] == 0) {
	
	$q =" SELECT * FROM subscription_types WHERE stripe_id != 100 AND free = 0 AND deactivated = 0 ORDER BY rental, stripe_id ASC";
	$r = @mysqli_query ($dbc, $q);
	$plans = array();
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		$stripe_id = $row['stripe_id'];
		
		$plans[] =  array(
		'stripe_id' => $stripe_id, 
		'name' => $row['name'], 
		'rental' =>$row['rental'], 
		'price' => $stripe_plans[$stripe_id]['amount'] / 100, 
		'interval'=>$stripe_plans[$stripe_id]['interval'], 
		'alias' => $row['alias'], 
		'trial' =>$row['stripe_trial'], 
		'total_properties' => $row['total_properties']);
	}
	echo '<ul class="subscription_table">';
		echo '<li></li>';
			foreach ($plans as $plan) {
				echo '<li';
					if($plan['rental'] == 1) {
						echo ' class="property_management_subscription"';
					}
					else {
						echo ' class="header_subscription"';
					}
				echo '>';
					echo '<p class="plan_alias">' . $plan['alias'] . '</p>';
					echo '<h3>';
					if($plan['rental'] == 1) {
						echo 'Property Managers';
					}
					else {
						echo 'Real Estate Agents';
					}
					echo '</h3>';
					
					echo '<h1>';
						echo '$' . $plan['price'];
					echo '</h1>';
					echo '<p>';
						echo 'Per ' . ucwords($plan['interval']);
					echo '</p>';
				echo '</li>';
			}
		echo '</ul>';
		echo '<ul class="subscription_table feature_line_subscription">';
			echo '<li class="feature_subscription subscription_callout"><p>30 Day Free Trial</p></li>';
			foreach ($plans as $plan) {
				echo '<li>';
				
					if($plan['trial']) {
						echo '<img src="https://s3-us-west-2.amazonaws.com/images.listfloat.com/assets/check_mark.png" alt="Check Mark" />';	
					}
					else {
						echo '<img src="https://s3-us-west-2.amazonaws.com/images.listfloat.com/assets/cross_mark.png" alt="Cross Mark" />';
					}
				echo '</li>';
			}
		
	echo '</ul>';
	
	echo '<ul class="subscription_table feature_line_subscription">';
			echo '<li class="feature_subscription"><p>Active Properties</p></li>';
			foreach ($plans as $plan) {
				echo '<li>';
						if($plan['total_properties'] == 0 ) {
							echo '<p>Unlimited</p>';
						}
						else {
							echo '<p>' . $plan['total_properties'] . '</p>';
						}
					
				echo '</li>';
			}
		
	echo '</ul>';
	
	echo '<ul class="subscription_table feature_line_subscription">';
			echo '<li class="feature_subscription"><p>Beacon Capacity</p></li>';
			foreach ($plans as $plan) {
				echo '<li>';
					echo '<p>Unlimited</p>';
						
				echo '</li>';
			}
	echo '</ul>';
	
	echo '<ul class="subscription_table choose_subscription_section">';
			echo '<li class="choose_subscription_li"></li>';
			foreach ($plans as $plan) {
				echo '<li>';
					echo '<form action="/activate_subscription"><button type="submit" id="select_sub' . $plan['stripe_id'] . '" class="select_sub">Choose</button></form>';
						
				echo '</li>';
			}
	echo '</ul>';
	echo '<script>';
	foreach ($plans as $plan) {
			$stripe_id = $plan['stripe_id'];
		?>
        	
            document.getElementById('select_sub<?php echo $stripe_id; ?>').addEventListener('click', function(e) {
            
            subscription = <?php echo $stripe_id; ?>
            
            
            handler.open({
              description: '<?php if ($stripe_plans[$stripe_id]['id'] == 1) { echo 'Unlimited'; } else { echo $plan['total_properties']; } echo ' Properties | $' . $stripe_plans[$stripe_id]['amount'] / 100 . ' / ' . $stripe_plans[$stripe_id]['interval']; ?>',
              amount: <?php echo $stripe_plans[$stripe_id]['amount']; ?>, 
              shippingAddress: true, 
              billingAddress: true, 
              closed: function() {$('.select_sub').text('Choose')}
             
            });
            e.preventDefault();
            });
			
		<?php
	}
	echo '</script>';
	/*$r = @mysqli_query ($dbc, $q);
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		echo '<div id="subscription_' . $row['id'] . '" class="subscription_levels">';
				$stripe_id = $row['stripe_id'];
				echo '<div class="current_sub"></div>';
				echo '<div class="sub_headline">';
					echo '<h4>' . $row['name'] . '</h4>';
				echo '</div>'; // END SUB HEADLINE
				echo '<div class="sub_info">';
					if($row['rental'] == 1) {
						echo '<h5>For Property Management</h5>';
					}
					else  {
						echo '<h5>For Real Estate Agents</h5>';
					}
					echo '<p>$' . $stripe_plans[$stripe_id]['amount'] / 100 . ' / ' . $stripe_plans[$stripe_id]['interval'] . ' </p><p>You can order beacons <br />in the next step.</p>';
					
					//echo '<div id="select_sub_' . $row['id'] . '" class="choose">CHOOSE</div>';
				echo '</div>'; // END SUB INFO
				echo '<form action="/activate_subscription"><button type="submit" id="select_sub' . $stripe_id . '" class="select_sub">Start Subscription</button></form>';
				?>
				<script>
				  document.getElementById('select_sub<?php echo $stripe_id; ?>').addEventListener('click', function(e) {
					
					subscription = <?php echo $stripe_id; ?>
					
					
					handler.open({
					  description: '<?php if ($stripe_plans[$stripe_id]['id'] == 1) { echo 'Unlimited'; } else { echo $stripe_plans[$stripe_id]['id']; } echo 'Properties | $' . $stripe_plans[$stripe_id]['amount'] / 100 . ' / ' . $stripe_plans[$stripe_id]['interval']; ?>',
					  amount: <?php echo $stripe_plans[$stripe_id]['amount']; ?>, 
					  shippingAddress: true, 
					  billingAddress: true, 
					  closed: function() {$('.select_sub').text('Start Subscription')}
					 
					});
					e.preventDefault();
				  });
				</script>
				
				<?php
		echo '</div>'; // END SUBSCRIPTION
	}
	// CUSTOM SUBSCRIPTION SECTION 
	/* echo '<div id="subscription_' . $row['id'] . '" class="subscription_levels">';
				$stripe_id = $row['stripe_id'];
				echo '<div class="sub_headline">';
					echo '<h4>Custom Plan?</h4>';
				echo '</div>'; // END SUB HEADLINE
				echo '<div class="sub_info">';
					echo '<p>For More Properties</p>';
					echo '<p><a href="mailto:' . SALES . '">Please contact sales</a> </p>';
				echo '</div>'; // END SUB INFO
		echo '</div>'; // END SUBSCRIPTION
		*/
	}
	else {
		echo '<p>You have reached this page in error</p>';
	}

include('includes/footer.php');



?>