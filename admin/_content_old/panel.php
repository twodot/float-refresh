<?php

require_login();
$page_title = $page_name = 'Control Panel';

include('includes/header.php');

include('_content/take_the_tour.php');

?>
<script type="text/javascript">
$( document ).ready(function() {
	$('#add_button').click(function(){
		$('#add_button').slideUp(300);
		$(".start_here").slideUp(300);
		$('#add_property').slideDown(300);		
	})
	$('#save').click(function(e){
		 e.preventDefault();
		if($("#property_title").val().length > 0){
			$.ajax({
				url: "add_property", 
				data: jQuery("#property_addition").serialize(),
				dataType:"script", 
				cache: false,
			}).done(function(data) {
				
			});
		}
	})
	
	
	<?php
	
	if(isset($_GET['highlight'])) {
		$highlight = (int) $_GET['highlight'];
		if($highlight != 0) {
			?>
			$( "#b_con_<?php echo $highlight; ?>" ).effect( "slide", "slow" );
			<?php
		}
	}
	
	?>
	
});

</script>
<?php

// SELECT FOR PROPERTY PRIMARY INFO


?>


<div id="error">
</div>
<div id="dialog">
</div>

<?php

//$q =" SELECT *, TIMESTAMPDIFF(DAY, NOW(), days_left) + 1 as days_left FROM subscription, subscription_types WHERE subscription.user_id = $user_id AND subscription.subscribe_level = subscription_types.id";
//$r = @mysqli_query ($dbc, $q);
	
/* if( mysqli_num_rows($r) == 1) {
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	if($row['active'] == 1) {
		if ($row['deactivate_in'] == 1) {
			echo '<div id="active_inactive" class="account_inactive">Your account is active and will deactivate in ' . $row['days_left'] . ' days, <div id="activate">Activate your account</div></div>';
			echo '<div id="days_left">You have ' . $row['days_left'] . ' days remaining before your account deactivates</div>';
		}
		
	}
	if($row['active'] == 0) {
	
		echo '<div id="active_inactive" class="account_inactive" class="account_inactive">Your account is inactive, <div id="activate">Activate your account</div></div>';
		echo '<div id="current_subscription">Previous Subscription - ' . $row['allowed_prop'] . ' Properties | ' . $row['days'] . ' Days</div>';
	
	}
}
else {
	echo '<div id="select_subscription">Please <a href="/select_subscription">select a subscription</a>.</div>';
}*/



?>
<div class="col-xs-12">
	
    <div id="add_button">Add a new property</div>

    <div id="add_property">
        <form id="property_addition">
            <input type="text" id="property_title" value="" name="title" placeholder="Title" maxlength="50" />
            <input type="text" id="price" value="" name="price" placeholder="Price" />
            <input type="hidden" name="u" value="<?php echo $user_id; ?>" />
            <input type="submit" id="save_property" name="Submit" value="SAVE" />
            <img src="/images/loading.gif" id="save_property_loader" class="loading_gif hidden_loader" />
            <input type="hidden" name="submitted" value="true" />
        </form> 
    </div>
</div><!-- END COL-XS-12 -->

<!--  
<div id="add_property">
	
	<form id="property_addition">
    	<input type="text" id="property_title" value="" name="title" placeholder="Title" maxlength="50" />
        <input type="text" id="price" value="" name="price" placeholder="Price" />
        <input type="hidden" name="u" value="<?php echo $user_id; ?>" />
        <input type="submit" id="save_property" name="Submit" value="SAVE" />
        <img src="/images/loading.gif" id="save_property_loader" class="loading_gif hidden_loader" />
        <input type="hidden" name="submitted" value="true" />
    </form> 
</div>
<!-- END ADD PROPERTY -->


<?php



echo '<div class="col-xs-12">';
	echo '<div id="download_marketing"><a href="https://s3-us-west-2.amazonaws.com/images.listfloat.com/assets/sign_rider.pdf" target="_blank">Download Yard Sign Rider</a></div>';
	echo '<div class="show_me_around helpme" id="show_me_around">?</div>';
echo '</div>';

$r = get_active($user_id, $dbc);
echo '<div class="properties_container col-xs-12 col-sm-12 col-md-4" >';
	echo '<div id="active_properties" class="col-xs-12 col-sm-12">';
	echo '<h3>Active Properties</h3>';
	while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		$beacon_title = $row['beacon_title'];
		$total_beacons = $row['total_beacons'] ;
		$price = $row['price'];
		$price = str_replace('$', '', $price);
		$price = str_replace(',', '', $price);
		$price = (int) $price;
		if (!empty($price)) {
			$price = '$' . number_format($price);	
		}
		echo '<div class="droppable active_property col-xs-12 col-sm-12" id="b_con_' . $row['property_id'] . '"><a href="/create_property/' . $row['property_id'] . '">' . $row['property_title'] . ' ' . $price . '</a>';
			if (is_null($beacon_title)) {
				echo ' <img src="' . IMAGES . 'caution.png" alt="caution" title="Please assign a beacon" class="caution" />';
			}
			else {
				echo '<a href="/delete_property?p=' . $row['property_id'] . '"  class="delete_property_button"><img src="' . IMAGES . 'delete.png" alt="Delete info" title="Delete ' . $row['property_title']	. '" class="tooltip" /></a>';
				echo '<div class="clear"></div><div class="beacon_prop">Primary Beacon<br />Beacon ' . $beacon_title . ' </div><div class="beacon_prop">Total Beacons<br />' . $total_beacons . ' beacon'; if ($total_beacons > 1) { echo 's';} echo '</div>'; 
			}
			echo '</div>';
	}
	echo '<div class="clear"></div>';
	echo '</div>'; // END PROPERTIES (WITH BEACONS)
echo '</div>'; // END PROPERTIES CONTAINER

$r = get_inactive($user_id, $dbc);
echo '<div class="properties_container col-xs-12 col-sm-12 col-md-4" >';
	echo '<div id="unassigned_properties" class="col-xs-12 col-sm-12">';
	echo '<h3>Inactive Properties</h3>';
	while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
			$beacon_title = $row['title'];
			$total_beacons = $row['total_beacons'] ;
			
			echo '<div class="';
			
				if(is_null($beacon_title)) {
					echo 'droppable property unassigned';
				}
				else {
					echo 'inactive_prop';
				}
				$price = $row['price'];
				$price = str_replace('$', '', $price);
				$price = str_replace(',', '', $price);
				$price = (int) $price;
				
				if (!empty($price)) {
					$price = '$' . number_format($price);	
				}
				else {
					$price = '';
				}
				
				echo '" id="b_con_' . $row['property_id'] . '"><a href="/create_property/' . $row['property_id'] . '">' . $row['property_title'] . ' ' . $price . '</a>';
			if (is_null($beacon_title)) {
				echo ' <img src="' . IMAGES . 'caution.png" alt="caution" title="Please assign a beacon" class="caution" />';
				echo '<a href="/delete_property?p=' . $row['property_id'] . '"  class="delete_property_button"><img src="' . IMAGES . 'delete.png" alt="Delete info" title="Delete ' . $row['property_title']	. '" class="tooltip" /></a>';
			}
			else {
				echo '<a href="/delete_property?p=' . $row['property_id'] . '"  class="delete_property_button"><img src="' . IMAGES . 'delete.png" alt="Delete info" title="Delete ' . $row['property_title']	. '" class="tooltip" /></a>';
				echo '<div class="clear"></div><div class="beacon_prop">Primary Beacon<br />Beacon ' . $beacon_title . ' </div><div class="beacon_prop">Total Beacons<br />' . $total_beacons . ' beacon'; if ($total_beacons > 1) { echo 's';} 
				echo '</div>'; 
				
			}
			
			echo '</div>';
	}
	
	echo '</div>'; // END INACTIVE PROPERTIES
echo '</div>'; // END PROPERTIES CONTAINER

$r = get_avail_beacons($user_id, $dbc);

echo '<div class="properties_container col-xs-12 col-sm-12 col-md-4" >';
	echo '<div id="available_beacons" class="col-sm-12">';
	echo '<h3 id="avail_beacons_label">Available Beacons</h3>';
	echo '<div class="holder">';
	while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		echo '<div id="beacon-' . $row['id'] . '" class="beacon draggable">' . $row['title'] . '</div>';
	}
	
	echo '</div>'; // END HOLDER
	
	// ACTIVE BEACONS START
	
			echo '<div class="active_beacons" id="active_beacons">';
				echo '<h3>Active Beacons</h3>';
				
				echo '<div class="holder_active_beacons col-sm-12">';
				$r = get_active_beacons($user_id, $dbc);
				
				while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
					echo '<a href="/create_property/' .  $row['property_id'] . '?step=5'; if(!empty($row['info_title'])){echo '#info_container_' . $row['prop_info_id']; } echo '" ><div id="beacon-' . $row['id'] . '" class="active_beacon" title="Active in ' . $row['property_title']; if(!empty($row['info_title'])){echo ' / ' . $row['info_title']; }  echo' ">' . $row['title'] . '</div></a>';
				}
				echo '</div>'; // END ACTIVE HOLDER //
			echo '</div>';// END ACTIVE BEACONS
	
	echo '</div>'; // END BEACONS
echo '</div>'; // END PROPERTIES CONTAINER

include('includes/footer.php');



?>