<?php

require_login();
$page_title = $page_name = 'Beacons';

include('includes/header.php');

?>
<script type="text/javascript">
$( document ).ready(function() {
	$('#add_button').click(function(){
		$('#add_button').slideUp(300);
		$('#add_property').slideDown(300);		
	})
	$('#save').click(function(e){
		 e.preventDefault();
		if($("#property_title").val().length > 0){
			$.ajax({
				url: "add_property", 
				data: jQuery("#property_addition").serialize(),
				dataType:"script", 
				cache: false,
			}).done(function(data) {
				
			});
		}
	})
	
});

</script>
<?php


?>
<div id="error">
</div>


<?php


$r = get_card($user_id, $dbc);
$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
if($row['total'] == 1) {
	
	$q_beacon = "SELECT * FROM products";
	$r_beacon = mysqli_query($dbc, $q_beacon);
	$row_beacons = mysqli_fetch_array($r_beacon, MYSQLI_ASSOC);
	
	$sku = $row_beacons['sku'];
	
	echo '<div id="order_beacons_wrapper" >';
	echo '<div id="order_beacons">';
		echo '<div class="order_beacons_title">Order Beacons</div>';
		
		echo '<form id="add_to_cart_form">
				<input type="hidden" value="' . $sku . '" name="product_sku" />
				<div class="select_holder" id="order_beacons_count">
					<select id="select_beacon_quantity" name="beacon_quantity">
						<option value="3">3</option>
						<option value="5">5</option>
						<option value="10">10</option>
					</select>
				</div>
				<div id="beacon_total">Total $' . BEACON_PRICE * 3 . '</div>
				<button id="add_to_cart" type="submit">Add to Cart</button>
			   </form>';
			   ?>
               
               <script> 
			   $( document ).ready(function() {
				   
			   		$('#select_beacon_quantity').change(function() {
						btotal = $('#select_beacon_quantity option:selected').val() * '<?php echo BEACON_PRICE; ?>'
						$('#beacon_total').html('Total $' + btotal);
				    });
			   })
               </script>
               <?php
	echo '</div>'; // END ORDER BEACONS
	
	if (!isset($_SESSION['cart'][$sku]['quantity'])) {
		echo '<style> #view_cart { display:none; }</style>';
	}
	
	echo '<div id="view_cart"><div id="cart_items">';
			if (isset($_SESSION['cart'][$sku]['quantity'])) {echo $_SESSION['cart'][$sku]['quantity'] . ' Items'; }
		echo '</div>View Cart</div>';
	
	echo '<div class="card_info">';
		echo '<div class="card_title">' . $row['card_type'] . '</div>';
		echo '<div class="card"> **** **** **** ' . $row['card'] . '</div>';
		echo '<div class="date">Exp ' . $row['exp_month'] . '/' . $row['exp_year'] . '</div>';
		echo '<button id="edit_card" type="submit">Update Card</button>';
	echo '</div>'; // END CARD INFO 
	echo '</div>'; // END PAYMENT WRAPPER
}




echo '<div id="beacons" class="one_half">' . "\n\n";
echo '<h3>Beacons</h3>' . "\n\n";
echo '<div class="holder">' . "\n\n";
$r = get_all_beacons($user_id, $dbc);
while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
	
	if($row['beacon_type'] == 'Location') {
		$name = $row['beacon_type'];
	}
	else {
		$name = 'Unassigned';
	}
	$property_type = '';
	if(!is_null($row['property_name'])) {
		$name = $row['property_name'];
		$property_type = 'Property';
	}
	else if(!is_null($row['building_name'])) {
		$name = $row['building_name'];
		$property_type = 'Building';
	}
	
    
	echo '<div id="beacon-' . $row['beacon_id'] . '" class="beacons_wrapper clear">
			<div '; if($name == 'Unassigned'){ echo ' class="beacons-available_beacon beacon_info" '; } else { echo 'class="active_beacon beacon_info" '; }  echo' >' . $row['beacon_title'] . '</div>' . "\n\n";
			echo '<div class="beacon-assign-info">';
				echo 'Beacon ' . $row['beacon_title'] . '<br />' . "\n";
				echo 'Assigned to: ' . $name. '<br />' . "\n";
				echo 'Beacon Type: ' . $row['beacon_type'] . '<br />' . "\n";
				
			echo '</div>';
				echo '<div id="beacon-holder-' . $row['beacon_id'] . '" class="beacon_info_holder" ><div class="beacon-temp"><img src="/images/loading.gif" class="loading_gif" /></div></div>' . "\n\n";
				echo '<div class="clear"></div>';
			
	echo '</div>'; // END BEACONS WRAPPER
}
echo '</div>' . "\n\n"; // END BEACONS HOLDER
echo '</div>' . "\n\n"; // END BEACONS


include('includes/footer.php');



?>