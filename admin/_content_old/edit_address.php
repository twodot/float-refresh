<?php

require_once('includes/config.inc.php');
require_login();


if (isset($_GET['p'])) {
	$property = (int) $_GET['p'];
	$table = 'properties';
	$page = 'property';
	$table_id = 'property_id';
}
else if (isset($_GET['b'])) {
	$property = (int) $_GET['b'];
	$table = 'building';
	$page = 'building';
	$table_id = 'building_id';
}


if(isset($_POST['submitted'])) {
	if (isset($_POST['p'])) {
		$property = (int) $_POST['p'];
		$table = 'properties';
		$page = 'property';
		$table_id = 'property_id';
	}
	else if (isset($_POST['b'])) {
		$property = (int) $_POST['b'];
		$table = 'building';
		$page = 'building';
		$table_id = 'building_id';
	}
	if (isset($property)) {
		
		if($property != 0) {
			
			$address = $in_address = $dbc->real_escape_string($_POST['address']);
			$lat = $dbc->real_escape_string($_POST['lat']);
			$long = $dbc->real_escape_string($_POST['long']);
			
			$in_address = htmlspecialchars ($in_address, ENT_QUOTES);
			
			 $q = "
				UPDATE 
				$table 
				
				SET 
				address = '$in_address',  
				latitude =  '$lat',
				longitude =  '$long' 
				
				WHERE 
				id = $property 
				AND user_id = $user_id 
				
			";
			$r = @mysqli_query ($dbc, $q);
			
			
			
			
					$address = urlencode($address);
			
			
					$url = "http://maps.google.com/maps/api/geocode/json?address=" . $address . "&sensor=false";
					$response = file_get_contents($url);
					$response = json_decode($response, true);
					
                    $lat = $response['results'][0]['geometry']['location']['lat'];
					$long = $response['results'][0]['geometry']['location']['lng'];
					
					
					
							$neighborhood = $county = $state = $zip = '';
					
					
					
					
					foreach ($response['results'][0]['address_components'] as $key => $val ) {
						
						if ($val['types'][0] == 'neighborhood') {
							$neighborhood = $val['short_name'];
						}
						
						if ($val['types'][0] == 'administrative_area_level_2') {
							$county =$val['short_name'];
							$county = str_replace(' County', '', $county);
						}
						
						if ($val['types'][0] == 'administrative_area_level_1') {
							$state =$val['short_name'];
						}
						
						if ($val['types'][0] == 'postal_code') {
							$zip =$val['short_name'];
						}
						
						
					}
					
					
					$response = '';
					
					$ws_url = "http://api.walkscore.com/score?format=json&address=$address";
					$ws_url .= "&lat=$lat&lon=$long&wsapikey=". WALKSCORE;
					$str = file_get_contents($ws_url); 
					$walkscore_json = json_decode($str, true);
					
					$walkscore = $walkscore_json['walkscore'];
					$walkscore_link = $walkscore_json['ws_link'];
					$walkscore_description = $walkscore_json['description'];
					
					$walkscore_json = '';
					
					$q = "DELETE FROM schools WHERE  $table_id ='$property'";
					$r= @mysqli_query($dbc, $q);
					
					$q = "SELECT COUNT(*) as schools FROM schools WHERE $table_id = $property";
					$r = @mysqli_query($dbc, $q);
					$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
					if ($row['schools'] == 0) {	
						$greatschools_url = 'http://api.greatschools.org/schools/nearby?key=' . GREATSCHOOLS . '&state=' . $state . '&zip=' . $zip . '&schoolType=public&minimumSchools=5&radius=5&limit=20';
						$gs_response = file_get_contents($greatschools_url);
						$gs_p = xml_parser_create();
						$gs_array = xml_parse_into_struct($gs_p, $gs_response, $vals, $index);;
						xml_parser_free($gs_p);
						
						$schools = array();
						$i = 0;
						foreach($vals as $school_val => $value) {
							if ($value['tag'] == 'SCHOOLS') {
							}
							if($value['tag'] == 'SCHOOL') {	
								
								if($value['type'] == 'open') {
								}
								else if($value['type'] == 'close') {
										if ($schools[$i]['GRADERANGE'] != 'n/a') { 
										
											
											
											$gsid = $schools[$i]['GSID'];
											$name = $schools[$i]['NAME'];
											$grade_range = $schools[$i]['GRADERANGE'];
											$grade_range_arr = explode('-', $grade_range);
											$grade_range_low = $grade_range_arr[0];
											$grade_range_high = $grade_range_arr[1];
											
											if ($grade_range_low == 'K' || $grade_range_low == 'PK') {
												$grade_range_low == 0;
											}
											
											$gs_rating = (isset($schools[$i]['GSRATING']) ? $schools[$i]['GSRATING'] : '');
											$parent_rating = (isset($schools[$i]['PARENTRATING']) ? $schools[$i]['PARENTRATING'] : '');
											$city = $schools[$i]['CITY'];
											$district = $schools[$i]['DISTRICT'];
											$website = (isset($schools[$i]['WEBSITE']) ? $schools[$i]['WEBSITE'] : '');
											$overview_link = $schools[$i]['OVERVIEWLINK'];
											$distance = (isset($schools[$i]['DISTANCE']) ? $schools[$i]['DISTANCE'] : '');
										
											$q = "
												INSERT INTO 
												schools 
												
												SET 
												gsid = '$gsid', 
												name = '$name', 
												grade_range = '$grade_range', 
												grade_range_low = '$grade_range_low', 
												grade_range_high = '$grade_range_high', 
												gs_rating = '$gs_rating', 
												parent_rating = '$parent_rating', 
												city = '$city', 
												district = '$district', 
												website = '$website', 
												overview_link = '$overview_link', 
												distance = '$distance', 
												$table_id = '$property'
												
												";
											
											$r= @mysqli_query($dbc, $q);
										}
										$i ++;
								} // END TYPE CLOSE CHECK	
							} // END SCHOOL CHECK 
							else {
								if (isset($value['value'])) {
									$schools[$i][$value['tag']] = $value['value'];
								}
							}
							
						} // END STYPE CHOOLS CHECK
					}// END SCHOOLS EXIST CHECK
					
					$q = "SELECT * FROM schools WHERE $table_id = $property ORDER BY grade_range_low, grade_range_high, distance ASC";
					
					$r = @mysqli_query($dbc, $q);
					echo "$('#school_list').html('<p>Please click to select schools for the property</p>');";
					while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
							//?>
                            	$('#school_list').append('<div class="school" id="school_<?php echo $row['id']; ?>"><div class="gs_rating"><?php echo $row['gs_rating']; ?></div><div class="gs_info"><div class="gs_name"><a href="<?php echo $row['overview_link']; ?>" target="_blank"><h4><?php echo $row['name']; ?></h4></a></div><div class="grade_range"><?php echo $row['grade_range']; ?></div><div class="gs_distance"><?php echo $row['distance']; ?></div></div><div class="clear"></div></div>');
                            <?php
							
					}
					
					
				 	$q = "
						UPDATE 
						$table  
						
						SET 
						address = '$in_address',  
						latitude =  '$lat',
						longitude =  '$long', 
						walkscore = '$walkscore', 
						walkscore_link = '$walkscore_link', 
						show_walkscore = 1, 
						show_greatschools = 1 
						
						WHERE 
						id = $property 
						AND user_id = $user_id 
						
					"; 
					
					$r = @mysqli_query ($dbc, $q);
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			$url = BASE_URL . $page . '/' . $property;
			ob_end_clean();
			header("Location: $url");
			
			
		}
	}
}
else if (isset($_GET['p'])) {
	$property = (int) $_GET['p'];
		
		if($property != 0) {
			$q = "
				SELECT 
				*    
				
				FROM 
				$table  
				
				WHERE 
				id = $property
			";
			
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			if ($user_id == $row['user_id']) {
					
					$lat = $row['latitude'];
					$long = $row['longitude'];
					$address = $row['address'];
					
					?>
                    
                    <script type="text/javascript">
						$('#get_lat_long_button').click(function() {
							
							navigator.geolocation.getCurrentPosition(getLocation, unknownLocation);
							
							function getLocation(pos)
							{
								var lat = pos.coords.latitude;
								var lon = pos.coords.longitude;
								
								$('#lat').val(lat);
								$('#lon').val(lon);
							
							}
							function unknownLocation()
							{
							alert('Could not find location');
							}
						})
					</script>
                    
                    <button id="get_lat_long_button">Use Current Location</button>
                    
                    <form action="/edit_address" method="post" id="change_map_form">
                    <p>Address<br />
                    <input type="input" name="address" value="<?php echo $address; ?>" /></p>
                    <p>Latitude<br />
                    <input type="lat" id="lat" name="lat" value="<?php echo $lat; ?>" /></p>
                    <p>Longitude<br />
                    <input type="long" id="lon" name="long" value="<?php echo $long; ?>" /></p>
                    <input type="submit" name="submit" value="Change Map Info">
                    <input type="hidden" name="p" value="<?php echo $property; ?>" />
                    <input type="hidden" name="submitted" value="true" />
                    </form>
                    
                    <?php
				
			}
			else {
				echo 'You have reached this page in error';
			}
			
		}// END PROPERTY INT CHECK
} // IN P ISSET CHECK



else if (isset($_GET['b'])) {
	$building = (int) $_GET['b'];
		
		if($building != 0) {
			$q = "
				SELECT 
				*    
				
				FROM 
				building  
				
				WHERE 
				id = $building
			";
			
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			if ($user_id == $row['user_id']) {
					
					$lat = $row['latitude'];
					$long = $row['longitude'];
					$address = $row['address'];
					
					?>
                    
                    <script type="text/javascript">
						$('#get_lat_long_button').click(function() {
							
							navigator.geolocation.getCurrentPosition(getLocation, unknownLocation);
							
							function getLocation(pos)
							{
								var lat = pos.coords.latitude;
								var lon = pos.coords.longitude;
								
								$('#lat').val(lat);
								$('#lon').val(lon);
							
							}
							function unknownLocation()
							{
							alert('Could not find location');
							}
						})
					</script>
                    
                    <button id="get_lat_long_button">Use Current Location</button>
                    
                    <form action="/edit_address" method="post" id="change_map_form">
                    <p>Address<br />
                    <input type="input" name="address" value="<?php echo $address; ?>" /></p>
                    <p>Latitude<br />
                    <input type="lat" id="lat" name="lat" value="<?php echo $lat; ?>" /></p>
                    <p>Longitude<br />
                    <input type="long" id="lon" name="long" value="<?php echo $long; ?>" /></p>
                    <input type="submit" name="submit" value="Change Map Info">
                    <input type="hidden" name="b" value="<?php echo $building; ?>" />
                    <input type="hidden" name="submitted" value="true" />
                    </form>
                    
                    <?php
				
			}
			else {
				echo 'You have reached this page in error';
			}
			
		}// END PROPERTY INT CHECK
} // IN P ISSET CHECK

	
?>

