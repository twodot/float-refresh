<?php

require_once('includes/config.inc.php');
include('includes/bitly.php');
require_login();
$page_title = $page_name = 'Add Property';

include('includes/header.php');

?>
<div id="error">
</div>

<div id="dialog">
</div>

<?php
if(isset($_GET['p']) || isset($p)) {
	if(isset($_GET['p']) ) {
		$property = (int) $_GET['p'];
	}
	else if(isset($p)) {
		$property = (int) $p;
	}
	if($property != 0) {
		
		
		$r = get_building($user_id, $dbc, $property);
		if(mysqli_num_rows($r) == 0) {
			header('Location: /multi-family');
		}
		
		
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		$primary_info_id = $row['info_id'];
		$property = $row['property_id'];
		$progress = $row['progress']; 
		$completed = $row['completed']; 
		$total_media = $row['total_media'];
		$address = $row['address'];
		$latitude = $row['latitude'] + 0;
		$longitude = $row['longitude'] + 0;
		$marketing_description = nl2br($row['description']);
		$sales_status = $row['sold_status'];
		$status_message = $row['status_message'];
		$property_title = $row['property_title'];
		$year_built = $row['year_built'];
		$community = $row['community'];
		$county = $row['county'];

		$show_walkscore = $row['show_walkscore'];
		$show_greatschools = $row['show_greatschools']; 
		$active = $row['active']; 
		$walkscore = $row['walkscore'] +0;  
		
		$collapse_details = $row['collapse_details'];
		$collapse_schools = $row['collapse_schools'];
		
		$main_beacon_title = $row['beacon_title'];
		$main_beacon_id = $row['beacon_id'];
		
		
		
		$property_title = $row['property_title'];
		if(!empty($bitly_hash)) {
			$short_url = BITLY_BASE_URL . 	$bitly_hash;
		}
		
		if (!empty($row['price'])) {
			$price = (int) $row['price'];
			$price = str_replace('$', '', $price);
			$price = str_replace(',', '', $price);
			$price = '$' . number_format($price);	
		}
		else {
			$price = '';
		}
		
		if ($latitude != 0.0000000 && $longitude != 0.0000000) {
			// MAP SECTION 
			?>
			
			<script type="text/javascript">
			  function initialize() {
				  var propertyLatLong = new google.maps.LatLng(<?php echo $latitude . ',' . $longitude; ?>);
				  var mapOptions = {
					zoom: 18,
					center: propertyLatLong, 
					//mapTypeId: google.maps.MapTypeId.HYBRID
				  }
				  var map = new google.maps.Map(document.getElementById('property-map'), mapOptions);
					
   				  var image = 'https://s3-us-west-2.amazonaws.com/images.listfloat.com/assets/float_map_dot.png';
				  var marker = new google.maps.Marker({
					  position: propertyLatLong,
					  map: map,
					  title: 'Hello World', 
					  icon: image
				  });
				}

				
				function loadMap() {
				  var script = document.createElement('script');
				  script.type = 'text/javascript';
				  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAiAePyQf5emFwHaIQlsvwFGX0VXmvEiNg&callback=initialize';
				  document.body.appendChild(script);
				}
				
				$(document).ready(function() {
					$(".fancybox").fancybox();
				});
				
			</script>
            <?php 
		}
		echo '<a href="/delete_property?p=' . $row['property_id'] . '"  class="delete_property_button">Delete Building</a>';

		include('_content/add_building/add_new_building.php');
	} // END IF PROPERTY != 0
} // END IF GET P EXISTS

require_once('js/global_ui.php');
?>

