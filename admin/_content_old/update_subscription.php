<?php

require_once('includes/config.inc.php');
require_login();
$q = "SELECT COUNT(*) as subscription FROM subscription WHERE user_id = $user_id";
$r = @mysqli_query ($dbc, $q);
$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
if($row['subscription'] != 0) {

	if(isset($_GET['s'])) {
		$sub_id = (int) $_GET['s'];
		Stripe::setApiKey(STRIPE_SECRET);
		
		
		
		$q ="SELECT stripe_id FROM users where user_id = $user_id";
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$stripe_customer = $row['stripe_id'];
		date_default_timezone_set("UTC"); 
		 
		$cu = Stripe_Customer::retrieve("$stripe_customer");
		$cu_number = $cu['subscriptions']['data'][0]['id'];
		$subscription = $cu->subscriptions->retrieve($cu_number);
		$subscription->plan = $sub_id;
		$subscription->trial_end = "now";
		$subscription->save();
		
		
		$start = $subscription['current_period_start'];
		$end = $subscription['current_period_end'];
		$trial_start = strtotime("+1 day");
		$trial_end = strtotime("+1 day");
         
		$q= "SELECT * FROM subscription_types WHERE stripe_id = $sub_id ";
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$active_prop = $row['stripe_id'];
		$amount = $row['stripe_amount'];
		$total_properties = $row['total_properties']; 
		
		$q= "UPDATE 
			subscription 
			
			SET 
			subscribe_level = $sub_id, 
			allowed_prop = $total_properties, 
			amount = $amount, 
			current_period_start = FROM_UNIXTIME($start), 
			current_period_end = FROM_UNIXTIME($end),
			trial_start = FROM_UNIXTIME($trial_start),  
			trial_end = FROM_UNIXTIME($trial_end), 
			active = 1
			
			WHERE 
			user_id = $user_id";
			
			$r = @mysqli_query ($dbc, $q);
		
		
			$url = '/change_subscription' ;
		
			ob_end_clean();
			
			header("Location: $url");
			
			exit();
		
		
	} // END GET CHECK
}
?>

