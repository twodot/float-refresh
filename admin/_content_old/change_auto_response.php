<?php

require_once('includes/config.inc.php');
require_login();
$page_title = 'Manage Email Response';
include('includes/header.php');
if(isset($_POST['submit'])) {
	$email_response = $_POST['email_response'];
	$email_response = htmlspecialchars ($email_response, ENT_QUOTES);
	$email_response = $dbc->real_escape_string($email_response);
	$email_response = trim($email_response);
	
	$q = "UPDATE users SET auto_email = '$email_response' WHERE user_id = $user_id";
	$r= @mysqli_query($dbc, $q);
}

$q = "SELECT auto_email FROM users WHERE user_id = $user_id";
$r = @mysqli_query($dbc, $q);
$row = mysqli_fetch_array($r, MYSQLI_ASSOC);

if(empty($row['auto_email'])) {
	$q = "SELECT auto_email FROM auto_response LIMIT 1";
	$r = @mysqli_query($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	$auto_email = $row['auto_email'];	
}else {
	$auto_email = $row['auto_email'];
}


?>
<div id="email_response_holder">
    <p><a href="/account">Back to my account</a></p>
    <form id="email_response" method="post" action="change_auto_response" >
    <textarea name="email_response" id="email_response_text"><?php if (isset($auto_email)) { echo trim($auto_email);}?>
    </textarea>
    <br />
    <input type="submit" name="submit" value="SUBMIT" />
    </form>
    <div class="clear"></div>
</div>
<div id="email_response_short_codes">
<h3>Available Short Codes</h3>
<p>Click in the textbox then click a link below to add the features to your auto responses.</p>
<script>

$(document).ready(function() {
jQuery.fn.extend({
		setCursorPosition: function(position){
		    if(this.length == 0) return this;
		    return $(this).setSelection(position, position);
		},

		setSelection: function(selectionStart, selectionEnd) {
		    if(this.length == 0) return this;
		    input = this[0];

		    if (input.createTextRange) {
		        var range = input.createTextRange();
		        range.collapse(true);
		        range.moveEnd('character', selectionEnd);
		        range.moveStart('character', selectionStart);
		        range.select();
		    } else if (input.setSelectionRange) {
		        input.focus();
		        input.setSelectionRange(selectionStart, selectionEnd);
		    }

		    return this;
		},

		focusEnd: function(){
		    this.setCursorPosition(this.val().length);
		            return this;
		},

		getCursorPosition: function() {
	        var el = $(this).get(0);
	        var pos = 0;
	        if('selectionStart' in el) {
	            pos = el.selectionStart;
	        } else if('selection' in document) {
	            el.focus();
	            var Sel = document.selection.createRange();
	            var SelLength = document.selection.createRange().text.length;
	            Sel.moveStart('character', -el.value.length);
	            pos = Sel.text.length - SelLength;
	        }
	        return pos;
	    },

	    insertAtCursor: function(myValue) {
	    	return this.each(function(i) {
			    if (document.selection) {
			      //For browsers like Internet Explorer
			      this.focus();
			      sel = document.selection.createRange();
			      sel.text = myValue;
			      this.focus();
			    }
			    else if (this.selectionStart || this.selectionStart == '0') {
			      //For browsers like Firefox and Webkit based
			      var startPos = this.selectionStart;
			      var endPos = this.selectionEnd;
			      var scrollTop = this.scrollTop;
			      this.value = this.value.substring(0, startPos) + myValue + 
                                this.value.substring(endPos,this.value.length);
			      this.focus();
			      this.selectionStart = startPos + myValue.length;
			      this.selectionEnd = startPos + myValue.length;
			      this.scrollTop = scrollTop;
			    } else {
			      this.value += myValue;
			      this.focus();
			    }
		  	})
	    }
    
	})



$(".click_insert").click(function(e) {
	e.preventDefault();
	addCode = $(this).attr("href");
	pos = $("#email_response_text").getCursorPosition();
   $("#email_response_text").insertAtCursor(addCode);
   // var theCode = $(this).attr("href");
});


})

</script>


<h4>Prospects:</h4>
<a href="{PROSPECT_FIRST_NAME}" class="click_insert">Prospect First Name</a><br />
<a href="{PROSPECT_LAST_NAME}" class="click_insert">Prospect Last Name</a><br />
<h4>Property:</h4>
<a href="{PROPERTY_TITLE}" class="click_insert">Property Title</a><br />
<a href="{PROPERTY_DESCRIPTION}" class="click_insert">Property Description</a><br />
<a href="{PROPERTY_ADDRESS}" class="click_insert">Property Address</a><br />
<a href="{PROPERTY_PRICE}" class="click_insert">Property Price</a><br />
<a href="{PROPERTY_DETAILS}" class="click_insert">Property Details</a><br /><span class="gray">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-(bed/bath/sqft)</span><br />
<h4>Agent:</h4>
<a href="{AGENT_FIRST_NAME}" class="click_insert">Your First Name</a><br />
<a href="{AGENT_LAST_NAME}" class="click_insert">Your Last Name</a><br />
<a href="{AGENT_LISTING_EMAIL}" class="click_insert">Your Email</a><br />
<a href="{AGENT_LISTING_PHONE}" class="click_insert">Your Phone Number</a><br />
<a href="{AGENT_LISTING_BROKERAGE}" class="click_insert">Your Brokerage</a><br />
</div>

<?php


include('includes/footer.php');