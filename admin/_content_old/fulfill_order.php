<?php
require_admin_login();
get_pseudo();

$page_title = $page_name = 'Fullfill Order';
include('includes/header.php');

$customer_id = (int) $_SESSION['pseudo_login']['id'];
$order_id = (int) $_SESSION['pseudo_login']['order_id'];
$q = "
	SELECT 
	
	beacon_orders.beacons,  
	
	beacon_orders.id as beacon_order_id, 
	beacon_orders.tracking,  
	
	users.first_name, 
	users.last_name,
	
	beacons.k_uniqueId, 
	beacons.title
	
	FROM 
	beacon_orders, 
	users
	
	LEFT JOIN (beacons)
                 ON (beacon_order_id = $order_id) 
				 
	LEFT JOIN (shipping)
                 ON (shipping.user_id = $customer_id ) 
	
	WHERE 
	beacon_orders.user_id = $customer_id
	AND users.user_id = $customer_id 
	AND beacon_orders.user_id = users.user_id 
	AND beacon_orders.id = $order_id 
	
	";
$r = mysqli_query($dbc, $q);
$row = mysqli_fetch_array($r, MYSQLI_ASSOC);

$tracking_number = $row['tracking'];

$q_customer = "SELECT * FROM users, shipping WHERE users.user_id = $customer_id AND shipping.user_id = $customer_id";
$r_customer = mysqli_query($dbc, $q_customer);
$row_customer = mysqli_fetch_array($r_customer, MYSQLI_ASSOC);

$first_name = $row_customer['first_name']; 
$last_name = $row_customer['last_name'];

$street = $row_customer['street'];
$city = $row_customer['city'];
$state = $row_customer['state_name'];
$zip = $row_customer['zip'];



$name = $first_name . ' ' . $last_name;

$email = $row_customer['email']; 


if ($row['beacons'] != 0 ) {
	echo '<div id="order_infor_for">Fulfilling order for ' . $name . ' - <a href="mailto:' . $email . '">' . $email . '</a></div>'; 
	echo '<div id="beacon_order_notification">Beacons left to fulfill <div id="beacons_left">' . $row['beacons'] . '</div></div>';
	
	echo '<div id="message"></div>';
	echo '<div id="new_title"></div>';
}
else {
	echo '<div id="shipping_address_to_client">';
		echo '<div id="order_infor_for"><h3 class="error">Order Fulfilled</h3><p> Ship to ' . $name . ' - <a href="mailto:' . $email . '">' . $email . '</a></p></div>'; 
		echo '<p>' . $street . '<br />' . $city . ', ' . $state . ', ' . $zip . '</p>';
	echo '</div>';
}

?>
<script src="/js/html5-qrcode.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	
	$('#reader').html5_qrcode(function(uid){
			
			$.ajax({
				url: "add_beacon_to_account?uid=" + uid + "&order_id=<?php echo $order_id; ?>", 
				data: uid,
				dataType:"script", 
				cache: false,
			}).done(function(data) {
				
			});
			
		},
		function(error){
			$('#read_error').html(error);
		}, function(videoError){
			$('#vid_error').html(videoError);
		}
	);
	
	$('#manual_submit').click(function(e) {
		e.preventDefault();
		var uid = $("#manual_id").val();
		
		$.ajax({
			url: "add_beacon_to_account?uid=" + uid + "&order_id=<?php echo $order_id; ?>",  
			data: uid,
			dataType:"script", 
			cache: false,
		}).done(function(data) {
			
		});	
	});
	
	$('#tracking_number_manual_submit').click(function(e) {
		e.preventDefault();
		var tNumber = $("#tracking_number").val();
		
		$.ajax({
			url: "add_tracking_number_to_order?tracking=" + tNumber + "&order_id=<?php echo $order_id; ?>",  
			data: tNumber,
			dataType:"script", 
			cache: false,
		}).done(function(data) {
			
		});	
	});
	
}); 
</script>

<?php if($row['beacons'] != 0) { ?>
<div id="beacon_input_holder" class="one_half">
<p>Manual Input</p>
<form id="manual_form">
<input type="input" name="uid" id="manual_id" />
<input type="submit" id="manual_submit" />
</form>
</div>
<?php } // END NO BEACON CHECK ?>
<div id="tracking_number_holder" class="one_half">
<?php if (empty($tracking_number)) { ?>
	<div id="add_tracking_number">
        <p>Add Tracking Number</p>
        <form id="tracking_number_form">
        <input type="input" name="tracking_number" id="tracking_number" />
        <input type="submit" id="tracking_number_manual_submit" />
        </form>
    </div>
<?php } ?>
    <div id="tracking_number_added">
    	<?php if (!empty($tracking_number)) { echo '<h3>Tracking number</h3><p>' . $tracking_number . '</p>'; } ?>
    </div>

</div>
<?php if($row['beacons'] != 0) { ?>
    <div id="reader" style="width:300px;height:250px">
     </div>
     <span id="read" class="center"></span>
    
    <div id="added_beacon_title"></div>
<?php } // END NO BEACON CHECK ?>
<div id="beacons_on_this_order"><h3>Beacons on this Order</h3>
<?php

$q_list = $q . " ORDER BY beacons.title ASC";

$r = mysqli_query($dbc, $q_list);
while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
	echo'<p>' . $row['k_uniqueId'] . ' - ' . $row['title'] . '</p>';
}
?>

</div>



<div class="clear"></div>
<?php
// Load the class

// require_once('USPS/USPSOpenDistributeLabel.php');


// Initiate and set the username provided from usps

/* $label = new USPSOpenDistributeLabel(USPS_USER);


$label->setFromAddress(OUR_NAME, '', '', OUR_ADDRESS . ' ' . OUR_SUITE, OUR_CITY, OUR_STATE, OUR_ZIP);
$label->setToAddress($name, $street, $city, $state, $zip);
$label->setWeightOunces(1);

// Perform the request and return result
$label->createLabel();
*/ 

// See if it was successful
/* if($label->isSuccess()) {
  echo 'Done';
  echo "\n Confirmation:" . $label->getConfirmationNumber();

  $label = $label->getLabelContents();
  if($label) {
  	 $contents = base64_decode($label);
  	 header('Content-type: application/pdf');
	   header('Content-Disposition: inline; filename="label.pdf"');
	   header('Content-Transfer-Encoding: binary');
	   header('Content-Length: ' . strlen($contents));
	   echo $contents;
	   exit;
  }

} else {
//  echo 'Error: ' . $label->getErrorMessage();
}
*/
include('includes/footer.php');

?>