<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_login();
?>
<h3><span class="add_property_name">Features / Tours</span><span class="add_property_icon">Icon</span></h3>
 <section>
 	<h2 class="create_property_headline">Time to Crank up the Volume.</h2>
	<p>Unleash the power of Float by calling-out special selling features you don’t want missed. Point out unique spaces, appliances, designer finishes and more. Go even further down the rabbit hole by adding sub-information for brand names, model types, building techniques and the finer details. </p>
    
    <script>
	$(function() {
		Dropzone.options.image360UploaderForm = false;
			var image360Upload = new Dropzone("#image_360_uploader_form", {
				acceptedFiles: "image/jpeg", 
				maxFilesize: 8, 
				dictFileTooBig: "The file size of the image uploaded ({{filesize}}mb) is larger than the maximum file size allowed. Please keep images under {{maxFilesize}}mb", 
				dictDefaultMessage: "Drag and drop, or click here to add, images to the gallery. (JPG format)", 
				dictInvalidFileType: "The file must be a JPG."
			});
			image360Upload.on("success", function(file, response) {
				window.location.replace('/create_property/<?php echo $property; ?>?step=4');
			});
		})
	
	</script>
		<div id="upload_image_360_holder">
        	
            <form action="/_content/add_property/image_360_upload.php" class="dropzone" method="post" enctype="multipart/form-data" id="image_360_uploader_form" >
                <div class="fallback">
                    <p>Upload images here</p>
                    <input type="file" multiple name="file" accept="image/jpeg" capture="camera" >
                    <div id="submit_holder">
                        <img src="/images/loading.gif" class="loader" />
                        <input type="submit" name="submit" value="Submit" id="upload_image" />
                    </div>
                </div>
                <input type="hidden" name="i" id="image360infoId" />
                <input type="hidden" name="p" value="<?php echo $property; ?>" />
            </form></p>
        </div>

    <?php 
    echo '<h4 class="add_info_show">Add property highlights (+)</h4>';
				
				?>
               
				<div id="add_info">
                    
                    <form id="info_addition">
                    
                        <p>Title (room or feature)<br />
                        <input type="text" id="info_title" value="" name="title" placeholder="Title" /></p>
                        
                        <p>Information<br />
                        <textarea id="info_box" name="info"></textarea></p>
                        
                        <p>Add a link to the property highlight screen.</p>
                        <input type="text" name="property_info_link_text" placeholder="Link Text" value="" class="new_info_link_text"  maxlength="50" />
                        <input type="text" name="property_info_link" placeholder="URL" value="" class="new_info_link" maxlength="255" />
                        <p><input type="checkbox" name="go_to_link_automatically" value="1" class="new_info_automatic_link_checkbox" /> Open this link in place of the property highlight screen.</p>
                    
                    	<p>Notes (will not show in app) <br />*Tip: Where beacon is located in house<br />
                        <textarea id="notes_box" name="notes"></textarea>
                        </p>
                        <input type="hidden" name="u" value="<?php echo $user_id; ?>" />
                        <input type="hidden" name="p" value="<?php echo $property; ?>" />
                        <input type="submit" id="save" name="Submit" value="SAVE" />
                        <input type="hidden" name="submitted" value="true" />
                        <button class="add_info_cancel">Cancel</button>
                    </form>
                </div><!-- END ADD INFO -->
                <br />
                
                <?php	
				echo '<div class="holder">' . "\n";
				echo '<ul id="sortable">';
				$r = get_info($user_id, $dbc, $property);
				while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
					
					$total_media = $row['total_media'];
					if(!$total_media) { $total_media = 0; };
					
					if ($row['primary_info'] != 1) {
					$info_id =  $row['info_id'];
					$beacon_title = $row['title'];
					$link_beacon = $row['link_beacon']; 
					$link_beacon_text = $row['link_beacon_text'];
					$link_beacon_link = $row['link_beacon_link'];
					
					$has_360_image = $row['has_360_image']; 
					$link_360_image = $row['link_360_image']; 
					
					echo '<li id="li_feature_' . $row['info_id'] . '"><div id="info_feature_' . $row['info_id'] . '" class="property_info">' . "\n";
							
							echo '<form action="/update_info" method="get" class="update_info_form">';
								if (is_null($beacon_title)) {
									echo '<div class="prop_info_holder property_info_child"><h4 class="info_title">' . $row['info_title'];
									
								}
								else {
									echo '<div class="prop_info_holder property_info_child"><h4 class="info_title">' . $row['info_title'];
									
								}
									
							echo '</h4>';
							echo '<div class="beacon_title_holder"><span id="prop_feature_id_' . $info_id . '">'; if($beacon_title != '') { echo 'Beacon ' . $beacon_title . ''; } echo '</span></div>';
							?><input class="edit_info_title" name="title" value="<?php echo $row['info_title']; ?>" /><?php
							echo '<ul class="prop_revisions"><li><a href="/update_info?i=' . $info_id . '"  class="edit_info_button">Edit</a></li><li><a class="add_child_info_show" id="add_child_info_to_' . $info_id . '">(+) Add Sub Information</a></li><li><a href="/delete_info?i=' . $info_id . '"  class="delete_info_button">(&ndash;) Delete</a></li></ul>';
							
								echo '<div class="edit_media"><a href="https://admin.listfloat.com/choose_media?i=' . $row['info_id'] . '" class="attach_media fancybox fancybox.iframe">Add Images</a>'; echo '<br /><span class="image_count_holder">(' . $total_media . ')'; if ($total_media == 1) {echo ' Image';} else {echo ' Images';} echo'</span></div>';
								echo '<div class="image360Holder">';
									echo '<div><p><a href="#" class="add_360" id="image_360_' . $info_id . '">'; echo ($has_360_image ? 'Edit your 360 image' : 'Add a 360 Image'); echo '</a></p></div>';
									if($has_360_image) {
										echo '<a href="/image_360.php?name=' . $info_id . '" target="_blank"><img src="http://images.listfloat.com/360-images/' . $info_id . '.jpg" class="Image360Thumb" /></a>';
									}
								echo '</div>'; // END IMAGE 360 HOLDER 
							echo '<div class="text"><p>' . $row['info_text'] . '</p>';?>
                            <textarea class="edit_info_box" name="info" ><?php echo $row['info_text'] ?></textarea></div> 
                            
                           
							<div class="property_info_link_holder">
                            	<?php if (!$has_360_image) { ?>
                                <div class="property_information_link_notification">
                                	<p>Property Highlight Link</p>
                                    <?php
										if(empty($link_beacon_link)) {
                                            echo '<p class="link_notification_warning">You have not added a link, click edit above to add a new link.</p>';
											echo '<p class="link_beacon_link_holder"></p>';
											echo '<p class="link_beacon_automatic_notification"></p>';
                                        }
                                        else {
                                            if(empty($link_beacon_text)) {
                                                $link_text = $link_beacon_link;
                                            }
                                            else {
                                                $link_text = $link_beacon_text;
                                            }
                                            echo '<p class="link_beacon_link_holder"><a href="' . $link_beacon_link . '" target="_blank">' . $link_text . '</a></p>';
                                            if($link_beacon) {
                                                echo '<p class="link_beacon_automatic_notification">This link will automatically open in place of the property highlight screen</p>';
                                            }
                                            else {
                                                echo '<p class="link_beacon_automatic_notification">This link will NOT automatically open in place of the property highlight screen</p>';
                                            }
                                        }
                                    ?>
                                </div>
                                <?php } // END 360 IMAGE CHECK ?>
                                <div class="property_information_link_form">
                                    <p>Add a link to the property highlight screen.</p>
                                    <input type="text" name="property_info_link_text" placeholder="Link Text" value="<?php echo $link_beacon_text; ?>"  maxlength="50" />
                                    <input type="text" name="property_info_link" placeholder="URL" class="property_info_link_input" value="<?php echo $link_beacon_link; ?>" maxlength="255" />
                                    <p><input type="checkbox" name="go_to_link_automatically" class="go_to_link_automatically" <?php if ($link_beacon) {echo 'checked="ckecked"';} ?> value="1" /> Open this link in place of the property highlight screen.</p>
                                </div>
                            </div>					
							<?php 
							echo '<div class="notes"><p>NOTES <span class="notes_instr">(Will not show in app.)</span></p><p class="notes_text">' . $row['info_notes'] . '</p>';?>
                            <textarea class="edit_notes_box" name="notes" ><?php echo $row['info_notes'] ?></textarea></div>
                            
                            <input type="hidden" name="u" value="<?php echo $user_id; ?>" />
                            <input type="hidden" name="i" value="<?php echo $row['info_id'] ?>" />
                            <input type="hidden" name="p" value="<?php echo $property; ?>" />
                            <input type="hidden" name="submitted" value="true" />
                            <input type="submit" class="edit_save" name="Submit" value="SAVE">
                            <input type="button" class="edit_cancel" value="CANCEL">
							<?php 
						echo '</form>';
					echo '</div>' . "\n";
						
							?>
                            
                            <div class="add_child_info">
                            	<div class="add_child_info_form">
                                <h4 class="info_title">Sub-information</h4>
                                <form class="child_info_addition">
                                
                                    <p>Title (room or feature)<br />
                                    <input type="text" class="info_title" value="" name="title" placeholder="Title" /></p>
                                    
                                    <p>Information<br />
                                    <textarea class="info_box" name="info"></textarea></p>
                                	
                                    <input type="hidden" name="u" value="<?php echo $user_id; ?>" />
                                    <input type="hidden" name="p" value="<?php echo $property; ?>" />
                                    <input type="hidden" name="i" value="<?php echo $info_id; ?>" />
                                    <input type="submit" class="save" name="Submit" value="SAVE" />
                                    <input type="hidden" name="submitted" value="true" />
                                	<button class="add_info_cancel">Cancel</button>
                                </form>
                                
                                </div> <!-- END INFO FORM -->
                            </div><!-- END ADD INFO -->
                            
                            <?php
							echo '<div class="holder">' . "\n";
							echo '<ul class="child_sort" id="ul_' . $row['info_id'] . '">';
							$r_c = get_child_info($user_id, $dbc, $property, $info_id);
							while ($row_c = mysqli_fetch_array($r_c, MYSQLI_ASSOC)) {
								echo '<li id="li_' . $row_c['id'] . '"><div id="info_feature_' . $row_c['id'] . '" class="child_container property_info property_info_child ">' . "\n";
									echo '<form action="/update_info" method="get" class="update_info_form">';
									echo '<div class="holder">' . "\n";
										echo '<h4 class="info_title">' . $row_c['title']	. '<span id="prop_id_' . $row_c['id'] . '"></span></h4>';
										?> <input class="edit_info_title" name="title" value="<?php echo $row_c['title']; ?>" /><?php
										echo '<ul class="prop_revisions"><li><a href="/update_info?i=' . $row_c['id']. '" class="edit_info_child_button">Edit</a></li><li><a href="/delete_info?i=' . $row_c['id'] . '"  class="delete_info_button">(&ndash;) Delete</a></li></ul><div class="text"><p>' . $row_c['text'] . '</p>'; ?><textarea class="edit_info_box" name="info" ><?php echo $row_c['text'] ?></textarea></div> <?php 
										
											?>
                                            <input type="hidden" name="u" value="<?php echo $user_id; ?>" />
                                            <input type="hidden" name="p" value="<?php echo $property; ?>" />
                                            <input type="hidden" name="i" value="<?php echo $row_c['id'] ?>" />
                                            
                                            <input type="hidden" name="submitted" value="true" />
                                            <input type="submit" class="edit_save" name="Submit" value="SAVE">
                                            <input type="button" class="edit_cancel" value="CANCEL" />
                                            <?php
									echo '</div> <!-- END HOLDER -->' . "\n"; // END HOLDER 
									echo '</form>';
								echo '</div><!-- END INFO -->' . "\n"; // END INFO			
								
							}// END GET CHILD INFO
							echo '</ul>'; // END CHILD LIST
						echo '</div><!-- END HOLDER -->' . "\n"; // END HOLDER 
					echo '</div><!-- END INFO -->' . "\n"; // END INFO			
					} // END PRIMARY CHECK 
					echo '</li>';
				} // END GET INFORMATION 
				echo '</ul>';
				echo '</div><!-- END HOLDER -->' . "\n"; // END HOLDER
				?>
</section>