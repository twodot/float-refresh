<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_login();
?>
<h3><span class="add_property_name">Media</span><span class="add_property_icon">Icon</span></h3>
<section>
<h2 class="create_property_headline">Nice! We&rsquo;ve Got to See This.</h2>
<p>Add images below.</p>
<?php	

$q = "SELECT id FROM property_info WHERE properties_id = $property AND primary_info = 1";
$r = mysqli_query($dbc, $q);
$row = mysqli_fetch_array($r, MYSQLI_ASSOC);

$i = $row['id'];

// Include the AWS SDK using the Composer autoloader
//require '/home/ec2-user/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

require_once('includes/config.inc.php');
require_login();

include("includes/resize.php");
?>

<div id="dialog">
</div>

<?php

if(isset($p)) {
	$i_sel = (int) $p;	
}


if(isset($i)) {
		$i = (int) $i;
		$i_sel = (int) $i;
}

if(isset($i_sel)) {
	
	$i = (int) $i_sel;	
		
	if($i != 0) {
		
		$q="SELECT COUNT(*) as property, properties.id as prop_id, properties.title as top_title, property_info.title as prop_title FROM properties, property_info  WHERE properties.user_id = $user_id AND properties.id = property_info.properties_id AND property_info.id = $i";
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		if($row['property'] == 1) {
			$top_title = $row['top_title'];
			$property_info_title = $row['prop_title'];
			$property_id = $row['prop_id'];
			
			$q="SELECT properties.id as property_id, properties.title as title, properties.price as price, property_info.id as info_id, property_info.primary_info as primary_info, property_info.primary_info as primary_info, media.title as media_title, media.id as media_id  FROM properties, property_info, media  WHERE properties.user_id = $user_id AND properties.id = property_info.properties_id AND property_info.id = $i AND media.property_id = properties.id AND media.info_id = $i  AND media.user_id = $user_id AND media.deleted != 1 ORDER By gallery_order ASC";
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			$primary_info = $row['primary_info'];
			
			if ($primary_info) {
				$q="SELECT properties.id as property_id, properties.title as title, properties.price as price, property_info.id as info_id, property_info.primary_info as primary_info, property_info.primary_info as primary_info, media.title as media_title, media.id as media_id  FROM properties, property_info, media  WHERE properties.user_id = $user_id AND properties.id = property_info.properties_id AND properties.id = $property_id AND properties.id = property_info.properties_id AND property_info.id = media.info_id AND media.property_id = properties.id AND media.property_id = $property_id  AND media.user_id = $user_id  AND media.deleted != 1 ORDER By main_gallery_order ASC";	
				$r = @mysqli_query ($dbc, $q);
				$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			}
			
			$image_count = mysqli_num_rows($r);
			
			?>
			<script>
			
			$(function() {
				$( "#media_sortable" ).sortable({ items: " li", placeholder: "media_sort_highlight", opacity: 0.5 });
				$( "#media_sortable" ).on( "sortupdate", function( event, ui ) {
					var sorted = $( "#media_sortable" ).sortable( "serialize", { key: "sort" } );
					$.ajax({
						url: "update_media_sort_primary?"+ sorted, 
						cache: false,
						
					}); 
				})
				$( "#media_sortable" ).disableSelection();
				
				Dropzone.options.mediaUploaderForm = false;
				var mediaUpload = new Dropzone("#media_uploader_form", {
					acceptedFiles: "image/jpeg,image/png,image/gif", 
					maxFilesize: 8, 
					dictFileTooBig: "The file size of the image uploaded ({{filesize}}mb) is larger than the maximum file size allowed. Please keep images under {{maxFilesize}}mb", 
					dictDefaultMessage: "Drag and drop, or click here to add, images to the gallery. (JPG, PNG or GIF format)", 
					dictInvalidFileType: "The file must be a JPG, PNG or GIF."
				});
				 
				 
				mediaUpload.on("success", function(file, response) {
				$('.hidden_message').fadeIn(500);
				$("#media_sortable").append('<li id="media_' + response + '"><div class="image_container"><a href="/delete_image/' + response + '" id="delete_image_' + response + '" class="delete_image" ><img src="<?php echo IMAGES; ?>delete.png" alt="Delete image" title="Delete" /></a><div class="sort_image"><img src="https://s3-us-west-2.amazonaws.com/images.listfloat.com/thumb/' + response + '.jpg" /></div></div></li>'); 
				
				});
				
			});
			
			</script>
			
            <div id="testDrop"></div>
            
			<?php
			
			echo '<div id="media_holder">';
				
					echo '<form action="/_content/add_property/media_upload.php?p=' . $property . '" class="" method="post" enctype="multipart/form-data" id="media_uploader_form" >
							<div class="fallback">
								<p>Upload images here</p>
								<input type="file" multiple name="file" accept="image/jpeg,image/png,image/gif" capture="camera" >
						  		<div id="submit_holder">
									<img src="/images/loading.gif" class="loader" />
									<input type="submit" name="submit" value="Submit" id="upload_image" />
								</div>
							</div>
					  </form></p>';
					 
					?><p>Your uploaded Images.</p><p <?php  if($image_count < 2) { echo 'class="hidden_message" ';} ?> id="#sort_message">Drag and drop the images to change gallery order.</p><?php 
					echo '<ul id="media_sortable">';
					$r = @mysqli_query ($dbc, $q);
			while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
				$image_name = str_replace('.jpg', '', $row['media_title']);
				echo '<li id="media_' . $row['media_id'] . '"';
				 if ($primary_info) {
					 if($row['primary_info'] == 0) {
					 	echo ' class="child_media" ';
					 }
				} 
				echo '><div class="image_container">
						 <a href="/delete_image/' . $row['media_id'] . '" id="delete_image_' . $row['media_id'] . '" class="delete_image" >
							<img src="' . IMAGES . 'delete.png" alt="Delete image" title="Delete" />
						</a> 
						<div class="sort_image">
							<img src="https://s3-us-west-2.amazonaws.com/images.listfloat.com/thumb/' . $row['media_id'] . '.jpg" />
						</div>
					</div></li>';
				
			} // END FOREACH
				echo '</ul>';
				if ($primary_info) {
					echo '<p class="child_media">Green highlighted images are assigned to rooms or features in this property.<br />Deleting them here will delete them from the other gallery.<br />Sorting them here will not affect their sort on the other gallery.</p>';
				} 
			echo '<div>'; // END MEDIA HOLDER 
		}
	}
}
?>
</section>