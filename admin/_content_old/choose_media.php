<?php
require_once('includes/config.inc.php');
require_login();

$page_title = $page_name = 'Select Media';
include('includes/non_nav_header.php');

?>
<style>
#admin_nav {
	display:none;
}
</style>
<div id="dialog">
</div>

<?php

if(isset($_GET['i'])) {
		$i = (int) $_GET['i'];
		$i_sel = (int) $_GET['i'];
	
	if($i != 0) {
		
		$q="SELECT properties.id as property_id, property_info.id as property_info_id, property_info.media as media_selected, media.* FROM media, properties, property_info WHERE property_info.id = $i AND properties.id = property_info.properties_id AND media.property_id = properties.id";
		$r = @mysqli_query ($dbc, $q);
		
			?>
						
			<script>
			
			$(function() {
				$( "#media_sortable, #media_sortable_selected" ).sortable({ items: " li", placeholder: "media_sort_highlight", opacity: 0.5, connectWith: ".connectedSortable" }).disableSelection();
				$( "#media_sortable_selected" ).on( "sortupdate", function( event, ui ) {
					var sorted = $( "#media_sortable_selected" ).sortable( "serialize", { key: "sort" } );
					var theURL = "update_media_sort_child?i=<?php echo $i; ?>&"+ sorted;
					var imageCount = $("#media_sortable_selected li").size();
					
					if(imageCount == 1) {
						imagesNames = 'Image';
					}
					else {
						imagesNames = 'Images';
					}
					
					$("#info_feature_<?php echo $i; ?> .image_count_holder", parent.document).html('(' + imageCount + ') ' + imagesNames);
					
					
					$.ajax({
						url: theURL, 
						cache: false,
						
					}).done(function( data ) {
					}); 
				})
			
			});
					
			</script>
			
			<?php
			
			echo '<div id="media_holder">';
				
					
			echo '<h4>Drag images here to add them to this feature or room</h4>';		
			echo '<ul id="media_sortable_selected" class="connectedSortable"">';
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			$media_selected = explode(",", $row['media_selected']);
			
			foreach($media_selected as $image) {
					$image = (int) trim($image);
					if($image != 0) {
						echo '<li id="sort_' . $image . '" class="choose_image"><div class="image_container"><div class="sort_image"><img src="https://s3-us-west-2.amazonaws.com/images.listfloat.com/thumb/' . $image . '.jpg" /></div></div></li>';
					}
			}
			 
			echo '</ul>';
				
				
				echo '<h4>Drag images here to remove them from this feature or room</h4>';
				echo '<ul id="media_sortable" class="connectedSortable"">';
			$r = @mysqli_query ($dbc, $q);
			while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
				$media_selected = explode(",", $row['media_selected']);
				$selected = false;
				foreach($media_selected as $image) {
					if($image == $row['id']) {
						$selected = true;
					}
				}
				if (!$selected) {
					echo '<li id="sort_' . $row['id'] . '" class="choose_image"><div class="image_container"><div class="sort_image"><img src="https://s3-us-west-2.amazonaws.com/images.listfloat.com/thumb/' . $row['id'] . '.jpg" /></div></div></li>';
				}
				
			} // END FOREACH
				echo '</ul>';
				
				
			
			echo '<div>'; // END MEDIA HOLDER 
		}
}
include('includes/non_nav_footer.php');