<?php

require_admin_login();

if(isset($_GET['id']) && isset($_GET['order_id'])) {
	session_start();
	$_SESSION['pseudo_login']['id'] = $_GET['id'];
	$_SESSION['pseudo_login']['order_id'] = $_GET['order_id'];
	ob_end_clean();
	
	$url = BASE_URL . 'fulfill_order';
	header("Location: $url");
	
	exit();
	
}
else {
			
	$url = BASE_URL . 'panel';
	header("Location: $url");
	
	exit();
}
?>