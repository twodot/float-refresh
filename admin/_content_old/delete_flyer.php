<?php

require '/home/ec2-user/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

require_once('includes/config.inc.php');
require_login();


if(isset($_GET['i'])) {
	$i = $_GET['i'];
	$prop_id = (int) $i;
	
	if($prop_id != 0) {
		// echo $info_id;
		$q = "
			SELECT 
			*, 
			COUNT(*) as count_files 
			 
			FROM 
			flyers   
			
			WHERE 
			property_id =  $prop_id 
			AND user_id = $user_id  
		";
		
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		if ($row['count_files'] == 1) {
			$flyer_id = $row['id'];
			$q = "
			DELETE  
			FROM 
			
			flyers   
			
			WHERE 
			user_id = $user_id  
			AND id = $flyer_id 
			
			"; 
			
			$pdf = FLYERS . $flyer_id . '.pdf';
			$s3pdf = $flyer_id . '.pdf';
			$bucket = 'images.listfloat.com/flyers';
			$S3 = S3Client::factory(array('region' => 'us-west-2'));
			
			$result = $S3->deleteObject(array(
				'Bucket' => $bucket,
				'Key'    => $s3pdf
			));  
			
			if (isset($pdf) && file_exists($pdf) && is_file($pdf)){
				unlink($pdf);
			}
			
			$r = @mysqli_query ($dbc, $q);
			
				?>	
					$('#flyer_holder').fadeOut(300, function(){$('#flyer_holder').html('');});
					$('#flyer_form_holder').slideDown(300);
					$('#flyer_form_holder').removeClass("hidden");
				<?php
				
		} // END FILES CHECK
		else {
			echo '<p>You have reached this page in error</p>';
		}
	} // P = 0 CHECK
	else {
		echo '<p>You have reached this page in error</p>';
	}
}// ISSET I CHECK
else {
	echo '<p>You have reached this page in error</p>';
}
?>

