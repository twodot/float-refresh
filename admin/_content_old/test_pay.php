<?php

require_login();
$page_title = $page_name = 'Pay';

include('includes/header.php');


?>

<form action="/test_subscribe" method="POST">
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="<?php echo STRIPE_API; ?>"
    data-image="/square-image.png"
    data-name="Demo Site">
  </script>
</form>

<?php 
include('includes/footer.php');
?>