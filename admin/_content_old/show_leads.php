<?php

require_once('includes/config.inc.php');
include('includes/bitly.php');
require_login();
$page_title = $page_name = 'Add Property';

include('includes/header.php');

?>
<div id="error">
</div>

<div id="dialog">
</div>

<?php
if(isset($_GET['p']) || isset($p)) {
	if(isset($_GET['p']) ) {
		$property = (int) $_GET['p'];
	}
	else if(isset($p)) {
		$property = (int) $p;
	}
	if($property != 0) {
		
		$q_leads = "
		SELECT 
		leads.id as leads_id, 
		leads.first_name, 
		leads.last_name, 
		leads.email, 
		leads.phone, 
		
		lead_message.id as message_id, 
		lead_message.lead_id, 
		lead_message.property_id, 
		lead_message.message, 
		lead_message.show_phone, 
		
		properties.id as property_id, 
		properties.user_id 
		
		FROM 
		leads, 
		lead_message, 
		properties 
		
		WHERE 
		lead_message.property_id = $property
		AND lead_message.lead_id = leads.id 
		AND properties.id = $property  
		AND properties.user_id = $user_id";
		
		$r_leads = mysqli_query($dbc, $q_leads);
		echo '<div id="leads_holder">';
			echo '<div class="lead">';
				echo '<div class="lead_first_name">First Name</div>';
				echo '<div class="lead_last_name">Last Name</div>';
				echo '<div class="lead_email">Email</div>';
				echo '<div class="lead_phone">Phone</div>';
				echo '<div class="lead_message">Message</div>';
			echo '</div>';
			
			while($row_leads = mysqli_fetch_array($r_leads, MYSQLI_ASSOC)) {
				$first_name = $row_leads['first_name'];
				$last_name = $row_leads['last_name'];
				$email = $row_leads['email'];
				$phone = $row_leads['phone'];
				$message = $row_leads['message'];
				$show_phone = $row_leads['show_phone'];
				echo '<div class="lead">';
					echo '<div class="lead_first_name">' . $first_name . '</div>';
					echo '<div class="lead_last_name">' . $last_name . '</div>';
					echo '<div class="lead_email">' . $email . '</div>';
					echo '<div class="lead_phone">'; if($show_phone == 1) { echo $phone; } /*else if(!empty($phone)) {echo '<a href="#">Get Phone Number</a>';}*/ echo '</div>';
					echo '<div class="lead_message">' . $message . '</div>';
				echo '</div>';
				
			} // END WHILE
			
		echo '</div>'; // END LEAD HOLDER
		
	} // END IF PROPERTY != 0
} // END IF GET P EXISTS

require_once('js/global_ui.php');
?>

