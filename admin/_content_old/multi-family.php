<?php

require_login();
$page_title = $page_name = 'Multi-family Buildings';

include('includes/header.php');

?>
<script type="text/javascript">
$( document ).ready(function() {
	$('#add_button').click(function(){
		$('#add_button').slideUp(300);
		$(".start_here").slideUp(300);
		$('#add_property').slideDown(300);		
		
	})
	$('#save').click(function(e){
		 e.preventDefault();
		if($("#property_title").val().length > 0){
			$.ajax({
				url: "add_property", 
				data: jQuery("#property_addition").serialize(),
				dataType:"script", 
				cache: false,
			}).done(function(data) {
				
			});
		}
	})
	
	<?php
	
	if(isset($_GET['highlight'])) {
		$highlight = (int) $_GET['highlight'];
		if($highlight != 0) {
			?>
			$( "#b_con_<?php echo $highlight; ?>" ).effect( "slide", "slow" );
			<?php
		}
	}
	
	?>
	
});

</script>
<?php

// SELECT FOR PROPERTY PRIMARY INFO


?>
<div id="error">
</div>
<div id="dialog"></div>

<?php

$q =" SELECT *, TIMESTAMPDIFF(DAY, NOW(), days_left) + 1 as days_left FROM subscription, subscription_types WHERE subscription.user_id = $user_id AND subscription.subscribe_level = subscription_types.id";
$r = @mysqli_query ($dbc, $q);
	
/* if( mysqli_num_rows($r) == 1) {
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	if($row['active'] == 1) {
		if ($row['deactivate_in'] == 1) {
			echo '<div id="active_inactive" class="account_inactive">Your account is active and will deactivate in ' . $row['days_left'] . ' days, <div id="activate">Activate your account</div></div>';
			echo '<div id="days_left">You have ' . $row['days_left'] . ' days remaining before your account deactivates</div>';
		}
		
	}
	if($row['active'] == 0) {
	
		echo '<div id="active_inactive" class="account_inactive" class="account_inactive">Your account is inactive, <div id="activate">Activate your account</div></div>';
		echo '<div id="current_subscription">Previous Subscription - ' . $row['allowed_prop'] . ' Properties | ' . $row['days'] . ' Days</div>';
	
	}
}
else {
	echo '<div id="select_subscription">Please <a href="/select_subscription">select a subscription</a>.</div>';
}*/



?>
<div class="clear"></div>
<div id="add_button">
	Add a new multi-family building
</div><!-- END ADD BUTTON --><?php if($show_hints) { ?> <img src="/images/start_here.png" class="hints start_here" /> <?php } ?>


<div id="add_property">
	
	<form id="building_addition">
    	<input type="text" id="building_title" value="" name="title" placeholder="Title" maxlength="50" />
        <input type="hidden" name="u" value="<?php echo $user_id; ?>" />
        <input type="submit" id="save_building" name="Submit" value="SAVE" />
        <input type="hidden" name="submitted" value="true" />
    </form>
</div><!-- END ADD PROPERTY -->
<div class="clear"></div>

<?php

if($show_hints) {
		echo '<div class="full_width">';
			
			echo '<div class="one_half">';
			
			echo '</div>';// END ONE THIRD
			
			
			echo '<div class="hints one_third narrow">';
				echo '<img src="/images/drag_and_drop.png" />';
			echo '</div>';// END ONE THIRD
			
			
			echo '<div class="clear"></div>';
		echo '</div>'; // END FULL WIDTH
}

$r = get_buildings($user_id, $dbc);

echo '<div id="buildings" class="one_half">';
echo '<h3>Multi-family Buildings</h3>';
echo '<div class="holder">';
while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
	$beacon_title = $row['beacon_title'];
	$total_beacons = $row['total_beacons'];
	echo '<div class="'; if(is_null($beacon_title)) { echo' droppable building_beacon_available unassigned'; } else { echo ' assigned'; } echo ' building" id="b_con_' . $row['building_id'] . '"><a href="/create_building/' . $row['building_id'] . '">' . $row['building_name'] . '</a>';
	
		if (is_null($beacon_title)) {
			echo ' <img src="' . IMAGES . 'caution.png" alt="caution" title="Please assign a beacon" class="caution" />';
			echo ' <a href="/delete_building?b=' . $row['building_id'] . '"  class="delete_building_button"><img src="' . IMAGES . 'delete.png" alt="Delete info" title="Delete ' . $row['building_name']	. '" class="tooltip" /></a>';
		}
		else {
			echo ' <a href="/delete_building?b=' . $row['building_id'] . '"  class="delete_building_button"><img src="' . IMAGES . 'delete.png" alt="Delete info" title="Delete ' . $row['building_name']	. '" class="tooltip" /></a>';
			echo '<div class="clear"></div><div class="beacon_prop">Primary Beacon<br />Beacon ' . $beacon_title . ' </div><div class="beacon_prop">Total Beacons<br />' . $total_beacons . ' beacon'; if ($total_beacons > 1) { echo 's';} echo '</div>'; 
		}
		echo '<div class="clear"></div></div>';
}
echo '</div>'; // END BBUILDINGS HOLDER
echo '</div>'; // END Buildings



$r = get_avail_beacons($user_id, $dbc);

echo '<div id="available_beacons" class="one_third">';
echo '<h3>Available Beacons</h3>';
echo '<div class="holder">';
while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
	echo '<div id="beacon-' . $row['id'] . '" class="beacon draggable">' . $row['title'] . '</div>';
}

echo '</div>'; // END HOLDER

// ACTIVE BEACONS START

echo '<div class="clear"></div>';
		echo '<div class="active_beacons">';
			echo '<h3>Active Beacons</h3>';
			
			echo '<div class="holder_active_beacons">';
			$r = get_active_beacons($user_id, $dbc);
			echo $row['property_title'];
			echo $row['building_name'];
			while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
				
			if (!is_null($row['property_title'])) {
				$prop_title = $row['property_title'];
				$link = 'property';
				$link_id = $row['property_id']; 
			}
			elseif (!is_null($row['building_name'])) {
				$prop_title = $row['building_name'];
				$link = 'building';
				$link_id = $row['building_id']; 
			}
				
				echo '<a href="/' . $link . '/' .  $link_id; if(!empty($row['info_title'])){echo '#info_container_' . $row['prop_info_id']; } echo '" ><div id="beacon-' . $row['id'] . '" class="active_beacon" title="Active in ' . $prop_title; if(!empty($row['info_title'])){echo ' / ' . $row['info_title']; }  echo' ">' . $row['title'] . '</div></a>';
			}
			echo '</div>'; // END ACTIVE HOLDER //
		echo '</div>';// END ACTIVE BEACONS

echo '</div>'; // END BEACONS


include('includes/footer.php');



?>