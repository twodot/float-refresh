<?php

require_once('includes/config.inc.php');
require_login();
if (isset($_GET['p'])) {
	$property = (int) $_GET['p'];
	$table = 'properties';
	$table_id = 'property_id';
}
else if (isset($_GET['b'])) {
	$property = (int) $_GET['b'];
	$table = 'properties';
	$table_id = 'property_id';
}
if (isset($property)) {	
		if($property != 0) {
			$q = "
				SELECT 
				user_id   
				
				FROM 
				$table  
				
				WHERE 
				id = $property
			";
			
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			if ($user_id == $row['user_id']) {
				if (isset($_GET['address'])) {
					
					$address = urlencode($_GET['address']);
					
					
					
					$url = "http://maps.google.com/maps/api/geocode/json?address=" . $address . "&sensor=false";
					$response = file_get_contents($url);
					$response = json_decode($response, true);
					
					$theAddress = array();
					foreach($response['results'][0]['address_components'] as $section) {
						$theTag = $section['types'][0];
						$theAddress[$theTag] = $section['short_name'];
					}
					

					
					// print_r($theAddress);
					
                    $lat = $response['results'][0]['geometry']['location']['lat'];
					$long = $response['results'][0]['geometry']['location']['lng'];
					
					$google_error = false;
					// STREET NUMBER
					if(isset($theAddress['street_number'])) {
						$street_number = $theAddress['street_number'];
					}
					else {
						$google_error = true;
					}
					
					// STREET NAME
					if(isset($theAddress['route'])) {
						$street_name = $theAddress['route'];
					}
					else {
						$google_error = true;
					}
					
					// CITY
					if(isset($theAddress['locality'])){
						$city = $theAddress['locality'];
					}
					else {
						$google_error = true;
					}
					
					// STATE
					if(isset($theAddress['administrative_area_level_1'])){
						$state = $theAddress['administrative_area_level_1'];
					}
					else {
						$google_error = true;
					}
					// ZIP
					if(isset($theAddress['postal_code'])){
						$zip = $theAddress['postal_code'];
					}
					else {
						$google_error = true;
					}
					
					// NEIGHBORHOOD
					if(isset($theAddress['neighborhood'])) {
						$neighborhood = $theAddress['neighborhood'];
					}
					else {
						$neighborhood = '';
					}
					
					// COUNTY
					if(isset($theAddress['administrative_area_level_2'])) {
						$county = $theAddress['administrative_area_level_2'];
						$county = str_replace(' County', '', $county);
					}
					else {
						$county = '';
					}
					
					
					if($google_error) {
						
						
						if(isset($state) && isset($city)) {
							
							$url = "http://maps.google.com/maps/api/geocode/json?address=" . urlencode($city) . '+' . urlencode($state) . "&sensor=false";
							$response = file_get_contents($url);
							$response = json_decode($response, true);	
									
							$lat = $response['results'][0]['geometry']['location']['lat'];
							$long = $response['results'][0]['geometry']['location']['lng'];
							$revisedAddress = $lat . ', ' . $long;
							?>
                            $('#no_address_found_state').val('<?php echo $state; ?>');
                            $('#no_address_found_city').val('<?php echo $city; ?>');
                            <?php
						}
						else if (isset($state)) {
							
							$url = "http://maps.google.com/maps/api/geocode/json?address=" . urlencode($state) . "&sensor=false";
							$response = file_get_contents($url);
							$response = json_decode($response, true);	
									
							$lat = $response['results'][0]['geometry']['location']['lat'];
							$long = $response['results'][0]['geometry']['location']['lng'];
							$revisedAddress = $lat . ', ' . $long;
							?>
                            $('#no_address_found_state').val('<?php echo $state; ?>');
                            <?php
							
						}
						if (isset($revisedAddress)) {
							?>
                            
                            var lat_long_map;
                            function lat_long_initialize() {
                                
                              var mapOptions = {
                                zoom: 8,
                                center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>)
                              };
                              lat_long_map = new google.maps.Map(document.getElementById('map_for_latitude_longitude'),
                                  mapOptions);
                                  
                                  
                                google.maps.event.addListener(lat_long_map, 'click', function( event ){
                                    
                                    theLat = document.getElementById("no_address_found_latitude");
                                    theLat.value = event.latLng.lat();
                                    
                                    
                                    theLong = document.getElementById("no_address_found_longitude");
                                    theLong.value = event.latLng.lng();
                                
                                    
                                });
            
                            }
                            
                            function loadMapLatLong() {
                              var script = document.createElement('script');
                              script.type = 'text/javascript';
                              script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAiAePyQf5emFwHaIQlsvwFGX0VXmvEiNg&callback=lat_long_initialize';
                              document.body.appendChild(script);
                            }
                            
                            
                            
                            loadMapLatLong()
                            
                            <?php
						}
						
						?>
                        
                        
                        $("#error").html("<p>There was an error with your address</p>").fadeIn(300);
                        $("#no_address_found").slideDown(500);
						$("#error").delay(2000).fadeOut(1000);
                        $("#get_lat_long").show();
                        $("#get_address_loader").addClass("hidden");
                        $("#form_address").slideUp(300);
                        
                        
                		<?php
						exit();
					}
					else {
						$in_address = $street_number . ' ' . $street_name . ', ' . $city . ', ' . $state;
					}
					
					
				 	$q = "
						UPDATE 
						$table  
						
						SET 
						address = '$in_address', 
						state = '$state', 
						zip = '$zip',  
						latitude =  '$lat',
						longitude =  '$long', 
						county = '$county', 
						community = '$neighborhood', 
						show_walkscore = 1, 
						show_greatschools = 1 
						
						WHERE 
						id = $property 
						AND user_id = $user_id 
						
					"; 
					
					$r = @mysqli_query ($dbc, $q);
					
					
					echo '$("#address").val("' . $in_address . '").fadeIn(300);';
					echo '$("#lat").val("' . $lat . '").fadeIn(300);';
					echo '$("#long").val("' . $long . '").fadeIn(300);';
					echo '$("#address_details_holder").fadeIn(300);';
					echo '$("#edit_map").fadeIn(300);';
					echo '$("#prop_map").html(\''; echo '<iframe width="100%" height="350" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBR5USylIliiJxWUroSXSaL7KD3T_WM328&q=' . $lat . ',' . $long .  '&zoom=18"></iframe>\');';
					echo '$("#show_walkscore").removeClass("hidden_score");';
					echo '$("#lat_long_holder").removeClass("hidden_score");';
					
					echo '$("#lat_long_holder").fadeIn(300);'; 
					echo '$("#form_address").slideUp(300);';
					echo '$("#no_address_found").slideUp(300);';
					echo '$("#neighborhood").val("' . $neighborhood . '");';
					echo '$("#county").val("' . $county . '");';
					echo '$("#prop_map").slideDown(500);';
			        echo '$("#address_holder").slideDown(500);';
						
					
				
				}// END GET ADDRESS CHECK 
			}
			else {
				echo 'You have reached this page in error';
			}
			
		}// END PROPERTY INT CHECK
} // IN P ISSET CHECK

	
?>

