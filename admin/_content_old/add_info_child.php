<?php

require_once('includes/config.inc.php');
require_login();
if ($user_id == $_GET['u']) {
	if(isset($_GET['submitted'])) {
		$trimmed = array_map('trim', $_GET);
		$title = $dbc->real_escape_string($trimmed['title']);
		$text = $dbc->real_escape_string($trimmed['info']);
		$notes = ''; //$dbc->real_escape_string($trimmed['notes']);
		$property = $dbc->real_escape_string($trimmed['p']);
		$info = $dbc->real_escape_string($trimmed['i']);
		
		$title = htmlspecialchars ($title, ENT_QUOTES);
		$text = htmlspecialchars ($text, ENT_QUOTES);
		$notes = htmlspecialchars ($notes, ENT_QUOTES);
		
		
		$q = "
			INSERT INTO 
			property_info 
			
			(
			properties_id, 
			title, 
			text, 
			notes, 
			child_of   
			)
			
			VALUES (
			'$property', 
			'$title', 
			'$text', 
			'$notes', 
			'$info'
			)
		";
		if($r = @mysqli_query ($dbc, $q)) {
			
			$info_id = mysqli_insert_id($dbc);
			
			?>
            
			$("#<?php echo 'info_feature_' . $info; ?> .child_sort").append('<li id="info_<?php echo $info_id ?>" ><div id="info_feature_<?php echo $info_id ?>" class="child_container property_info property_info_child property_info_avail"><form action="/update_info" method="get" class="update_info_form"><div class="holder"><h4 class="info_title"><?php echo $title; ?><span id="prop_id_<?php echo $info_id; ?>"></span></h4><input class="edit_info_title" name="title" value="<?php echo $title; ?>" /><ul class="prop_revisions"><li><a href="/update_info?i=<?php echo $info_id ?>" class="edit_info_child_button">Edit</a></li><li><a href="/delete_info?i=<?php echo $info_id ?>"  class="delete_info_button">(&ndash;) Delete</a></li></ul><div class="property_info_text"><p><?php echo $text; ?></p><textarea class="edit_info_box" name="info" ><?php echo $text; ?></textarea></div><input type="hidden" name="u" value="<?php echo $user_id; ?>" /><input type="hidden" name="p" value="<?php echo $property; ?>" /><input type="hidden" name="i" value="<?php echo $info_id ?>" /><input type="hidden" name="submitted" value="true" /><input type="submit" class="edit_save" name="Submit" value="SAVE"><input type="button" class="edit_cancel" value="CANCEL" /></div></form></div></li>').fadeIn(300); 
            
            $('#<?php echo 'info_feature_' . $info; ?> .child_info_addition .info_title').val("");
            $('#<?php echo 'info_feature_' . $info; ?> .child_info_addition textarea').val("");
            $('#<?php echo 'info_feature_' . $info; ?> .add_child_info_form').slideUp(500);
            remove_info();
            start_info();
			<?php
		}//END INSERT CHECK 
		
	} // END SUBMITTED CHECK
	

} // END USER ID = GET USER ID CHECK
?>

