<?php
	require_admin_login();
	header('Content-Type: application/json');
	
	$data["status"]="success";
	$data["message"]="";
	
	if(isset($_GET['id']) && isset($_GET['f'])) {
		$get_id = (int) $_GET['id'];
		$get_f = process_input($_GET['f']);
		
		$q_users = "SELECT * FROM users WHERE user_id=".$get_id;
		$r_users = mysqli_query($dbc, $q_users);
		
		if($row=mysqli_fetch_assoc($r_users)){
			if($get_f=="true" && $row["test"]==1){
				$data["message"]="User Has Already Been A Test User";
			} else if($get_f=="false" && $row["test"]==0){
				$data["message"]="User Is Not A Test User";
			} else if($get_f=="true" && $row["test"]==0){
				// query
				$q_sub = "UPDATE users SET test=1 WHERE user_id=".$get_id;
				if(!mysqli_query($dbc, $q_sub)){
					$data["status"]="failure";
					$data["message"]=mysqli_error($dbc);
				} else
					$data["message"]="User Has Been Set To Be A Test User";
			} else{
				// query
				$q_sub = "UPDATE users SET test=0 WHERE user_id=".$get_id;
				if(!mysqli_query($dbc, $q_sub)){
					$data["status"]="failure";
					$data["message"]=mysqli_error($dbc);
				} else
					$data["message"]="User's Test State Has Been Removed";
			}
		} else{
			$data["status"]="failure";
			$data["message"]="User Does Not Exist";
			
		}
	} else{
		$data["status"]="failure";
		$data["message"]="Unsupported Operation";
	}
	
	echo json_encode($data);
	
	function process_input($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>