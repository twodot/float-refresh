<?php
	require_admin_login();
	header('Content-Type: application/json');
	
	$data["status"]="success";
	$data["message"]="";
	
	if(isset($_GET['id']) && isset($_GET['f'])) {
		$get_id = (int) $_GET['id'];
		$get_f = process_input($_GET['f']);
		
		$q_sub = "SELECT * FROM subscription WHERE user_id=".$get_id;
		$q_users = "SELECT * FROM users WHERE user_id=".$get_id;
		$r_sub = mysqli_query($dbc, $q_sub);
		$r_users = mysqli_query($dbc, $q_users);
		
		if(mysqli_num_rows($r_users)){
			if($row = mysqli_fetch_assoc($r_sub)){
				if($get_f=="true" && $row["active"]==1){
					$data["message"]="User's Subscription Has Already Been Activated";
				} else if($get_f=="false" && $row["active"]==0){
					$data["message"]="Subscription Has Already Been Turned Off";
				} else if($get_f=="true" && $row["active"]==0){
					// query
					$q_sub = "UPDATE subscription SET active=1 WHERE user_id=".$get_id;
					if(!mysqli_query($dbc, $q_sub)){
						$data["status"]="failure";
						$data["message"]=mysqli_error($dbc);
					} else
						$data["message"]="User's Subscription Has Been Activated";
				} else{
					// query
					$q_sub = "UPDATE subscription SET active=0 WHERE user_id=".$get_id;
					if(!mysqli_query($dbc, $q_sub)){
						$data["status"]="failure";
						$data["message"]=mysqli_error($dbc);
					} else
						$data["message"]="User's Subscription Has Been Deactivated";
				}
			} else{
				$data["status"]="failure";
				$data["message"]="The User Does Not Have Subscription";
			}
		} else{
			$data["status"]="failure";
			$data["message"]="User Does Not Exist";
			
		}
	} else{
		$data["status"]="failure";
		$data["message"]="Unsupported Operation";
	}
	
	echo json_encode($data);
	
	function process_input($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>