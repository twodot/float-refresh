<?php

require_once('includes/config.inc.php');
require_login();
if ($user_id == $_GET['u']) {
if(isset($_GET)) {
	$trimmed = array_map('trim', $_GET);
	
	$beacon_id = (int) str_replace('beacon-', '', $trimmed['b']);
	
	$property = (int) str_replace('b_con_', '', $trimmed['p']);
	
	$prop_info_id  = $dbc->real_escape_string(str_replace('info_container_', '', $trimmed['i']));
	
		$q = "
			UPDATE 
			property_info 
			
			SET 
			beacon_id = $beacon_id 
			
			WHERE 
			properties_id = $property 
			AND id = $prop_info_id  
			
			";
		
		$r = @mysqli_query ($dbc, $q);
		
		
		$q="SELECT 
			properties.id as the_prop_id, 
			properties.*, 
			property_info.* 
			
			FROM 
			properties, 
			property_info 
			
			WHERE 
			properties.id = property_info.properties_id 
			AND property_info.id = $prop_info_id ";
			
		$r = @mysqli_query($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$prop_id = $row['the_prop_id'];
		
		if($row['primary_info'] == 1) {
			$beacon_type = ucwords ($row['property_data_type']);
			$activate_available = TRUE;
			
			$q = "
			UPDATE 
			properties
			
			SET 
			active = 1 
			
			WHERE 
			id = $property 
			";
		
			$r = @mysqli_query ($dbc, $q);
			
		}
		else {
			$beacon_type = 'Information';
			$activate_available = FALSE;
		}
		
		$q = "
			UPDATE 
			beacons
			
			SET 
			property_info_id = $prop_info_id, 
			beacon_type = '$beacon_type'
			
			WHERE 
			id = $beacon_id 
			
			";
		$r = @mysqli_query ($dbc, $q);	

		if (mysqli_affected_rows($dbc) ==1){
			
			$q = "
			SELECT 
			beacons.title, 
			beacons.id, 
			beacons.property_info_id, 
			
			property_info.title as info_title, 
			property_info.properties_id, 
			property_info.beacon_id, 
			property_info.id as prop_info_id, 
			property_info.primary_info,  
			
			properties.title as property_title, 
			properties.id as property_id
			
			FROM 
			beacons, 
			property_info, 
			properties  
			
			WHERE 
			beacons.user_id = $user_id 
			AND beacons.property_info_id != 0 
			AND beacons.property_info_id = property_info.id 
			
			AND beacons.id = property_info.beacon_id 
			AND property_info.properties_id = properties.id
			
			AND beacons.id = $beacon_id";
			
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			$title = $row['title'];
			$id = $row['id'];
			$primary_info = $row['primary_info'];
			
				if($activate_available) {
					?>
                    $('#lift_off').show();
                    
                    $('#big_ole_activation_warning').hide();
                    <?php
				}
				
			?> 
            
            	
                
                $('#prop_id_<?php echo $prop_info_id; ?>').html(' Beacon <?php echo $title; ?> <a href="/remove_beacon?b=<?php echo $beacon_id; ?><?php if ($primary_info == 1) {echo '&p=' . $property;} ?>&i=<?php echo $prop_info_id; ?>" class="remove_beacon" ><img src="/images/delete.png" class="remove_beacon_image" alt="Remove beacon" title="Remove this beacon" /></a>').fadeIn(300); 
                
                $('#prop_feature_id_<?php echo $prop_info_id; ?>').html(' Beacon <?php echo $title; ?>').fadeIn(300); 
                
                
                $("#beacon-<?php echo $beacon_id; ?>").remove();
                if($('#prop_id_<?php echo $prop_info_id; ?>').parent().attr("class") == "beacon_title_holder") {
                	$('#prop_id_<?php echo $prop_info_id; ?>').parent().addClass("beacon_assigned");
                }
               
				$("#info_<?php echo $prop_info_id; ?>").removeClass("property_info_avail");
                $("#info_container_<?php echo $prop_info_id; ?>").removeClass("property_info_avail");
                
                $(".holder_active_beacons").prepend('<?php echo '<a href="/property/' .  $row['property_id']; if(!empty($row['info_title'])){echo '#info_container_' . $row['prop_info_id']; } echo '" ><div id="beacon-' . $row['id'] . '" class="active_beacon" title="Active in ' . $row['property_title']; if(!empty($row['info_title'])){echo ' / ' . $row['info_title']; }  echo' ">' . $row['title'] . '</div></a>'; ?>').delay(500);
                $( '.active_beacon' ).tooltip();
                
                beacon_drag()
                
                
			<?php
			
		}
		else {
			echo '$("#error").html("<p>The beacon could not be added to the property!</p>").fadeIn(300);';
			echo '$("#error").delay(2000).fadeOut(1000);';
		}
	
		
	
}
}

?>

