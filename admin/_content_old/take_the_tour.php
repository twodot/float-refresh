<script>

jQuery(document).ready(function( $ ) {
	<?php 
		if($Subscription->active == 0 &&  $Subscription->free != 1) {
			?>
			$( "#subscription_popup" ).dialog({
			  dialogClass: "no-close",
			  modal: true,
			  width: 500, 
			  dialogClass: "get_started_popup"
				
			});
			<?php
		}
		else if ($UserInfo->show_tour==1) {
			?>
			$( "#subscription_popup" ).dialog({
			  dialogClass: "no-close",
			  modal: true,
			  width: 500, 
			  dialogClass: "get_started_popup"
				
			});
			<?php
		}
	?>
	
	$( "#tinker" ).click(function(){
		$( "#subscription_popup" ).dialog( "close" );
		$.ajax({
			url: "taken_tour", 
			cache: false,
		}).done(function(data) {
			
		});
	})
	
	$( "#start_subscription_button" ).click(function(){
		window.location.href = "/select_subscription";
	})
	$( ".show_me_around, .help" ).click(function(){
		$( "#subscription_popup" ).dialog( "close" );
		introJs().oncomplete(function() {
			$.ajax({
				url: "taken_tour", 
				cache: false,
			}).done(function(data) {
			
			});
		}).setOptions(
		{
            steps: [
              
              {
                element: '#add_button',
                intro: "Let&rsquo;s get you acquainted with your Float control panel. If you ever get stuck, you can take this tour again at any time. First, start here to create a new property by adding the address and listing price. This will open the step-by-step property setup tool.",
                position: 'right'
              },
			  
              {
                element: '#unassigned_properties',
                intro: 'Once you have completed the property setup, the property listing will appear here. If it has a beacon assigned to it, you can activate the property by sliding it to the Active Properties section. Inactive properties can remain in this queue and be reactivated at any time, but they must have at least one beacon assigned.',
                position: 'left'
              },
			  {
                element: '#active_properties',
                intro: "The Active Properties section shows a list of all of the live properties visible to home-shoppers through the Float App. In order to activate the property, and broadcast the information out to the world, you&rsquo;ll need an active Float subscription. ", 
				position: 'right'
              },
              {
                element: '#avail_beacons_label',
                intro: 'Shortly after you purchase your beacons, you&rsquo;ll find their icons here in the Available Beacons section. The numbers on the icons coincide with the numbers on the top of the beacons&rsquo; cases. As soon as you see them appear you can assign them to an inactive property. <br /><br />They look like this&hellip; <br /><div class="beacon" style="position: relative;">1</div></p><div class="clear"></div>',
                position: 'left'
              },
              {
                element: '#active_beacons',
                intro: 'Active beacons in use appear here. Clicking on an active beacon will show you the property it is assigned to for easy inventory management. <br /><br />They look like this&hellip; <br /><div class="active_beacon" style="position: relative;">1</div></p><div class="clear"></div>', 
				position: 'left'
              },
              {
                element: '#download_marketing',
                intro: 'Yard sign riders are available for download and printing so you can make sure everyone knows your property is Float-enabled. Of course, existing app users will know the moment they walk within range of a beacon.', 
				position: 'right'
              },
              {
                element: '.multi-family_link',
                intro: 'Is your property in a multi-family building? Create a building listing to show the building&rsquo;s amenities and features in addition to the available property. Multiple properties can be assigned to a single building listing.', 
				position: 'bottom'
              },
              {
                element: '.my_account_link',
                intro: 'Manage your account profile, billing/shipping information, transaction history and subscription level in the My Account section.', 
				position: 'bottom'
              },
              {
                element: '.beacons_link',
                intro: 'Order your beacons and view their information here. Orders are processed and shipped within a few business days.', 
				position: 'bottom'
              },
              {
				element: '.help', 
                intro: 'That&rsquo;s it! You&rsquo;re ready to rock. Keep an eye out for the help icons like this one throughout the control panel for instant assistance if you need it. If you have any further questions, please contact customer support at 970-77-FLOAT (970-773-5628) or email <a href="mailto:support@listfloat.com">support@listfloat.com</a>', 
				position: 'bottom'
              }
            ], 
			showProgress: true, 
			showBullets: false, 
          }).start();
	})
})

</script>