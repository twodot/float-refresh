<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_login();
?>
<h3><span class="add_property_name">General</span><span class="add_property_icon">Icon</span></h3>
 <section>
    
    <?php // END HIDE FLYER DIV ?>
    <form id="info_update_2">
    
        <label for="property_title">Property Title</label>
        <input type="text" id="property_title" value="<?php echo $property_title; ?>" name="title" placeholder="Title" maxlength="50" />
        
        <?php // DESCRIPTION ?>
        <label for="description">Marketing Description</label>
        <textarea name="description" id="marketing_description_text_box"><?php echo $marketing_description; ?></textarea>
        
        <input type="hidden" name="p" value="<?php echo $property; ?>" />
        <input type="hidden" name="submitted" value="true" />
    
    </form>
    <?php // END DESCRIPTION ?>
</section>