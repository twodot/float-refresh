<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_login();
?>

<script type="text/javascript">
jQuery(document).ready(function( $ ) {
	$("#property_setup").steps({
		headerTag: "h3",
		enableContentCache: true, 
		bodyTag: "section",
		transitionEffect: "slide",
		saveState: true, 
		<?php 
		if($completed) {
			echo 'enableAllSteps: true, 
			 cssClass: "wizard property_tabs", ';
		}
		else {
			echo 'startIndex: ' . $progress . ', ';
		}
		
		if(isset($_GET['step'])) {
			$step = (int) $_GET['step'];
			echo 'startIndex: ' . $step . ', ';
		}
		
		?>
		stepsOrientation: "vertical", 
		labels: {
			current: "current step:",
			pagination: "Pagination",
			finish: "Finish",
			next: "Save & Next",
			previous: "Previous",
			loading: "Loading ..."
		}, 
		saveState: true, 
		<?php 
		if ($latitude != 0.0000000 && $longitude != 0.0000000) {
		?>
			onInit: function (event, currentIndex) {
				if(currentIndex == 0) {
					loadMap();
				}
			},
		<?php 
		}?>
		onStepChanged: function (event, currentIndex, newIndex) { 
			if(currentIndex == 0) {
				loadMap();
			}
		}, 
		onStepChanging: function (event, currentIndex, newIndex) { 
			
			var url = '/_content/add_property/save_step_' + currentIndex + '?p=<?php echo $property; ?>'; 
			
			var formID = "#info_update_" + currentIndex;
			
			$.ajax({
				url: url, 
				data: jQuery($(formID)).serialize(),
				type: "POST",
				cache: false,
			}).done(function(data) {
			});
			
			
			if(currentIndex == 0) {
				if($('#address').val() == '') {
					$('#get_lat_long').css('background-color', '#ef4036');
					$('#get_lat_long').css('color', '#fff');
					return(false);
				}
				else {
					return(true);
				}
			}
			else {
				return(true);
			}
			
		}
	});
		
	$('#get_beacon_step').click(function(e){
		e.preventDefault();
		$("#property_setup").steps("previous");
	})
	
})
</script>
<div id="property_setup">
   
     <?php 
	
	 
	 include('_content/add_property/property_address.php'); 
	 
	 include('_content/add_property/property_details.php'); 
	 
	 include('_content/add_property/property_general_info.php'); 
	 
	 include('_content/add_property/property_media.php');
	 
	 include('_content/add_property/property_features.php');
	 
	 include('_content/add_property/property_walk_schools.php');
	 
	 include('_content/add_property/property_beacons.php');
	 
	 include('_content/add_property/property_activation.php'); 
	 
	 ?>
     
   
    <div class="steps_clear"></div>
</div>