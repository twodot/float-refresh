<?php

session_start();
// Include the AWS SDK using the Composer autoloader
require '/home/ec2-user/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

require_once('../../includes/config.inc.php');
require_login();

if (isset($_GET['p'])) {
	$p = (int) $_GET['p'];
	
	$q = "SELECT user_id FROM properties WHERE id = $p";
	$r = mysqli_query($dbc, $q);
	if(mysqli_num_rows($r) == 1) {
	
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	if($row['user_id'] == $user_id) {
		include("../../includes/resize.php");
	
		function reArrayFiles(&$file_post) {
		
			$file_ary = array();
			$file_count = count($file_post['name']);
			$file_keys = array_keys($file_post);
		
			for ($i=0; $i<$file_count; $i++) {
				foreach ($file_keys as $key) {
					$file_ary[$i][$key] = $file_post[$key][$i];
				}
			}
		
			return $file_ary;
		}
				//$file_ary = reArrayFiles($_FILES['file']);
				// var_dump($_FILES['file']);
				
				$q="SELECT COUNT(*) as property, property_info.id as property_info_id FROM properties, property_info  WHERE properties.user_id = $user_id AND properties.id = property_info.properties_id AND property_info.properties_id = $p AND primary_info = 1";
				$r = @mysqli_query ($dbc, $q);
				$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
				if($row['property'] == 1) {
					$i = $row['property_info_id'];
					
					$q="SELECT properties.id as property_id, properties.title as title, properties.price as price, property_info.id as info_id, property_info.primary_info as primary_info  FROM properties, property_info  WHERE properties.user_id = $user_id AND properties.id = property_info.properties_id AND property_info.id = $i";
					$r = @mysqli_query ($dbc, $q);
					$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
					$p = $row['property_id'];
					
					
					foreach ($_FILES as $file) {
						
					   if (is_uploaded_file ($file['tmp_name'])){
							$temp = UPLOADS . md5($file['name']);
							
							if (move_uploaded_file($file['tmp_name'], $temp)){
								$image = $dbc->real_escape_string($file['name']);;
								$type = 'image/jpeg'; //$file['type'];
								
								$bucket = 'images.listfloat.com';
								$keyname = $temp;
								// $filepath should be absolute path to a file on disk						
								$filepath = $temp;
								
								
								$extension = strtolower(strrchr($file['name'], '.'));
								$file_name = str_replace($extension, '', $image);
								
								$q="INSERT INTO media SET property_id = $p, info_id = $i, user_id = $user_id, media_type = '$type', title = '$image'";
								$r = @mysqli_query ($dbc, $q);
								
								$id = mysqli_insert_id($dbc);
								
								rename ($temp, UPLOADS . "$id" . $extension);
								$image_rename =  '../float_req/images/' . "$id" . $extension;
								
								$theFile = UPLOADS . "$id" . $extension;
								// Instantiate the client.
								$s3 = S3Client::factory(array('region' => 'us-west-2'));
								
								$theFileUploaded = UPLOADS . "thumb/$id" . $extension;
								
								// THUMBNAIL SECTION
								$thumbBucket = 'images.listfloat.com/thumb';
								$thumbS3 = S3Client::factory(array('region' => 'us-west-2'));
								$thumbnail = generate_image_thumbnail($theFile, $theFileUploaded, $id);	
	
								try {
									$upload = $thumbS3->upload($thumbBucket, $id . '.jpg', fopen($thumbnail, 'rb'), 'public-read');
								} catch(Exception $e) { 
								}					
								
								if (isset($thumbnail) && file_exists($thumbnail) && is_file($thumbnail)){
									unlink($thumbnail);
								}	
								echo $id;
								
								// PHONE SECTION
								$phoneBucket = 'images.listfloat.com/phone';
								$phoneS3 = S3Client::factory(array('region' => 'us-west-2'));
								$phone = generate_image_phone($theFile, $theFileUploaded, $id);	
	
								try {
									$upload = $phoneS3->upload($phoneBucket, $id . '.jpg', fopen($phone, 'rb'), 'public-read');
								} catch(Exception $e) { 
								}					
								
								if (isset($phone) && file_exists($phone) && is_file($phone)){
									unlink($phone);
								}
								
								// TABLET SECTION
								$tabletBucket = 'images.listfloat.com/tablet';
								$tabletS3 = S3Client::factory(array('region' => 'us-west-2'));
								$tablet = generate_image_tablet($theFile, $theFileUploaded, $id);	
	
								try {
									$upload = $tabletS3->upload($tabletBucket, $id . '.jpg', fopen($tablet, 'rb'), 'public-read');
								} catch(Exception $e) { 
								}					
								
								if (isset($tablet) && file_exists($tablet) && is_file($tablet)){
									unlink($tablet);
								}
								
								if (isset($theFileUploaded) && file_exists($theFileUploaded) && is_file($theFileUploaded)){
									unlink($theFileUploaded);
								}
								
								if (isset($theFile) && file_exists($theFile) && is_file($theFile)){
									unlink($theFile);
								}
							}
							
						}
						
					} // END FOREACH
					if(isset($_POST['submit'])) {
						header('Location: /create_property/' . $p . '?step=3' );
						exit;
					}
				}// END PROPERTY SELECT CHECK 
		} // END USER ID CHECK
	} // END NUM ROWS CHECK
}// END POST PROPERTY CHECK
