<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_login();
?>
<h3><span class="add_property_name">Location</span><span class="add_property_icon">Icon</span></h3>
<section>
	<?php
	echo '<div id="lat_long">';
		if($latitude == 0.0000000 && $longitude == 0.0000000) {
		
			?><form id="form_address">
 	            <input type="hidden" value="<?php echo $property; ?>" name="p" />
				<input name="address" placeholder="Address" id="get_address" />
				<input type="submit" name="submit" value="Get Address Details" id="get_lat_long" />
                <img src="/images/loading.gif" id="get_address_loader" class="loading_gif hidden">
				</form>
			 <?php 
		} // END IF NOT SET LAT LONG
		?>   
		<div id="prop_map" class="details_hide_show">
		<?php
			if ($latitude != 0.0000000 && $longitude != 0.0000000) {
				?>
                <div id="property-map"></div>
                <?php
				
			}
			else {
				echo '<style> #address_holder {display:none;}</style>';
			}
		?>
        </div>
		<div id="address_holder">
            <form id="info_update_0">
                <label for="address">Address</label>
                <input type="text" name="address" id="address" value="<?php if (!empty($address)) {echo $address; } ?>" />
                <label for="latitude">Latitude</label>
                <input type="text" name="latitude" id="lat" value="<?php if($latitude != 0.0000000) { echo $latitude; } ?>" />
                <label for="longetude">Longitude</label>
                <input type="text" name="longitude" id="long" value="<?php if($longitude != 0.0000000) { echo $longitude; } ?>" />
                <input type="hidden" value="<?php echo $property; ?>" name="p" />
            </form>
        </div>
        <div id="no_address_found">
        	<h1>It looks like you&rsquo;re forging new ground!</h1>
            <p>The address you entered is not on the map. Please enter it manually.</p> 
        	<form id="no_address_found_form" action="process_no_address_found" method="get" >
            	<input type="text" name="street" placeholder="Street" id="no_address_found_street" /><br />
                <input type="text" name="city" placeholder="City" id="no_address_found_city" /><br />
                <input type="text" name="state" placeholder="State" id="no_address_found_state" /><br />
                <input type="text" name="zipcode" placeholder="zipcode" id="no_address_found_zipcode" /><br />
                <input type="text" name="latitude" placeholder="latitude" id="no_address_found_latitude" /><br />
                <input type="text" name="longitude" placeholder="longitude" id="no_address_found_longitude" />
                <input type="hidden" value="<?php echo $property; ?>" name="p" /><br /><br />
                <input type="submit" name="submit" value="Get Address Details" id="process_no_address_found" />
            </form>
        </div>
</section>