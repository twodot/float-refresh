<?php
session_start();

require_once('../../includes/config.inc.php');
require_login();
if (isset($_POST['p'])) {
	$property = (int) $_POST['p'];
	$table = 'properties';
	$table_id = 'property_id';
}
if (isset($property)) {	
		if($property != 0) {
			$q = "
				SELECT 
				user_id   
				
				FROM 
				$table  
				
				WHERE 
				id = $property
			";
			
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			if ($user_id == $row['user_id']) {
				
					
					$trimmed = array_map('trim', $_POST);
					if(isset($trimmed['bed'])) {
						$beds = $dbc->real_escape_string($trimmed['bed']);
					}
					else {
						$beds = '';
					}
					if(isset($trimmed['bath'])) {
						$bath = $dbc->real_escape_string($trimmed['bath']);
					}
					else {
						$bath = '';
					}
					if(isset($trimmed['sqft'])) {
						$sqft = $dbc->real_escape_string($trimmed['sqft']);
					}
					else {
						$sqft = '';
					}
					if(isset($trimmed['lot_size'])) {
						$lot_size = $dbc->real_escape_string($trimmed['lot_size']);
					}
					else {
						$lot_size = '';
					}
					if(isset($trimmed['property_type'])) {
						$property_type = $dbc->real_escape_string($trimmed['property_type']);
					}
					else {
						$property_type = '';
					}
					if(isset($trimmed['year_built'])) {
						$year_built = $dbc->real_escape_string($trimmed['year_built']);
					}
					else {
						$year_built = '';
					}
					if(isset($trimmed['community'])) {
						$community = $dbc->real_escape_string($trimmed['community']);
					}
					else {
						$community = '';
					}
					if(isset($trimmed['county'])) {
						$county = $dbc->real_escape_string($trimmed['county']);
					}
					else {
						$county = '';
					}
					if(isset($trimmed['mls'])) {
						$mls = $dbc->real_escape_string($trimmed['mls']);
					}
					else {
						$mls = '';
					}
					
					
				 	$q = "
						UPDATE 
						$table  
						
						SET 
						beds = '$beds',  
						bath = '$bath', 
						sqft = '$sqft', 
						lot_size = '$lot_size', 
						property_type = '$property_type', 
						year_built = '$year_built', 
						community = '$community', 
						county= '$county', 
						mls = '$mls', 
						progress = 2 
						
						WHERE 
						id = $property 
						AND user_id = $user_id 
						
					"; 
					
					$r = @mysqli_query ($dbc, $q);
				
			}
			else {
				echo 'You have reached this page in error';
			}
			
		}// END PROPERTY INT CHECK
} // IN P ISSET CHECK
?>