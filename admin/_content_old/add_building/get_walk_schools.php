<?php
session_start();
require_once('../../includes/config.inc.php');
require_login();
if (isset($_GET['p'])) {
	$property = (int) $_GET['p'];
	$table = 'properties';
	$table_id = 'property_id';
}
else if (isset($_GET['b'])) {
	$property = (int) $_GET['b'];
	$table = 'building';
	$table_id = 'building_id';
}
if (isset($property)) {	
		if($property != 0) {
			$q = "
				SELECT 
				user_id, 
				address, 
				show_walkscore, 
				show_greatschools, 
				collapse_schools 
				
				FROM 
				$table  
				
				WHERE 
				id = $property
			";
			
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			$show_walkscore = $row['show_walkscore'];
			$show_greatschools = $row['show_greatschools']; 
			$collapse_schools = $row['collapse_schools'];
			
			if ($user_id == $row['user_id']) {
					
				$address = urlencode($row['address']);
				
				$url = "http://maps.google.com/maps/api/geocode/json?address=" . $address . "&sensor=false";
				$response = file_get_contents($url);
				$response = json_decode($response, true);
				
				$latitude = $response['results'][0]['geometry']['location']['lat'];
				$longitude = $response['results'][0]['geometry']['location']['lng'];
				
				$google_error = false;
				// STREET NUMBER
				if(isset($response['results'][0]['address_components'][0]['short_name'])) {
					$street_number = $response['results'][0]['address_components'][0]['short_name'];
				}
				else {
					$google_error = true;
				}
				
				// STREET NAME
				if(isset($response['results'][0]['address_components'][1]['short_name'])) {
					$street_name = $response['results'][0]['address_components'][1]['short_name'];
				}
				else {
					$google_error = true;
				}
				
				// CITY
				if(isset($response['results'][0]['address_components'][3]['short_name'])){
					$city = $response['results'][0]['address_components'][3]['short_name'];
				}
				else {
					$google_error = true;
				}
				
				// STATE
				if(isset($response['results'][0]['address_components'][5]['short_name'])){
					$state = $response['results'][0]['address_components'][5]['short_name'];
				}
				else {
					$google_error = true;
				}
				// ZIP
				if(isset($response['results'][0]['address_components'][7]['short_name'])){
					$zip = $response['results'][0]['address_components'][7]['short_name'];
				}
				else {
					$google_error = true;
				}
				
				// NEIGHBORHOOD
				if(isset($response['results'][0]['address_components'][2]['short_name'])) {
					$neighborhood = $response['results'][0]['address_components'][2]['short_name'];
				}
				else {
					$neighborhood = '';
				}
				
				// COUNTY
				if(isset($response['results'][0]['address_components'][4]['short_name'])) {
					$county = $response['results'][0]['address_components'][4]['short_name'];
					$county = str_replace(' County', '', $county);
				}
				else {
					$county = '';
				}
				
				
				if($google_error) {
					?>
					There has been an error receiving the school data
					<?php
					exit();
				}
				else {
					$in_address = $street_number . ' ' . $street_name . ', ' . $city . ', ' . $state;
				}
				
				$response = '';
				
				$ws_url = "http://api.walkscore.com/score?format=json&address=$address";
				$ws_url .= "&lat=$latitude&lon=$longitude&wsapikey=". WALKSCORE;
				$str = file_get_contents($ws_url); 
				$walkscore_json = json_decode($str, true);
				
				$walkscore = $walkscore_json['walkscore'];
				$walkscore_link = $walkscore_json['ws_link'];
				$walkscore_description = $walkscore_json['description'];
				$ws_description = $walkscore_json['description'];
				
				$ws_class = strtolower(str_replace(' ', '-', $ws_description));
				$ws_class = str_replace('&rsquo;s', '', $ws_class);
				
				$walkscore_json = '';
				
				$q = "DELETE FROM schools WHERE  property_id ='$property'AND selected = 0";
				$r= @mysqli_query($dbc, $q);
				
				$q = "SELECT COUNT(*) as schools FROM schools WHERE $table_id = $property";
				$r = @mysqli_query($dbc, $q);
				$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
				
				$greatschools_url = 'http://api.greatschools.org/schools/nearby?key=' . GREATSCHOOLS . '&state=' . $state . '&zip=' . $zip . '&schoolType=public&minimumSchools=5&radius=5&limit=20';
				$gs_response = file_get_contents($greatschools_url);
				$gs_p = xml_parser_create();
				$gs_array = xml_parse_into_struct($gs_p, $gs_response, $vals, $index);;
				xml_parser_free($gs_p);
				
				$schools = array();
				$availableSchools = array();
				
				$i = 0;
				
				foreach($vals as $school_val => $value) {
					if ($value['tag'] == 'SCHOOLS') {
					}
					if($value['tag'] == 'SCHOOL') {	
						
						if($value['type'] == 'open') {
						}
						else if($value['type'] == 'close') {
								if ($schools[$i]['GRADERANGE'] != 'n/a') { 
								
									
									
									$gsid = $schools[$i]['GSID'];
									$name = $schools[$i]['NAME'];
									$grade_range = $schools[$i]['GRADERANGE'];
									$grade_range_arr = explode('-', $grade_range);
									if(isset($grade_range_arr[0])) {
										$$grade_range_low = $grade_range_arr[0];
									}
									else {
										$grade_range_low = '';
									}
									
									if(isset($grade_range_arr[1])) {
										$grade_range_high = $grade_range_arr[1];
									}
									else {
										$grade_range_high = '';
									}
									
									if ($grade_range_low == 'K' || $grade_range_low == 'PK') {
										$grade_range_low == 0;
									}
									
									$gs_rating = (isset($schools[$i]['GSRATING']) ? $schools[$i]['GSRATING'] : '');
									$parent_rating = (isset($schools[$i]['PARENTRATING']) ? $schools[$i]['PARENTRATING'] : '');
									$city = $schools[$i]['CITY'];
									$district = $schools[$i]['DISTRICT'];
									$website = (isset($schools[$i]['WEBSITE']) ? $schools[$i]['WEBSITE'] : '');
									$overview_link = $schools[$i]['OVERVIEWLINK'];
									$distance = (isset($schools[$i]['DISTANCE']) ? $schools[$i]['DISTANCE'] : '');
									
									$q = "
										SELECT 
										* 
										FROM 
										schools 
										
										WHERE 
										gsid = '$gsid'
										AND state = '$state'  
										AND $table_id = '$property' 
										";
									$r= @mysqli_query($dbc, $q);
									if (mysqli_num_rows($r) == 0) {
										$q = "
											INSERT INTO 
											schools 
											
											SET 
											gsid = '$gsid', 
											state = '$state', 
											$table_id = '$property' ";
										if($gs_rating != 0 || $gs_rating != '') {
											$r= @mysqli_query($dbc, $q);
										}
									}
									$availableSchools[$gsid] = array(
										'gsid' => $gsid, 
										'name' => $name, 
										'grade_range' => $grade_range,
										'grade_range_low' => $grade_range_low,
										'grade_range_high' => $grade_range_high, 
										'gs_rating' => $gs_rating, 
										'parent_rating' => $parent_rating, 
										'city' => $city, 
										'district' => $district, 
										'website' => $website, 
										'overview_link' => $overview_link, 
										'distance' => $distance
									
									);
									
								}
								$i ++;
						} // END TYPE CLOSE CHECK	
					} // END SCHOOL CHECK 
					else {
						if (isset($value['value'])) {
							$schools[$i][$value['tag']] = $value['value'];
						}
					}
					
				} // END STYPE CHOOLS CHECK
				
			
				
				echo "<p>Please click to select schools for the property</p>";
				echo '<div id="school_list">';
				?>
				<script type="text/javascript">                
					$(function() {
						$('.school').click(function(e){
							
							var school_sel = $(this).attr("id");
							$.ajax({
								url: "select_school?s=" + school_sel, 
								dataType:"script", 
								cache: false,
							}).done(function(data) {
							});
							
						})
						
						
						
						$('#show_walkscore_input').click(function(){
							$("#actual_walk_score").slideToggle(300);
							$.ajax({
								url: "/show_scores", 
								data: jQuery("#show_scores").serialize(),
								dataType:"script", 
								cache: false,
							}).done(function(data) {
								
							});
							
						})
						$('#show_schoolscore_input').click(function(){
							$("#actual_school_score").slideToggle(300);
							$.ajax({
								url: "/show_scores", 
								data: jQuery("#show_scores").serialize(),
								dataType:"script", 
								cache: false,
							}).done(function(data) {
								
							});
							
						})
						
					})
				</script>
                
                <form action="/show_scores" method="post" id="show_scores" >
                	<div id="show_walkscore">
                    	Show Walkscore Data in App 
                    	<input type="checkbox" id="show_walkscore_input" name="show_walkscore" value="1" <?php if ($show_walkscore) {echo 'checked="checked"';} ?> /><br />
                        <div id="actual_walk_score" class="<?php echo $ws_class; if (!$show_walkscore) {echo ' hidden_score ';}?> details_hide_show">
                        	Walk Score: 
                            <div id="walkscore"><?php echo $walkscore . ' ' . $ws_description ?></div>
                        </div>
                    </div>
                    <div id="show_schoolscore" >
                    	Show GreatSchools Data in App 
                        <input type="checkbox" id="show_schoolscore_input" name="show_greatschools" value="1" <?php if ($show_greatschools) {echo 'checked="checked"'; }?> /><br />
                    </div>
                    <input type="hidden" name="p" value="<?php echo $property; ?>" />
                </form>
                <div id="actual_school_score" class="<?php if (!$show_greatschools) {echo ' hidden_score ';}?> ">
                <div id="schools_hide_show" class="property-<?php echo $property; ?>"><h4>GreatSchools Info</h4></div>
                    
                    <div id="school_list">
                    <p>Please click to select schools for the property</p>
                        <?php
                            $q_school = "SELECT * FROM schools WHERE property_id = $property";
                            $r_school = @mysqli_query($dbc, $q_school);
                            while($row_school = mysqli_fetch_array($r_school, MYSQLI_ASSOC)) {
                               	
								$gsId = $row_school['gsid'];
								$schoolId = $row_school['id'];
								$row_school['selected'];
							    
								?>
                            <div class="school<?php if($row_school['selected'] == 1) {echo ' selected'; }?>" id="school_<?php echo $schoolId; ?>">
                                <div class="gs_rating rating_<?php echo $availableSchools[$gsId]['gs_rating']; ?>"><?php echo $availableSchools[$gsId]['gs_rating']; ?>
                                    <div class="out_of">out of 10</div>
                                </div>
                                <div class="gs_info">
                                    <div class="gs_name"><h4><?php echo $availableSchools[$gsId]['name']; ?> <a href="<?php echo $availableSchools[$gsId]['overview_link']; ?>" target="_blank"><img src="<?php echo IMAGES; ?>get_link.png" alt="got to link" /></a></h4></div>
                                    <div class="grade_range"><?php echo $availableSchools[$gsId]['grade_range']; ?></div>
                                    <div class="gs_distance">Distance <?php echo $availableSchools[$gsId]['distance']; ?></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <?php 
                            }
                        ?>
                    
                    </div>
                </div>
                
                <?php
					
				echo '</div>'; // END SCHOOL LIST
				
				$q = "
					UPDATE 
					$table  
					
					SET 
					address = '$in_address',  
					latitude =  '$latitude',
					longitude =  '$longitude', 
					county = '$county', 
					community = '$neighborhood', 
					walkscore = '$walkscore', 
					walkscore_link = '$walkscore_link', 
					show_walkscore = 1, 
					show_greatschools = 1 
					
					WHERE 
					id = $property 
					AND user_id = $user_id 
					
				"; 
				
				$r = @mysqli_query ($dbc, $q);
				
				$ws_class = strtolower(str_replace(' ', '-', $walkscore_description));
				$ws_class = str_replace('&rsquo;s', '', $ws_class);
				$ws_class = str_replace("'", '', $ws_class);
				
				$walkscore_description = str_replace("'", '&rsquo;', $walkscore_description);
				
			}
			else {
				echo 'You have reached this page in error';
			}
			
		}// END PROPERTY INT CHECK
} // IN P ISSET CHECK

	
?>

