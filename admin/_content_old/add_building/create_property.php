<?php

require_once('../../includes/config.inc.php');
include('../../includes/bitly.php');

require_login();
exit();

$rental = rental($user_id, $dbc);

if(isset($_GET['submitted'])) {
	$trimmed = array_map('trim', $_GET);
	
	$title = $dbc->real_escape_string($trimmed['title']);
	$price = $dbc->real_escape_string($trimmed['price']);
	$price = $dbc->real_escape_string(str_replace('$', '', $price));
	$id = $dbc->real_escape_string($trimmed['u']);
	
	
	$title = htmlspecialchars ($title, ENT_QUOTES);
	$price = htmlspecialchars ($price, ENT_QUOTES);
	
	$price = (int) $price;
	
	$q_check = "
	SELECT 
	title
	
	FROM 
	properties
	
	WHERE
	title = '$title' 
	AND user_id = $user_id 
	
	";
	
	$r_check = @mysqli_query ($dbc, $q_check);
	
	if (mysqli_num_rows($r_check) > 0) {
			echo '$("#error").html("<p>This property already exists</p>").fadeIn(300)';
	
	}
	else { // NUM ROWS
			
	
		$q = "
			INSERT INTO 
			
			properties 
			
			(user_id, title, price)
			
			VALUES 
			
			(?, ?, ?)";
			
		$stmt= mysqli_prepare($dbc, $q);
				
		mysqli_stmt_bind_param($stmt, 'iss', $id, $title, $price);
		
		mysqli_stmt_execute($stmt);
		
		if (mysqli_stmt_affected_rows($stmt) ==1){
			$ri = mysqli_stmt_insert_id($stmt);
			if($rental == 1) {
				$q_status = "UPDATE properties SET status = 3 WHERE id = $ri";
				$r_status = @mysqli_query($dbc, $q_status);
			}
			$results = bitly_v3_shorten(PUBLIC_BASE_URL . '/view_property/' . $ri, BITLY_APP_GENERIC_ACCESS, 'bit.ly');
			$short_url = $results['hash'];
			
			$fb_results = bitly_v3_shorten(PUBLIC_BASE_URL . '/view_property/' . $ri . '?utm_source=facebook&utm_medium=share_panel&utm_campaign=property', BITLY_APP_GENERIC_ACCESS, 'bit.ly');
			$fb_short_url = $fb_results['hash'];
			
			$tw_results = bitly_v3_shorten(PUBLIC_BASE_URL . '/view_property/' . $ri . '?utm_source=twitter&utm_medium=share_panel&utm_campaign=property', BITLY_APP_GENERIC_ACCESS, 'bit.ly');
			$tw_short_url = $tw_results['hash'];
			
			$q = "UPDATE properties SET short_url_hash = '$short_url', facebook_share = '$fb_short_url', twitter_share = '$tw_short_url' WHERE id = $ri";
			$r = @mysqli_query($dbc, $q);
			
			$SESSION['created_property'] = $ri;
			
			$q_info = "
			INSERT INTO 
			
			property_info 
			
			(properties_id, primary_info)
			
			VALUE 
			
			(?, ?)";
			
			$stmt_info= mysqli_prepare($dbc, $q_info);
			$primary = 1;		
			mysqli_stmt_bind_param($stmt_info, 'ii', $ri, $primary);
		
			mysqli_stmt_execute($stmt_info);
			
			mysqli_stmt_close($stmt_info);
			
			$price = (int) $price;
			if (!empty($price)) {
				$price = '$' . number_format($price);	
			}
			
			
			
			$q = "
			INSERT INTO 
			property_info 
			
			SET 
			properties_id = $ri, 
			title = 'Location',  
			location = 1";
			
			$r = mysqli_query($dbc, $q);
		
			?> 
            
            
            	$("#unassigned_properties .holder").prepend('<div class="droppable property unassigned ui-droppable ui-draggable" id="b_con_<?php echo $ri ?>"><a href="/property/<?php echo $ri ?>"><?php echo $title . ' ' . $price; ?> <img src="/images/caution.png" alt="caution" title="This property is does not have a beacon assigned to it."></a> </div>').fadeIn(300); 
				beacon_drag();
                unassigned();
                
                $('#add_button').slideDown(300);
				$('#add_property').slideUp(300);
                $('.start_here').slideUp(300);
				$("#property_title").val('');
                $("#price").val('');
			<?php
		
		
		}
		else {
			echo '$("#error").html("<p>The property could not be added</p>").fadeIn(300);';
		}
	
		mysqli_stmt_close($stmt);
	}

}
?>

