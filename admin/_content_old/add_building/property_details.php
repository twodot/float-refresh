<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_login();
?>
<h3><span class="add_property_name">Marketing</span><span class="add_property_icon">Icon</span></h3>
<section>
	<?php
		echo '<div id="property_details" class="clear">';
			echo '<h4 id="details" class="property-' . $property . '">Property Details </h4>';
			?>
            	
                <form id="info_update_1">
                    <table>
                        <tr><td>Year Built </td><td><input type="text" name="year_built" value="<?php echo $year_built; ?>" /></td></tr>
                        <tr><td>Neighborhood </td><td><input type="text" name="community" id="neighborhood" value="<?php echo $community; ?>" /></td></tr>
                        <tr><td>County </td><td><input type="text" name="county" id="county" value="<?php echo $county; ?>" /></td></tr>
                       
                    </table>
                    <input type="hidden" value="<?php echo $property; ?>" name="p" />
                </form>
			</div>
</section>