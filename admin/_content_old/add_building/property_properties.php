<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_login();
?>
<h3><span class="add_property_name">Associated Properties</span><span class="add_property_icon">Icon</span></h3>
 <section>
    <?php 
    echo '<h4>Add properties to the building</h4>';
	
	echo '<div id="building-' . $property. '" class="attached_properties holder"><h3>Associated Properties</h3>' . "\n";
	echo '<ul id="sortable_props">';
	$r = get_building_properties($user_id, $dbc, $property);
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		echo '<li id="li_' . $row['id'] . '" class="attched_prop"><a href="/property/' . $row['id'] . '">' . $row['title'] . '</a><a href="/detach_property?p=' . $row['id'] . '&b=' . $property . '" class="detach_property"><img src="/images/delete.png" class="detach_property_image" alt="Detach Property" title="Detach Property from ' . $property_title . '"></a></li>';
	} // END GET PROPERTIES 
	echo '</ul>';
	echo '</div><!-- END HOLDER -->' . "\n"; // END HOLDER
	echo '<div id="available_building_properties">';
		echo '<h3>Available Properties</h3>';
		echo '<div class="holder">';
		$r = get_non_building_properties($user_id, $dbc);
		while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
			echo '<div id="property-' . $row['id'] . '" class="available_property_building">' . $row['title'] . '</div>';
		}
		
		echo '</div>'; // END HOLDER
	echo '</div>'; // END AVAILABLE BUILDING PROPERTIES
	
	
	?>
    
    
</section>