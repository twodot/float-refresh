<?php

require_once('includes/config.inc.php');
require_login();

if(isset($_GET['prop_id'])) {
	$prop_id = (int) $_GET['prop_id'];
	
	if($prop_id != 0) {
		
		$q = "SELECT * FROM properties WHERE id = '$prop_id' AND user_id = '$user_id'";
		
		$r = mysqli_query($dbc, $q);
		
		if(mysqli_num_rows($r) != 0) {
			
			$q = "UPDATE properties SET address = '', state = '', zip = '', latitude = '', longitude = '', community = '', county = '' WHERE id = '$prop_id'";
			$r = mysqli_query($dbc, $q);
			
			$q_del = "DELETE FROM schools WHERE property_id = '$prop_id'";
			$r_del = mysqli_query($dbc, $q_del);
			
			?>
			
			$('#prop_map').slideUp(500);
			$('#address_holder').slideUp(500);
			
            $('#get_address').val('');
			$('#get_lat_long').show();
			$('#get_address_loader').hide();
            
            $('#form_address').slideDown(500);
            window.location.reload();
			<?php
		
		} // END NUM ROWS SELECTED CHECK
		
	} // END PROP ID 0 CHECK
	
} // END GET PROP ID CHECK

?>
