<?php
// THIS PAGE IS NOW create_building.php
require_login();
if(isset($_GET['p']) || isset($p)) {
	if(isset($_GET['p']) ) {
		$building = (int) $_GET['p'];
	}
	else if(isset($p)) {
		$building = (int) $p;
	}
}
else {
	$building = 0;
}
if($building != 0) {
	header('Location: /create_building/' . $building);
}
else {
	header('Location: /multi-family');
}

$page_title = $page_name = 'Property';

include('includes/header.php');

?>

<div id="error">
</div>

<div id="dialog">
</div>

<?php

// SELECT FOR PROPERTY PRIMARY INFO

if(isset($_GET['p']) || isset($p)) {
	if(isset($_GET['p']) ) {
		$building = (int) $_GET['p'];
	}
	else if(isset($p)) {
		$building = (int) $p;
	}
	if($building != 0) {
		
		

		$arr = get_building($user_id, $dbc, $building);
		
		$row = mysqli_fetch_array($arr, MYSQLI_ASSOC);
		$building_name = $row['building_name'];
		$status= $row['status'];
		$total_media = $row['total_media'];	
		$address = $row['address'];
		$latitude = $row['latitude'] + 0;
		$longitude = $row['longitude'] + 0;
		$marketing_description = nl2br($row['description']);
		$building_title = $row['building_name'];
		$community = $row['community'];
		$county = $row['county'];
		$walkscore = $row['walkscore']; 
		$show_walkscore = $row['show_walkscore'];
		$show_greatschools = $row['show_greatschools']; 
		$walkscore = $row['walkscore'] +0;  
		$elm = $row['gs_elm'];
		$jh = $row['gs_jh'];
		$hs = $row['gs_hs'];
		$bitly_hash = $row['short_url_hash'];
		$collapse_details = $row['collapse_details'];
		$collapse_schools = $row['collapse_schools'];
		
		if(!empty($bitly_hash)) {
			$short_url = BITLY_BASE_URL . 	$bitly_hash;
		}
		
		
		?>
        <script>
		
		$(function() {
			$( "#sortable_props" ).sortable({ items: " li", placeholder: "sort_highlight", opacity: 0.5 });
			$( "#sortable_props" ).on( "sortupdate", function( event, ui ) {
				var sorted = $( "#sortable_props" ).sortable( "serialize", { key: "sort" } );
				
				$.ajax({
					url: "update_sort_building_props?"+ sorted, 
					cache: false,
				})
			})
			$( "#sortable_props" ).disableSelection();
			
			<?php 
				if ($collapse_details == 1) {
					?>$('.details_hide_show').hide(); <?php 
				}
				if ($collapse_schools == 1) {
					?>$('#school_list_building').hide(); <?php 
				}
			?>
			
		});
				
		</script>
        
        
        <?php
		
		
		
		echo '<div class="one_half">' . "\n";
			echo '<div id="building_' . $row['properties_id'] . '" class="top_parent"><div id="info_container_' . $row['info_id'] . '" class="'; if ($status == 'inactive') { echo ' property_info_avail top_level_available"';} echo 'prop_container_main"><h3 class="property_title">' . $row['building_name'] . '</h3>' . "\n";	
					
				
                echo '<ul class="prop_revisions">
						<li>
							<a href="/update_building?b=' . $row['properties_id'] . '"  class="edit_info_button">
								<img src="' . IMAGES . 'edit.png" alt="Edit info" title="Edit ' . $row['building_name']	. '" />
							</a>
						</li>';
						/*
						<li>
							<img src="' . IMAGES . 'add.png" class="add_info_show" title="Add Information to ' . $row['building_name']	. '" alt="Add Information" />
						</li>
						*/
				echo '		<li>
							<a href="/delete_building?b=' . $row['properties_id'] . '"  class="delete_building_button">
								<img src="' . IMAGES . 'delete.png" alt="Delete info" title="Delete ' . $row['building_name']	. '" />
							</a>
						</li>
					</ul>';
					echo '<div class="clear"></div>';
					
					
					echo '<div class="beacon_prop">Primary Beacon<br />
					<span id="prop_id_' . $row['info_id'] . '">' . "\n";
					
				if ($row['beacon_id']) {
					echo 'Beacon '. $row['beacon_title'] . ' <a href="/remove_beacon?b=' . $row['beacon_id'] . '&bu=' . $row['properties_id'] . '&i=' . $row['info_id'] . '" class="remove_beacon" ><img src="/images/delete.png" class="remove_beacon_image" alt="Remove beacon" title="Remove beacon from ' . $row['building_name'] . '" /></a>';
				}
				else {
					echo 'Un Assigned';
					echo '<button id="add_building_beacon_button" >Add Beacon</button>';
				}
				
				
				echo'</span></div>';
				
				
				echo '<div id="property_details" class="clear">';
					echo '<h4 id="details" class="building-' . $building . '">Building Details <div '; if ($collapse_details == 1) {echo 'class="icon-rightarrow"';}else {echo 'class="icon-downarrow"';}  echo'></div></h4>';
					echo '<div class="details_hide_show">';
						echo '<a href="edit_building_details?b=' . $building . '" id="edit_building_detials">Edit Detials</a>';
						
						echo '<table>';
								echo '<tr><td>Neighborhood </td><td>' . $community . '</td></tr>';
							echo '<tr><td>County </td><td>' . $county . '</td></tr>';
						echo '</table>';
						echo '<br />';
					echo '</div>';
				echo '</div>'; // END PROPERTY DETAILS
				
				echo '<div id="sales_status" class="clear">';
				echo '</div>';
				
				echo '<div id="lat_long" >';
				if($latitude == 0.0000000 && $longitude == 0.0000000) {
				
					?><form id="form_address" >
						<input name="address" placeholder="Address" id="get_address" />
                        <input type="hidden" value="<?php echo $building; ?>" name="b" />
                        <input type="submit" name="submit" value="Add Address" id="get_lat_long" />
						</form>
                     <?php 
				}
					 ?>   
                     	<div id="prop_map" class="details_hide_show">
                        <?php
							if ($latitude != 0.0000000 && $longitude != 0.0000000) {
								echo '<iframe width="100%" height="350" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBR5USylIliiJxWUroSXSaL7KD3T_WM328&q=' . $latitude . ',' . $longitude .  '&zoom=18&maptype=satellite"></iframe>';
							}
						?>
                        </div>
						
                        <p id="lat_long_holder" class="details_hide_show <?php if ($latitude == 0.0000000 && $longitude == 0.0000000) { echo 'hidden_score '; }?>"><a href="/edit_address?b=<?php echo $building; ?>" id="edit_map" <?php if($latitude == 0.0000000 && $longitude == 0.0000000) { echo ' class="hide_edit_map" '; } ?>>Edit Map Info</a><br /><span id="address"><?php if (!empty($address)) {echo $address; } ?></span><br />
                        Lat <span id="lat"><?php if($latitude != 0.0000000) { echo $latitude; } ?></span> / Long <span id="long"><?php if($longitude != 0.0000000) { echo $longitude; } ?></span></p>
                        <?php 
							$q_ws =" SELECT *, COUNT(*) as walkscore_count FROM walkscore WHERE high >= $walkscore AND low <= $walkscore";
							$r_ws = @mysqli_query ($dbc, $q_ws);
							$row_ws = mysqli_fetch_array($r_ws, MYSQLI_ASSOC);
							if($row_ws['walkscore_count'] == 1) {
								$ws_description = $row_ws['description'];
							}
							else {
								$ws_description = '';
							}
							$ws_class = strtolower(str_replace(' ', '-', $ws_description));
							$ws_class = str_replace('&rsquo;s', '', $ws_class)
						?>
                        <form action="/show_scores" method="post" id="show_scores" >
                        	<div id="show_walkscore">Show Walkscore Data in App <input type="checkbox" id="show_walkscore_input" name="show_walkscore" value="1" <?php if ($show_walkscore) {echo 'checked="checked"';} ?> /><br /><div id="actual_walk_score" class="<?php echo $ws_class; if (!$show_walkscore) {echo ' hidden_score ';}?> details_hide_show ">Walk Score: <div id="walkscore"><?php echo $walkscore . ' ' . $ws_description ?></div></div></div>
                            <div id="show_schoolscore">Show GreatSchools Data in App <input type="checkbox" id="show_schoolscore_input" name="show_greatschools" value="1" <?php if ($show_greatschools) {echo 'checked="checked"'; }?> /><br />
                            	<div id="actual_school_score" class="<?php if (!$show_greatschools) {echo ' hidden_score ';}?>">
                                <div id="schools_hide_show" class="building-<?php echo $building; ?>"><h4>GreatSchools Info <div <?php if ($collapse_schools == 1) {echo 'class="icon-rightarrow"';}else {echo 'class="icon-downarrow"';} ?>> </div></h4></div>
                                <div id="school_list_building">
                                 <p>Please click to select schools for the property</p>   
                                    	<?php
											$q_school = "SELECT * FROM schools WHERE property_id = $building ORDER BY grade_range_low, grade_range_high, distance ASC";
											$r_school = @mysqli_query($dbc, $q_school);
											while($row_school = mysqli_fetch_array($r_school, MYSQLI_ASSOC)) {
												?>
											<div class="school<?php if($row_school['selected'] == 1) {echo ' selected'; }?>" id="school_<?php echo $row_school['id']; ?>">
                                            	<div class="gs_rating rating_<?php echo $row_school['gs_rating']; ?>"><?php echo $row_school['gs_rating']; ?>
                                                	<div class="out_of">out of 10</div>
                                                </div>
                                                <div class="gs_info">
                                                	<div class="gs_name"><a href="<?php echo $row_school['overview_link']; ?>" target="_blank"><h4><?php echo $row_school['name']; ?></h4></a></div>
                                                    <div class="grade_range"><?php echo $row_school['grade_range']; ?></div>
                                                    <div class="gs_distance">Distance <?php echo $row_school['distance']; ?></div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <?php 
                                            }
										?>
                                    
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="p" value="<?php echo $building; ?>" />
                        </form>
						
						<?php echo '</div>';// END LAT LONG
				
				
				echo '<div id="marketing_description" class="clear">';
					if (empty($marketing_description)) {
						echo '<p><a href="/marketing_description?b=' . $building . '" id="edit_marketing_description">Add a marketing description</a></p>';
					}
					else {
						echo '<p><a href="/marketing_description?b=' . $building . '" id="edit_marketing_description">Edit marketing description</a></p>';
						echo '<p>' . $marketing_description . '</p>';
					}
				echo '</div>'; // END MARKETING DESCRIPTION
				
				
				echo '<div class="edit_media">';
				echo '<a href="/media_building/' . $row['info_id'] . '">Add/Edit Media</a>'; echo '<br />(' . $total_media . ')'; if ($total_media == 1) {echo ' Image';} else {echo ' Images';} echo'</div>';
                
				
				
				?>
               
                <div class="clear"></div>
                </div> <?php // END info_container_ ?>
				
                
                
                <?php	
				echo '<div id="building-' . $building . '" class="attached_properties holder"><h3> Attached Properties</h3>' . "\n";
				echo '<ul id="sortable_props">';
				$r = get_building_properties($user_id, $dbc, $building);
				while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
					echo '<li id="li_' . $row['id'] . '" class="attched_prop"><a href="/property/' . $row['id'] . '">' . $row['title'] . '</a><a href="/detach_property?p=' . $row['id'] . '&b=' . $building . '" class="detach_property"><img src="/images/delete.png" class="detach_property_image" alt="Detach Property" title="Detach Property from ' . $building_name . '"></a></li>';
				} // END GET PROPERTIES 
				echo '</ul>';
				echo '</div><!-- END HOLDER -->' . "\n"; // END HOLDER
				
			echo '</div>' . "\n"; // CLOSE PROPERTY
			
			
			echo '<div id="listing_info">';
		
			$q_co_listing = "SELECT
				*, 
				COUNT(*) as co_listing
				
				FROM 
				co_agent  
				
				WHERE 
				user_id=$user_id 
				AND id = (SELECT co_agent FROM building WHERE id = $building AND user_id = $user_id) 
				";
			$r_co_listing = @mysqli_query ($dbc, $q_co_listing);
			$row_co_listing = mysqli_fetch_array($r_co_listing, MYSQLI_ASSOC);
			
		
			$q_listing = "SELECT
		
				listing_name, 
				listing_email, 
				listing_phone, 
				listing_brokerage 
				
				FROM 
				users 
				
				WHERE 
				user_id=$user_id 
				";
			$r_listing = @mysqli_query ($dbc, $q_listing);
			$row_listing = mysqli_fetch_array($r_listing, MYSQLI_ASSOC);
			if(empty($row_listing['listing_name'])) {
				$listing_error = true;
			}
			else if(empty($row_listing['listing_email'])) {
				$listing_error = true;
			}
			else if(empty($row_listing['listing_phone'])) {
				$listing_error = true;
			}
			else if(empty($row_listing['listing_brokerage'])) {
				$listing_error = true;
			}
			else {
				$listing_error = false;
			}	
				
			if($listing_error) {
				echo '<div class="error" id="property_listing_error">Please <a href="/account">enter your listing information</a></div>';	
			}
			else {
				echo '<h3>Listing Info</h3>';
				if ($row_co_listing['co_listing'] != 0) {
					echo '<div class="listing_info_holder co_agent"><h4>Co Agent:</h4><p>' . $row_co_listing['name'] . '<br />' . $row_co_listing['email'] . '<br />' . $row_co_listing['phone'] . '<br />' . $row_co_listing['brokerage'] . '</p>
					<p><a href="edit_co_agent?ca=' . $row_co_listing['id'] . '&p=' . $building . '" id="edit_co_agent">Edit this agent</a></p>
					<p><a href="add_co_agent_building?p=' . $building . '" id="add_co_agent">Change a co-listed agent</a></p></div>';
				}
				echo '<div class="listing_info_holder"><p>' . $row_listing['listing_name'] . '<br />' . $row_listing['listing_email'] . '<br />' . $row_listing['listing_phone'] . '<br />' . $row_listing['listing_brokerage'] . '</p></div>';
			}
			
			if ($row_co_listing['co_listing'] == 0) {
				echo '<div class="listing_info_holder"><a href="add_co_agent_building?p=' . $building . '" id="add_co_agent">Add a co-listed agent</a></div>';
			}
		
		echo '</div>'; // END LISTING INFO
			
			
		echo '</div>' . "\n"; // CLOSE ONE HALF PROPERTIES
		
		echo '<div id="property_beacons" class="one_half">';
		if (isset($short_url)) {
			echo '<div id="share_url" class="clear">
					<h3>Share</h3>
					<p><a href="' . $short_url . '" target="_blank" >' . $short_url . '</a></p>
					<p>For social sharing and	yard arm signs when people don&rsquo;t have the app installed</p>
				</div>';
		}
		
		echo '<div id="available_beacons" >';
		echo '<h3>Available Properties</h3>';
		echo '<div class="holder">';
		$r = get_non_building_properties($user_id, $dbc);
		while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
			echo '<div id="property-' . $row['id'] . '" class="available_property_building">' . $row['title'] . '</div>';
		}
		
		echo '</div>'; // END HOLDER

echo '</div>'; // END BEACONS
		echo '</div>'; // END PROPERTY BEACONS
	}
}
else {
	echo '<p>You&rsquo;ve reached this page in error</p>';
}

include('includes/footer.php');
?>