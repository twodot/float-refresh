<?php

require_once('includes/config.inc.php');
require_login();

if(isset($_GET['b'])) {
	$beacon = str_replace('beacon-', '', $_GET['b']);
	$beacon = (int) $beacon;
	
	
	$q= "SELECT * FROM beacons WHERE id = $beacon AND user_id = $user_id";
	$r = @mysqli_query($dbc, $q);
	if (mysqli_num_rows($r) == 1) {
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		$json = get_beacon_info($row['uuid'], $row['k_major'], $row['k_minor']);	
		$beacon_info = json_decode($json);
		?>$('#beacon-holder-<?php echo $beacon; ?>').html('');<?php
			?>
            
            
            $('#beacon-holder-<?php echo $beacon; ?>').append('<div id="beacon-proximity-<?php echo $beacon; ?>" class="beacon-proximity beacon-hide beacon_info_line"><div class="beacon-info-title">Proximity UUID </div><div class="beacon-info-value"><?php echo $beacon_info->proximity; ?></div></div>').fadeIn(300);
            $('#beacon-proximity-<?php echo $beacon; ?>').fadeIn(300);
            
            $('#beacon-holder-<?php echo $beacon; ?>').append('<div id="beacon-major-<?php echo $beacon; ?>" class="beacon-major beacon-hide beacon_info_line"><div class="beacon-info-title">Major Value </div><div class="beacon-info-value"><?php echo $beacon_info->major; ?></div></div>').fadeIn(300);
            $('#beacon-major-<?php echo $beacon; ?>').fadeIn(300);
            
            $('#beacon-holder-<?php echo $beacon; ?>').append('<div id="beacon-minor-<?php echo $beacon; ?>" class="beacon-minor beacon-hide beacon_info_line"><div class="beacon-info-title">Minor Value </div><div class="beacon-info-value"><?php echo $beacon_info->minor; ?></div></div>').fadeIn(300);
            $('#beacon-minor-<?php echo $beacon; ?>').fadeIn(300);
            
            $('#beacon-holder-<?php echo $beacon; ?>').append('<div id="beacon-txpower-<?php echo $beacon; ?>" class="beacon-txpower beacon-hide beacon_info_line"><div class="beacon-info-title">tx Power Value </div><div class="beacon-info-value"><?php echo $beacon_info->txPower; ?></div></div>').fadeIn(300);
            $('#beacon-txpower-<?php echo $beacon; ?>').fadeIn(300);
            
            $('#beacon-holder-<?php echo $beacon; ?>').append('<div id="beacon-unique-id-<?php echo $beacon; ?>" class="beacon-unique-id beacon-hide beacon_info_line"><div class="beacon-info-title">Unique ID </div><div class="beacon-info-value"><?php echo $beacon_info->uniqueId; ?></div></div>').fadeIn(300);
            $('#beacon-unique-id-<?php echo $beacon; ?>').fadeIn(300);
            $('#beacon-holder-<?php echo $beacon; ?>').append('<div class="clear"></div>').fadeIn(300);
            <?php
		
	}
	else {
		echo '<p>You have reached tis page in error</p>';
	}
}
else {
	echo '<p>You have reached tis page in error</p>';
}
?>

