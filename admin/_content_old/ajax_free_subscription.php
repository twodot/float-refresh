<?php
	require_admin_login();
	header('Content-Type: application/json');
	
	$data["status"]="success";
	$data["message"]="";
	
	if(isset($_GET['id']) && isset($_GET['f'])) {
		$get_id = (int) $_GET['id'];
		$get_f = process_input($_GET['f']);
		
		$q_sub = "SELECT * FROM subscription WHERE user_id=".$get_id;
		$q_users = "SELECT * FROM users WHERE user_id=".$get_id;
		$r_sub = mysqli_query($dbc, $q_sub);
		$r_users = mysqli_query($dbc, $q_users);
		
		if(mysqli_num_rows($r_users)){
			$row = mysqli_fetch_assoc($r_sub);
			if(!$row){
				echo "Creating User Data\n";
				$q_sub = "INSERT INTO subscription 
							(user_id, subscribe_level, allowed_prop, amount, active, deactivate_in, current_period_start, free) 
							VALUES
							(".$get_id.", 1, 0, 0, 1, 0, NULL, 1)";
				if(!mysqli_query($dbc, $q_sub)){
					$data["status"]="failure";
					$data["message"]=mysqli_error($dbc);
				} else{
					echo "User Data Successfully Created.\nWorking On Operation\n";
					// select the created row again for the operation.
					$q_sub = "SELECT * FROM subscription WHERE user_id=".$get_id;
					$r_sub = mysqli_query($dbc, $q_sub);
					$row = mysqli_fetch_assoc($r_sub);
				}
				
			}
			if($row){
				if($get_f=="true" && $row["free"]==1){
					$data["message"]="User Already Has Free Subscription";
				} else if($get_f=="false" && $row["free"]==0){
					$data["message"]="Free Subscription Has Already Been Turned Off";
				} else if($get_f=="true" && $row["free"]==0){
					// query
					$q_sub = "UPDATE subscription SET free=1 WHERE user_id=".$get_id;
					if(!mysqli_query($dbc, $q_sub)){
						$data["status"]="failure";
						$data["message"]=mysqli_error($dbc);
					} else
						$data["message"]="A Free Subscription Has Been Added To The User";
				} else{
					// query
					$q_sub = "UPDATE subscription SET free=0 WHERE user_id=".$get_id;
					if(!mysqli_query($dbc, $q_sub)){
						$data["status"]="failure";
						$data["message"]=mysqli_error($dbc);
					} else
						$data["message"]="User's Free Subscription Has Been Removed";
				}
			} else{
				$data["status"]="failure";
				$data["message"]="Something Is Wrong, Please Try Later";
			}
		} else{
			$data["status"]="failure";
			$data["message"]="User Does Not Exist";
			
		}
	} else{
		$data["status"]="failure";
		$data["message"]="Unsupported Operation";
	}
	
	echo json_encode($data);
	
	function process_input($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>