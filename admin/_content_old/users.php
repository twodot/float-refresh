<?php

require_admin_login();
unset($_SESSION['pseudo_user']); 

$page_title = $page_name = 'Users';

include('includes/header.php');
	
		$q_users = "SELECT 
			users.*, 
			(SELECT active FROM subscription WHERE users.user_id = subscription.user_id) as active_subscription, 
			(SELECT deactivate_in FROM subscription WHERE users.user_id = subscription.user_id) as deactivate_in_subscription, 
			(SELECT free FROM subscription WHERE users.user_id = subscription.user_id) as free_subscription,
			(SELECT trial_end FROM subscription WHERE users.user_id = subscription.user_id) as trial_end
			
			FROM 
			users
			
			";
		$r_users = mysqli_query($dbc, $q_users);
		
		?>
        <script>
		
		function CreateUser(theUser) {
			var self = this;
			self.user = ko.observable(theUser);
		}
		
        function usersModel() {
        	var self = this;
			self.users = ko.observableArray();
			
			self.usersPaySubscription = ko.observableArray();
			self.usersFreeSubscription = ko.observableArray();
			self.usersNoSubscription = ko.observableArray();
			
			self.usersNoEmailVerification = ko.observableArray();
			self.usersCancelled = ko.observableArray();
			self.usersDeactivate = ko.observableArray();
			self.currentFilter = ko.observable('All');
			
			self.currentFilterLabel = ko.observable('All');
			
			self.filterList = ko.observableArray();
			
			<?php
				$filters = array();
				$filters['All'] = 'All';
				while($row_users = mysqli_fetch_array($r_users, MYSQLI_ASSOC)) {
					$users_id = $row_users['user_id'];
					$users_first = $row_users['first_name'];
					$users_last = $row_users['last_name'];
					$users_email = $row_users['email'];
					$users_url = '/set_pseudo_user?id=' . $row_users['user_id'];
					$user_url = '/user?id=' . $row_users['user_id'];
					$user_active_account = $row_users['active'];
					$users_stripe_id = $row_users['stripe_id'];
					$users_email_verification = $row_users['active'];
					$users_active_subscription = $row_users['active_subscription'];
					$users_deactivate_in = $row_users['deactivate_in_subscription'];
					$users_free_subscription = $row_users['free_subscription'];
					$users_trial_end = $row_users['trial_end'];
					if(!empty($users_stripe_id) && $users_deactivate_in == 1) {
						$subscriptionActive = 'Deactivating';
						$subscriptionFilter = 'usersDeactivate';
						$filters['Deactivating Users'] = 'usersDeactivate';
					}
					else if($users_deactivate_in == 0 && $users_active_subscription == 1 && $users_free_subscription !=1) {
						
						$subscriptionActive = 'Active';
						$subscriptionFilter = 'usersPaySubscription';
						$filters['Paying Users'] = $subscriptionFilter;
					}
					else if($users_free_subscription ==1) {
						$subscriptionActive = 'Free';
						$subscriptionFilter = 'usersFreeSubscription';
						$filters['Free Users'] = $subscriptionFilter;
					}
					else if(!empty($users_stripe_id) && $users_active_subscription == 0) {
						$subscriptionActive = 'Canceled';
						$subscriptionFilter = 'usersCancelled';
						$filters['Canceled Subscription Users'] = $subscriptionFilter;
					}
					
					else if(empty($users_stripe_id) && $users_free_subscription !=1) {
						$subscriptionActive = '0';
						$subscriptionFilter = 'usersNoSubscription';
						$filters['Users That Have Not Subscribed'] = $subscriptionFilter;
					}
					else if ($user_active_account) {
						$subscriptionActive = '0';
						$subscriptionFilter = 'usersNoEmailVerification';
						$filters['Users That Have Not Verified Email'] = $subscriptionFilter;
					}
					
					?>
					var theUser = {userID: "<?php echo $users_id; ?>", firstName: "<?php echo $users_first; ?>", lastName: "<?php echo  $users_last; ?>", email: "<?php echo $users_email; ?>", url: "<?php echo $users_url; ?>", user_url: "<?php echo $user_url; ?>", email_url: "<?php echo 'mailto:' . $users_email; ?>", subscriptionActive: "<?php echo $subscriptionActive; ?>", freeSubscription: "<?php echo $users_free_subscription; ?>", subscriptionFilter: "<?php echo $subscriptionFilter; ?>" };
					self.users.push(new CreateUser(theUser));
					
					<?php
					
					if(!empty($users_stripe_id) && $users_deactivate_in == 1) {
						//Deactivating
						echo 'self.usersDeactivate.push(new CreateUser(theUser));';
					}
					else if($users_deactivate_in == 0  && $users_active_subscription == 1 && $users_free_subscription !=1) {
						//Payed 
						echo 'self.usersPaySubscription.push(new CreateUser(theUser));';
					}
					else if($users_free_subscription ==1) {
						//Free
						echo 'self.usersFreeSubscription.push(new CreateUser(theUser));';
					}
					else if(!empty($users_stripe_id) && $users_active_subscription == 0) {
						//Canceled
						echo 'self.usersCancelled.push(new CreateUser(theUser));';
					}
					else if(empty($users_stripe_id)) {
						echo 'self.usersNoSubscription.push(new CreateUser(theUser));';
					}
					else if ($user_active_account) {
						//No Email Verification
						echo 'self.usersNoEmailVerification.push(new CreateUser(theUser));';
					}
					
					/*echo '<ul class="users_table">
							<li>' . $users_id . '</li>
							<li><a href="/set_pseudo_user?id=' . $users_id . '">' . $users_first . ' ' . $users_last . '</a></li>
							<li><a href="mailto:' . $users_email . '">' . $row_users['email']  . '</a></li>
							<li><a href="/set_pseudo_user?id=' . $users_id . '">Set User</a></li>
						  </ul>'; */
					
				}
				?>
				
				self.filterUsers = function(theFilter) {
					self.currentFilter(theFilter.filter);
					self.currentFilterLabel(theFilter.filterName);
					
				}
				<?php
				foreach($filters as $filter=>$value) {
					?>self.filterList.push({filterName: "<?php echo $filter; ?>", filter: "<?php echo $value; ?>"}); <?php
				}
				?><?php
				
				
			?>
			
			self.displayedUsers =  ko.computed(function() {
				if(!self.currentFilter()) {
					return self.users(); 
				} 
				else if (self.currentFilter() == 'All') {
					return self.users(); 
				}
				else {
						
						var theCurrentFilter = eval("self." +  self.currentFilter() + "()");
						console.log(theCurrentFilter);
						return eval("self." +  self.currentFilter() + "()");
				}
			})
			
			/*console.log("Pay Users - " + self.usersPaySubscription());
			console.log("Free Users - " + self.usersFreeSubscription());
			console.log("No Subscription Users - " + self.usersNoSubscription());
			
			console.log("Non Verified Users - " + self.usersNoEmailVerification());
			console.log("Canceled Users - " + self.usersCancelled());
			console.log("Deactivating Users - " + self.usersDeactivate()); */
		
        }
        
        </script>
        <div id="filterList">
        	<div  id="filterLabel"><span data-bind="text: currentFilterLabel()"></span></div>
            <ul data-bind="foreach: filterList">
                <li><a data-bind="text: filterName, click: $root.filterUsers"></a></li>
            </ul>
        </div>
        <div id="users_table">
            <ul class="users_table_header users_table">
                <li>User Id</li>
                <li>User Name</li>
                <li>Email</li>
                <li>Set User</li>
            </ul>
    
           <div data-bind="foreach: displayedUsers">
                <ul class="users_table">
                    <li><span data-bind="text: user().userID"></span></li>
                    <li><a data-bind="attr:{href: user().user_url}"><span data-bind="text: user().firstName + ' ' + user().lastName"></span></a></li>
                    <li><a data-bind="attr:{href: user().email_url}"><span data-bind="text: user().email"></span></a></li>
                    <li><a data-bind="attr:{href: user().url}">Set User</a></li>
                </ul>
            </div>
            
		
			<script>
            
                ko.applyBindings(new usersModel());
            </script>
		</div> <?php // end users table ?>
        
		<?php
		
		
include('includes/footer.php');

?>