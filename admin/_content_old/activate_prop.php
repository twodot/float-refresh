<?php

require_once('includes/config.inc.php');
require_login();
if ($user_id == $_GET['u']) {
	
	$q = "SELECT COUNT(*) as total_active, 
		
		(SELECT 
			total_properties 
	
			FROM 
			subscription_types 
	
			WHERE 
			subscription_types.id 
				IN (
					SELECT 
					subscribe_level 
					
					FROM 
					subscription  
					
					WHERE 
					user_id = $user_id) )
			as subscribe_level, 
		
		(SELECT active FROM subscription WHERE user_id = $user_id) as active
		
		FROM 
		properties 
		
		WHERE 
		user_id = $user_id 
		AND active = 1
		
		";
	$r = @mysqli_query ($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	
	$total_active = $row['total_active'];
	$subscribe_level = $row['subscribe_level'];
	$active = $row['active'];
	
	$q_listing = "
		SELECT 
		listing_name, 
		listing_email, 
		listing_phone, 
		listing_brokerage
		
		FROM 
		users 
		
		WHERE
		user_id = $user_id 
		
		";
	$r_listing = @mysqli_query ($dbc, $q_listing);
	$row_listing = mysqli_fetch_array($r_listing, MYSQLI_ASSOC);
	
		if(empty($row_listing['listing_name'])) {
			$listing_error = true;
		}
		else if(empty($row_listing['listing_email'])) {
			$listing_error = true;
		}
		else if(empty($row_listing['listing_phone'])) {
			$listing_error = true;
		}
		else if(empty($row_listing['listing_brokerage'])) {
			$listing_error = true;
		}
		else {
			$listing_error = false;
		}
		
	if($listing_error) {
		echo '$("#error").html(\'<p>You have incomplete listing information in your <a href="/account">account</a>.</p>\').fadeIn(300);';
		echo '$("#error").delay(4000).fadeOut(1000);';
	}
	else if (is_null($subscribe_level)) {
		echo '$("#error").html("<p>You have not started your subscription, please subscribe before activating a property.</p>").fadeIn(300);';
		echo '$("#error").delay(4000).fadeOut(1000);';
	}
	else if ($active == 0) {
		?> $("#error").html('<p>Your account is not active <a href="/account">activate your account</a> before activating properties.</p>').fadeIn(300); <?php
		echo '$("#error").delay(4000).fadeOut(1000);';
	}
	else if ($total_active >= $subscribe_level && $subscribe_level != 0) {
		echo '$("#error").html("<p>Wow, you&rsquo;re Awesome! You&rsquo;ve reached your maximum amount of active properties (' . $subscribe_level . '). Please <a href="/change_subscription">update your subscription</a></p>").fadeIn(300);';
		echo '$("#error").delay(4000).fadeOut(1000);';
	}
	else {
	
		if(isset($_GET)) {
			$trimmed = array_map('trim', $_GET);
			
			$property = (int) str_replace('b_con_', '', $trimmed['p']);
			
				$q = "
					UPDATE 
					properties 
					
					SET 
					active = 1 
					
					WHERE 
					id = $property 
					
					";
				
				$r = @mysqli_query ($dbc, $q);		
		
				if (mysqli_affected_rows($dbc) ==1){
					
					$q = "SELECT * FROM properties WHERE id = $property";
					$r = @mysqli_query ($dbc, $q);
					$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
					$title= $row['title'];
					$price= $row['price'];
					$price = str_replace('$', '', $price);
					$price = str_replace(',', '', $price);
					$price = (int) $price;
					if (!empty($price)) {
						$price = '$' . number_format($price);	
					}
					else {
						$price = '';
					}
					
					
					$q = "SELECT 
						property_info.beacon_id, 
						beacons.title as title, 
						beacons.id as beacon_id, 
						COUNT(beacons.id) as total_beacons, 
						properties.id 
						
						FROM 
						property_info, 
						beacons, 
						properties  
						
						WHERE 
						property_info.beacon_id = beacons.id 
						AND property_info.primary_info = 1 
						AND beacons.user_id = $user_id 
						AND beacons.property_info_id = property_info.id 
						AND properties.id = $property 
						AND property_info.properties_id = $property 
						
						
						";
					
					$r = @mysqli_query ($dbc, $q);
					$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
					
					$beacon_title = $row['title'];
					$total_beacons = $row['total_beacons'];
					$beacon_id = $row['beacon_id'];
					
					?> 											
						$("#active_properties .holder").prepend('<div class="droppable active_property ui-draggable" id="b_con_<?php echo $property; ?>"><a href="/create_property/<?php echo $property; ?>"><?php echo $title; ?> <?php echo $price; ?></a><a href="/delete_property?p=<?php echo $property; ?>"  class="delete_property_button"><img src="<?php echo IMAGES; ?>delete.png" alt="Delete info" title="Delete <?php echo $title; ?>" class="tooltip" /></a><div class="clear"></div><div class="beacon_prop">Primary Beacon<br />Beacon <?php echo $beacon_title; ?> </div><div class="beacon_prop">Total Beacons<br /><?php echo $total_beacons; ?> beacon<?php  if ($total_beacons > 1) { echo 's';}?></div><div class="clear"></div></div>').fadeIn(300); 
                         $("#unassigned_properties #b_con_<?php echo $property; ?>").remove();
						
                        inactive_drag();
                        active_drag();
					
					<?php
				
				
				}
				else {
					echo '$("#error").html("<p>There was an error, the property could not be deactivated!</p>").fadeIn(300);';
					echo '$("#error").delay(2000).fadeOut(1000);';
				}
			
				
			
		} // END GET CHECK
	}// END SUBSCRIBE LEVEL CHECK
	
	if( mysqli_num_rows($r) == 0) {
		echo '$("#error").html("<p>Your account is not active, please activate your account before activating properties.</p>").fadeIn(300);';
		echo '$("#error").delay(2000).fadeOut(1000);';
	}
} // END GET_[USER] == $USER_ID

?>

