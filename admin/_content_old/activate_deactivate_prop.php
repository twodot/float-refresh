<?php

require_once('includes/config.inc.php');
require_login();
if ($user_id == $_GET['u']) {
	
	$q = "SELECT COUNT(*) as total_active, 
		
		(SELECT 
			total_properties 
	
			FROM 
			subscription_types 
	
			WHERE 
			subscription_types.id 
				IN (
					SELECT 
					subscribe_level 
					
					FROM 
					subscription  
					
					WHERE 
					user_id = $user_id) )
			as subscribe_level, 
		
		(SELECT active FROM subscription WHERE user_id = $user_id) as active
		
		FROM 
		properties 
		
		WHERE 
		user_id = $user_id 
		AND active = 1
		
		";
	$r = @mysqli_query ($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	
	$total_active = $row['total_active'];
	$subscribe_level = $row['subscribe_level'];
	$active = $row['active'];
	
	$q_listing = "
		SELECT 
		listing_name, 
		listing_email, 
		listing_phone, 
		listing_brokerage
		
		FROM 
		users 
		
		WHERE
		user_id = $user_id 
		
		";
	$r_listing = @mysqli_query ($dbc, $q_listing);
	$row_listing = mysqli_fetch_array($r_listing, MYSQLI_ASSOC);
	
		if(empty($row_listing['listing_name'])) {
			$listing_error = true;
		}
		else if(empty($row_listing['listing_email'])) {
			$listing_error = true;
		}
		else if(empty($row_listing['listing_phone'])) {
			$listing_error = true;
		}
		else if(empty($row_listing['listing_brokerage'])) {
			$listing_error = true;
		}
		else {
			$listing_error = false;
		}
	
	if(isset($_GET)) {
		$trimmed = array_map('trim', $_GET);
		$property = (int) str_replace('activate_property_form_', '', $trimmed['p']);
		
		$q_beacons= " 
		SELECT
		beacon_id 
		
		FROM 
		property_info 
		
		WHERE 
		properties_id = $property
		AND primary_info = 1
		";
		
		$r_beacons = @mysqli_query ($dbc, $q_beacons);
		$row_beacons = mysqli_fetch_array($r_beacons, MYSQLI_ASSOC);
		
		if($row_beacons['beacon_id'] == 0) {
			$beacons_error = true;	
		}
	}
	
	
	
	if($listing_error) {
		echo '$("#error").html(\'<p>You have completed your listing information in your <a href="/account">account</a>.</p>\').fadeIn(300);';
		echo '$("#error").delay(4000).fadeOut(1000);';
		?>$("#activate_prop_check").attr('checked', false);<?php
	}
	else if (is_null($subscribe_level)) {
		echo '$("#error").html("<p>You have not started your subscription, please subscribe before activating a property.</p>").fadeIn(300);';
		echo '$("#error").delay(4000).fadeOut(1000);';
	}
	else if ($active == 0) {
		?> $("#error").html('<p>Your account is not active <a href="/account">activate your account</a> before activating properties.</p>').fadeIn(300); 
		$("#activate_prop_check").attr('checked', false);
		<?php
		echo '$("#error").delay(4000).fadeOut(1000);';
	}
	else if ($beacons_error) {
		?> $("#error").html('<p>You must add a beacon to the property before activating it.</p>').fadeIn(300); 
		$("#activate_prop_check").attr('checked', false); 
		<?php
		echo '$("#error").delay(4000).fadeOut(1000);';
	}
	/* REMOVE FOR SUBSCRIBE LEVEL CHECK 
		else if ($total_active >= $subscribe_level) {
		echo '$("#error").html("<p>You have reached the maximum properties for your subscription level. Please either deactivate some properties or upgrade your subscription.</p>").fadeIn(300);';
		echo '$("#error").delay(4000).fadeOut(1000);';
		?>$("#activate_prop_check").attr('checked', false);<?php
	}*/
	else {
	
		if(isset($_GET)) {
			$trimmed = array_map('trim', $_GET);
			
			$property = (int) str_replace('activate_property_form_', '', $trimmed['p']);

			$q_active = "SELECT active, COUNT(*) as props FROM properties WHERE id = $property";
			$r_active = @mysqli_query ($dbc, $q_active);
			$row_active = mysqli_fetch_array($r_active, MYSQLI_ASSOC);
			if ($row_active['props'] == 1) {
				$active = $row_active['active'];
				if($active == 0) {
					$active = 1;
				}
				else {
					$active = 0;
				}
				$q_update = "
					UPDATE 
					properties 
					
					SET 
					active = $active 
					
					WHERE 
					id = $property 
					
					";
				
				$r_update = @mysqli_query ($dbc, $q_update);		
		
		
				if (mysqli_affected_rows($dbc) ==1){
					if($active == 0) {
					$active = 'deactivated';
					}
					else {
						$active = 'activated';
					}
					echo '$("#error").html("<p>The property has been ' . $active . '!</p>").fadeIn(300);';
					echo '$("#error").delay(2000).fadeOut(1000);';
				}
				else {
					if($active == 0) {
						$active = 'deactivated';
					}
					else {
						$active = 'activated';
					}
					echo '$("#error").html("<p>There was an error, the property could not be ' . $active . '!</p>").fadeIn(300);';
					echo '$("#error").delay(2000).fadeOut(1000);';
				}
			
			}
			
		} // END GET CHECK
	}// END SUBSCRIBE LEVEL CHECK
	
	if( mysqli_num_rows($r) == 0) {
		echo '$("#error").html("<p>Your account is not active, please activate your account before activating properties.</p>").fadeIn(300);';
		echo '$("#error").delay(2000).fadeOut(1000);';
	}
} // END GET_[USER] == $USER_ID

?>

