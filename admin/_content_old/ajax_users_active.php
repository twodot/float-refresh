<?php
	require_admin_login();
	header('Content-Type: application/json');
	
	$data["status"]="failure";
	$data["message"]="";
	
	if(isset($_GET['id']) && isset($_GET['f'])) {
		$get_id = (int) $_GET['id'];
		$get_f = process_input($_GET['f']);
		
		$q_users = "SELECT * FROM users WHERE user_id=".$get_id;
		$r_users = mysqli_query($dbc, $q_users);
		
		if($row=mysqli_fetch_assoc($r_users)){
			if($get_f=="true" && $row["active"]!=null){
				// query
				$q_sub = "UPDATE users SET active=NULL WHERE user_id=".$get_id;
				if(!mysqli_query($dbc, $q_sub))
					$data["message"]=mysqli_error($dbc);
				else{
					$data["status"]="success";
					$data["message"]="User's Account Has Been Activated";
				}
			} else
				$data["message"]="Unsupported Operation";
		} else
			$data["message"]="User Does Not Exist";
	} else
		$data["message"]="Unsupported Operation";
	
	echo json_encode($data);
	
	function process_input($data){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>