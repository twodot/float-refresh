<?php

require_once('includes/config.inc.php');
require_login();
if ($user_id == $_GET['u']) {
	if(isset($_GET['submitted'])) {
		$trimmed = array_map('trim', $_GET);
		$title = $dbc->real_escape_string($trimmed['title']);
		$text = $dbc->real_escape_string($trimmed['info']);
		$notes = $dbc->real_escape_string($trimmed['notes']);
		$property = $dbc->real_escape_string($trimmed['p']);
		
		
		$title = htmlspecialchars ($title, ENT_QUOTES);
		$text = htmlspecialchars ($text, ENT_QUOTES);
		
		$link = '';
		if(isset($_GET['property_info_link'])) {
			if(!empty($_GET['property_info_link'])) {
				$link = preg_replace('/^(?!https?:\/\/)/', 'http://', $_GET['property_info_link']);
				$link=$dbc->real_escape_string($link);
			}
		}
		
		$link_text = '';
		if (isset($_GET['property_info_link_text'])) {
			if(!empty($_GET['property_info_link_text'])) {
				$link_text=$dbc->real_escape_string($_GET['property_info_link_text']);
			}
		}
		
		$automatic = (int) 0;
		if (isset($_GET['go_to_link_automatically'])) {
			if(!empty($_GET['go_to_link_automatically'])) {
				$automatic = (int) $_GET['go_to_link_automatically'];
			}
		}
		
		
		$notes = htmlspecialchars ($notes, ENT_QUOTES);
		
		
		$q = "
			SELECT 
			id 
			
			FROM 
			property_info 
			
			WHERE 
			properties_id = $property 
			AND primary_info = 1 
		";
		
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$property_info_primary = $row['id'];
	
		$q = "
			INSERT INTO 
			property_info 
			
			(
			properties_id, 
			title, 
			text, 
			link_beacon, 
			link_beacon_text, 
			link_beacon_link, 
			
			notes, 
			child_of   
			)
			
			VALUES (
			'$property', 
			'$title', 
			'$text', 
			 $automatic, 
			'$link_text', 
			'$link', 
			'$notes', 
			'$property_info_primary'
			)
		";
		if($r = @mysqli_query ($dbc, $q)) {
			
			$info_id = mysqli_insert_id($dbc);
			?>
            $("#sortable").prepend('<li id="li_feature_<?php echo $info_id; ?>"><div id="info_feature_<?php echo $info_id; ?>" class="property_info"><div class="prop_info_holder property_info_child property_info_avail"><form action="/update_info" method="get" class="update_info_form"><h4 class="info_title"><?php echo $title; ?></h4><div class="beacon_title_holder"><span id="prop_feature_id_<?php echo $info_id; ?>"></span></div><input class="edit_info_title" name="title" value="<?php echo $title; ?>" /><ul class="prop_revisions"><li><a href="/update_info/<?php echo $info_id; ?>" class="edit_info_button">Edit</a></li><li><a class="add_child_info_show" id="add_child_info_to_<?php echo $info_id; ?>">(+) Add Sub Information</a></li><li><a href="/delete_info?i=<?php echo $info_id; ?>"  class="delete_info_button">(&ndash;) Delete</a></li></ul><div class="edit_media"><a href="https://admin.listfloat.com/choose_media?i=<?php echo $info_id; ?>" class="attach_media fancybox fancybox.iframe">Add Images</a><br /><span class="image_count_holder">(0) Images</span></div><div class="image360Holder"><div><p><a href="#" class="add_360" id="image_360_<?php echo $info_id; ?>">Add a 360 Image</a></p></div></div><div><span id="prop_feature_id_<?php echo $info_id; ?>></span></div><div class="property_info_text text"><p><?php echo $text; ?></p><textarea class="edit_info_box" name="info" ><?php echo $text; ?></textarea></div><div class="property_info_link_holder"><div class="property_information_link_notification"><p>Property Highlight Link</p><?php
             $link_beacon = 0;
			 $link_beacon_text = '';
			 $link_beacon_link = '';
			 
			 if(empty($link)) {
                echo '<p class="link_notification_warning">You have not added a link</p>';
            }
            else {
                if(empty($link_text)) {
                    $link_text = $link;
                }
                else {
                    $link_text = $link_text;
                }
                echo '<p class="link_beacon_link_holder"><a href="' . $link . '" target="_blank">' . $link_text . '</a></p>';
                if($automatic) {
                    echo '<p class="link_beacon_automatic_notification">This link will automatically open in place of the property highlight screen</p>';
                }
                else {
                    echo '<p class="link_beacon_automatic_notification">This link will NOT automatically open in place of the property highlight screen</p>';
                }
            }
        ?></div><div class="property_information_link_form"><p>Add a link to the property highlight screen.</p><input type="text" name="property_info_link_text" placeholder="Link Text" value="<?php echo $link_beacon_text; ?>"  maxlength="50" /><input type="text" name="property_info_link" placeholder="URL" class="property_info_link_input" value="<?php echo $link_beacon_link; ?>" maxlength="255" /><p><input type="checkbox" name="go_to_link_automatically" <?php if ($link_beacon) {echo 'checked="ckecked"';} ?> value="1" /> Open this link in place of the property highlight screen.</p></div></div><div class="property_info_notes"><p>NOTES <span class="notes_instr">(Will not show in app.)</span></p><p class="notes_text"><?php echo $notes; ?></p><textarea class="edit_notes_box" name="notes" ><?php echo $notes; ?></textarea></div><input type="hidden" name="u" value="<?php echo $user_id; ?>" /><input type="hidden" name="i" value="<?php echo $info_id; ?>" /><input type="hidden" name="p" value="<?php echo $property; ?>" /><input type="hidden" name="submitted" value="true" /><input type="submit" class="edit_save" name="Submit" value="SAVE"><input type="button" class="edit_cancel" value="CANCEL"></form></div><div class="add_child_info"><div class="add_child_info_form"><form class="child_info_addition"><p>Title (room or feature)<br /><input type="text" class="info_title" value="" name="title" placeholder="Title" /></p><p>Information<br /><textarea class="info_box" name="info"></textarea></p><p>Notes (will not show in app) <br />*Tip: Where beacon is located in house<br /><textarea class="notes_box" name="notes"></textarea></p><input type="hidden" name="u" value="<?php echo $user_id; ?>" /><input type="hidden" name="p" value="<?php echo $property; ?>" /><input type="hidden" name="i" value="<?php echo $info_id; ?>" /><input type="submit" class="save" name="Submit" value="SAVE" /><input type="hidden" name="submitted" value="true" /><button class="add_info_cancel">Cancel</button></form></div></div><div class="holder"><ul class="child_sort" id="ul_<?php echo $info_id; ?>"></ul></div></div></div></li>').fadeIn(300); 
$("#b_con_<?php echo $property; ?>").append('<li id="li_<?php echo $info_id; ?>"><div id="info_<?php echo $info_id; ?>" class="property_info"><div id="info_container_<?php echo $info_id; ?>" class="prop_info_holder property_info_child property_info_avail"><h4 class="info_title"><?php echo $title; ?><div><span id="prop_id_<?php echo $info_id; ?>"></span></div></h4></div></div></li>');
            
            
			<?php /* $("#<?php echo 'b_con_' . $property; ?> > .holder").prepend('<div id="info_<?php echo $info_id ?>"><div class="property_info"><h4 class="info_title"><?php echo $title; ?></h4><div class="property_info_text"><?php echo $text; ?></div><div class="property_info_notes"><?php echo $notes; ?></div></div>').fadeIn(300); */ ?>
            
            $('#info_addition #info_title').val("");
            $('#info_addition textarea').val("");
            
            $('#info_addition .new_info_link_text').val("");
            $('#info_addition .new_info_link').val("");
            $('#info_addition .new_info_automatic_link_checkbox').attr('checked', false);
            
            $('#add_info').slideUp(500);
            start_info();
            remove_info();
            beacon_assign();
            
			<?php
		}//END INSERT CHECK 
		
	} // END SUBMITTED CHECK
	

} // END USER ID = GET USER ID CHECK
?>

