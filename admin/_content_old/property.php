<?php
// THIS PAGE IS NOW create_property.php

require_login();

if(isset($_GET['p']) || isset($p)) {
	if(isset($_GET['p']) ) {
		$property = (int) $_GET['p'];
	}
	else if(isset($p)) {
		$property = (int) $p;
	}
}
else {
	$property = 0;
}
	
if($property != 0) {
	header('Location: /create_property/' . $property);
}
else {
	header('Location: /panel');
}

$page_title = $page_name = 'Property';

include('includes/header.php');

?>

<div id="error">
</div>

<div id="dialog">
</div>

<?php

// SELECT FOR PROPERTY PRIMARY INFO

if(isset($_GET['p']) || isset($p)) {
	if(isset($_GET['p']) ) {
		$property = (int) $_GET['p'];
	}
	else if(isset($p)) {
		$property = (int) $p;
	}
	if($property != 0) {
		
		
		$arr = get_property($user_id, $dbc, $property);
		$r= $arr['r'];
		$status= $arr['status'];
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		$total_media = $row['total_media'];
		$address = $row['address'];
		$latitude = $row['latitude'] + 0;
		$longitude = $row['longitude'] + 0;
		$marketing_description = nl2br($row['description']);
		$sales_status = $row['sold_status'];
		$status_message = $row['status_message'];
		$property_title = $row['property_title'];
		$beds = $row['beds'];
		$bath = $row['bath'] + 0;
		$sqft = $row['sqft'];
		$lot_size = $row['lot_size'];
		$property_type = $row['property_type'];
		$style = $row['style'];
		$year_built = $row['year_built'];
		$community = $row['community'];
		$county = $row['county'];
		$mls = $row['mls'];
		$walkscore = $row['walkscore']; 
		$show_walkscore = $row['show_walkscore'];
		$show_greatschools = $row['show_greatschools']; 
		$active = $row['active']; 
		$walkscore = $row['walkscore'] +0;  
		$elm = $row['gs_elm'];
		$jh = $row['gs_jh'];
		$hs = $row['gs_hs'];
		$flyer_id = $row['flyer_id'];
		$flyer_name = $row['flyer_name'];
		$bitly_hash = $row['short_url_hash'];
		$collapse_details = $row['collapse_details'];
		$collapse_schools = $row['collapse_schools'];
		$facebook_share = $row['facebook_share'];
		$twitter_share = $row['twitter_share'];
		
		$fb_bitly = BITLY_BASE_URL . 	$facebook_share;
		$tw_bitly = BITLY_BASE_URL . 	$twitter_share;
		
		$facebook_link = 'http://www.facebook.com/sharer/sharer.php?u=' . $fb_bitly;
		$twitter_link = 'https://twitter.com/intent/tweet?url=' . $tw_bitly;
		
		$property_title = $row['property_title'];
		if(!empty($bitly_hash)) {
			$short_url = BITLY_BASE_URL . 	$bitly_hash;
		}
		
		?>
        <script>
		
		$(function() {
			$( "#sortable" ).sortable({ items: " li", placeholder: "sort_highlight", opacity: 0.5 });
			$( "#sortable" ).on( "sortupdate", function( event, ui ) {
				var sorted = $( "#sortable" ).sortable( "serialize", { key: "sort" } );
				
				$.ajax({
					url: "update_sort?"+ sorted, 
					cache: false,
				})
			})
			$( "#sortable" ).disableSelection();
			
			
			$( ".child_sort" ).sortable({ items: "> li", placeholder: "sort_highlight", opacity: 0.5 });
			$( ".child_sort" ).on( "sortupdate", function( event, ui ) {
				var sorted = $( ".child_sort" ).sortable( "serialize", { key: "sort" } );
			})
			
			$( "#sortable .child_sort" ).disableSelection();
			
			<?php 
				if ($collapse_details == 1) {
					?>$('.details_hide_show').hide(); <?php 
				}
				if ($collapse_schools == 1) {
					?>$('#school_list').hide(); <?php 
				}
			?>
			
		});
				
		</script>
        
        <?php
		
		
		echo '<div class="one_half">' . "\n";
			echo '<div id="b_con_' . $row['property_id'] . '" class="top_parent"><div id="info_container_' . $row['info_id'] . '" class="'; if ($status == 'inactive') { echo ' property_info_avail top_level_available"';} echo 'prop_container_main"><h3 class="property_title">' . $row['property_title'] . '</h3>' . "\n";	
					
				
                echo '<ul class="prop_revisions">
						<li>
							<a href="/update_property?i=' . $row['property_id'] . '"  class="edit_info_button">
								<img src="' . IMAGES . 'edit.png" alt="Edit info" title="Edit ' . $row['property_title']	. '" />
							</a>
						</li>';
						/*
						<li>
							<img src="' . IMAGES . 'add.png" class="add_info_show" title="Add Information to ' . $row['property_title']	. '" alt="Add Information" />
						</li>
						*/
				echo '		<li>
							<a href="/delete_property?p=' . $row['property_id'] . '"  class="delete_property_button">
								<img src="' . IMAGES . 'delete.png" alt="Delete info" title="Delete ' . $row['property_title']	. '" class="tooltip" />
							</a>
						</li>
					</ul>';
					echo '<div class="clear"></div>';
					if (!empty($row['price'])) {
						$price = $row['price'];
						$price = str_replace('$', '', $price);
						$price = str_replace(',', '', $price);
						$price = '$' . number_format($price);	
						echo '<div class="beacon_prop">Price<br />' . $price . '</div>';
					}
					
					echo '<div class="beacon_prop">Primary Beacon<br />
					<span id="prop_id_' . $row['info_id'] . '">' . "\n";
					
				if ($status == 'active') {
					echo 'Beacon '. $row['beacon_title'] . ' <a href="/remove_beacon?b=' . $row['beacon_id'] . '&p=' . $row['property_id'] . '&i=' . $row['info_id'] . '" class="remove_beacon" ><img src="/images/delete.png" class="remove_beacon_image" alt="Remove beacon" title="Remove beacon from ' . $row['property_title'] . '" /></a>';
				}
				else {
					echo 'Un Assigned';
				}
				
				
				echo'</span></div>';
				echo '<div id="property_details" class="clear">';
				echo '<form id="activate_property_form_' . $property . '" class="activate_property_form '; if ($active == 1) {echo 'form_checked';} echo '" ><input type="checkbox" id="activate_prop_check"'; if ($active == 1) {echo 'checked="checked"';}  echo ' />';  if ($active == 1) { echo 'This property is active'; } else { echo 'Activate this property'; } echo '</form>';
				
				if($flyer_id) {
					echo '
					<div id="flyer_holder" class="clear">
						<h4>Uploaded Flyer</h4>
                   		<a href="/show_flyer?i=' . $property . '&n=' . urlencode($flyer_name) . '" target="_blank">' . $flyer_name . '</a>
						<a href="/delete_flyer?i=' . $property . '&n=' . urlencode($flyer_name) . '"  id="delete_flyer_button">
							<img src="' . IMAGES . 'delete.png" alt="Delete info" title="Delete ' . $flyer_name. '" class="delete_button" />
						</a>	
                    </div>';
                	
				}// END FLYER ID CHECK
					?>
					<div id="flyer_form_holder" class="clear<?php if($flyer_id) {echo ' hidden'; } ?>" >
					<h4>Add Flyer</h4>
						<form action="/flyer" method="post" enctype="multipart/form-data" id="flyer_upload">
							<input type="hidden" value="<?php echo md5(uniqid(rand(), true)); ?>" name="hash" />
							<input type="file" name="flyer" accept="application/pdf" >
							<div id="submit_holder">
							<img src="/images/loading.gif" class="loader" />
							<input type="hidden" name="p" value="<?php echo $property; ?>" />
							<input type="submit" name="submit" value="Submit" id="upload_image" />
							</div>
						</form>
				   </div>
					
					<?php
				
					echo '<h4 id="details" class="property-' . $property . '">Property Details <div '; if ($collapse_details == 1) {echo 'class="icon-rightarrow"';}else {echo 'class="icon-downarrow"';}  echo'></div></h4>';
					echo '<div class="details_hide_show">';
						echo '<a href="edit_property_details?p=' . $property . '" id="edit_property_detials">Edit Detials</a>';
						echo '<table>';
							echo '<tr><td>Beds </td><td>' . $beds . '</td></tr>';
							echo '<tr><td>Bath </td><td>' . $bath . '</td></tr>';
							echo '<tr><td>SqFt </td><td>' . $sqft . '</td></tr>';
							echo '<tr><td>Lot Size </td><td>' . $lot_size . '</td></tr>';
							echo '<tr><td>Property Type </td><td>' . $property_type . '</td></tr>';
							echo '<tr><td>Year Built </td><td>' . $year_built . '</td></tr>';
							echo '<tr><td>Neighborhood </td><td>' . $community . '</td></tr>';
							echo '<tr><td>County </td><td>' . $county . '</td></tr>';
							echo '<tr><td>MLS# </td><td>' . $mls . '</td></tr>';
						echo '</table>';
						echo '<br />';
					echo '</div>'; // END DETAILS HIDE SHOW
				echo '</div>'; // END PROPERTY DETAILS
				
				echo '<div id="sales_status" class="clear details_hide_show">';
					?>
                    	<form id="property_status_form">
                        	<ul>
                            	<?php if(!$rental) { ?>
                                    <li class="status_click <?php if ($sales_status == 0) {echo ' form_checked'; } ?>" ><input type="radio" name="sales_status" value="0" id="status_for_sale" <?php if ($sales_status == 0) {echo 'checked="checked"'; } ?> /> For Sale</li>
                                    <li class="status_click <?php if ($sales_status == 1) {echo ' form_checked'; } ?>" ><input type="radio" name="sales_status" value="1" id="status_pending" <?php if ($sales_status == 1) {echo 'checked="checked"'; } ?> /> Pending</li>
                                    <li class="status_click <?php if ($sales_status == 2) {echo ' form_checked'; } ?>" ><input type="radio" name="sales_status" value="2" id="status_sold" <?php if ($sales_status == 2) {echo 'checked="checked"'; } ?> /> Sold</li>
                                     <li id="custom_status_message_holder" class="status_click <?php if ($sales_status == 5) {echo ' form_checked'; } ?>" ><input type="radio" name="sales_status" value="5" id="status_custom" <?php if ($sales_status == 5) {echo 'checked="checked"'; } ?> /> Custom </li>
                                     <li><input type="text" name="sales_status_message" maxlength="20" <?php if (!empty($status_message)) {echo 'value="' . $status_message . '"'; } ?> placeholder="<?php if (!empty($status_message)) {echo 'value="' . $status_message . '"'; } else {echo 'Custom Message'; } ?>" id="custom_message_input"/><img src="/images/add.png" id="add_custom_message_button" title="Save Custom Message" alt="Save Custom Message"></li>
                                <?php 
								} // END RENTAL CHECK 
								else {
									?>
									<li>Available <input type="radio" name="sales_status" value="3" id="status_available" <?php if ($sales_status == 3) {echo 'checked="checked"'; } ?> /></li>
                                    <li>Rented <input type="radio" name="sales_status" value="4" id="status_rented" <?php if ($sales_status == 4) {echo 'checked="checked"'; } ?> /></li>
								<?php 
								} // END RENTAL ELSE
								?>
                            </ul>
                            <input type="hidden" name="p" id="sales_status_property_id" value="<?php echo $property; ?>" />
                        </form><br />
                    <?php
				echo '</div>';
				
				echo '<div id="lat_long">';
				if($latitude == 0.0000000 && $longitude == 0.0000000) {
				
					?><form id="form_address">
						<input name="address" placeholder="Address" id="get_address" />
                        <input type="hidden" value="<?php echo $property; ?>" name="p" />
                        <input type="submit" name="submit" value="Add Address" id="get_lat_long" />
						</form>
                     <?php 
				}
					 ?>   
                     	<div id="prop_map" class="details_hide_show">
                        <?php
							if ($latitude != 0.0000000 && $longitude != 0.0000000) {
								echo '<iframe width="100%" height="350" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBR5USylIliiJxWUroSXSaL7KD3T_WM328&q=' . $latitude . ',' . $longitude .  '&zoom=18&maptype=satellite"></iframe>';
							}
						?>
                        </div>
						
                        <p id="lat_long_holder" class="details_hide_show" <?php if ($latitude == 0.0000000 && $longitude == 0.0000000) { echo ' class="hidden_score" '; }?>><a href="/edit_address?p=<?php echo $property; ?>" id="edit_map" <?php if($latitude == 0.0000000 && $longitude == 0.0000000) { echo ' class="hide_edit_map" '; } ?>>Edit Map Info</a><br /><span id="address"><?php if (!empty($address)) {echo $address; } ?></span><br />
                        Lat <span id="lat"><?php if($latitude != 0.0000000) { echo $latitude; } ?></span> / Long <span id="long"><?php if($longitude != 0.0000000) { echo $longitude; } ?></span></p>
                        <?php 
							$q_ws =" SELECT *, COUNT(*) as walkscore_count FROM walkscore WHERE high >= $walkscore AND low <= $walkscore";
							$r_ws = @mysqli_query ($dbc, $q_ws);
							$row_ws = mysqli_fetch_array($r_ws, MYSQLI_ASSOC);
							if($row_ws['walkscore_count'] == 1) {
								$ws_description = $row_ws['description'];
							}
							else {
								$ws_description = '';
							}
							$ws_class = strtolower(str_replace(' ', '-', $ws_description));
							$ws_class = str_replace('&rsquo;s', '', $ws_class)
						?>
                        <form action="/show_scores" method="post" id="show_scores" >
                        	<div id="show_walkscore">Show Walkscore Data in App <input type="checkbox" id="show_walkscore_input" name="show_walkscore" value="1" <?php if ($show_walkscore) {echo 'checked="checked"';} ?> /><br /><div id="actual_walk_score" class="<?php echo $ws_class; if (!$show_walkscore) {echo ' hidden_score ';}?> details_hide_show">Walk Score: <div id="walkscore"><?php echo $walkscore . ' ' . $ws_description ?></div></div></div>
                            <div id="show_schoolscore" >Show GreatSchools Data in App <input type="checkbox" id="show_schoolscore_input" name="show_greatschools" value="1" <?php if ($show_greatschools) {echo 'checked="checked"'; }?> /><br />
                            	<div id="actual_school_score" class="<?php if (!$show_greatschools) {echo ' hidden_score ';}?> ">
                                <div id="schools_hide_show" class="property-<?php echo $property; ?>"><h4>GreatSchools Info <div <?php if ($collapse_schools == 1) {echo 'class="icon-rightarrow"';}else {echo 'class="icon-downarrow"';} ?>> </div></h4></div>
                                    
                                    <div id="school_list">
                                    <p>Please click to select schools for the property</p>
                                    	<?php
											$q_school = "SELECT * FROM schools WHERE property_id = $property ORDER BY grade_range_low, grade_range_high, distance ASC";
											$r_school = @mysqli_query($dbc, $q_school);
											while($row_school = mysqli_fetch_array($r_school, MYSQLI_ASSOC)) {
												?>
											<div class="school<?php if($row_school['selected'] == 1) {echo ' selected'; }?>" id="school_<?php echo $row_school['id']; ?>">
                                            	<div class="gs_rating rating_<?php echo $row_school['gs_rating']; ?>"><?php echo $row_school['gs_rating']; ?>
                                                	<div class="out_of">out of 10</div>
                                                </div>
                                                <div class="gs_info">
                                                	<div class="gs_name"><a href="<?php echo $row_school['overview_link']; ?>" target="_blank"><h4><?php echo $row_school['name']; ?></h4></a></div>
                                                    <div class="grade_range"><?php echo $row_school['grade_range']; ?></div>
                                                    <div class="gs_distance">Distance <?php echo $row_school['distance']; ?></div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <?php 
                                            }
										?>
                                    
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="p" value="<?php echo $property; ?>" />
                        </form>
						
						<?php
						
				echo '</div>';// END LAT LONG
				
				
				echo '<div id="marketing_description" class="clear">';
					if (empty($marketing_description)) {
						echo '<p><a href="/marketing_description?p=' . $property . '" id="edit_marketing_description">Add a Marketing Description</a></p>';
					}
					else {
						echo '<p><a href="/marketing_description?p=' . $property . '" id="edit_marketing_description">Edit Marketing Description</a></p>';
						echo '<p>' . $marketing_description . '</p>';
					}
				echo '</div>'; // END MARKETING DESCRIPTION
				
				
				echo '<div class="edit_media">';
				echo '<a href="/media/' . $row['info_id'] . '">Add/Edit Media</a>'; echo '<br />(' . $total_media . ')'; if ($total_media == 1) {echo ' Image';} else {echo ' Images';} echo'</div>';
                
				
				echo '<h4>Add property highlights <img src="' . IMAGES . 'add.png" class="add_info_show" title="Add Information to ' . $row['property_title']	. '" alt="Add Information" /></h4>';
				
				?>
               
                <div class="clear"></div>
                </div> <?php // END info_container_ ?>
				<div id="add_info">
                    
                    <form id="info_addition">
                    
                        <p>Title (room or feature)<br />
                        <input type="text" id="info_title" value="" name="title" placeholder="Title" /></p>
                        
                        <p>Information<br />
                        <textarea id="info_box" name="info"></textarea></p>
                    
                    	<p>Notes (will not show in app) <br />*Tip: Where beacon is located in house<br />
                        <textarea id="notes_box" name="notes"></textarea>
                        </p>
                        <input type="hidden" name="u" value="<?php echo $user_id; ?>" />
                        <input type="hidden" name="p" value="<?php echo $property; ?>" />
                        <input type="submit" id="save" name="Submit" value="SAVE" />
                        <input type="hidden" name="submitted" value="true" />
                    </form>
                    <h4 id="add_info_cancel">Cancel</h4>
                </div><!-- END ADD INFO -->
                
                
                <?php	
				echo '<div class="holder">' . "\n";
				echo '<ul id="sortable">';
				$r = get_info($user_id, $dbc, $property);
				while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
					if ($row['primary_info'] != 1) {
					$info_id =  $row['info_id'];
					$beacon_title = $row['title'];
					echo '<li id="li_' . $row['info_id'] . '"><div id="info_' . $row['info_id'] . '" class="property_info">' . "\n";
						
							if (is_null($beacon_title)) {
								echo '<div id="info_container_' . $row['info_id'] . '" class="prop_info_holder property_info_child property_info_avail"><h4 class="info_title">' . $row['info_title'];
								
							}
							else {
								echo '<div id="info_container_' . $row['info_id'] . '" class="prop_info_holder property_info_child"><h4 class="info_title">' . $row['info_title'];
								
							}
						
						echo '</h4><ul class="prop_revisions"><li><a href="/update_info?i=' . $info_id . '"  class="edit_info_button"><img src="' . IMAGES . 'edit.png" alt="Edit info" title="Edit ' . $row['info_title']	. '" /></a></li><li><img src="' . IMAGES . 'add.png" class="add_child_info_show" id="add_child_info_to_' . $info_id . '" title="Add Information to ' . $row['info_title']	. '" alt="Add Information" /></li><li><a href="/delete_info?i=' . $info_id . '"  class="delete_info_button"><img src="' . IMAGES . 'delete.png" alt="Delete info" title="Delete ' . $row['info_title']	. '" /></a></li></ul>';
						
							echo '<div class="edit_media"><br /><a href="/media/' . $row['info_id'] . '">Add/Edit Media</a>'; echo '<br />(' . $row['total_media'] . ')'; if ($row['total_media'] == 1) {echo ' Image';} else {echo ' Images';} echo'</div>';
						
							if (is_null($beacon_title)) {
								echo '<div class="beacon_title_holder"><span id="prop_id_' . $row['info_id'] . '"></span></div>';
							}
							else {
								echo '<div class="beacon_title_holder beacon_assigned"><span id="prop_id_' . $row['info_id'] . '"> Beacon ' . $beacon_title . ' <a href="/remove_beacon?b=' . $row['beacon_id'] . '&i=' . $row['info_id'] . '" class="remove_beacon" ><img src="/images/delete.png" class="remove_beacon_image" alt="Remove beacon" title="Remove beacon from ' .  $row['info_title'] . '" /></a></span></div>';
							}
						
						echo '<div class="text"><p>' . $row['info_text'] . '</p></div>'; if (!empty($row['info_notes'])) {echo '<div class="notes"><p>NOTES <span class="notes_instr">(Will not show in app.)</span></p><p>' . $row['info_notes'] . '</p></div>'; } echo '</div>' . "\n";
						
							?>
                            
                            <div class="add_child_info">
                            	<div class="add_child_info_form">
                                <form class="child_info_addition">
                                
                                    <p>Title (room or feature)<br />
                                    <input type="text" class="info_title" value="" name="title" placeholder="Title" /></p>
                                    
                                    <p>Information<br />
                                    <textarea class="info_box" name="info"></textarea></p>
                                
                                    <p>Notes (will not show in app) <br />*Tip: Where beacon is located in house<br />
                                    <textarea class="notes_box" name="notes"></textarea>
                                    </p>
                                    <input type="hidden" name="u" value="<?php echo $user_id; ?>" />
                                    <input type="hidden" name="p" value="<?php echo $property; ?>" />
                                    <input type="hidden" name="i" value="<?php echo $info_id; ?>" />
                                    <input type="submit" class="save" name="Submit" value="SAVE" />
                                    <input type="hidden" name="submitted" value="true" />
                                </form>
                                <h5 class="add_info_cancel">Cancel</h5>
                                </div> <!-- END INFO FORM -->
                            </div><!-- END ADD INFO -->
                            
                            <?php
							echo '<div class="holder">' . "\n";
							echo '<ul class="child_sort" id="ul_' . $row['info_id'] . '">';
							$r_c = get_child_info($user_id, $dbc, $property, $info_id);
							while ($row_c = mysqli_fetch_array($r_c, MYSQLI_ASSOC)) {
								echo '<li id="li_' . $row_c['id'] . '"><div id="info_' . $row_c['id'] . '" class="child_container property_info property_info_child ">' . "\n";
									echo '<div class="holder">' . "\n";
										echo '<h4>' . $row_c['title']	. '<span id="prop_id_' . $row_c['id'] . '"></span></h4><ul class="prop_revisions"><li><a href="/update_info?i=' . $row_c['id']. '" class="edit_info_child_button"><img src="' . IMAGES . 'edit.png" alt="Edit ' . $row_c['title']	. '" title="Edit ' . $row_c['title']	. '" /></a></li><li><a href="/delete_info?i=' . $row_c['id'] . '"  class="delete_info_button"><img src="' . IMAGES . 'delete.png" alt="Delete ' . $row_c['title']	. '" title="Delete ' . $row_c['title']	. '" /></a></li></ul><div class="text"><p>' . $row_c['text'] . '</p></div>'; if (!empty($row_c['notes'])) {echo '<div class="notes"><p>NOTES <span class="notes_instr">(Will not show in app.)</span></p><p>' . $row_c['notes'] . '</p></div>'; } echo '' . "\n";
										
									echo '</div> <!-- END HOLDER -->' . "\n"; // END HOLDER 
								echo '</div><!-- END INFO -->' . "\n"; // END INFO			
								
							}// END GET CHILD INFO
							echo '</ul>'; // END CHILD LIST
						echo '</div><!-- END HOLDER -->' . "\n"; // END HOLDER 
					echo '</div><!-- END INFO -->' . "\n"; // END INFO			
					} // END PRIMARY CHECK 
					echo '</li>';
				} // END GET INFORMATION 
				echo '</ul>';
				echo '</div><!-- END HOLDER -->' . "\n"; // END HOLDER
				
			echo '</div>' . "\n"; // CLOSE PROPERTY
			
			
			echo '<div id="listing_info">';
		
			$q_co_listing = "SELECT
				*, 
				COUNT(*) as co_listing
				
				FROM 
				co_agent  
				
				WHERE 
				user_id=$user_id 
				AND id = (SELECT co_agent FROM properties WHERE id = $property AND user_id = $user_id) 
				";
			$r_co_listing = @mysqli_query ($dbc, $q_co_listing);
			$row_co_listing = mysqli_fetch_array($r_co_listing, MYSQLI_ASSOC);
			
		
			$q_listing = "SELECT
		
				listing_name, 
				listing_email, 
				listing_phone, 
				listing_brokerage 
				
				FROM 
				users 
				
				WHERE 
				user_id=$user_id 
				";
			$r_listing = @mysqli_query ($dbc, $q_listing);
			$row_listing = mysqli_fetch_array($r_listing, MYSQLI_ASSOC);
			if(empty($row_listing['listing_name'])) {
				$listing_error = true;
			}
			else if(empty($row_listing['listing_email'])) {
				$listing_error = true;
			}
			else if(empty($row_listing['listing_phone'])) {
				$listing_error = true;
			}
			else if(empty($row_listing['listing_brokerage'])) {
				$listing_error = true;
			}
			else {
				$listing_error = false;
			}	
				
			if($listing_error) {
				echo '<div class="error" id="property_listing_error">Please <a href="/account">enter your listing information</a></div>';	
			}
			else {
				echo '<div id="listing_hide_show"><h3>Listing Info <div class="icon-downarrow"> </div></h3></div>';
				if ($row_co_listing['co_listing'] != 0) {
					echo '<div class="listing_info_holder co_agent"><h4>Co Agent:</h4><p>' . $row_co_listing['name'] . '<br />' . $row_co_listing['email'] . '<br />' . $row_co_listing['phone'] . '<br />' . $row_co_listing['brokerage'] . '</p>
					<p><a href="edit_co_agent?ca=' . $row_co_listing['id'] . '&p=' . $property . '" id="edit_co_agent">Edit this agent</a></p>
					<p><a href="add_co_agent?p=' . $property . '" id="add_co_agent">Change a co-listed agent</a></p></div>';
				}
				echo '<div class="listing_info_holder"><p>' . $row_listing['listing_name'] . '<br />' . $row_listing['listing_email'] . '<br />' . $row_listing['listing_phone'] . '<br />' . $row_listing['listing_brokerage'] . '</p></div>';
			}
			
			if ($row_co_listing['co_listing'] == 0) {
				echo '<div class="listing_info_holder"><a href="add_co_agent?p=' . $property . '" id="add_co_agent">Add a co-listed agent</a></div>';
			}
		
		echo '</div>'; // END LISTING INFO
			
			
		echo '</div>' . "\n"; // CLOSE ONE HALF PROPERTIES
		
		$r = get_avail_beacons($user_id, $dbc);

		echo '<div id="property_beacons" class="one_half">';
		
		if (isset($short_url)) {
			echo '<div id="share_url" class="clear">
					<h3>Sharing</h3>
					<ul id="social_share_links">
						<li><a href="' . $facebook_link . '" target="_blank" ><img src="/images/facebook.png" /></a></li>
						<li><a href="' . $twitter_link . '"  target="_blank" ><img src="/images/twitter.png" /></a></li>
					</ul>
					<p>Short URL<br /><a href="' . $short_url . '" target="_blank" >' . $short_url . '</a></p>
					<p>For social sharing and	yard arm signs when people don&rsquo;t have the app installed.</p>
				</div>';
		}
		
		
		echo '<p><a href="/remove_beacons/' . $property . '" id="remove_all" >Remove all beacons from this property</a></p>';
		echo '<div id="available_beacons" >';
		echo '<div id="location_beacons">';
			echo '<h3>Location Beacons <a href="/remove_location_beacons?p=' . $property . '" ><img src="/images/delete.png" class="remove_beacon_image" alt="Remove all beacons" title="Remove location beacons from ' . $property_title . '" /></a></h3>';
			echo '<div id="location_beacons_holder_' . $property . '" class="location_beacon">';
				$r_location_beacons = get_location_beacons($user_id, $dbc, $property);
				while($row_location_beacons = mysqli_fetch_array($r_location_beacons, MYSQLI_ASSOC)) {
				
					if (!is_null($row_location_beacons['property_title'])) {
						$prop_title = $row_location_beacons['property_title'];
						$link = 'property';
						$link_id = $row_location_beacons['property_id']; 
					}
					elseif (!is_null($row_location_beacons['building_name'])) {
						$prop_title = $row_location_beacons['building_name'];
						$link = 'building';
						$link_id = $row_location_beacons['building_id']; 
					}
						echo '<div id="beacon-' . $row_location_beacons['id'] . '" class="active_beacon" title="Active in ' . $prop_title; if(!empty($row_location_beacons['info_title'])){echo ' / ' . $row_location_beacons['info_title']; }  echo' ">' . $row_location_beacons['title'] . '</div>';
			}
			echo '</div>'; // END LOCATION BEACONS HOLDER	
			echo '<div class="clear"></div>';	
		echo '</div>'; // END LOCATION BEACONS
		echo '<h3>Available Beacons</h3>';
			
		echo '<div class="holder">';
		while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
			echo '<div id="beacon-' . $row['id'] . '" class="available_beacon">' . $row['title'] . '</div>';
		}
		
		echo '</div>'; // END HOLDER
		
		echo '<div class="clear"></div>';
		echo '<div class="active_beacons">';
			echo '<h3>Active Beacons</h3>';
			
			echo '<div class="holder_active_beacons">';
			$r = get_active_beacons($user_id, $dbc);
			echo $row['property_title'];
			echo $row['building_name'];
			while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
				
			if (!is_null($row['property_title'])) {
				$prop_title = $row['property_title'];
				$link = 'property';
				$link_id = $row['property_id']; 
			}
			elseif (!is_null($row['building_name'])) {
				$prop_title = $row['building_name'];
				$link = 'building';
				$link_id = $row['building_id']; 
			}
				
				echo '<a href="/' . $link . '/' .  $link_id; if(!empty($row['info_title'])){echo '#info_container_' . $row['prop_info_id']; } echo '" ><div id="beacon-' . $row['id'] . '" class="active_beacon" title="Active in ' . $prop_title; if(!empty($row['info_title'])){echo ' / ' . $row['info_title']; }  echo' ">' . $row['title'] . '</div></a>';
			}
			echo '<div class="clear"></div>';
			echo '</div>'; // END ACTIVE HOLDER //
		echo '</div>';// END ACTIVE BEACONS

echo '</div>'; // END BEACONS
		echo '</div>'; // END PROPERTY BEACONS
	}
}
else {
	echo '<p>You&rsquo;ve reached this page in error</p>';
}

include('includes/footer.php');
?>