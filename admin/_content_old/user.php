<?php

require_admin_login();
unset($_SESSION['pseudo_user']); 

$page_title = $page_name = 'Users';

$get_id=null;
if(isset($_GET['id'])) {
	$get_id = (int) $_GET['id'];
}
include('includes/header.php');
		
		$q_users = "SELECT 
			users.*, 
			(SELECT active FROM subscription WHERE users.user_id = subscription.user_id) as active_subscription, 
			(SELECT deactivate_in FROM subscription WHERE users.user_id = subscription.user_id) as deactivate_in_subscription, 
			(SELECT free FROM subscription WHERE users.user_id = subscription.user_id) as free_subscription,
			(SELECT trial_end FROM subscription WHERE users.user_id = subscription.user_id) as trial_end
			
			FROM 
			users
					
			";
			
			if($get_id) {
				$q_users .= " WHERE user_id = " . $get_id;
			}
		$r_users = mysqli_query($dbc, $q_users);
		
		?>
		
		<link href="css/bootstrap.css">
		<link href="css/bootstrap-switch.css" rel="stylesheet">
		<script src="js/bootstrap-switch.js"></script>
		
	    <script>
	    $(function(argument) {
	      $('[type="checkbox"]').bootstrapSwitch();
	    })
	    </script>

        <script>
		
		$( document ).ready(function() {
	
			$('#active_user').on('switchChange.bootstrapSwitch', function(event, state) {
				theUrl =  "/ajax_users_active?id=<?php echo $get_id; ?>&f=" + state;
				$.ajax({
					url: theUrl,
					cache: false,
				}).done(function(data) {
					$('#users_active_message').html(data.message);
				});
				
				
			});	
			
			$('#test_user').on('switchChange.bootstrapSwitch', function(event, state) {
				theUrl =  "/ajax_users_test?id=<?php echo $get_id; ?>&f=" + state;
				$.ajax({
					url: theUrl,
					cache: false,
				}).done(function(data) {
					$('#users_test_message').html(data.message);
				});
				
				
			});
			
			$('#active_subscription').on('switchChange.bootstrapSwitch', function(event, state) {
				theUrl =  "/ajax_sub_active?id=<?php echo $get_id; ?>&f=" + state;
				$.ajax({
					url: theUrl,
					cache: false,
				}).done(function(data) {
					$('#sub_active_message').html(data.message);
				});
				
				
			});
			
			$('#free_subscription').on('switchChange.bootstrapSwitch', function(event, state) {
				theUrl =  "/ajax_free_subscription?id=<?php echo $get_id; ?>&f=" + state;
				$.ajax({
					url: theUrl,
					cache: false,
				}).done(function(data) {
					$('#free_subscription_message').html(data.message);
				});
				
				
			});
		});
		function CreateUser(theUser) {
			var self = this;
			self.user = ko.observable(theUser);
		}
		
        function usersModel() {
        	var self = this;
			self.users = ko.observableArray();
			
			self.usersPaySubscription = ko.observableArray();
			self.usersFreeSubscription = ko.observableArray();
			self.usersNoSubscription = ko.observableArray();
			
			self.usersNoEmailVerification = ko.observableArray();
			self.usersCancelled = ko.observableArray();
			self.usersDeactivate = ko.observableArray();
			self.currentFilter = ko.observable('All');
			
			self.currentFilterLabel = ko.observable('All');
			
			self.filterList = ko.observableArray();
			
			<?php
				$filters = array();
				$filters['All'] = 'All';
				while($row_users = mysqli_fetch_array($r_users, MYSQLI_ASSOC)) {
					$users_id = $row_users['user_id'];
					$users_first = $row_users['first_name'];
					$users_last = $row_users['last_name'];
					$users_email = $row_users['email'];
					$users_url = '/set_pseudo_user?id=' . $row_users['user_id'];
					$user_url = '/user?id=' . $row_users['user_id'];
					$user_active_account = $row_users['active'];
					$users_stripe_id = $row_users['stripe_id'];
					$users_email_verification = $row_users['active'];
					$users_active_subscription = $row_users['active_subscription'];
					$users_deactivate_in = $row_users['deactivate_in_subscription'];
					$users_free_subscription = $row_users['free_subscription'];	
					$users_trial_end = $row_users['trial_end'];
					
					if(!empty($users_stripe_id) && $users_deactivate_in == 1) {
						$subscriptionActive = 'Deactivating';
						$subscriptionFilter = 'usersDeactivate';
						$filters['Deactivating Users'] = 'usersDeactivate';
					}
					else if($users_deactivate_in == 0 && $users_active_subscription == 1 && $users_free_subscription !=1) {
						
						$subscriptionActive = 'Active';
						$subscriptionFilter = 'usersPaySubscription';
						$filters['Paying Users'] = $subscriptionFilter;
					}
					else if($users_free_subscription ==1) {
						$subscriptionActive = 'Free';
						$subscriptionFilter = 'usersFreeSubscription';
						$filters['Free Users'] = $subscriptionFilter;
					}
					else if(!empty($users_stripe_id) && $users_active_subscription == 0) {
						$subscriptionActive = 'Canceled';
						$subscriptionFilter = 'usersCancelled';
						$filters['Canceled Subscription Users'] = $subscriptionFilter;
					}
					
					else if(empty($users_stripe_id) && $users_free_subscription !=1) {
						$subscriptionActive = '0';
						$subscriptionFilter = 'usersNoSubscription';
						$filters['Users That Have Not Subscribed'] = $subscriptionFilter;
					}
					else if ($user_active_account) {
						$subscriptionActive = '0';
						$subscriptionFilter = 'usersNoEmailVerification';
						$filters['Users That Have Not Verified Email'] = $subscriptionFilter;
					}
					
					?>
					var theUser = {userID: "<?php echo $users_id; ?>", firstName: "<?php echo $users_first; ?>", lastName: "<?php echo  $users_last; ?>", email: "<?php echo $users_email; ?>", url: "<?php echo $users_url; ?>", user_url: "<?php echo $user_url; ?>", email_url: "<?php echo 'mailto:' . $users_email; ?>", subscriptionActive: "<?php echo $subscriptionActive; ?>", freeSubscription: "<?php echo $users_free_subscription; ?>", subscriptionFilter: "<?php echo $subscriptionFilter; ?>" };
					self.users.push(new CreateUser(theUser));
					
					<?php
					
					if(!empty($users_stripe_id) && $users_deactivate_in == 1) {
						//Deactivating
						echo 'self.usersDeactivate.push(new CreateUser(theUser));';
					}
					else if($users_deactivate_in == 0  && $users_active_subscription == 1 && $users_free_subscription !=1) {
						//Payed 
						echo 'self.usersPaySubscription.push(new CreateUser(theUser));';
					}
					else if($users_free_subscription ==1) {
						//Free
						echo 'self.usersFreeSubscription.push(new CreateUser(theUser));';
					}
					else if(!empty($users_stripe_id) && $users_active_subscription == 0) {
						//Canceled
						echo 'self.usersCancelled.push(new CreateUser(theUser));';
					}
					else if(empty($users_stripe_id)) {
						echo 'self.usersNoSubscription.push(new CreateUser(theUser));';
					}
					else if ($user_active_account) {
						//No Email Verification
						echo 'self.usersNoEmailVerification.push(new CreateUser(theUser));';
					}
					
					/*echo '<ul class="users_table">
							<li>' . $users_id . '</li>
							<li><a href="/set_pseudo_user?id=' . $users_id . '">' . $users_first . ' ' . $users_last . '</a></li>
							<li><a href="mailto:' . $users_email . '">' . $row_users['email']  . '</a></li>
							<li><a href="/set_pseudo_user?id=' . $users_id . '">Set User</a></li>
						  </ul>'; */
					
				}
				?>
				
				self.filterUsers = function(theFilter) {
					self.currentFilter(theFilter.filter);
					self.currentFilterLabel(theFilter.filterName);
					
				}
				<?php
				foreach($filters as $filter=>$value) {
					?>self.filterList.push({filterName: "<?php echo $filter; ?>", filter: "<?php echo $value; ?>"}); <?php
				}
				?><?php
				
				
			?>
			
			self.displayedUsers =  ko.computed(function() {
				if(!self.currentFilter()) {
					return self.users(); 
				} 
				else if (self.currentFilter() == 'All') {
					return self.users(); 
				}
				else {
						
						var theCurrentFilter = eval("self." +  self.currentFilter() + "()");
						console.log(theCurrentFilter);
						return eval("self." +  self.currentFilter() + "()");
				}
			})
			
			/*console.log("Pay Users - " + self.usersPaySubscription());
			console.log("Free Users - " + self.usersFreeSubscription());
			console.log("No Subscription Users - " + self.usersNoSubscription());
			
			console.log("Non Verified Users - " + self.usersNoEmailVerification());
			console.log("Canceled Users - " + self.usersCancelled());
			console.log("Deactivating Users - " + self.usersDeactivate()); */
		
        }
        
        </script>
        <div id="filterList">
        	<div  id="filterLabel"><span data-bind="text: currentFilterLabel()"></span></div>
            <ul data-bind="foreach: filterList">
                <li><a data-bind="text: filterName, click: $root.filterUsers"></a></li>
            </ul>
        </div>
        <div id="users_table">
            <ul class="users_table_header users_table">
                <li>User Id</li>
                <li>User Name</li>
                <li>Email</li>
                <li>Set User</li>
            </ul>
    
           <div data-bind="foreach: displayedUsers">
                <ul class="users_table">
                    <li><span data-bind="text: user().userID"></span></li>
                    <li><a data-bind="attr:{href: user().user_url}"><span data-bind="text: user().firstName + ' ' + user().lastName"></span></a></li>
                    <li><a data-bind="attr:{href: user().email_url}"><span data-bind="text: user().email"></span></a></li>
                    <li><a data-bind="attr:{href: user().url}">Set User</a></li>
                </ul>
            </div>
            
		
			<script>
            
                ko.applyBindings(new usersModel());
            </script>
		</div> <?php // end users table ?>
        
        <?php
	    if($user = new User($get_id, $dbc)){
		    $listing_info=$user->get_listing_info();
		    
		    if ($_SERVER["REQUEST_METHOD"] == "POST"){
				
				$user->set_first_name(process_input($_POST["first_name"]));
				$user->set_last_name(process_input($_POST["last_name"]));
				$user->set_email(process_input($_POST["email"]));
				
				if($_POST["pass1"]!=null && $_POST["pass2"]!=null){
					if($_POST["pass1"]===$_POST["pass2"])
						$user->set_pass(sha1($_POST["pass1"], false));
					else
						echo "Password Did Not Changed: Two Passwords Need To Be The Same.\n";
				}
				
				$user->set_admin(isset($_POST["user_level"]));
				if(isset($_POST["users_active"]))
					$user->activate();
				$user->set_ph_num(process_input($_POST["users_ph_num"]));
				
				// $input["test"] = isset($_POST["users_test"])? '1': '0';
				// $input["test"] = ($users_test == $input["test"])? '': $input["test"];
				
				$user->set_listing_name(process_input($_POST["users_listing_name"]));
				$user->set_listing_email(process_input($_POST["users_listing_email"]));
				$user->set_listing_phone(process_input($_POST["users_listing_phone"]));
				$user->set_listing_brokerage(process_input($_POST["users_listing_brokerage"]));
				$user->set_broker_website(process_input($_POST["users_broker_website"]));
				
				$update_message=$user->update_DB();
				if($update_message){
					echo $update_message;
					$user->reload_DB();
				} else
					echo "<h1>Success.</h1><br>";
			}
		    ?>
		    <div>
		        <strong>User ID: </strong><?php echo $user->get_user_id();?><br>
		        <strong>Registration Date: </strong><?php echo $user->get_registration_date();?><br><br>
		        
		        <form method="post">
			        
                	<label for="first_name">First Name</label>
			        <input type="text" name="first_name" placeholder="First Name" value="<?php echo $user->get_first_name();?>"><br><br>
                	
                	<label for="last_name">Last Name</label>
			        <input type="text" name="last_name" placeholder="Last Name" value="<?php echo $user->get_last_name();?>"><br><br>
                	
                	<label for="email">Email</label>
			        <input type="text" name="email" placeholder="Email" value="<?php echo $user->get_email();?>"><br><br>
			        
                	<label for="pass1">Change Password</label>
			        <input type="password" name="pass1" placeholder="Change Password"><br><br>
			        
                	<label for="pass2">Verify Password</label>
			        <input type="password" name="pass2" placeholder="Verify Password"><br><br>
			        
                	<label for="user_level">Admin</label>
                	<input type="checkbox" name="user_level" <?php echo ($user->is_admin()? "checked": "");?> 
                	data-on-text="Yes" data-off-text="No"><br><br>
			        
                	<label for="users_active">User Active</label>
                    <div id="users_active_message"></div>
                	<input id="active_user" type="checkbox" name="users_active" <?php echo ($user->is_active()? "checked readonly":"");?> 
                	data-on-text="Yes" data-off-text="No"><br><br>
			        
                	<label for="users_ph_num">Phone Number</label>
			        <input type="text" name="users_ph_num" placeholder="Phone Number" value="<?php echo $user->get_ph_num();?>"><br><br>
			        
                	<!--<label for="users_test">Test User</label>
                    <div id="users_test_message"></div>
                	<input id="test_user" type="checkbox" name="users_test" <?php echo ($users_test? "checked": "");?> 
                	data-on-text="Yes" data-off-text="No"><br><br>-->
			        
                	<label for="users_listing_name">Listing Name</label>
			        <input type="text" name="users_listing_name" placeholder="Listing Name" value="<?php echo $listing_info["listing_name"];?>"><br><br>
			        
                	<label for="users_listing_email">Listing Email</label>
			        <input type="text" name="users_listing_email" placeholder="Listing Email" value="<?php echo $listing_info["listing_email"];?>"><br><br>
			        
                	<label for="users_listing_phone">Listing Phone</label>
			        <input type="text" name="users_listing_phone" placeholder="Listing Phone" value="<?php echo $listing_info["listing_phone"];?>"><br><br>
			        
                	<label for="users_listing_brokerage">Listing Brokerage</label>
			        <input type="text" name="users_listing_brokerage" placeholder="Listing Brokerage" 
			        value="<?php echo $listing_info["listing_brokerage"];?>"><br><br>
			        
                	<label for="users_broker_website">Broker Website</label>
			        <input type="text" name="users_broker_website" placeholder="Broker Website" 
			        value="<?php echo $listing_info["broker_website"];?>"><br><br>
			        
                	<label for="sub_active">Active</label>
                    <div id="sub_active_message"></div>
                	<input id="active_subscription" type="checkbox" name="sub_active" <?php echo ($user->is_sub_active()? "checked": "");?> 
                	data-on-text="Yes" data-off-text="No"><br><br>
			        
                	<label for="sub_free">Free Subscription</label>
                    <div id="free_subscription_message"></div>
                	<input id="free_subscription" type="checkbox" name="sub_free" <?php echo ($user->is_sub_free()? "checked": "");?> 
                	data-on-text="Yes" data-off-text="No"><br><br>
			        
			        <input type="submit" name="submit">
			    </form>
			</div>
		    <?php
	    }
	    	
		function process_input($data){
			$data = trim($data);
			$data = stripslashes($data);
			$data = htmlspecialchars($data);
			return $data;
		}
		
include('includes/footer.php');
?>