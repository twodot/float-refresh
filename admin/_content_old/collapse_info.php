<?php

require_once('includes/config.inc.php');
require_login();

if (isset($_GET['p'])) {
	
	$arr = explode('-', $_GET['p']);
	if($arr[0] == 'property') {
		$table = 'properties';
		$property = (int) $arr[1];
	}
	else if($arr[0] == 'building') {
		$table = 'building';
		$property = (int) $arr[1];
	}
}
if(isset($property)) {
		
		if($property != 0) {
			$q = "
				SELECT 
				*    
				
				FROM 
				$table  
				
				WHERE 
				id = $property
			";
			
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			if ($user_id == $row['user_id']) {
				$column = $_GET['c'];
				// GET WHAT COLUMN TO UPDATE
				if ($column == 'details') {
					$get_column = true;
					$column = 'collapse_details';
				}
				else if ($column == 'schools') {
					$get_column = true;
					$column = 'collapse_schools';
				}
				if($get_column) {
					// GET VALUE OF COLLAPSE AND CHANGE IT
					$collapse_val = $row[$column];
					if($collapse_val == 1) {
						$collapse_val = 0;
					}
					else {
						$collapse_val = 1;
					}
					
					$q = "
						UPDATE 
						$table 
						
						SET 
						$column = $collapse_val
					
						WHERE 
						id = $property 
						AND user_id = $user_id 
						
					";
					
					$r = @mysqli_query ($dbc, $q);
				}
				
			}
			else {
				echo 'You have reached this page in error';
			}
			
		}// END PROPERTY INT CHECK

}
?>

