<?php

require_login();
$page_title = $page_name = 'All Transactions';

include('includes/header.php');

		(int) $user_id;

		if($user_id != 0) {
			$q = "
				SELECT 
				*    
				
				FROM 
				past_subscriptions  
				
				WHERE 
				user_id = $user_id 
				
				ORDER BY id 
				DESC
			";
			
			if(isset($_GET['l'])) {
				$l = (int) $_GET['l'];
				if ($l != 0) {
					$q.= " LIMIT $l ";
				}
			}
			echo '<table>';
			echo '<tr  class="transactions_header"><td>Transaction Type</td><td>Date</td><td>Amount</tdh></tr>';
			echo '<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
			$r = @mysqli_query ($dbc, $q);
			while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
				
				switch ($row['event_type']) {
					
					case 'charge.succeeded':
						$title = 'Charge';
					break;
					
					case 'invoice.payment_succeeded':
						$title = 'Invoice Payment';
					break;
					
					case 'customer.subscription.updated':
						$title = 'Subscription Updated';
					break;
					
					case 'customer.subscription.deleted':
						$title = 'Subscription Canceled';
					break;
					
					default:
				      $title = 'Untitled Transaction';
						
				}
				$phpdate = strtotime( $row['invoice_date'] );
				$date_run = date("m / j / Y", $phpdate);
				
				echo '<tr  class="transactions"><td><a href="transaction?t=' . $row['id'] . '" target="_blank">' . $title . '</a></td><td>' . $date_run . '</td><td>$' . $row['amount'] / 100 . '</td></tr>';
			}
			echo '</table>';
			
			
		}// END PUSER INT CHECK

include('includes/footer.php');
	
?>

