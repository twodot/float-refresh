<?php

require '/home/ec2-user/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

require_once('includes/config.inc.php');
require_login();


if(isset($_POST['submit'])) {
	
	if(isset($_POST['p'])) {
		$p = (int) $_POST['p'];
	
		if($p != 0) {
			$q="SELECT COUNT(*) as properties_count FROM properties WHERE id = $p AND user_id = $user_id";
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			if($row['properties_count'] == 1) {
				$hash = $_POST['hash'];
				$q = "SELECT COUNT(*) as flyers_total, serial_num FROM flyers WHERE serial_num = '$hash'";
				$r = @mysqli_query ($dbc, $q);
				$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
				
				if($row['flyers_total'] == 0) {
					$ext = substr($_FILES['flyer']['name'], -4);
					
					if($ext == '.pdf' || $ext == '.PDF') {
						$file = $_FILES['flyer'];
						if (is_uploaded_file ($file['tmp_name'])){
							$temp = FLYERS . md5($file['name']);
							if (move_uploaded_file($file['tmp_name'], $temp)){
								$name = $file['name'];
								$q="INSERT INTO flyers SET property_id = $p, name='$name', user_id = $user_id, serial_num='$hash'";
								$r = @mysqli_query ($dbc, $q);
								
								$flyer_id = mysqli_insert_id($dbc);
								
								rename ($temp, FLYERS . "$flyer_id" . '.pdf');
								
								$flyer_location = FLYERS . "$flyer_id" . '.pdf';
								
								// THUMBNAIL SECTION
								$flyerBucket = 'images.listfloat.com/flyers';
								$flyerS3 = S3Client::factory(array('region' => 'us-west-2'));
								
								try {
									$upload = $flyerS3->upload($flyerBucket, $flyer_id . '.pdf', fopen($flyer_location, 'rb'), 'public-read');
								} catch(Exception $e) { 
								}					
								
								if (isset($flyer_location) && file_exists($flyer_location) && is_file($flyer_location)){
									unlink($flyer_location);
								} // DELETE FLYER FROM TEMP SECTION 
								
								if (isset($temp) && file_exists($temp) && is_file($temp)){
									unlink($temp);
								}// END TEMP FILE CHECK
								
							} // END IF MOVE FILE
							
						}// END IF IS UPLOADED
						
					} // END IF PDF				
					
				}// END CHECK HASH
				
				echo $url = '/property/' . $p ;
						
				ob_end_clean();
				
				header("Location: $url");
				
			} // END USER/PROPERTY CHECK
			else {		
				$url = '/panel' ;
						
				ob_end_clean();
				
				header("Location: $url");
			}
			
		} // END P CHECK
		
	} // END POST P CHECK
	else {		
		$url = '/panel' ;
				
		ob_end_clean();
		
		header("Location: $url");
	}
	
}// END SUBMIT CHECK
else {		
	$url = '/panel' ;
			
	ob_end_clean();
	
	header("Location: $url");
}