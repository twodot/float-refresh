<?php

$page_title = 'Contact Float';
if(isset($_GET['API_KEY'])) {
	if(isset($_GET['property'])) {
		$page_title = $page_name = 'Contact Float';
		
		$api_key = sanitize($_GET['API_KEY'], $dbc);
		$property = (int) sanitize($_GET['property'], $dbc);
		
		if($property != 0) {
			$q = "SELECT * FROM api_key WHERE api_key = '$api_key'";
			$r = mysqli_query($dbc, $q);
			if(mysqli_num_rows($r) == 1 ) {	
			
				$q = "SELECT address FROM properties WHERE id = '$property'";
				$r = mysqli_query($dbc, $q);
				if(mysqli_num_rows($r) == 1) {
					$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
					include('includes/header.php');
					
					// HIDE PAGE ASSETS
					?>
					
					
					
					<script>
					
					
					jQuery(document).ready(function( $ ) {
						 var intervalID = setInterval(function(){
							
								empty_field = false;
								$(".required").each(function(index, element) {
									if($(this).val() == '') {
										empty_field = true;
									}
								});
								if (empty_field == false) {
									$("#in_app_form_submit").prop( "disabled", false );
								}
								else {
									$("#in_app_form_submit").prop( "disabled", true );
								}
								
						  }, 100); // 100 ms check
						
						$('#in_app_form_submit').click(function(e) {
							e.preventDefault();
							
							$("#load_modal").show();
							$(".loader").show();
							
							$.ajax({
								url: "app_contact_process", 
								data: jQuery("#ios_in_app_form").serialize(),
								dataType:"script", 
								cache: false,
							}).done(function(data) {
								
									
								
								$("#load_modal_confirm").animate({
									top: 0
								}, 2000, function() {
									$("#load_modal").hide();
									$("#load_modal").css( "top", "0" );
								});
								$("#load_modal_confirm").delay(1500).animate({
									top: 5000
								}, 2000, function() {
									
								});
							});
						})
					})
					</script>
					<div id="load_modal">
						<div class="loader">
							<div class="loader-inner ball-scale">
								<div></div>
							</div>
						</div>
					</div>                
					<div id="load_modal_confirm"><div class="message">Your request has been sent!</div></div>
					<form method="get" action="/app_contact_process" id="ios_in_app_form">
						<input type="text" placeholder="First Name" name="first_name" class="required" /><br />
						<input type="text" placeholder="Last Name" name="last_name" class="required" /><br />
						<input type="tel" name="phone" placeholder="Phone" /><br />
						<input type="email" name="email" placeholder="Email" class="required" /><br />
						<textarea name="message">I would like more information about <?php echo $row['address']; ?>.</textarea><br />
						<input type="hidden" name="id" value="<?php echo $property; ?>" />
						<input type="hidden" name="submitted" value="true" />
						<input type="submit" name="Submit" id="in_app_form_submit" disabled="disabled" />
					</form>
					<?
					include('includes/footer.php');
				}// END PROPERTY ID CHECK
			}// END NUM ROWS CHECK FOR API_KEY
		} // END PROPERTY "0" CHECK
	}// END GET PROPERTY ID
	else if (isset($_GET['general'])) {
			
		include('includes/header.php');
				
				// HIDE PAGE ASSETS
				?>
				<style>
					#admin_nav { display: none; }
					html, #contain_all, #content, body {height:100%;}
				   
				</style>
				
				
				<script>
				
				
				jQuery(document).ready(function( $ ) {
					 var intervalID = setInterval(function(){
						
							empty_field = false;
							$(".required").each(function(index, element) {
								if($(this).val() == '') {
									empty_field = true;
								}
							});
							if (empty_field == false) {
								$("#in_app_form_submit").prop( "disabled", false );
							}
							else {
								$("#in_app_form_submit").prop( "disabled", true );
							}
							
					  }, 100); // 100 ms check
					
					$('#in_app_form_submit').click(function(e) {
						e.preventDefault();
						
						$("#load_modal").show();
						$(".loader").show();
						
						$.ajax({
							url: "app_contact_process", 
							data: jQuery("#ios_in_app_form").serialize(),
							dataType:"script", 
							cache: false,
						}).done(function(data) {
							
								
							
							$("#load_modal_confirm").animate({
								top: 0
							}, 2000, function() {
								$("#load_modal").hide();
								$("#load_modal").css( "top", "0" );
							});
							$("#load_modal_confirm").delay(1500).animate({
								top: 5000
							}, 2000, function() {
								
							});
						});
					})
				})
				</script>
				<div id="load_modal">
					<div class="loader">
						<div class="loader-inner ball-scale">
							<div></div>
						</div>
					</div>
				</div>                
				<div id="load_modal_confirm"><div class="message">Your request has been sent!</div></div>
				<form method="get" action="/app_contact_process" id="ios_in_app_form">
					<input type="text" placeholder="First Name" name="first_name" class="required" /><br />
					<input type="text" placeholder="Last Name" name="last_name" class="required" /><br />
					<input type="tel" name="phone" placeholder="Phone" /><br />
					<input type="email" name="email" placeholder="Email" class="required" /><br />
					<textarea name="message">Hey Float,<?php echo "\n\n"; ?>Listen up. </textarea><br />
                    <input type="hidden" name="general" value="1" />
					<input type="hidden" name="submitted" value="true" />
					<input type="submit" name="Submit" id="in_app_form_submit" disabled="disabled" />
				</form>
				<?
				include('includes/footer.php');
		
	}// END GENERAL CHECK
}// END GET CHECK

?>

<style>
	#admin_nav { display: none; }
	html, #contain_all, #content, body {height:100%;}
	#contain_all, #content, #ios_in_app_form {
		padding-top: 0px !important;
	}
	#content {
		margin-top: 20px;
	}
	#ios_in_app_form textarea {
		height:200px;
	}
	
	#ios_in_app_form input[type="submit"] {
		border: 1px solid #00AEC7;
		background:#00AEC7;
		color:#fff;
	}
	
   
</style>