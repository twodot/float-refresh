<?php

require_once('includes/config.inc.php');
require_login();
if (isset($_POST['existing-submitted'])) {
	$co_agent_id = (int) $_POST['existing_agent'];
	$property = (int) $_POST['property'];
	$q= "UPDATE 
	building 
	
	SET 
	co_agent = $co_agent_id 
	
	WHERE 
	id = $property 
	AND user_id = $user_id";
	$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
	mysqli_close($dbc);
	$url = BASE_URL . 'building/' . $property;
	
	ob_end_clean();

	header("Location: $url");
}
else if (isset($_POST['submitted'])){
	
	$em = FALSE;
	$trimmed = array_map('trim', $_POST);
	
	$listing_name = $dbc->real_escape_string($trimmed['name']);
	
	if (preg_match ('/^[\w.-]+@[\w.-]+\.[A-Za-z]{2,6}$/', $trimmed['email'])) {
		$em = $dbc->real_escape_string($trimmed['email']);
	}
	else {
		$e = 5;
		$em = '';
	} 
	
	$listing_phone = $dbc->real_escape_string($trimmed['phone']);
	
	$listing_brokerage = $dbc->real_escape_string($trimmed['brokerage']);
	$listing_brokerage = htmlspecialchars ($listing_brokerage, ENT_QUOTES);
	$prop = $_POST['property'];
			
	$q = "INSERT INTO 
	co_agent
	
	SET 
	name = '$listing_name', 
	email = '$em', 
	phone = '$listing_phone', 
	brokerage = '$listing_brokerage', 
	user_id = $user_id
	";
	if (!empty($listing_name) && !empty($em) && !empty($listing_phone) && !empty($listing_brokerage)) {
		$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
		$insert_id = mysqli_insert_id($dbc);
		
		$q= "UPDATE 
		building 
		
		SET 
		co_agent = $insert_id 
		
		WHERE 
		id = $prop 
		AND user_id = $user_id";
		$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
		
		mysqli_close($dbc);
	}

	$url = BASE_URL . 'building/' . $prop;
	
	ob_end_clean();

	header("Location: $url");

}

if(isset($_GET['p'])) {
	
	$property_id = (int) $_GET['p'];
	
	if($property_id != 0) {
		
		$q = "SELECT
	id,
	name
	
	FROM 
	co_agent  
	
	WHERE 
	user_id=$user_id 
	";
	$r = @mysqli_query ($dbc, $q);
	
	$num_rows = mysqli_num_rows($r);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	if ($num_rows > 0) {
		echo '<p>Existing Agents</p>';
		echo '<form action="/add_co_agent_building" method="post" id="existing_listing_info_form">';
		echo '<div class="select_holder"><select name="existing_agent"><option value="0">Select Existing Agent</option>';
		
		$r = @mysqli_query ($dbc, $q);
		while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
			echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
		}
		echo '</select></div></p>
		
		<input type="submit" name="submit" value="Add Existing Co-Agent" />
		<input type="hidden" name="property" value="' .  $property_id . '" />
		<input type="hidden" name="existing-submitted" value="TRUE" />
		</form>';
		echo '<br /><p>Add a New Agents</p>';
	}
	
		?>
		<form action="/add_co_agent_building" method="post" id="update_listing_info_form">
			 <p class="forms"><input type="text" name="name" size="40" placeholder="Name" /></p>
			 <p class="forms"><input type="text" name="email" size="40" placeholder="Email" /></p>
			 <p class="forms"><input type="text" name="phone" size="40" placeholder="Phone" /></p>
			 <p class="forms"><input type="text" name="brokerage" size="40" placeholder="Brokerage" /></p>
		
		<br />
		<input type="submit" name="submit" value="Add Co-Agent" />
        <input type="hidden" name="property" value="<?php echo $property_id; ?>" />
		<input type="hidden" name="submitted" value="TRUE" />
		</form>
<?php 
		echo '<form action="/add_co_agent_building" method="post" >';
		echo '<input type="hidden" name="existing_agent" value="0" />';
			
		echo '<p><input type="submit" name="submit" class="error" value="Remove Co-Agent" /></p>
		<input type="hidden" name="property" value="' .  $property_id . '" />
		<input type="hidden" name="existing-submitted" value="TRUE" />
		</form>';

	}
}
?>