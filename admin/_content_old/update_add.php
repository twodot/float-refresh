<?php

require_once('includes/config.inc.php');
require_login();
if ($user_id == $_GET['u']) {
	
	$trimmed = array_map('trim', $_GET);
	
	$street = $dbc->real_escape_string($trimmed['street']);
	$street_cont = $dbc->real_escape_string($trimmed['street_cont']);
	$apt_number = $dbc->real_escape_string($trimmed['apt_number']);
	$city = $dbc->real_escape_string($trimmed['city']);
	$state_name = $dbc->real_escape_string($trimmed['state']);
	$zip = $dbc->real_escape_string($trimmed['zip']);
	
	$table = $dbc->real_escape_string($trimmed['t']);
	
	$q = "
		UPDATE 
		$table 
		
		set 
		street = '$street', 
		street_cont = '$street_cont', 
		apt_number = '$apt_number', 
		city = '$city', 
		state_name = '$state_name', 
		zip = '$zip'
		
		WHERE 
		user_id = $user_id
		
		";
	$r = @mysqli_query ($dbc, $q);
	
	if (mysqli_affected_rows($dbc) ==1){
		
		if ($table == 'shipping'){
			echo '$("#current_ship_add").html("<p>' . $street . ' ' .$street_cont  . ' ' .$apt_number . ' ' . $city  . ', ' . $state_name . ' ' .$zip . '</p>").fadeIn(300);';
			echo '$("#edit_ship").slideUp(500);';
		}
		if($table == 'billing') {
			echo '$("#current_bill_add").html("<p>' . $street . ' ' .$street_cont  . ' ' .$apt_number . ' ' . $city  . ', ' . $state_name . ' ' .$zip . '</p>").fadeIn(300);';
			echo '$("#edit_bill").slideUp(500);';
		}
		
	}// END AFFECTED ROWS CHECK 
	else {
		
		if ($table == 'shipping'){
			
			$r = get_shipping($user_id, $dbc);
			while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
				echo '$("#current_ship_add").html(<p>' . $row['street'] . ' ' .$row['street_cont']  . ' ' .$row['apt_number'] . ' ' .$row['city']  . ', ' .$row['state'] . ' ' .$row['zip'] . '</p>).fadeIn(300);';
				echo '$("#edit_ship").slideUp(500);';
			}
		}
		if($table == 'billing') {
			$r = get_billing($user_id, $dbc);
			while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
				echo '$("#current_bill_add").html(<p>' . $row['street'] . ' ' .$row['street_cont']  . ' ' .$row['apt_number'] . ' ' .$row['city']  . ', ' .$row['state'] . ' ' .$row['zip'] . '</p>).fadeIn(300);';
				echo '$("#edit_bill").slideUp(500);';
			}
		}
		echo '$("#error").html("<p>There was an error, the address could not be updated!</p>").fadeIn(300);';
		echo '$("#error").delay(2000).fadeOut(1000);';
		
	}
} // END GET_[USER] == $USER_ID

?>

