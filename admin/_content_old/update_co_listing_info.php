<?php

require_once('includes/config.inc.php');
require_login();
if (isset($_POST['submitted'])){
	
	$em = FALSE;
	$trimmed = array_map('trim', $_POST);
	
	$listing_name = $dbc->real_escape_string($trimmed['name']);
	
	if (preg_match ('/^[\w.-]+@[\w.-]+\.[A-Za-z]{2,6}$/', $trimmed['email'])) {
		$em = $dbc->real_escape_string($trimmed['email']);
	}
	else {
		$e = 5;
		$em = '';
	} 
	
	$listing_phone = $dbc->real_escape_string($trimmed['phone']);
	
	$listing_brokerage = $dbc->real_escape_string($trimmed['brokerage']);
	$listing_brokerage = htmlspecialchars ($listing_brokerage, ENT_QUOTES);
	
			
	$q = "UPDATE 
	users 
	
	SET 
	listing_name = '$listing_name', 
	listing_email = '$em', 
	listing_phone = '$listing_phone', 
	listing_brokerage = '$listing_brokerage' 
	
	WHERE 
	user_id=$user_id 
	
	LIMIT 1";
	
	$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
	
	mysqli_close($dbc);

	$url = BASE_URL . 'account?pc=1';
	if (isset($e)) {
		$url .= '&e=' . $e;
	}
	else if (isset($s)) {
		$url .= '&sv=1&s=' . $s;
	}

	ob_end_clean();

	header("Location: $url");

}

$q = "SELECT
	
	listing_name, 
	listing_email, 
	listing_phone, 
	listing_brokerage 
	
	FROM 
	users 
	
	WHERE 
	user_id=$user_id 
	";
	$r = @mysqli_query ($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
?>
<form action="/update_listing_info" method="post" id="update_listing_info_form">
     <p class="forms"><input type="text" name="name" size="40" <?php if(!empty($row['listing_name'])) {echo 'value="' . $row['listing_name'] . '"'; } ?> placeholder="Name" /></p>
     <p class="forms"><input type="text" name="email" size="40" <?php if(!empty($row['listing_email'])) {echo 'value="' . $row['listing_email'] . '"'; } ?> placeholder="Email" /></p>
     <p class="forms"><input type="text" name="phone" size="40" <?php if(!empty($row['listing_phone'])) {echo 'value="' . $row['listing_phone'] . '"'; } ?> placeholder="Phone" /></p>
     <p class="forms"><input type="text" name="brokerage" size="40" <?php if(!empty($row['listing_brokerage'])) {echo 'value="' . $row['listing_brokerage'] . '"'; } ?> placeholder="Brokerage" /></p>

<br />
<input type="submit" name="submit" value="Change my listing info" />
<input type="hidden" name="submitted" value="TRUE" />
</form>
