<?php

require_login();
$page_title = $page_name = 'Select a Subscription';

include('includes/header.php');


// SELECT FOR PROPERTY PRIMARY INFO

require_once('./lib/Stripe.php');
Stripe::setApiKey(STRIPE_SECRET);

$subs = Stripe_Plan::all();
$get_subs = $subs['data'];
$stripe_plans = array();

foreach ($get_subs as $key ) {
	
	$id =  $key['id'];
	$amount = $key['amount'];
	$trial = $key['trial_period_days'];
	$interval = $key['interval'];
	
	$stripe_plans[$id] = array('id' => "$id", 'amount' => "$amount", 'trial' => "$trial", 'interval' => "$interval");
	
	$q= "UPDATE subscription_types SET stripe_trial = $trial, stripe_amount = $amount, stripe_interval = '$interval' WHERE stripe_id = $id";
	$r = @mysqli_query ($dbc, $q);
}

?>

<div id="error">
</div>

<?php
	$q ="SELECT COUNT(*) as subscription, subscribe_level FROM subscription WHERE user_id = $user_id";
	$r = @mysqli_query ($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	if($row['subscription'] == 1) {
	$subscribe_level = $row['subscribe_level'];
	$q =" SELECT * FROM subscription_types WHERE free !=1 AND deactivated !=1 ORDER BY stripe_id ASC";
	$r = @mysqli_query ($dbc, $q);
	
	
	
	
	
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		$stripe_id = $row['stripe_id'];
		$the_current_subscription = 0;
		if($subscribe_level == $stripe_id) {
			$the_current_subscription = 1;	
		}
		$plans[] =  array('stripe_id' => $stripe_id, 'name' => $row['name'], 'rental' =>$row['rental'], 'price' => $stripe_plans[$stripe_id]['amount'] / 100, 'interval'=>$stripe_plans[$stripe_id]['interval'], 'alias' => $row['alias'], 'trial' =>$row['stripe_trial'], 'total_properties' => $row['total_properties'], 'current_subscription' => $the_current_subscription);
	}
	echo '<ul class="subscription_table">';
		echo '<li></li>';
			foreach ($plans as $plan) {
				echo '<li';
					if($plan['rental'] == 1) {
						echo ' class="property_management_subscription"';
					}
					else {
						echo ' class="header_subscription"';
					}
				echo '>';
					echo '<p class="plan_alias">' . $plan['alias'] . '</p>';
					echo '<h3>';
					if($plan['rental'] == 1) {
						echo 'Property Managers';
					}
					else {
						echo 'Real Estate Agents';
					}
					echo '</h3>';
					
					echo '<h1>';
						echo '$' . $plan['price'];
					echo '</h1>';
					echo '<p>';
						echo 'Per ' . ucwords($plan['interval']);
					echo '</p>';
				echo '</li>';
			}
		echo '</ul>';
		echo '<ul class="subscription_table feature_line_subscription">';
			echo '<li class="feature_subscription"><p>30 Day Free Trial</p></li>';
			foreach ($plans as $plan) {
				echo '<li>';
				
					if($plan['trial']) {
						echo '<img src="https://s3-us-west-2.amazonaws.com/images.listfloat.com/assets/check_mark.png" alt="Check Mark" />';	
					}
					else {
						echo '<img src="https://s3-us-west-2.amazonaws.com/images.listfloat.com/assets/cross_mark.png" alt="Cross Mark" />';
					}
				echo '</li>';
			}
		
	echo '</ul>';
	
	echo '<ul class="subscription_table feature_line_subscription">';
			echo '<li class="feature_subscription"><p>Active Properties</p></li>';
			foreach ($plans as $plan) {
				echo '<li>';
						if($plan['total_properties'] == 0 ) {
							echo '<p>Unlimited</p>';
						}
						else {
							echo '<p>' . $plan['total_properties'] . '</p>';
						}
					
				echo '</li>';
			}
		
	echo '</ul>';
	
	echo '<ul class="subscription_table feature_line_subscription">';
			echo '<li class="feature_subscription"><p>Beacon Capacity</p></li>';
			foreach ($plans as $plan) {
				echo '<li>';
					echo '<p>Unlimited</p>';
						
				echo '</li>';
			}
	echo '</ul>';
	
	echo '<ul class="subscription_table choose_subscription_section">';
			echo '<li class="choose_subscription_li"></li>';
			foreach ($plans as $plan) {
				echo '<li>';
					if($plan['current_subscription'] == 1 ) {
						echo '<button class="current_sub">Current</button>';
					}
					else {
						echo '<form action="/update_subscription"><input type="hidden" name="s" value="' . $plan['stripe_id'] . '"/><button type="submit" class="select_sub">Choose</button></form>';
					}
						
				echo '</li>';
			}
	echo '</ul>';
	echo '<script>';
	foreach ($plans as $plan) {
			$stripe_id = $plan['stripe_id'];
		?>
        	
            document.getElementById('select_sub<?php echo $stripe_id; ?>').addEventListener('click', function(e) {
            
            subscription = <?php echo $stripe_id; ?>
            
            
            handler.open({
              description: '<?php if ($stripe_plans[$stripe_id]['id'] == 1) { echo 'Unlimited'; } else { echo $plan['total_properties']; } echo ' Properties | $' . $stripe_plans[$stripe_id]['amount'] / 100 . ' / ' . $stripe_plans[$stripe_id]['interval']; ?>',
              amount: <?php echo $stripe_plans[$stripe_id]['amount']; ?>, 
              shippingAddress: true, 
              billingAddress: true, 
              closed: function() {$('.select_sub').text('Choose')}
             
            });
            e.preventDefault();
            });
            			
		<?php
	}
	echo '</script>';
	
	
	
	
	/* while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {$stripe_id = $row['stripe_id'];
		if($row['rental'] == 1) {
		echo '<div id="subscription_' . $row['id'] . '" class="subscription_levels'; if ($subscribe_level == $stripe_id){echo ' current_subscription';} echo '">';
				
				
				echo '<div class="sub_headline">';
					echo '<h4>Property Management <br />' . $row['name'] . '</h4>';
				echo '</div>'; // END SUB HEADLINE
				echo '<div class="sub_info">';
					if ($subscribe_level == $stripe_id){echo '<p>Current Subscription</p>';}
					echo '<p>$' . $stripe_plans[$stripe_id]['amount'] / 100 . ' / ' . $stripe_plans[$stripe_id]['interval'] . ' </p>';
					
					//echo '<div id="select_sub_' . $row['id'] . '" class="choose">CHOOSE</div>';
				echo '</div>'; // END SUB INFO
				if ($subscribe_level != $stripe_id){echo '<form action="/update_subscription"><input type="hidden" name="s" value="' . $stripe_id . '"/><button type="submit" class="select_sub">CHOOSE</button></form>';}
				?>
				<script>
				  document.getElementById('select_sub<?php echo $stripe_id; ?>').addEventListener('click', function(e) {
					
					subscription = <?php echo $stripe_id; ?>
					
					
					handler.open({
					  description: '<?php echo $stripe_plans[$stripe_id]['id'] . ' Active Properties | $' . $stripe_plans[$stripe_id]['amount'] / 100 . ' / ' . $stripe_plans[$stripe_id]['interval']; ?>',
					  amount: <?php echo $stripe_plans[$stripe_id]['amount']; ?>
					 
					});
					e.preventDefault();
				  });
				</script>
				
				<?php
		echo '</div>'; // END SUBSCRIPTION
		} //END RENTAL CHECK
	}
	$r = @mysqli_query ($dbc, $q);
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {$stripe_id = $row['stripe_id'];
		if($row['rental'] != 1) {
		echo '<div id="subscription_' . $row['id'] . '" class="subscription_levels'; if ($subscribe_level == $stripe_id){echo ' current_subscription';} echo '">';
				
				
				echo '<div class="sub_headline">';
					echo '<h4>' . $row['name'] . '</h4>';
				echo '</div>'; // END SUB HEADLINE
				echo '<div class="sub_info">';
					if ($subscribe_level == $stripe_id){echo '<p>Current Subscription</p>';}
					echo '<p>$' . $stripe_plans[$stripe_id]['amount'] / 100 . ' / ' . $stripe_plans[$stripe_id]['interval'] . ' </p>';
					
					//echo '<div id="select_sub_' . $row['id'] . '" class="choose">CHOOSE</div>';
				echo '</div>'; // END SUB INFO
				if ($subscribe_level != $stripe_id){echo '<form action="/update_subscription"><input type="hidden" name="s" value="' . $stripe_id . '"/><button type="submit" class="select_sub">CHOOSE</button></form>';}
				?>
				<script>
				  document.getElementById('select_sub<?php echo $stripe_id; ?>').addEventListener('click', function(e) {
					
					subscription = <?php echo $stripe_id; ?>
					
					
					handler.open({
					  description: '<?php echo $stripe_plans[$stripe_id]['id'] . ' Active Properties | $' . $stripe_plans[$stripe_id]['amount'] / 100 . ' / ' . $stripe_plans[$stripe_id]['interval']; ?>',
					  amount: <?php echo $stripe_plans[$stripe_id]['amount']; ?>
					 
					});
					e.preventDefault();
				  });
				</script>
				
				<?php
		echo '</div>'; // END SUBSCRIPTION
		} //END RENTAL CHECK
	} */
	
	
	}
	else {
		echo '<p>You have reached this page in error</p>';
	}

include('includes/footer.php');



?>