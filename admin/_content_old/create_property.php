<?php

require_once('includes/config.inc.php');
include('includes/bitly.php');
require_login();
$page_title = $page_name = 'Add Property';

include('includes/header.php');

?>
<div id="error">
</div>

<div id="dialog">
</div>

<?php
if(isset($_GET['p']) || isset($p)) {
	if(isset($_GET['p']) ) {
		$property = (int) $_GET['p'];
	}
	else if(isset($p)) {
		$property = (int) $p;
	}
	if($property != 0) {
		
		
		$arr = get_property($user_id, $dbc, $property);
		
		$r= $arr['r'];
		if(mysqli_num_rows($r) == 0) {
			header('Location: /panel/');
		}
		$status= $arr['status'];
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		echo '<!-- '; 
		//print_r($row); 
		echo ' -->';
		$primary_info_id = $row['info_id'];
		$property = $row['property_id'];
		$progress = $row['progress']; 
		$completed = $row['completed']; 
		$total_media = $row['total_media'];
		$address = $row['address'];
		$latitude = $row['latitude'] + 0;
		$longitude = $row['longitude'] + 0;
		$marketing_description = nl2br($row['description']);
		$sales_status = $row['sold_status'];
		$status_message = $row['status_message'];
		$property_title = $row['property_title'];
		$beds = $row['beds'];
		$bath = $row['bath'] + 0;
		$sqft = $row['sqft'];
		$lot_size = $row['lot_size'];
		$property_type = $row['property_type'];
		$style = $row['style'];
		$year_built = $row['year_built'];
		$community = $row['community'];
		$county = $row['county'];
		$mls = $row['mls'];
		$walkscore = $row['walkscore']; 
		$show_walkscore = $row['show_walkscore'];
		$show_greatschools = $row['show_greatschools']; 
		$active = $row['active']; 
		$walkscore = $row['walkscore'] +0;  
		$elm = $row['gs_elm'];
		$jh = $row['gs_jh'];
		$hs = $row['gs_hs'];
		$flyer_id = $row['flyer_id'];
		$flyer_name = $row['flyer_name'];
		$bitly_hash = $row['short_url_hash'];
		$collapse_details = $row['collapse_details'];
		$collapse_schools = $row['collapse_schools'];
		$facebook_share = $row['facebook_share'];
		$twitter_share = $row['twitter_share'];
		$main_beacon_title = $row['beacon_title'];
		$main_beacon_id = $row['beacon_id'];
		
		
		$crm_id = $row['crm'];
		$crm_field_1_value = $row['crm_field_1_value'];
		$crm_field_2_value = $row['crm_field_2_value'];
		$crm_field_3_value = $row['crm_field_3_value'];
		$crm_field_4_value = $row['crm_field_4_value'];
		$crm_field_5_value = $row['crm_field_5_value'];
		$crm_field_6_value = $row['crm_field_6_value'];
		
		
		$fb_bitly = BITLY_BASE_URL . 	$facebook_share;
		$tw_bitly = BITLY_BASE_URL . 	$twitter_share;
		
		$facebook_link = $fb_bitly;
		$twitter_link = $tw_bitly;
		
		$property_title = $row['property_title'];
		if(!empty($bitly_hash)) {
			$short_url = BITLY_BASE_URL . 	$bitly_hash;
		}
		
		if (!empty($row['price'])) {
			$price = (int) $row['price'];
			$price = str_replace('$', '', $price);
			$price = str_replace(',', '', $price);
			$price = '$' . number_format($price);	
		}
		else {
			$price = '';
		}
		
		if ($latitude != 0.0000000 && $longitude != 0.0000000) {
			// MAP SECTION 
			?>
			
			<script type="text/javascript">
			  function initialize() {
				  var propertyLatLong = new google.maps.LatLng(<?php echo $latitude . ',' . $longitude; ?>);
				  var mapOptions = {
					zoom: 18,
					center: propertyLatLong, 
					//mapTypeId: google.maps.MapTypeId.HYBRID
				  }
				  var map = new google.maps.Map(document.getElementById('property-map'), mapOptions);
					
   				  var image = 'https://s3-us-west-2.amazonaws.com/images.listfloat.com/assets/float_map_dot.png';
				  var marker = new google.maps.Marker({
					  position: propertyLatLong,
					  map: map,
					  title: 'Float Property', 
					  icon: image
				  });
				}

				
				function loadMap() {
				  var script = document.createElement('script');
				  script.type = 'text/javascript';
				  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAiAePyQf5emFwHaIQlsvwFGX0VXmvEiNg&callback=initialize';
				  document.body.appendChild(script);
				}
				
				$(document).ready(function() {
					$(".fancybox").fancybox();
				});
				
				
				
				
				
			</script>
            <?php 
		}
		
		echo '<div id="delete_property_holder"><a href="/delete_property?p=' . $row['property_id'] . '"  class="delete_property_button">Delete Property</a></div>';
		
		$q_leads = "
		SELECT 
		leads.id as leads_id, 
		leads.first_name, 
		leads.last_name, 
		leads.email, 
		leads.phone, 
		
		count(lead_message.id) as leads_count, 
		lead_message.id as message_id, 
		lead_message.lead_id, 
		lead_message.property_id, 
		lead_message.message 
		
		FROM 
		leads, 
		lead_message 
		
		WHERE 
		lead_message.property_id = $property
		AND lead_message.lead_id = leads.id";
		
		$r_leads = mysqli_query($dbc, $q_leads);
		$row_leads = mysqli_fetch_array($r_leads, MYSQLI_ASSOC);
		$leads_count = $row_leads['leads_count'];
		if($leads_count > 0) {
			// echo '<div id="leads_flag"><a href="/show_leads/' . $property . '" >' . $leads_count . ' Lead'; if($leads_count >1) {echo's';} echo'</a></div>';
		}
		include('_content/add_property/add_new_property.php');
	} // END IF PROPERTY != 0
} // END IF GET P EXISTS

require_once('js/global_ui.php');
?>

