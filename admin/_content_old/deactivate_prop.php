<?php

require_once('includes/config.inc.php');
require_login();
if ($user_id == $_GET['u']) {
	
		if(isset($_GET)) {
			$trimmed = array_map('trim', $_GET);
			
			$property = $dbc->real_escape_string(str_replace('b_con_', '', $trimmed['p']));
			
			$property = $dbc->real_escape_string(str_replace('prop_', '', $property));
			
			$property = (int) $property;
				$q = "
					UPDATE 
					properties 
					
					SET 
					active = 0 
					
					WHERE 
					id = $property 
					
					";
				
				$r = @mysqli_query ($dbc, $q);		
		
				if (mysqli_affected_rows($dbc) ==1){
					
					$q = "SELECT * FROM properties WHERE id = $property";
					$r = @mysqli_query ($dbc, $q);
					$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
					$title= $row['title'];
					$price= $row['price'];
					$price = str_replace('$', '', $price);
					$price = str_replace(',', '', $price);
					$price = (int) $price;
					if (!empty($price)) {
						$price = '$' . number_format($price);	
					}
					else {
						$price = '';
					}
					$q = "SELECT 
						property_info.beacon_id, 
						beacons.title as title, 
						beacons.id, 
						COUNT(beacons.id) as total_beacons, 
						properties.id 
						
						FROM 
						property_info, 
						beacons, 
						properties  
						
						WHERE 
						property_info.beacon_id = beacons.id 
						AND property_info.primary_info = 1 
						AND beacons.user_id = $user_id 
						AND beacons.property_info_id = property_info.id 
						AND properties.id = $property 
						AND property_info.properties_id = $property 
						
						
						";
					
					$r = @mysqli_query ($dbc, $q);
					$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
					
					$beacon_title = $row['title'];
					$total_beacons = $row['total_beacons'];
					
					?> 
                    
						$("#unassigned_properties .holder").prepend('<div class="droppable inactive_prop" id="b_con_<?php echo $property; ?>"><a href="/create_property/<?php echo $property; ?>"><?php echo $title; ?> <?php echo $price; ?></a><a href="/delete_property?p=<?php echo $property; ?>"  class="delete_property_button"><img src="<?php echo IMAGES; ?>delete.png" alt="Delete info" title="Delete <?php echo $title; ?>" class="tooltip" /></a><div class="clear"></div><div class="beacon_prop">Primary Beacon<br />Beacon <?php echo $beacon_title; ?> </div><div class="beacon_prop">Total Beacons<br /><?php echo $total_beacons; ?> beacon<?php  if ($total_beacons > 1) { echo 's';}?></div><div class="clear"></div></div>').fadeIn(300); 
                         $("#b_con_<?php echo $property; ?>").remove();
						
                        inactive_drag();
                        active_drag();
					
					<?php
				
				
				}
				else {
					echo '$("#error").html("<p>There was an error, the property could not be activated!</p>").fadeIn(300);';
					echo '$("#error").delay(2000).fadeOut(1000);';
				}
			
				
			
		} // END GET CHECK
} // END GET_[USER] == $USER_ID

?>

