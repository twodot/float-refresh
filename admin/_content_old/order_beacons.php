<?php

require_once('includes/config.inc.php');

$UserInfo = new user($user_id, $dbc);

include('includes/func.taxcloud.php');
require_once('USPS/USPSRate.php');
require_login();
if (isset($user_id)) {
	$q =" SELECT *, TIMESTAMPDIFF(DAY, NOW(), current_period_end) + 1 as days_left, TIMESTAMPDIFF(DAY, current_period_start, current_period_end) as days FROM subscription, subscription_types WHERE subscription.user_id = $user_id AND subscription.subscribe_level = subscription_types.stripe_id";
	$r = @mysqli_query ($dbc, $q);
	
	if( mysqli_num_rows($r) == 1) {
			
		if(isset($_SESSION['cart'])) {			
			
			$zip = $UserInfo->ship_zip; // DEST ZIP FOR SHIPPING COST
			
			$subTotal = 0;
			$shippingCost = 0;
			foreach($_SESSION['cart'] as $sku => $quantity) {
				if(is_int($sku)) { 
						$q_price = "SELECT title, price, tic FROM products WHERE sku = '$sku'";
						$r_price = mysqli_query($dbc, $q_price);
						$row_price = mysqli_fetch_array($r_price, MYSQLI_ASSOC);
						$price = $row_price['price'];
						$tic = $row_price['tic'];
						$qty = $quantity['quantity'];
						$product_title = $row_price['title'];
						$theSku = $sku;
						$q_pack_weight = "SELECT * FROM packages WHERE sku = $sku AND qty = $qty";
						$r_pack_weight = mysqli_query($dbc, $q_pack_weight);
						$row_pack_weight = mysqli_fetch_array($r_pack_weight, MYSQLI_ASSOC);
						$lbs = $row_pack_weight['lb'];
						$oz = $row_pack_weight['oz'];
						
						$rate = new USPSRate(USPS_USER);
						
						$package = new USPSRatePackage;
						$package->setService(USPSRatePackage::SERVICE_PRIORITY);
						$package->setFirstClassMailType(USPSRatePackage::MAIL_TYPE_PARCEL);
						$package->setZipOrigination(OUR_ZIP);
						$package->setZipDestination($zip);
						$package->setPounds($lbs);
						$package->setOunces($oz);
						$package->setContainer('');
						$package->setSize(USPSRatePackage::SIZE_REGULAR);
						$package->setField('Machinable', true);
						
						// add the package to the rate stack
						$rate->addPackage($package);
						
						// Perform the request and print out the result
						$rate->getRate();
						$theRate = $rate->getArrayResponse();
						
						// Was the call successful
						if($rate->isSuccess()) {
						  $shippingCost = $shippingCost + $theRate['RateV4Response']['Package']['Postage']['Rate'];
						}
						$subTotal = $subTotal + ($price * $qty);
					
				}
			}
			
			$taxArr = get_cart_tax($user_id, $dbc, $shippingCost, $UserInfo);
			$salesTax = round ($taxArr['tax'], 2);
			$salesTax = number_format($salesTax, 2);
			$subTotal = number_format($subTotal, 2);
			$total = $subTotal + $shippingCost + $salesTax;
			
			
			Stripe::setApiKey(STRIPE_SECRET);
			$q_s_id ="
			SELECT 
			*,  
			users.user_id as user_id, 
			shipping.user_id, 
			
			shipping.street as ship_street, 
			shipping.city as ship_city, 
			shipping.state_name as ship_state, 
			shipping.zip as ship_zip, 

			billing.street as bill_street, 
			billing.city as bill_city, 
			billing.state_name as bill_state, 
			billing.zip as bill_zip

			
			FROM 
			users as users, 
			cards as cards
			
			LEFT JOIN shipping ON shipping.user_id = $user_id 
			LEFT JOIN billing ON billing.user_id = $user_id 
			
			WHERE 
			users.user_id = $user_id 
			AND cards.user_id = $user_id";
			$r_s_id = @mysqli_query ($dbc, $q_s_id);
			$row_s_id = mysqli_fetch_array($r_s_id, MYSQLI_ASSOC);
			$stripeTotal = $total*100;
			$user_email = $row_s_id['email'];
			$first_name = $row_s_id['first_name'];
			$last_name = $row_s_id['last_name'];
			echo $stripe_customer = $row_s_id['stripe_id'];
			$card_type = $row_s_id['card_type'];
			$card_four = $row_s_id['card'];
			
			$shipping_address = $row_s_id['ship_street'] . '<br />' . $row_s_id['ship_city'] . ', ' . $row_s_id['ship_state'] . ', ' . $row_s_id['ship_zip'];
			
			$billing_address = $row_s_id['bill_street'] . '<br />' . $row_s_id['bill_city'] . ', ' . $row_s_id['bill_state'] . ', ' . $row_s_id['bill_zip'];
			
			date_default_timezone_set("UTC"); 
			
			$cu = Stripe_Customer::retrieve("$stripe_customer");
			
			$q_card ="SELECT stripe_card_id FROM cards where user_id = $user_id";
			$r_card = @mysqli_query ($dbc, $q_card);
			$row_card = mysqli_fetch_array($r_card, MYSQLI_ASSOC);
			
			$card = $cu->cards->retrieve($row_card['stripe_card_id']);
			
			
			try {
				$charge = Stripe_Charge::create(array(
				  "amount" => $stripeTotal, 
				  "currency" => "usd",
				  "customer" => $stripe_customer,
				  "description" => "Purchase of $qty Beacons"
				  )
				);
				
				$charge_id = $charge->id;
				
				$q_beacons = "INSERT INTO beacon_orders SET user_id = '$user_id', beacons = '$qty', qty = '$qty', sku = '$theSku', price = '$price', total = '$stripeTotal', sub_total = '$subTotal', shipping = '$shippingCost', tax = $salesTax, charge_id = '$charge_id'";
				$r_beacons = @mysqli_query ($dbc, $q_beacons);
				
				$order_id = 'beacons_' . mysqli_insert_id($dbc);
			
				$cartId = $_SESSION['cart']['id'];
				
				$errMsg = array();
				$commit = func_authorized_with_capture_taxcloud($user_id, $cartId, $order_id, $errMsg);
				
				echo '$("#view_cart_message").html("<p>Thank you for your order. You will receive an order confirmation by email shortly.</p><p>To view your order, please visit the transactions list under My Account.</p>").fadeIn(300);';
				echo '$("#view_cart_message").addClass("error");';	
				
				unset($_SESSION['cart']);
				
				
				$HTMLEmailoutput = $row_s_id['email'] . ' has ordered ' . $qty . ' beacons';
				
				include(MAIL_SALES);

				$mail_sales->FromName = 'Order From Float';
				$mail_sales->addAddress(SALES);     // Add a recipient
				$mail_sales->addReplyTo('orders@' . EMAIL_URL, 'Order From Float');
				
				$mail_sales->WordWrap = 50;                                 // Set word wrap to 50 characters
				$mail_sales->isHTML(true);                                  // Set email format to HTML
				
				$mail_sales->Subject = 'A New order of '. $qty . ' beacons has been submitted';
				
				$mail_sales->Body    = $HTMLEmailoutput;

				if (!$mail_sales->send()) {
					
					
					unset($mail_sales);
				
					include(MAIL_SALES);
					
					$mail_sales->FromName = 'Order From Float';
					$mail_sales->addAddress(SALES);     // Add a recipient
					$mail_sales->addReplyTo('orders@' . EMAIL_URL, 'Order From Float');
					
					$mail_sales->WordWrap = 50;                                 // Set word wrap to 50 characters
					$mail_sales->isHTML(true);                                  // Set email format to HTML
					
					$mail_sales->Subject = 'Your order of '. $qty . ' beacons has been received';
					ob_clean();
					
					include('email_beacons_ordered.php');
					$HTMLEmailoutput = ob_get_contents();
					ob_clean();
					$mail_sales->Body    = $HTMLEmailoutput;
					$mail_sales->send();
					
					
					
				}
				else {
					
					unset($mail_sales);
				
					include(MAIL_SALES);
	
					$mail_sales->FromName = 'Order From Float';
					$mail_sales->addAddress($user_email);     // Add a recipient
					$mail_sales->addReplyTo('orders@' . EMAIL_URL, 'Order From Float');
					
					$mail_sales->WordWrap = 50;                                 // Set word wrap to 50 characters
					$mail_sales->isHTML(true);                                  // Set email format to HTML
					
					$mail_sales->Subject = 'Your order of '. $qty . ' beacons has been received';
					ob_clean();
					
					// include('email_beacons_ordered.php');
					include('email_templates/order_conf.php');
					$HTMLEmailoutput = ob_get_contents();
					ob_clean();
					
					$mail_sales->Body    = $HTMLEmailoutput;
					$mail_sales->send();
					
					
					echo '$("#view_cart_message").html("<p>Thank you for your order. You will receive an order confirmation by email shortly.</p><p>To view your order, please visit the transactions list under My Account.</p>").fadeIn(300);';
					echo '$("#view_cart_message").addClass("error");';	
				} 
				
			
			} catch(Stripe_CardError $e) {
				echo '$("#view_cart_message").html("<p>Your order could not be processed.</p>").fadeIn(300);';
					echo '$("#view_cart_message").addClass("error");';		
			}
		}
		
	}
	else {
		echo '$("#view_cart_message").html("<p>You havent chosen a subscription.</p>").fadeIn(300);';
		echo '$("#view_cart_message").addClass("error");';		
	}
	
} // END USER ID SET VERIFICATION

?>

