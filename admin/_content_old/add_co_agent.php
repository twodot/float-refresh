<?php

require_once('includes/config.inc.php');
require_login();
if (isset($_GET['existing-submitted'])) {
	$co_agent_id = (int) $_GET['existing_agent'];
	$property = (int) $_GET['property'];
	$q= "UPDATE 
	properties 
	
	SET 
	co_agent = $co_agent_id 
	
	WHERE 
	id = $property 
	AND user_id = $user_id";
	$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
	
	$q_co_listing = "SELECT
				*, 
				COUNT(*) as co_listing
				
				FROM 
				co_agent  
				
				WHERE 
				user_id=$user_id 
				AND id = (SELECT co_agent FROM properties WHERE id = $property AND user_id = $user_id) 
				";
			$r_co_listing = @mysqli_query ($dbc, $q_co_listing);
			$row_co_listing = mysqli_fetch_array($r_co_listing, MYSQLI_ASSOC);
	
	?>
   	$('#co_agent_holder').fadeOut(500, function() {
    	<?php 
		if(isset($_GET["remove"])) {?>
			$('#co_agent_holder').html('');
        	$('#co_agent_holder').fadeIn(500);
            $('#co_agent_form').slideUp(1000);
            $('#add_co_agent_show').fadeIn(500);
		<?php 
		}else {
		?>
    		$('#co_agent_holder').html('<h4>Co Agent:</h4><p><?php echo $row_co_listing['name']; ?><br /><?php echo $row_co_listing['email']; ?><br /><?php echo $row_co_listing['phone']; ?> <br /><?php echo $row_co_listing['brokerage']; ?></p><p><a href="edit_co_agent?ca=<?php echo $row_co_listing['id']; ?>&p=<?php echo $property; ?>" id="edit_co_agent">Edit this agents info</a></p><div id="update_co_agent_info_form_holder"><form action="/edit_co_agent" id="update_co_agent_info_form"><p class="forms"><input type="text" name="name" size="40" value="<?php echo $row_co_listing['name']; ?>" placeholder="Name" /></p><p class="forms"><input type="text" name="email" size="40" value="<?php echo $row_co_listing['email']; ?>" placeholder="Email" /></p><p class="forms"><input type="text" name="phone" size="40" value="<?php echo $row_co_listing['phone']; ?>" placeholder="Phone" /></p><p class="forms"><input type="text" name="brokerage" size="40" value="<?php echo $row_co_listing['brokerage']; ?>" placeholder="Brokerage" /></p><br /><input type="submit" name="submit" value="Save <?php echo $row_co_listing['name']; ?>&rsquo;s Info" id="save_co_agent_info_form"/><button id="cancel_co_agent_info_form">Cancel</button><input type="hidden" name="agent" value="<?php echo $row_co_listing['id']; ?>" /><input type="hidden" name="property" value="<?php echo $property; ?>" /><input type="hidden" name="submitted" value="TRUE" /></form></div><p><a href="add_co_agent?p=<?php echo $property; ?>" id="change_co_agent_button">Change the co-listed agent</a></p><form action="/add_co_agent" method="post" id="remove_co_agent_form" ><input type="hidden" name="existing_agent" value="0" /><p><input type="submit" name="submit" class="error" value="Remove Co-Agent" id="remove_co_agent" /></p><input type="hidden" name="remove" value="TRUE" /><input type="hidden" name="property" value="<?php echo $property; ?>" /><input type="hidden" name="existing-submitted" value="TRUE" /></form>');
        	$('#co_agent_holder').fadeIn(500);
            $('#co_agent_form').slideUp(1000);
            $('#add_co_agent_show').fadeOut(500);
        <?php }?>
    })
    <?php
}
else if (isset($_GET['submitted'])){
	$property = (int) $_GET['property'];
	$em = FALSE;
	$trimmed = array_map('trim', $_GET);
	
	$listing_name = $dbc->real_escape_string($trimmed['name']);
	
	if (preg_match ('/^[\w.-]+@[\w.-]+\.[A-Za-z]{2,6}$/', $trimmed['email'])) {
		$em = $dbc->real_escape_string($trimmed['email']);
	}
	else {
		$e = 5;
		$em = '';
	} 
	
	$listing_phone = $dbc->real_escape_string($trimmed['phone']);
	
	$listing_brokerage = $dbc->real_escape_string($trimmed['brokerage']);
	$listing_brokerage = htmlspecialchars ($listing_brokerage, ENT_QUOTES);
	$prop = $_GET['property'];
	$q = "SELECT * FROM co_agent WHERE name = '$listing_name' AND email = '$em'";
	$r = mysqli_query($dbc, $q);
	if(mysqli_num_rows($r) == 0) {
		$q = "INSERT INTO 
		co_agent
		
		SET 
		name = '$listing_name', 
		email = '$em', 
		phone = '$listing_phone', 
		brokerage = '$listing_brokerage', 
		user_id = $user_id
		";
		if (!empty($listing_name) && !empty($em) && !empty($listing_phone) && !empty($listing_brokerage)) {
			$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
			$insert_id = mysqli_insert_id($dbc);
			
			$q= "UPDATE 
			properties 
			
			SET 
			co_agent = $insert_id 
			
			WHERE 
			id = $prop 
			AND user_id = $user_id";
			$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
			
			?>
			$('#co_agent_holder').fadeOut(500, function() {
				$('#co_agent_holder').html('<h4>Co Agent:</h4><p><?php echo $listing_name; ?><br /><?php echo $em; ?><br /><?php echo $listing_phone; ?> <br /><?php echo $listing_brokerage; ?></p><p><a href="edit_co_agent?ca=<?php echo $insert_id; ?>&p=<?php echo $prop; ?>" id="edit_co_agent">Edit this agents info</a></p><div id="update_co_agent_info_form_holder"><form action="/edit_co_agent" id="update_co_agent_info_form"><p class="forms"><input type="text" name="name" size="40" value="<?php echo $listing_name; ?>" placeholder="Name" /></p><p class="forms"><input type="text" name="email" size="40" value="<?php echo $em; ?>" placeholder="Email" /></p><p class="forms"><input type="text" name="phone" size="40" value="<?php echo $listing_phone; ?>" placeholder="Phone" /></p><p class="forms"><input type="text" name="brokerage" size="40" value="<?php echo $listing_brokerage; ?>" placeholder="Brokerage" /></p><br /><input type="submit" name="submit" value="Save <?php echo $listing_name; ?>&rsquo;s Info" id="save_co_agent_info_form"/><button id="cancel_co_agent_info_form">Cancel</button><input type="hidden" name="agent" value="<?php echo $insert_id; ?>" /><input type="hidden" name="property" value="<?php echo $property; ?>" /><input type="hidden" name="submitted" value="TRUE" /></form></div><p><a href="add_co_agent?p=<?php echo $prop; ?>" id="change_co_agent_button">Change the co-listed agent</a></p><form action="/add_co_agent" method="post" id="remove_co_agent_form" ><input type="hidden" name="existing_agent" value="0" /><p><input type="submit" name="submit" class="error" value="Remove Co-Agent" id="remove_co_agent" /></p><input type="hidden" name="remove" value="TRUE" /><input type="hidden" name="property" value="<?php echo $prop; ?>" /><input type="hidden" name="existing-submitted" value="TRUE" /></form>');
				
				$('#co_agent_holder').fadeIn(500);
				$('#co_agent_form').slideUp(1000);
				$('#add_co_agent_show').fadeOut(500);
			});
			$('#existing_agent_select').append('<option value="<?php echo $insert_id; ?>"  id="add_co_agent_select_<?php echo $insert_id; ?>"><?php echo $listing_name; ?></option>');
			
			<?php
		} // END 0 ROWS CHECK
	}
	?>
    <?php

}
?>