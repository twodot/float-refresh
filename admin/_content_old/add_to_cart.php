<?php

require_once('includes/config.inc.php');
require_login();
if (isset($user_id)) {
	 $q =" SELECT *, TIMESTAMPDIFF(DAY, NOW(), current_period_end) + 1 as days_left, TIMESTAMPDIFF(DAY, current_period_start, current_period_end) as days FROM subscription, subscription_types WHERE subscription.user_id = $user_id AND subscription.subscribe_level = subscription_types.stripe_id";
	$r = @mysqli_query ($dbc, $q);
	
	if( mysqli_num_rows($r) == 1) {
		if(isset($_GET['beacon_quantity'])) {
			$sku = $dbc->real_escape_string($_GET['product_sku']);
			if(isset($_GET['beacon_quantity'])) {
				$quantity = (int) $_GET['beacon_quantity'];
				
				session_start();
				
				if (isset($_SESSION['cart'][$sku]['quantity'])) {
					$_SESSION['cart'][$sku]['quantity'] = $_SESSION['cart'][$sku]['quantity'] + $quantity;
					ob_end_clean();
				}
				else {
					$_SESSION['cart'][$sku]['quantity'] = $quantity;
					ob_end_clean();
				}
				
				echo '$("#cart_items").html("' . $_SESSION['cart'][$sku]['quantity'] . ' Items");';
				echo '$("#view_cart").slideDown("slow");';
				
			} // END BEACON QUANTITY
		} // END SKU
	}
	else {
		echo '$("#error").html("<p>You havent chosen a subscription level!</p>").fadeIn(300);';
		echo '$("#error").delay(2000).fadeOut(1000);';	
	}
} // END USER ID SET VERIFICATION

?>

