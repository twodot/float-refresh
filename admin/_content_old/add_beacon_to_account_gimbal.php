<?php
require_admin_login();

if (isset($_GET['uid'])) {
	if(isset($_SESSION['pseudo_login']['id'])) {
		
		$customer_id = (int) $_SESSION['pseudo_login']['id'];
		$q = "SELECT id, beacons FROM beacon_orders WHERE user_id = $customer_id AND beacons != 0 LIMIT 1";
		$r = mysqli_query($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		if ($row['beacons'] != 0 ) {
			$beacons_order_id = $row['id'];
			$uid = $_GET['uid'];
			// GIMBAL SECTION //


			$data = array("factory_id" => $uid, "name" => "Float Beacon", "config_id" => "14029", "visibility" => "Public" );                                                                    
			$data_string = json_encode($data);                                                                                   
			 
			$ch = curl_init();     
			curl_setopt($ch, CURLOPT_URL,"https://manager.gimbal.com/api/beacons");             
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(  
				'AUTHORIZATION: Token token=' . GIMBAL_API,                                                                         
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data_string))                                                                       
			);                                                                                                                                                              
			 
			$result = curl_exec($ch);
			
			$beacon_info = json_decode($result);
			$factory_id = $beacon_info->factory_id;
			
			$ids = explode("-", $factory_id);
			$major = $ids[0];
			$minor = $ids[1];
			
			// GET NEXT TITLE
			$q="SELECT title FROM beacons WHERE user_id = $customer_id ORDER BY title DESC";
			$r= mysqli_query($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			$title = (int) $row['title'];
			
			$new_title = $title +1;
			// CHECK FOR MAJOR AND MINOR VAL FOR DUPLICATES
			$q = "
			SELECT * 
			
			FROM 
			
			beacons 
				
			WHERE 
			k_major = '$major' 
			AND k_minor = '$minor'";
			
			$r = mysqli_query($dbc, $q);
			
			if(mysqli_num_rows($r) == 0) {
			
				$q = "
				INSERT INTO 
				
				beacons 
				
				(
				user_id, 
				title, 
				k_major, 
				k_minor
				)
				
				VALUES 
				(
				'$customer_id', 
				'$new_title', 
				'$major', 
				'$minor'
				)";
				
				$r = mysqli_query($dbc, $q);
				
				$q = "UPDATE beacon_orders SET beacons = beacons -1 WHERE id = $beacons_order_id";
				$r = mysqli_query($dbc, $q);
				
				$q = "SELECT beacons FROM beacon_orders WHERE user_id = $customer_id AND id = $beacons_order_id";
				$r = mysqli_query($dbc, $q);
				$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
				
				?>
                
                $('#beacons_left').text('<?php echo $row['beacons']; ?>');
                $("#message").html('<div id="error">This Beacon has been added.</div>');
                $("#new_title").html('<div><?php echo $new_title; ?></div>');
                $("#error").fadeIn(1000);
                $("#error").delay(3000).fadeOut(1000);
                
                <?php
				
			} // NUM ROWS = 0 CHECK
			else {
				?>
                $("#message").html('<div id="error">This Beacon has already been assigned.</div>');
                $("#error").fadeIn(1000);
                $("#error").delay(3000).fadeOut(1000);
                <?php
			}
			
		}// END BEACONS ORDERED COUNT
		
	}// END PSEUDO LOGIN
	else {
		$url = BASE_URL . 'orders';
		header("Location: $url");
		exit();
	}
} // UID ID CHECK
else {
	$url = BASE_URL . 'panel';
	header("Location: $url");
	exit();
}

?>

