
<?php

// TAXCLOUD

include('includes/func.taxcloud.php');
require_once('USPS/USPSRate.php');
require_login();
$page_title = $page_name = 'View Cart';

if(isset($_SESSION['cart']['id'])) {
	unset($_SESSION['cart']['id']);
}
echo '<div id="dialog"></div><div id="quantity_warning" style="display:none;">The Quantity in your cart is different than the quantity you have selected. Please press "Update Cart".</div>';
include('includes/header.php');
if(isset($_POST['submitted'])) {
	foreach($_POST as $sku=>$quantity) {
		if ($sku != 'submitted') {
			$quantity = (int) $_POST[$sku];
			$_SESSION['cart'][$sku]['quantity'] = $quantity;
			
			if($_SESSION['cart'][$sku]['quantity'] == 0) {
				unset($_SESSION['cart'][$sku]);
			}
		}
	}
}
if(empty($_SESSION['cart'])) {
	unset($_SESSION['cart']);
}

?>

<script>

var handler = StripeCheckout.configure({
	key: '<?php echo STRIPE_API; ?>',
	image: '<?php echo IMAGES; ?>logo-126.jpg',
	token: function(token, args) {
		 
		 var dataString = JSON.stringify(args);
		 
		 $.ajax({
			type: 'POST',
			url: "/change_addresses", 
			data: { address: dataString } , 
			cache: false,
		}).done(function(data) {
			window.location.replace('/update_card?r=view_cart&t='+token.id);
		});
		 
		 
	}, 
	name: 'Float',
	email: '<?php echo $user_email; ?>', 
	allowRememberMe: false, 
	panelLabel: "Update Card", 
	currency: "USD", 
    shippingAddress: true, 
    billingAddress: true, 
  });
  

</script>

<style>

.ui-button:nth-child(2) {
	background: #38b449 !important;
}

</style>

<?php

if (isset($user_id)) {
	$q =" SELECT *, TIMESTAMPDIFF(DAY, NOW(), current_period_end) + 1 as days_left, TIMESTAMPDIFF(DAY, current_period_start, current_period_end) as days FROM subscription, subscription_types WHERE subscription.user_id = $user_id AND subscription.subscribe_level = subscription_types.stripe_id";
	$r = @mysqli_query ($dbc, $q);
	if( mysqli_num_rows($r) == 1) {
		if (isset($_SESSION['cart'])) {
			
			// GET ADDRESS INFO
			
			
			$zip = $UserInfo->ship_zip; // DEST ZIP FOR SHIPPING COST
			
			//END GET ADDRESS INFO
			
			echo '<div id="view_cart_holder">';
				echo '<div id="view_cart_form_holder">';
					echo '<form action="#" method="post" id="view_cart_form">';
						$subTotal = 0;
						$shippingCost = 0;
						foreach($_SESSION['cart'] as $sku => $quantity) {
							if(is_int($sku)) { 
								echo '<div class="cart_product_holder">';
									$q_price = "SELECT title, price, tic FROM products WHERE sku = '$sku'";
									$r_price = mysqli_query($dbc, $q_price);
									$row_price = mysqli_fetch_array($r_price, MYSQLI_ASSOC);
									$price = $row_price['price'];
									$tic = $row_price['tic'];
									$qty = $quantity['quantity'];
									$product_title = $row_price['title'];
									
									$q_pack_weight = "SELECT * FROM packages WHERE sku = $sku AND qty = $qty";
									$r_pack_weight = mysqli_query($dbc, $q_pack_weight);
									$row_pack_weight = mysqli_fetch_array($r_pack_weight, MYSQLI_ASSOC);
									$lbs = $row_pack_weight['lb'];
									$oz = $row_pack_weight['oz'];
									
									$rate = new USPSRate(USPS_USER);
									
									$package = new USPSRatePackage;
									$package->setService(USPSRatePackage::SERVICE_PRIORITY);
									$package->setFirstClassMailType(USPSRatePackage::MAIL_TYPE_PARCEL);
									$package->setZipOrigination(OUR_ZIP);
									$package->setZipDestination($zip);
									$package->setPounds($lbs);
									$package->setOunces($oz);
									$package->setContainer('');
									$package->setSize(USPSRatePackage::SIZE_REGULAR);
									$package->setField('Machinable', true);
									
									// add the package to the rate stack
									$rate->addPackage($package);
									
									// Perform the request and print out the result
									$rate->getRate();
									$theRate = $rate->getArrayResponse();
									
									// Was the call successful
									if($rate->isSuccess()) {
									  $shippingCost = $shippingCost + $theRate['RateV4Response']['Package']['Postage']['Rate'];
									}
									
									
									echo '<div class="cart_product_sku">' . $product_title . ' - ' . $sku . '</div>';
									echo '<div class="cart_quantity">Qty - ' . $qty . '</div>';
										echo '<p>Update Quantity<br />';
											echo '<div class="select_holder" >';
												echo '<select name="' . $sku . '" class="quantity_select"  >';
													echo '<option value="0">Remove</option>';
													echo '<option '; if ($qty == 3) {echo 'selected="selected" ';} echo 'value="3">3</option>';
													echo '<option '; if ($qty == 5) {echo 'selected="selected" ';} echo 'value="5">5</option>';
													echo '<option '; if ($qty == 10) {echo 'selected="selected" ';} echo 'value="10">10</option>';
												echo '</select>';
											echo '</div>'; // END Select Holder
										echo '</p>'; // END Update Quantity "P"
									echo '<div class="cart_price">$' . number_format($price * $qty, 2) . '</div>';
									
									$subTotal = $subTotal + ($price * $qty);
								echo '</div>'; // END PRODUCT HOLDER
						
							}
						}
						
						$taxArr = get_cart_tax($user_id, $dbc, $shippingCost, $UserInfo);
						if($taxArr != -1) {
							
							$salesTax = round ($taxArr['tax'], 2);
							$salesTax = number_format($salesTax, 2);
							$subTotal = number_format($subTotal, 2);
							$total = $subTotal + $shippingCost + $salesTax;
							echo '<div class="cart_sub_total">Sub Total - <div class="cart_number">$' . $subTotal . '</div></div>';
							echo '<div class="cart_shipping">Shipping - <div class="cart_number">$' . number_format($shippingCost, 2) . '</div></div>';
							echo '<div class="cart_tax">' . $taxArr['name'] . ' - <div class="cart_number">$' . $salesTax . '</div></div>';
							
							echo '<div class="cart_total">Total - <div class="cart_number">$' . number_format($total, 2) . '</div></div>';
							
							echo '<input type="hidden" name="submitted" value="true" />';
							echo '<button formaction="#" id="update_cart" >Update Cart</button>';
						}
						else {
							echo 'There is an error with your address';
						}
					echo '</form>';
				echo '</div>';
				echo '<div id="view_cart_message"></div>';
				
			echo '</div>'; // END CART HOLDER 
			
			echo '<div id="checkout_holder">';
				echo '<button id="submit_order_checkout">Checkout</button>';
					
			if ($UserInfo->address_count !=0) {
			?>
			
			
			<h4>Shipping Address</h4>
				<p><?php echo $UserInfo->ship_street; ?><br />
				<?php echo $UserInfo->ship_city; ?>, 
				<?php echo $UserInfo->ship_state; ?> 
				<?php echo $UserInfo->ship_zip; ?></p>
			<h4>Billing Address</h4>
				<p><?php echo $UserInfo->bill_street; ?><br />
				<?php echo $UserInfo->bill_city; ?>, 
				<?php echo $UserInfo->bill_state; ?> 
				<?php echo $UserInfo->bill_zip; ?></p>
				
			<?php if($taxArr != -1) { 
				echo '<p class="hint">Your addresses are associated with your card info, please edit them by clicking the "Update Card" Button.</p>';
			} 
			
			
			echo '<div class="card_info">';
				echo '<div class="card_title">Billing Information</div>';
				echo '<div class="card"> **** **** **** ' . $UserInfo->card . '</div>';
				echo '<div class="card"> ' . $UserInfo->card_type . '</div>';
				echo '<div class="date">Exp ' . $UserInfo->exp_month . '/' . $UserInfo->exp_year . '</div>';
				echo '<button id="edit_card" type="submit">Update Card</button>';
			echo '</div>'; // END CARD INFO 
			
			
			?>
            
            
			<script>
				
				$( "select" ).change(function () {
					if($(this).val() != <?php echo $qty; ?>) {
						$('#quantity_warning').slideDown(200);
					}
					else {
						$('#quantity_warning').slideUp(200);
					}
				}).change();
				
			
				document.getElementById('edit_card').addEventListener('click', function(e) {
					handler.open({
					  description: 'Edit Card'
					});
					e.preventDefault();
				});
				
              
            </script>

            
            
			<?php }// END ADDRESSES COUNT 
			
			echo '</div>'; // END PAYMENT HOLDER
			
		}
		else {
			echo '<div><h1>Curses!</h1><h3>Your cart is empty</h3></div>';
		}
	
	
	}
	
	
} // END USER ID SET VERIFICATION


include('includes/footer.php');
?>

