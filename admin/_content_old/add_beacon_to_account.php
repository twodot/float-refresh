<?php
require_admin_login();

if (isset($_GET['uid'])) {
	if(isset($_SESSION['pseudo_login']['id'])) {
		$order_id = $_GET['order_id'];
		$customer_id = (int) $_SESSION['pseudo_login']['id'];
		$q = "SELECT id, beacons FROM beacon_orders WHERE user_id = $customer_id AND beacons != 0 AND id = '$order_id'  LIMIT 1";
		$r = mysqli_query($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		if ($row['beacons'] != 0 ) {
			$beacons_order_id = $row['id'];
			$uid = $_GET['uid'];
			$json = get_beacon_by_uid($uid);
			$beacon_info = json_decode($json);
			
			
			
			$proximity_uuid = $beacon_info->proximity;
			$k_id = $beacon_info->id;
			$k_major = $beacon_info->major;
			$k_minor = $beacon_info->minor;
			$k_uniqueId = $beacon_info->uniqueId;
			$k_txPower = $beacon_info->txPower;
			$k_interval = $beacon_info->interval;
			$k_name = $beacon_info->name;
			
			// GET NEXT TITLE
			$q="SELECT title FROM beacons WHERE user_id = $customer_id ORDER BY title DESC";
			$r= mysqli_query($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			$title = (int) $row['title'];
			
			$new_title = $title +1;
			// CHECK FOR MAJOR AND MINOR VAL FOR DUPLICATES
			$q = "
			SELECT * 
			
			FROM 
			
			beacons 
				
			WHERE 
			k_major = '$k_major' 
			AND k_minor = '$k_minor'";
			
			$r = mysqli_query($dbc, $q);
			
			if(mysqli_num_rows($r) == 0) {
			
				$q = "
				INSERT INTO 
				
				beacons 
				
				(
				user_id, 
				uuid, 
				title, 
				k_id,  
				k_major, 
				k_minor, 
				k_uniqueId, 
				k_txPower, 
				k_interval, 
				k_name, 
				beacon_order_id 
				)
				
				VALUES 
				(
				'$customer_id', 
				'$proximity_uuid', 
				'$new_title', 
				'$k_id', 
				'$k_major', 
				'$k_minor', 
				'$k_uniqueId', 
				'$k_txPower', 
				'$k_interval', 
				'$k_name', 
				'$order_id'
				)";
				
				$r = mysqli_query($dbc, $q);
				
				$q = "UPDATE beacon_orders SET beacons = beacons -1 WHERE id = $beacons_order_id";
				$r = mysqli_query($dbc, $q);
				
				$q = "SELECT beacons FROM beacon_orders WHERE user_id = $customer_id AND id = $beacons_order_id";
				$r = mysqli_query($dbc, $q);
				$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
				
				
				?>
                $('#manual_id').val('');
                $('#beacons_left').text('<?php echo $row['beacons']; ?>');
                $('#added_beacon_title').text('<?php echo $new_title; ?>');
                $('#beacons_on_this_order').append( "<p><?php echo $k_uniqueId; ?> - <?php echo $new_title; ?></p>" );
                $("#message").html('<div id="error">This Beacon has been added.</div>');
                $("#error").fadeIn(1000);
                $("#error").delay(3000).fadeOut(1000);
                
                <?php
				
				if($row['beacons'] == 0) {
					$q = "UPDATE beacon_orders SET order_fullfilled = 1, date_fullfilled = now() WHERE id = $beacons_order_id";
					$r = mysqli_query($dbc, $q);
				}
				
			} // NUM ROWS = 0 CHECK
			else {
				?>
                $("#message").html('<div id="error">This Beacon has already been assigned.</div>');
                $("#error").fadeIn(1000);
                $("#error").delay(3000).fadeOut(1000);
                <?php
			}
			
		}// END BEACONS ORDERED COUNT
		
	}// END PSEUDO LOGIN
	else {
		$url = BASE_URL . 'orders';
		//header("Location: $url");
		exit();
	}
} // UID ID CHECK
else {
	$url = BASE_URL . 'panel';
	//header("Location: $url");
	exit();
}

?>

