<?php


// Include the AWS SDK using the Composer autoloader
require '/home/ec2-user/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

require_once('includes/config.inc.php');
require_login();
$page_title = 'Manage Media';
include('includes/header.php');

include("includes/resize.php");
?>

<div id="dialog">
</div>

<?php
function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

if(isset($p)) {
	$i_sel = (int) $p;	
}

if(isset($_POST['submit'])) {
	
	if(isset($p)) {
		$i = (int) $p;
	
		if($i != 0) {
			
			$q = "SELECT serial_num from property_info WHERE id = $i";
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			if($row['serial_num'] != $_POST['hash']) {
				
				$hash = $_POST['hash'];
				
				$q = "UPDATE property_info SET serial_num = '$hash' WHERE id = $i";
				$r = @mysqli_query ($dbc, $q);
			
				$file_ary = reArrayFiles($_FILES['photos']);
				
				$q="SELECT COUNT(*) as property FROM properties, property_info  WHERE properties.user_id = $user_id AND properties.id = property_info.properties_id AND property_info.id = $i";
				$r = @mysqli_query ($dbc, $q);
				$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
				if($row['property'] == 1) {
					
					$q="SELECT properties.id as property_id, properties.title as title, properties.price as price, property_info.id as info_id, property_info.primary_info as primary_info  FROM properties, property_info  WHERE properties.user_id = $user_id AND properties.id = property_info.properties_id AND property_info.id = $i";
					$r = @mysqli_query ($dbc, $q);
					$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
					$p = $row['property_id'];
					
					
					foreach ($file_ary as $file) {
					   if (is_uploaded_file ($file['tmp_name'])){
							$temp = UPLOADS . md5($file['name']);
							if (move_uploaded_file($file['tmp_name'], $temp)){
								$image = $dbc->real_escape_string($file['name']);;
								$type = 'image/jpeg'; //$file['type'];
								
								$bucket = 'images.listfloat.com';
								$keyname = $temp;
								// $filepath should be absolute path to a file on disk						
								$filepath = $temp;
								
								
								$extension = strtolower(strrchr($file['name'], '.'));
								$file_name = str_replace($extension, '', $image);
								
								$q="INSERT INTO media SET property_id = $p, info_id = $i, user_id = $user_id, media_type = '$type', title = '$image'";
								$r = @mysqli_query ($dbc, $q);
								
								$id = mysqli_insert_id($dbc);
								
								rename ($temp, UPLOADS . "$id" . $extension);
								$image_rename =  '../float_req/images/' . "$id" . $extension;
								
								$theFile = UPLOADS . "$id" . $extension;
								// Instantiate the client.
								$s3 = S3Client::factory(array('region' => 'us-west-2'));
								
								$theFileUploaded = UPLOADS . "thumb/$id" . $extension;
								
								// THUMBNAIL SECTION
								$thumbBucket = 'images.listfloat.com/thumb';
								$thumbS3 = S3Client::factory(array('region' => 'us-west-2'));
								$thumbnail = generate_image_thumbnail($theFile, $theFileUploaded, $id);	
	
								try {
									$upload = $thumbS3->upload($thumbBucket, $id . '.jpg', fopen($thumbnail, 'rb'), 'public-read');
								} catch(Exception $e) { 
								}					
								
								if (isset($thumbnail) && file_exists($thumbnail) && is_file($thumbnail)){
									unlink($thumbnail);
								}	
							
								
								// PHONE SECTION
								$phoneBucket = 'images.listfloat.com/phone';
								$phoneS3 = S3Client::factory(array('region' => 'us-west-2'));
								$phone = generate_image_phone($theFile, $theFileUploaded, $id);	
	
								try {
									$upload = $phoneS3->upload($phoneBucket, $id . '.jpg', fopen($phone, 'rb'), 'public-read');
								} catch(Exception $e) { 
								}					
								
								if (isset($phone) && file_exists($phone) && is_file($phone)){
									unlink($phone);
								}
								
								// TABLET SECTION
								$tabletBucket = 'images.listfloat.com/tablet';
								$tabletS3 = S3Client::factory(array('region' => 'us-west-2'));
								$tablet = generate_image_tablet($theFile, $theFileUploaded, $id);	
	
								try {
									$upload = $tabletS3->upload($tabletBucket, $id . '.jpg', fopen($tablet, 'rb'), 'public-read');
								} catch(Exception $e) { 
								}					
								
								if (isset($tablet) && file_exists($tablet) && is_file($tablet)){
									unlink($tablet);
								}
								
								if (isset($theFileUploaded) && file_exists($theFileUploaded) && is_file($theFileUploaded)){
									unlink($theFileUploaded);
								}
								
								if (isset($theFile) && file_exists($theFile) && is_file($theFile)){
									unlink($theFile);
								}
							}
							
						}
						
					} // END FOREACH
				}// END PROPERTY SELECT CHECK 
			} // SERIAL NUMBER CHECK //
		}  // END I INT CHECK
	}// P CHECK
	
}

if(isset($i)) {
		$i = (int) $i;
		$i_sel = (int) $i;
}

if(isset($i_sel)) {
	
	$i = (int) $i_sel;	
		
	if($i != 0) {
		
		$q="SELECT COUNT(*) as property, properties.id as prop_id, properties.title as top_title, property_info.title as prop_title FROM properties, property_info  WHERE properties.user_id = $user_id AND properties.id = property_info.properties_id AND property_info.id = $i";
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		if($row['property'] == 1) {
			$top_title = $row['top_title'];
			$property_info_title = $row['prop_title'];
			$property_id = $row['prop_id'];
			
			$q="SELECT properties.id as property_id, properties.title as title, properties.price as price, property_info.id as info_id, property_info.primary_info as primary_info, property_info.primary_info as primary_info, media.title as media_title, media.id as media_id  FROM properties, property_info, media  WHERE properties.user_id = $user_id AND properties.id = property_info.properties_id AND property_info.id = $i AND media.property_id = properties.id AND media.info_id = $i  AND media.user_id = $user_id AND media.deleted != 1 ORDER By gallery_order ASC";
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			$primary_info = $row['primary_info'];
			
			if ($primary_info) {
				$q="SELECT properties.id as property_id, properties.title as title, properties.price as price, property_info.id as info_id, property_info.primary_info as primary_info, property_info.primary_info as primary_info, media.title as media_title, media.id as media_id  FROM properties, property_info, media  WHERE properties.user_id = $user_id AND properties.id = property_info.properties_id AND properties.id = $property_id AND properties.id = property_info.properties_id AND property_info.id = media.info_id AND media.property_id = properties.id AND media.property_id = $property_id  AND media.user_id = $user_id  AND media.deleted != 1 ORDER By main_gallery_order ASC";	
				$r = @mysqli_query ($dbc, $q);
				$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			}
			
			?>
						
			<script>
			
			$(function() {
				$( "#media_sortable" ).sortable({ items: " li", placeholder: "media_sort_highlight", opacity: 0.5 });
				$( "#media_sortable" ).on( "sortupdate", function( event, ui ) {
					var sorted = $( "#media_sortable" ).sortable( "serialize", { key: "sort" } );
					$.ajax({
						url: "update_media_sort<?php if ($primary_info) {echo'_primary';} ?>?"+ sorted, 
						cache: false,
						
					})
				})
				$( "#media_sortable" ).disableSelection();
				
			
			});
					
			</script>
			
			<?php
			
			echo '<div id="media_holder">';
					echo '<h3>' . $top_title  . '</h3>';
					if (!empty($property_info_title)) {
						echo '<p>' . $property_info_title . '</p>';	
					}
					
					echo '<div class="edit_media"><p><a href="/property/' . $property_id . '">Back to property</a><br /><form action="/media/' . $i . '" method="post" enctype="multipart/form-data" id="form_' . $i . '" >
							<input type="hidden" value="' . md5(uniqid(rand(), true)) . '" name="hash" />
						  	<input type="file" multiple name="photos[]" accept="image/jpeg,image/png,image/gif" capture="camera" >
						  	<div id="submit_holder">
							<img src="/images/loading.gif" class="loader" />
							<input type="submit" name="submit" value="Submit" id="upload_image" />
							</div>
					  </form></p>';
					  
					echo '<ul id="media_sortable">';
					$r = @mysqli_query ($dbc, $q);
			while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
				$image_name = str_replace('.jpg', '', $row['media_title']);
				echo '<li id="media_' . $row['media_id'] . '"';
				 if ($primary_info) {
					 if($row['primary_info'] == 0) {
					 	echo ' class="child_media" ';
					 }
				} 
				echo '><div class="image_container"><a href="/delete_image/' . $row['media_id'] . '" id="delete_image_' . $row['media_id'] . '" class="delete_image" ><img src="' . IMAGES . 'delete.png" alt="Delete image" title="Delete" /></a><div class="sort_image"><img src="http://images.listfloat.com/thumb/' . $row['media_id'] . '.jpg" /></div></div></li>';
				
			} // END FOREACH
				echo '</ul>';
				if ($primary_info) {
					echo '<p class="child_media">Green highlighted images are from other information galleries in this property.<br />Deleting them here will delete them from the other gallery.<br />Sorting them here will not affect their sort on the other gallery.</p>';
				}
			echo '<div>'; // END MEDIA HOLDER 
		}
	}
}
include('includes/footer.php');