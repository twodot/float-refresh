<?php
require_once('includes/config.inc.php');
require_login();
$street = sanitize($_GET['street'], $dbc);
$city = sanitize($_GET['city'], $dbc);
$state = sanitize($_GET['state'], $dbc);
$zipcode = (int) sanitize($_GET['zipcode'], $dbc);
$latitude = sanitize($_GET['latitude'], $dbc);
$longitude = sanitize($_GET['longitude'], $dbc);
$property = sanitize($_GET['p'], $dbc);

$is_error = FALSE;

if(empty($street)) {
	echo '$("#no_address_found_street").addClass("form_error");';
	$is_error = TRUE;
}
if(empty($city)) {
	echo '$("#no_address_found_city").addClass("form_error");';
	$is_error = TRUE;
}
if(empty($state)) {
	echo '$("#no_address_found_state").addClass("form_error");';
	$is_error = TRUE;
}
if(empty($zipcode)) {
	echo '$("#no_address_found_zipcode").addClass("form_error");';
	$is_error = TRUE;
}
if(empty($latitude)) {
	echo '$("#no_address_found_latitude").addClass("form_error");';
	$is_error = TRUE;
}
if(empty($longitude)) {
	echo '$("#no_address_found_longitude").addClass("form_error");';
	$is_error = TRUE;
}
if(empty($property)) {
	$is_error = TRUE;
}

if(!$is_error) {
	
	$state = strtoupper($state);
	
	$address = $street . ', ' . $city . ' ' . $state;
	
	$q = "UPDATE 
	properties 
	
	SET 
	address = '$address', 
	state = '$state', 
	zip = '$zipcode', 
	latitude = '$latitude', 
	longitude = '$longitude' 
	
	WHERE 
	id = $property";
	$r = mysqli_query($dbc, $q);
	
	echo '$("#address").val("' . $address . '").fadeIn(300);';
	echo '$("#lat").val("' . $latitude . '").fadeIn(300);';
	echo '$("#long").val("' . $longitude . '").fadeIn(300);';
	echo '$("#address_details_holder").fadeIn(300);';
	echo '$("#edit_map").fadeIn(300);';
	echo '$("#prop_map").html(\''; echo '<iframe width="100%" height="350" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBR5USylIliiJxWUroSXSaL7KD3T_WM328&q=' . $latitude . ',' . $longitude .  '&zoom=18"></iframe>\');';
	echo '$("#no_address_found").slideUp(300);';
	echo '$("#lat_long_holder").fadeIn(300);'; 
	echo '$("#form_address").slideUp(300);';
	echo '$("#prop_map").slideDown(500);';
	echo '$("#address_holder").slideDown(500);';
	
	
}
else {
	echo '$("#error").html("<p>There was an error with your address</p>").fadeIn(300);
		  $("#error").delay(2000).fadeOut(1000);';
}

?>