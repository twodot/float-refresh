<?php
// THIS PAGE IS NOW create_property.php

require_login();
get_property_id();
users_property();
$page_title = $page_name = 'Property';

include('includes/header.php');

$pro=new Property($property_id, $dbc);
//var_dump($pro);
?>

<div id="error">
</div>

<div id="dialog">
</div>

<script src="js/dropzone.js"></script>
<script>
// START JQUERY
$( document ).ready(function() {
	var data=JSON.parse('<?php echo $pro->get_json();?>');
	
	function AppViewModel() {
		var self=this;
		
		self.user=ko.observable(data.user);
		self.rental_subscription = ko.observable(<?php echo $UserInfo->is_sub_rental();?>);
		self.building=ko.observable(data.building);
		self.building_sort=ko.observable(data.building_sort);
		self.property_title=ko.observable(data.title);
		self.description=ko.observable(data.description);
		self.price=ko.observable(priceFormatter(data.price));
		self.active=ko.observable(data.active);
		self.address=ko.observable(data.address);
		self.state=ko.observable(data.state);
		self.zip=ko.observable(data.zip);
		self.latitude=ko.observable(data.latitude);
		self.longitude=ko.observable(data.longitude);
		self.status=ko.observable(data.status);
		self.status_message=ko.observable(data.status_message);
		self.co_agent=ko.observable(data.co_agent);
		self.beds=ko.observable(data.beds);
		self.bath=ko.observable(data.bath);
		self.sqft=ko.observable(data.sqft);
		self.lot_size=ko.observable(data.lot_size);
		self.property_type=ko.observable(data.property_type);
		self.property_style=ko.observable(data.style);
		self.year_built=ko.observable(data.year_built);
		self.community=ko.observable(data.community);
		self.county=ko.observable(data.county);
		self.mls=ko.observable(data.mls);
		self.walkscore=ko.observable(data.walkscore);
		self.walkscore_link=ko.observable(data.walkscore_link);
		self.show_walkscore=ko.observable(data.show_walkscore);
		self.show_greatschools=ko.observable(data.show_greatschools);
		self.collapse_schools=ko.observable(data.collapse_schools);
		self.short_url_hash=ko.observable(data.short_url_hash);
		self.facebook_share=ko.observable(data.facebook_share);
		self.twitter_share=ko.observable(data.twitter_share);
		self.updated=ko.observable(data.updated);
		self.property_data_type=ko.observable(data.property_data_type);
		self.progress=ko.observable(data.progress);
		self.completed=ko.observable(data.completed);
		self.amenities=ko.observable(data.amenities);
		self.crm=ko.observable(data.crm);
		self.crm_field_1_value=ko.observable(data.crm_field_1_value);
		self.crm_field_2_value=ko.observable(data.crm_field_2_value);
		self.crm_field_3_value=ko.observable(data.crm_field_3_value);
		self.crm_field_4_value=ko.observable(data.crm_field_4_value);
		self.crm_field_5_value=ko.observable(data.crm_field_5_value);
		self.crm_field_6_value=ko.observable(data.crm_field_6_value);
		
		self.info_list=ko.observableArray(data.info_list);
		self.media=ko.observableArray(get_media(data.info_list));
		self.beacon_list=ko.observableArray(get_beacon_list(data));
		
		self.flyer_id=ko.observable();
		self.flyer_name=ko.observable(data.flyer_name);
		
		console.log(data.info_list);
		
		self.addPropertyInfo = function(){
			alert('TEST');
			console.log(self.info_list);
			var checked=document.getElementById("go_to_link_automatically").is(":checked")? 1: 0;
			self.info_list.push({
				title: document.getElementById("info_title").value, 
				text: document.getElementById("info_box").value,
				link_beacon_text: document.getElementById("property_info_link_text").value,
				link_beacon_link: document.getElementById("property_info_link").value,
				notes: document.getElementById("notes_box").value,
				link_beacon: checked,
			});
		}
	}
	
	function priceFormatter(price){
		var n=parseInt(price);
		if(n<1000)
			return '$'+n;
		var res=('000'+n%1000).slice(-3);
		for(n=Math.floor(n/1000); n>=1000; n=Math.floor(n/1000))
			res=('000'+n%1000).slice(-3)+','+res;
		return '$'+n%1000+','+res;
	}
	
	$("#property_setup").steps({
		headerTag: "h3",
		enableContentCache: true, 
		bodyTag: "section",
		transitionEffect: "slide",
		saveState: true, 
		<?php 
		if($pro->is_completed()) {
			echo 'enableAllSteps: true, 
			 cssClass: "wizard property_tabs", 
			 enableCancelButton: true, ';
		}
		else {
			echo 'startIndex: ' . $pro->get_progress() . ', ';
		}
		
		if(isset($_GET['step'])) {
			$step = (int) $_GET['step'];
			echo 'startIndex: ' . $step . ', ';
		}
		
		?>
		stepsOrientation: "vertical", 
		labels: {
			current: "current step:",
			<?php 
			if($pro->is_completed()) {
				echo ' cancel: "Cancel", ';
			}
			?>
			pagination: "Pagination",
			finish: "Finish",
			next: "Save & Next",
			previous: "Previous",
			loading: "Loading ..."
		}, 
		onFinished: function (event, currentIndex) { window.location.replace("/panel?highlight=<?php echo $property_id; ?>");},
		saveState: true, 
		<?php 
		if ($pro->get_latitude() != 0.0000000 && $pro->get_longitude() != 0.0000000) {
		?>
			onInit: function (event, currentIndex) {
				if(currentIndex == 0) {
					loadMap();
				}
			},
		<?php 
		}?>
		onStepChanged: function (event, currentIndex, newIndex) { 
			if(currentIndex == 0) {
				loadMap();
			}
		}, 
		onStepChanging: function (event, currentIndex, newIndex) {
			property.viewModel.progress(newIndex);
			var json_data=ko.toJSON(property.viewModel);
			
			//console.log(json_data);
			
			var url = '/ajax/property_json?property=<?php echo $property_id; ?>'; 
			
			var formID = "#info_update_" + currentIndex;
			
			$.ajax({
				type: "POST",
				url: url, 
			    data: {"data": JSON.stringify(json_data)},
			    success: function(data) {
					console.log("done");
					console.log(data);
				}
			});
			
			
			if(currentIndex == 0) {
				if($('#address').val() == '') {
					$('#get_lat_long').css('background-color', '#ef4036');
					$('#get_lat_long').css('color', '#fff');
					return(false);
				}
				else {
					return(true);
				}
			}
			else {
				return(true);
			}
			
		}
	});
		
	$('#get_beacon_step').click(function(e){
		e.preventDefault();
		$("#property_setup").steps("previous");
		$("#property_setup").steps("previous");
	})
	
	$('.add_360').on( "click", image360Popup );
	
	function image360Popup(e) {
		e.preventDefault();
		infoId = $(this).attr('id');
		infoId = infoId.replace('image_360_','');
		$('#image360infoId').val(infoId);
		$('#upload_image_360_holder').dialog({
			
			dialogClass: "no-close",
			modal: true, 
			draggable: false, 
			buttons: [
				{
				  text: "Remove Image",
				  click: function() {
					  window.location = '/remove_360_image?p=<?php echo $property_id; ?>&i=' + $('#image360infoId').val();
				  }
				},
				{
				  text: "Close",
				  click: function() {
					$( this ).dialog( "close" );
				  }
				}
			]
			
			});
		
	}
	
	// Activates knockout.js
	property = { viewModel: new AppViewModel() };
	ko.applyBindings(property.viewModel);

});

function get_beacon_list(info){
	var beacon_list=[];
	beacon_list.push({beacon_title: info.property_beacon, title: info.title});
	if(info.info_list!=null)
		info.info_list.forEach(function(item){
			beacon_list.push({beacon_title: item.beacon_title, title: item.title});
		});
	return beacon_list;
}

function get_media(info_list){
	var media_list=[];
	if(info_list!=null)
		info_list.forEach(function(item){
			if(item.media!=null)
				media_list=media_list.concat(item.media.split(", "));
		});
	return media_list;
}

function get_subinfo_list(info_list){
	var subinfo_list=[];
	if(info_list!=null)
		info_list.forEach(function(item){
			subinfo_list.push(item.subinfo_list);
		});
	return subinfo_list;
}

</script>

<script type="text/javascript">

  function initialize() {
	  var propertyLatLong = new google.maps.LatLng(<?php echo $pro->get_latitude() . ',' . $pro->get_longitude(); ?>);
	  var mapOptions = {
		zoom: 18,
		center: propertyLatLong, 
		//mapTypeId: google.maps.MapTypeId.HYBRID
	  }
	  var map = new google.maps.Map(document.getElementById('property-map'), mapOptions);
		
	  var image = 'https://s3-us-west-2.amazonaws.com/images.listfloat.com/assets/float_map_dot.png';
	  var marker = new google.maps.Marker({
		  position: propertyLatLong,
		  map: map,
		  title: 'Float Property', 
		  icon: image
	  });
	}

	
	function loadMap() {
	  var script = document.createElement('script');
	  script.type = 'text/javascript';
	  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAiAePyQf5emFwHaIQlsvwFGX0VXmvEiNg&callback=initialize';
	  document.body.appendChild(script);
	}
	
</script>

<main id="property_setup">
	<h3>Location</h3>
	<section>
    	<h2>Location</h2>
        <div id="property-map"></div>
        <label for="address">Address</label>
        <input id="address" name="address" data-bind="value: address" placeholder="Address">
        <label for="latitude">Latitude</label>
        <input id="latitude" name="latitude" data-bind="value: latitude" placeholder="Latitude">
        <label for="longitude">Longitude</label>
        <input id="longitude" name="longitude" data-bind="value: longitude" placeholder="Longitude">
	</section>
	
	<h3>Details</h3>
    <section>
	    <h2>Interesting. Tell Us More.</h2>
	    <section id="status">
			<!-- ko ifnot: rental_subscription -->
	        <input type="radio" id="status1" name="status" value="0" data-bind="checked: status">For Sale
	        <input type="radio" id="status2" name="status" value="1" data-bind="checked: status">Pending
	        <input type="radio" id="status3" name="status" value="2" data-bind="checked: status">Sold
	        <!-- /ko -->
	        <!-- ko if: rental_subscription -->
	        <input type="radio" id="status4" name="status" value="3" data-bind="checked: status">For Rent
	        <input type="radio" id="status5" name="status" value="4" data-bind="checked: status">Rented
	        <!-- /ko -->
	        <input type="radio" id="status5" name="status" value="5" data-bind="checked: status">Custom
	        <input type="text" name="sales_status_message" maxlength="20" data-bind="value: status_message" placeholder="value=" id="custom_message_input">
	    </section>
        <label for="beds">Beds</label>
        <input id="beds" name="beds" data-bind="value: beds" placeholder="Beds">
        <label for="bath">Bath</label>
        <input id="bath" name="bath" data-bind="value: bath" placeholder="Bath">
        <label for="sqft">SqFt</label>
        <input id="sqft" name="sqft" data-bind="value: sqft" placeholder="SqFt">
        <label for="lot_size">Lot Size</label>
        <input id="lot_size" name="lot_size" data-bind="value: lot_size" placeholder="Lot Size">
        <label for="property_type">Property Type</label>
        <input id="property_type" name="property_type" data-bind="value: property_type" placeholder="Property Type">
        <label for="year_built">Year Built</label>
        <input id="year_built" name="year_built" data-bind="value: year_built" placeholder="Year Built">
        <label for="neighborhood">Neighborhood</label>
        <input id="neighborhood" name="neighborhood" data-bind="value: community" placeholder="Neighborhood">
        <label for="county">County</label>
        <input id="county" name="county" data-bind="value: county" placeholder="County">
        <label for="mls">MLS#</label>
        <input id="mls" name="mls" data-bind="value: mls" placeholder="MLS#">
    </section>
	
	<h3>Marketing</h3>
    <section>
	    <h2>Ok. You Have Our Attention.</h2>
	    <label for="flyer">Add a pdf flyer below</label>
		<div id="file_uploaded" data-bind="css: {hide_flyer: !flyer_id()}">
			<img src="/images/pdf_icon.png" alt="pdf icon" id="pdf_icon" data-bind="visible: flyer_id()"/><span data-bind="text: flyer_name"></span>
	    	<a href="/delete_flyer?i=<?php echo $property_id; ?>" id="delete_flyer_button">
				<img src="/images/delete.png" alt="Delete flyer" title="Delete your flyer" class="delete_button">
			</a>
	    </div>
	    
		<div data-bind="css: {hide_flyer: flyer_id()}">
			<form action="/_content/add_property/flyer_upload.php?p=<?php echo $property_id; ?>" class="dropzone " method="post" enctype="multipart/form-data" id="flyer_uploader_form">
		        <div class="fallback">
		            <input type="file" multiple name="file" accept="application/pdf" capture="camera" >
		            <div id="submit_holder">
		                <img src="/images/loading.gif" class="loader" />
		                <input type="submit" name="submit" value="Submit" id="upload_image" />
		            </div>
		        </div>
		    </form>
	    </div>
	    
	    <label for="property_title">Property Title</label>
        <input id="property_title" name="property_title" data-bind="value: property_title" placeholder="Property Title">
	    <label for="price">Price</label>
        <input id="price" name="price" data-bind="value: price" placeholder="Price">
	    <label for="marketing_description">Marketing Description</label>
        <textarea id="marketing_description" name="marketing_description" data-bind="value: description" placeholder="Marketing Description"></textarea>
	</section>
	
	<h3>Media</h3>
	<section>
		<h2>Nice! We’ve Got to See This.</h2>
		<ul id="media" data-bind="foreach: media">
			<li>
				<div>
					<img data-bind="attr:{src: 'https://s3-us-west-2.amazonaws.com/images.listfloat.com/thumb/'+$data+'.jpg'}">
				</div>
			</li>
		</ul>
	</section>
	
	<h3>Features / Tours</h3>
	<section>
		<h2>Time to Crank up the Volume.</h2>
		<p>
			Unleash the power of Float by calling-out special selling features you don’t want missed. Point out unique spaces, appliances, designer finishes and more. Go even further down the rabbit hole by adding sub-information for brand names, model types, building techniques and the finer details.
		</p>
		
		<h4 class="add_info_show">Add property highlights (+)</h4>
		<div id="add_info" style="display: block;">
            <p>Title (room or feature)<br>
            <input type="text" id="info_title" value="" name="title" placeholder="Title"></p>
            
            <p>Information<br>
            <textarea id="info_box" name="info"></textarea></p>
            
            <p>Add a link to the property highlight screen.</p>
            <input type="text" id="property_info_link_text" name="property_info_link_text" placeholder="Link Text" value="" class="new_info_link_text" maxlength="50">
            <input type="text" id="property_info_link" name="property_info_link" placeholder="URL" value="" class="new_info_link" maxlength="255">
            <p><input type="checkbox" id="go_to_link_automatically" name="go_to_link_automatically" value="1" class="new_info_automatic_link_checkbox"> Open this link in place of the property highlight screen.</p>
        	<p>Notes (will not show in app) <br>*Tip: Where beacon is located in house<br>
            <textarea id="notes_box" name="notes"></textarea>
            </p>
            <button data-bind="click: addPropertyInfo">ADD</button>
            <button class="add_info_cancel">Cancel</button>
        </div>
        
		<ul id="info_list" data-bind="foreach: info_list">
			<li>
				<h3 data-bind="text: $data.title"></h3>
				<!-- ko if: $data.beacon_title -->
				<span data-bind="text: 'Beacon '+$data.beacon_title"></span>
				<!-- /ko -->
				<nav>
					<button>Edit</button>
					<button>(+) Add Sub Information</button>
					<button>(-) Delete</button>
				</nav>
				<p data-bind="text: '('+(($data.media)? $data.media.split(', ').length: 0)+') Images'"></p>
				<section id="image_360">
					<!-- ko if: $data.has_360_image == 1 -->
						<a data-bind="attr:{href: '/image_360.php?name='+$data.link_360_image}">
							<img data-bind="attr:{src: 'https://s3-us-west-2.amazonaws.com/images.listfloat.com/360-images/'+$data.link_360_image+'.jpg'}" class="Image360Thumb">
						</a>
					<!-- /ko -->
				</section>
				<p data-bind="text: $data.text"></p>
				<h3>Notes<span class="notes_instr">(Will not show in app.)</span></h3>
				<p data-bind="text: $data.notes"></p>
				<section data-bind="foreach: $data.subinfo_list">
				    <label for="subinfo_title">Title (room or feature)</label>
					<input id="subinfo_title" name="subinfo_title" placeholder="Title" data-bind="value: $data.title">
				    <label for="subinfo_text">Information</label>
			        <textarea  id="subinfo_text" name="subinfo_text" placeholder="Information" data-bind="value: $data.text"></textarea>
				</section>
			</li>
		</ul>
	</section>
	
	<h3>Float Tour</h3>
	<section>
		<h2>The Big Finale</h2>
		<ul id="beacon_list" data-bind="foreach: beacon_list">
			<li>
				<h3 data-bind="text: $data.title"></h3>
				<h4 data-bind="text: 'Beacon '+$data.beacon_title"></h4>
			</li>
		</ul>
	</section>
	
	<h3>Neighborhood & Schools</h3>
	<section>
		<h2>What’s There to do Around Here?</h2>
	</section>
	
	<h3>Lift-off</h3>
	<section>
		<h2>Lift-off</h2>
		<p>
			Short URL | For social sharing and yard arm signs when people don’t have the app installed.<br>
			<a data-bind="attr: {href: 'http://bit.ly/'+short_url_hash()}, text: 'http://bit.ly/'+short_url_hash()"></a>
		</p>
		<p>
			Facebook URL | For sharing on Facebook.<br>
			<a data-bind="attr: {href: 'http://bit.ly/'+facebook_share()}, text: 'http://bit.ly/'+facebook_share()">Facebook URL</a>
		</p>
		<p>
			Twitter URL | For sharing on Twitter.<br>
			<a data-bind="attr: {href: 'http://bit.ly/'+twitter_share()}, text: 'http://bit.ly/'+twitter_share()">Twitter URL</a>
		</p>
	</section>
</main>
<script type="text/javascript">
$(function() {
	
	Dropzone.options.flyerUploaderForm = false;
	
	var flyerUpload = new Dropzone("#flyer_uploader_form", {
		acceptedFiles: "application/pdf", 
		dictDefaultMessage: "Drag and drop a PDF flyer here.", 
		dictInvalidFileType: "The file must be a pdf.", 
		maxFiles: 1, 
	});
	 
	 
	flyerUpload.on("success", function(file, response) {
		$('#flyer_uploader_form').fadeOut(300);
		$("#file_uploaded").prepend( '<img src="/images/pdf_icon.png" alt="pdf icon" id="pdf_icon"/>' + response +'.pdf'); 
		$('#file_uploaded').fadeIn(500);
	});
});
</script>
<?php 
	include('includes/footer.php');
?>