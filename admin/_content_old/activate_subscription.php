<?php

require_once('includes/config.inc.php');
require_login();
$q = "SELECT COUNT(*) as subscription FROM subscription WHERE user_id = $user_id";
$r = @mysqli_query ($dbc, $q);
$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
if($row['subscription'] == 0) {

	if(isset($_GET['t'])) {
		
		require_once('./lib/Stripe.php');
		Stripe::setApiKey(STRIPE_SECRET);

		$token = $_GET['t'];
		$stripe_token = Stripe_Token::retrieve($token);
		Stripe::setApiKey(STRIPE_SECRET);
		
		$last_four = $stripe_token['card']['last4'];
		$type = $stripe_token['card']['type'];
		$exp_month = $stripe_token['card']['exp_month'];
		$exp_year = $stripe_token['card']['exp_year'];
		$card_id = $stripe_token['card']['id'];
		
		$bill_address_line_1 = $stripe_token['card']['address_line1'];
		$bill_address_line_2 = $stripe_token['card']['address_line2'];
		$bill_address_city = $stripe_token['card']['address_city'];
		$bill_address_state = $stripe_token['card']['address_state'];
		$bill_address_zip = $stripe_token['card']['address_zip'];
		
		$subscribe_level = (int) $_GET['s'];
		
		
		$emailHash = md5(strtolower($_SESSION['login']['email']));
		if($allowed_props == 0) {
			$data ='{"interests": {"' . SUBSCRIPTION_TYPE_ACTIVE . '": true, "' . SUBSCRIPTION_LEVEL_GENERAL_REAL_ESTATE . '":true, "' . SUBSCRIPTION_TYPE_NONE . '":false}}';
		}
		else {
			$data ='{"interests": {"' . SUBSCRIPTION_TYPE_ACTIVE . '": true, "' . SUBSCRIPTION_LEVEL_PROPERTY_MANAGEMENT . '":true, "' . SUBSCRIPTION_TYPE_NONE . '":false}}';
		}
		$url = '/lists/' . FLOAT_LIST_ID . '/members/' . $emailHash;
		$mcMemberJson = mc_request('PUT', $url, $data);
		$mcMember = json_decode($mcMemberJson);
		
		
		$customer = Stripe_Customer::create(array(
		  "card" => $stripe_token, 
		  "plan" => "$subscribe_level",
		  "email" => $stripe_token['email'])
		);
		
		$customer_id = $customer['id'];
		
		$amount = $customer['subscriptions']['data'][0]['plan']['amount'];
		$subscription_id = $customer['subscriptions']['data'][0]['plan']['id'];
		$start = $customer['subscriptions']['data'][0]['current_period_start'];
		$end = $customer['subscriptions']['data'][0]['current_period_end'];
		$trial_start = $customer['subscriptions']['data'][0]['trial_start'];
		$trial_end = $customer['subscriptions']['data'][0]['trial_end'];
		
		$q_props = "SELECT total_properties FROM subscription_types WHERE stripe_id = $subscription_id";
        $r_props = @mysqli_query ($dbc, $q_props);                                     
		$row_props = mysqli_fetch_array($r_props, MYSQLI_ASSOC);
		
		$allowed_props = $row_props['total_properties'];
		
		$q= "UPDATE users SET stripe_id = '$customer_id' WHERE user_id = $user_id ";
		$r = @mysqli_query ($dbc, $q);
		
		$q= "SELECT COUNT(*) as subscriptions FROM subscription WHERE user_id = $user_id ";
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		
		// REFERRAL SECTION 
		
		$q_referred_by_id = "SELECT referred_by FROM users WHERE stripe_id = '$customer_id'";
		$r_referred_by_id = @mysqli_query($dbc, $q_referred_by_id);
		$row_referred_by_id = mysqli_fetch_array($r_referred_by_id, MYSQLI_ASSOC);
		$referred_by_id = $row_referred_by_id['referred_by'];
		
		$q_referred_by_stripe_id = "SELECT stripe_id FROM users WHERE user_id = $referred_by_id";
		$r_referred_by_stripe_id = mysqli_query($dbc, $q_referred_by_stripe_id);
		$row_referred_by_stripe_id = mysqli_fetch_array($r_referred_by_stripe_id, MYSQLI_ASSOC);
		$referred_by_stripe_id = $row_referred_by_stripe_id['stripe_id'];
		
		// Credit Referral Source	
		if(!empty($referred_by_stripe_id)) {
			Stripe_InvoiceItem::create(array(
				"customer" => $referred_by_stripe_id,
				"amount" => -1000,
				"currency" => "usd",
				"description" => "Referral")
			);
			
			$q_update_referral = "UPDATE users SET referred_credited = referred_credited + 1 WHERE user_id = $referred_by_id";
			$r_update_referral = @mysqli_query($dbc, $q_update_referral);
		
			// Credit Referred
			Stripe::setApiKey(STRIPE_SECRET);
			
			Stripe_InvoiceItem::create(array(
				"customer" => $customer_id,
				"amount" => -1000,
				"currency" => "usd",
				"description" => "Referral Code")
			);
		} // END EMPTY REFERRRER ID
		
		
		
		
		
		if($row['subscriptions'] == 0) {
			
			$q= "SELECT * FROM subscription_types WHERE stripe_id = $subscribe_level ";
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			$active_prop = $row['stripe_id'];
			$amount = $row['stripe_amount'];
			$rental = $row['rental'];
			
			$q= "INSERT INTO 
			subscription 
			(
			user_id, 
			subscribe_level, 
			allowed_prop, 
			amount, 
			current_period_start, 
			current_period_end,
			trial_start,  
			trial_end, 
			active, 
			rental 
			) 
			
			VALUES 
			(
			$user_id, 
			$subscribe_level, 
			$allowed_props, 
			$amount, 
			FROM_UNIXTIME($start), 
			FROM_UNIXTIME($end),
			FROM_UNIXTIME($trial_start), 
			FROM_UNIXTIME($trial_end), 
			1, 
			$rental 
			)";
			
			$r = @mysqli_query ($dbc, $q);
			
			
			// ADD CARD SECTION 
			$q= "INSERT INTO cards (user_id, card, card_type, exp_month, exp_year, stripe_card_id) VALUES ('$user_id', '$last_four', '$type', '$exp_month', '$exp_year', '$card_id')";
			$r = @mysqli_query ($dbc, $q);
			
			// ADD ADDRESS 
			/* $q= "INSERT INTO billing (user_id, street, street_cont, city, state_name, zip) VALUES ('$user_id', '$bill_address_line_1', '$bill_address_line_2', '$bill_address_city', '$bill_address_state', '$bill_address_zip')";
			$r = @mysqli_query ($dbc, $q);
			
			$bill_id = mysqli_insert_id($dbc);
			
			$q= "INSERT INTO addresses (user_id, billing_id) VALUES ('$user_id', '$bill_id')";
			$r = @mysqli_query ($dbc, $q);
			*/
			
			$url = '/account' ;
		
			ob_end_clean();
			
			header("Location: $url");
			
			exit();
		}
		
	} // END GET CHECK
}
?>

