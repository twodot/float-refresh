<?php
require_login();


if (isset($_POST['submitted'])){

	$p = FALSE;
	
	if (preg_match ('/^((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})$/', $_POST['password1'])){ 
		if ($_POST['password1'] == $_POST['password2']) {
			$p = $dbc->real_escape_string($_POST['password1']);
		}
		else{
			echo '<div id="bumper"><p class"error"> Your Passwords did not match</p></div>';
		}
	}
	else {
		echo '<div id="bumper"><p class="error">Please enter a valid password! (It must be between 8 and 20 charactors, at least one upper case letter, one lower case letter, and one number.)</p></div>';
	}
	
	if ($p){
		$q = "UPDATE users SET pass=SHA1('$p') WHERE user_id=$user_id LIMIT 1";
		
		$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
		
		if (mysqli_affected_rows($dbc) == 1) {
			
			
			mysqli_close($dbc);
			
		}
		else {
			echo '<div id="bumper"><p class="error">Your password was not changed. Make sure that your new password is different than the current. Contact the system <a href="' . ADMIN . '" class="body_link">admininistrator</a> if you think an error has occurred.</p></div>';
		}
	}
	else {
		echo '<div id="bumper"><p class="error">Please try again.</p></div>';
	}
	mysqli_close($dbc);
}
		
?>