<?php

require_once('includes/config.inc.php');
require_login();

if (isset($_GET['p'])) {
	$p = (int) $_GET['p'];
	if ($p != 0) {
		$q = "
			
		UPDATE 
		property_info, 
		beacons 
		
		SET 
			property_info.beacon_id = 0, 
			beacons.property_info_id = 0,  
			beacons.beacon_type = ''
		WHERE 
		property_info.properties_id = $p 
		AND beacons.property_info_id = property_info.id 
		AND property_info.location = 1
		AND beacons.user_id = $user_id 
		AND beacons.beacon_type = 'Location' 
		
		";
		
		$r = @mysqli_query ($dbc, $q);
		
		$q = "
			
		UPDATE 
		properties 
		
		SET 
			active = 0
			
		WHERE 
		id = $p 
		";
		
		$r = @mysqli_query ($dbc, $q);	
		
		header('Location: /create_property/' . $p . '?step=5');
		
	}// END INT P CHECK 
}

else {
	echo '$("#error").html("<p>There was an error, the beacons could not be deleted!</p>").fadeIn(300);';
	echo '$("#error").delay(2000).fadeOut(1000);';
	
}

?>

