<?php

require_once('includes/config.inc.php');
require_login();

if (isset($_GET['b'])) {
	$beacon = (int) $_GET['b'];
	if ($beacon != 0) {
		
		$q = "SELECT 
		* 
		FROM 
		property_info, 
		beacons 
		
		WHERE 
		beacons.id = $beacon
		AND property_info.beacon_id = beacons.id 
		AND beacons.property_info_id = property_info.id 
		AND beacons.user_id = $user_id";
		
		$r = mysqli_query($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		$primary = FALSE;
		if($row['primary_info'] == 1 ) {
			$primary = TRUE;
		}
		else {
			$primary = FALSE;
		}
		
		$q = "
			
		UPDATE 
		property_info, 
		beacons 
		
		SET 
			property_info.beacon_id = 0, 
			beacons.property_info_id = 0, 
			beacons.beacon_type = ''
			
		WHERE 
		beacons.id = $beacon
		AND property_info.beacon_id = beacons.id 
		AND beacons.property_info_id = property_info.id 
		AND beacons.user_id = $user_id 
		
		";
		
		$r = @mysqli_query ($dbc, $q);
		
		
		
		if(isset($_GET['p'])) {
			$p = (int) $_GET['p'];
			$q = "
				
			UPDATE 
			properties 
			
			SET 
				active = 0
				
			WHERE 
			id = $p 
			AND user_id = $user_id
			";
			
			$r = @mysqli_query ($dbc, $q);	
			
		}
		
		$q = "
			
		SELECT * 
		
		FROM 
		beacons 
		
		WHERE 
		id = $beacon
		";
		
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		
		if (isset($_GET['i'])) {
			$i = (int) $_GET['i'];
			
			if($primary) {
				
				// OPPOSITE BECAUSE THIS IS REMOVING THE BEACON
				?>  
                $('#big_ole_activation_button').hide();
                $('#lift_off').hide();
                $('#big_ole_activation_warning').show();
				<?php
			}
			
			?>
            $('#prop_id_<?php echo $i; ?>').html('Beacon removed');
            $('#prop_feature_id_<?php echo $i; ?>').html('');
            $('#prop_id_<?php echo $i; ?>').delay(500).fadeOut(500, function() {
            	$('#info_container_<?php echo $i; ?> .beacon_title_holder').removeClass('beacon_assigned');
            });
            $('#info_container_<?php echo $i; ?>').addClass('property_info_avail');
            $('#beacon-<?php echo $beacon; ?>').fadeOut(300, function(){
            	$('#beacon-<?php echo $beacon; ?>').parent().detach(); 
                $("#available_beacons .holder").prepend('<div id="beacon-<?php echo $beacon; ?>" class="available_beacon ui-draggable"><?php echo $row['title']; ?></div>'); 
                unassigned();
                beacon_assign();
                inactive_drag(); 
            });
            
            $('#info_container_<?php echo $i; ?>').droppable( "enable" );
            
            <?php 
		}
		
		
		
	}// END INT P CHECK 
}

else {
	echo '$("#error").html("<p>There was an error, the beacon could not be removed!</p>").fadeIn(300);';
	echo '$("#error").delay(2000).fadeOut(1000);';
	
}

?>

