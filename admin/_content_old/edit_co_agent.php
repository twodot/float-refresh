<?php

require_once('includes/config.inc.php');
require_login();
if (isset($_GET['submitted'])){
	
	$em = FALSE;
	$trimmed = array_map('trim', $_GET);
	
	$listing_name = $dbc->real_escape_string($trimmed['name']);
	
	if (preg_match ('/^[\w.-]+@[\w.-]+\.[A-Za-z]{2,6}$/', $trimmed['email'])) {
		$em = $dbc->real_escape_string($trimmed['email']);
	}
	else {
		$e = 5;
		$em = '';
	} 
	
	$listing_phone = $dbc->real_escape_string($trimmed['phone']);
	
	$listing_brokerage = $dbc->real_escape_string($trimmed['brokerage']);
	$listing_brokerage = htmlspecialchars ($listing_brokerage, ENT_QUOTES);
	$agent = $_GET['agent'];
	$prop = $_GET['property'];	
	$q = "Update
	co_agent
	
	SET 
	name = '$listing_name', 
	email = '$em', 
	phone = '$listing_phone', 
	brokerage = '$listing_brokerage', 
	user_id = $user_id 
	
	WHERE 
	id = $agent
	
	";
	
	$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
	?>
    $('#co_agent_holder').fadeOut(500, function() {
    	$('#co_agent_holder').html('<h4>Co Agent:</h4><p><?php echo $listing_name; ?><br /><?php echo $em; ?><br /><?php echo $listing_phone; ?><br /><?php echo $listing_brokerage; ?></p><p><a href="edit_co_agent?ca=<?php echo $agent; ?>&p=<?php echo $prop; ?>" id="edit_co_agent">Edit this agents info</a></p><div id="update_co_agent_info_form_holder"><form action="/edit_co_agent" id="update_co_agent_info_form"><p class="forms"><input type="text" name="name" size="40" value="<?php echo $listing_name; ?>" placeholder="Name" /></p><p class="forms"><input type="text" name="email" size="40" value="<?php echo $em; ?>" placeholder="Email" /></p><p class="forms"><input type="text" name="phone" size="40" value="<?php echo $listing_phone; ?>" placeholder="Phone" /></p><p class="forms"><input type="text" name="brokerage" size="40" value="<?php echo $listing_brokerage; ?>" placeholder="Brokerage" /></p><br /><input type="submit" name="submit" value="Save <?php echo $listing_name; ?>&rsquo;s Info" id="save_co_agent_info_form"/><button id="cancel_co_agent_info_form">Cancel</button><input type="hidden" name="agent" value="<?php echo $agent; ?>" /><input type="hidden" name="property" value="<?php echo $prop; ?>" /><input type="hidden" name="submitted" value="TRUE" /></form></div><p><a href="add_co_agent?p=<?php echo $prop; ?>" id="change_co_agent_button">Change the co-listed agent</a></p><form action="/add_co_agent" method="post" id="remove_co_agent_form" ><input type="hidden" name="existing_agent" value="0" /><p><input type="submit" name="submit" class="error" value="Remove Co-Agent" id="remove_co_agent" /></p><input type="hidden" name="remove" value="TRUE" /><input type="hidden" name="property" value="<?php echo $prop; ?>" /><input type="hidden" name="existing-submitted" value="TRUE" /></form>');
    	$('#add_co_agent_select_<?php echo $agent; ?>').remove();
        $('#update_co_agent_info_form_holder').slideUp(1000);
    	$('#edit_co_agent').fadeIn(500);
    	$('#change_co_agent_button').fadeIn(500);
    	$('#remove_co_agent').fadeIn(500);
    	$('#add_co_agent_show').fadeOut(500);
        $('#co_agent_holder').fadeIn(500);
        $('#existing_agent_select').append('<option value="<?php echo $agent; ?>"  id="add_co_agent_select_<?php echo $agent; ?>"><?php echo $listing_name; ?></option>');
     })
    <?php
}
