<?php

require_login();
$page_title = $page_name = 'Property';

include('includes/header.php');

?>
<script src="/js/html5-qrcode.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	$('#reader').html5_qrcode(function(data){
			$('#read').html(data);
		},
		function(error){
			$('#read_error').html(error);
		}, function(videoError){
			$('#vid_error').html(videoError);
		}
	);
});
</script>

<div id="reader" style="width:300px;height:250px">
 </div>
 <span id="read" class="center"></span>
<?php

include('includes/footer.php');

?>