<?php
/* REMOVE ON LAUNCH 

ini_set("display_errors", 1);
ini_set("track_errors", 1);
ini_set("html_errors", 1);
error_reporting(E_ALL);

// END REMOVE */

$id_check = false;
$error = true;
$error_message = array();
if (isset($_GET['first_name'])) {
	$first_name = $dbc->real_escape_string($_GET['first_name']);
}
else {
	$first_name_error = true;
	$error_message[] = array('title' => 'name', 'description' => 'You didn&rsquo;t enter your first name.');
}

if (isset($_GET['last_name'])) {
	$last_name = $dbc->real_escape_string($_GET['last_name']);
}
else {
	$last_name_error = true;
	$error_message[] = array('title' => 'name', 'description' => 'You didn&rsquo;t enter your last name.');
}

// EMAIL VALIDATION 
if (isset($_GET['email'])) {
	$checkEmail = explode("@",$_GET['email']);
	$domain = array_pop($checkEmail);
	// DOMAIN CHECK
	// EMAIL CHECK
		if (!filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) {
			$email_error = true;
			$error_message[] = array('title' => 'email', 'description' => 'You entered an invalid email.');
		}
		else {
			if(checkdnsrr($domain,"MX")){
				$email = $dbc->real_escape_string($_GET['email']);
			}
			else {
				$email_error = true;
				$error_message[] = array('title' => 'domain', 'description' => 'The email domain you entered is not valid.');
			} //END DOMAIN CHECK
	} // VALID EMAIL CHECK
}
else {
	$email_error = true;
	$error_message[] = array('title' => 'email', 'description' => 'You didn&rsquo;t enter your email.');
}
// END EMAIL VALIDATION 

if (isset($_GET['phone'])) {
	$phone = $dbc->real_escape_string($_GET['phone']);
}
else {
	$phone = '';
}

if (isset($_GET['message'])) {
	$message = $dbc->real_escape_string($_GET['message']);
}
else {
	$message = '';
}

if (isset($_GET['id'])) {
	$property_id = (int) $_GET['id'];
	if($property_id != 0) {
		$error = false;
	}
}
else {
	$property_id = 0;
	$property_id_error = true;
	$error_message[] = array('title' => 'property id', 'description' => 'No property id was supplied.');
}

if (isset($_GET['general']) && $_GET['general'] == 1) {
	$q= "SELECT * FROM leads WHERE email = '$email' LIMIT 1";
	$r= mysqli_query($dbc, $q);
	
	if(mysqli_num_rows($r) == 1) {
		$row=mysqli_fetch_array($r, MYSQLI_ASSOC);
		$lead_id = $row['id'];
	}
	else {
		$q= "INSERT INTO leads (first_name, last_name, email, phone) VALUES ('$first_name', '$last_name', '$email', '$phone')";
		$r= mysqli_query($dbc, $q);
		$lead_id = mysqli_insert_id($dbc);
	}
	
	$q= "INSERT INTO lead_message (lead_id, general, message) VALUES ($lead_id, '1', '$message')";
	$r = mysqli_query($dbc, $q);
	
	
	$HTMLEmailoutput_general = '
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>General App Contact</title>
</head>

<body style="text-align:center">

<table cellpadding="0" cellspacing="0" border="0" style="font-family:Helvetica, Arial, sans-serif; min-width:700px; text-align:left; margin:auto; padding:20px;">
	<tr>
    	<td>
        	<table width="100%">
        		<tr>
                	<td style="padding-right:20px; width:20%;">
                		<img src="' . BASE_URL . 'images/logo-126.jpg"  style="width:100%; max-width:75px; min-width:50px; height:auto" />
                    </td>
                   
               </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
        	<p>Hey, ' . $first_name . ' contacted you from the app.</p>
        	<p>' . $message . '</p>
            <p>Thanks,<br />' . $first_name . ' ' . $last_name . '<br /><a href="mailto:' . $email . '">' . $email . '</a><br />' . $phone . '</p>
        </td>
	</tr>
    
    
</table>
</body>
</html>';
			
			$mail_requests = null;
			ob_clean();
			unset($mail_requests);
							
					include(MAIL_LEADS);
			
					$mail_leads->FromName = 'App Contact';
					$mail_leads->addAddress('info@listfloat.com');     // Add a recipient
					
					$mail_leads->WordWrap = 50;                                 // Set word wrap to 50 characters
					$mail_leads->isHTML(true);                                  // Set email format to HTML
					
					$mail_leads->Subject = 'General Float Inquery';
					
					$mail_leads->Body    = $HTMLEmailoutput_general;
					$mail_leads->send();
	
	
	
	?>
        $("#load_modal_confirm .message").html("Your request has been sent!");
        $("#ios_in_app_form").delay(1500).find("input[type=text], input[type=tel], input[type=email], textarea").val("");
    <?php
}
else if(!isset($property_id) || $property_id == 0) {
	?>
    $("#load_modal_confirm .message").text("No property id was received.");
    <?php
}
else {
	
	
	if (isset($first_name) && isset($last_name) && isset($email)) {
		$q= "SELECT * FROM leads WHERE email = '$email' LIMIT 1";
		$r= mysqli_query($dbc, $q);
		
		if(mysqli_num_rows($r) == 1) {
			
		if(!empty($phone)) {
			$q_update= "UPDATE leads SET first_name = '$first_name', last_name = '$last_name', phone ='$phone' WHERE email = '$email'";
		}
		else {
			$q_update= "UPDATE leads SET first_name = '$first_name', last_name = '$last_name' WHERE email = '$email'";
		}
		
		$r_update= mysqli_query($dbc, $q_update);
			
			$row=mysqli_fetch_array($r, MYSQLI_ASSOC);
			$lead_id = $row['id'];
		}
		else {
			$q= "INSERT INTO leads (first_name, last_name, email, phone) VALUES ('$first_name', '$last_name', '$email', '$phone')";
			$r= mysqli_query($dbc, $q);
			$lead_id = mysqli_insert_id($dbc);
		}
		
	
		if(!empty($phone)) {
			$show_phone = 1;	
		}
		else {
			$show_phone = 0;
		}
		
		$q= "INSERT INTO lead_message (lead_id, property_id, message, show_phone) VALUES ($lead_id, $property_id, '$message', $show_phone)";
		$r = mysqli_query($dbc, $q);
		
		// SET MAIL SECTION HERE 
		// GET AGENT AND PDF INFO
		
		$q = "SELECT 
		properties.user_id, 
		properties.id as property_id, 
		properties.title, 
		properties.description, 
		properties.address, 
		properties.price, 
		properties.beds, 
		properties.bath, 
		properties.sqft, 
		properties.short_url_hash, 
		properties.facebook_share, 
		properties.twitter_share, 
		properties.crm, 
		properties.crm_field_1_value, 
		properties.crm_field_2_value, 
		properties.crm_field_3_value, 
		properties.crm_field_4_value, 
		properties.crm_field_5_value, 
		properties.crm_field_6_value, 
		
		users.user_id, 
		users.listing_email, 
		users.auto_email, 
		users.first_name, 
		users.last_name, 
		users.listing_phone, 
		users.listing_brokerage, 
		
		flyers.name as flyer_name, 
		flyers.id as flyer_id, 
		
		auto_response.auto_email as standard_response
		
		FROM 
		users, 
		properties 
		
		LEFT JOIN 
			(flyers) 
			on 
			(flyers.property_id = properties.id) 
			
		LEFT JOIN 
			(auto_response) 
			on 
			(auto_response.id = 1) 
			
		WHERE properties.user_id = users.user_id
		AND properties.id = $property_id
		";
		$r = mysqli_query($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$replace[] = array("\n", '<br />');
		
		$replace[] = array( '{AGENT_FIRST_NAME} ' ,$agent_first_name = $row['first_name']);
		$replace[] = array( '{AGENT_LAST_NAME}',$agent_last_name  = $row['last_name']);
		$replace[] = array( '{AGENT_LISTING_EMAIL}',$agent_email = $row['listing_email']);
		$replace[] = array( '{AGENT_LISTING_PHONE}',$agent_phone = $row['listing_phone']);
		$replace[] = array( '{AGENT_LISTING_BROKERAGE}',$agent_brokerage = $row['listing_brokerage']);
		
		$property_id = $row['property_id'];
		$replace[] = array( '{PROPERTY_TITLE}',$propertytitle = $row['title']);
		$replace[] = array( '{PROPERTY_DESCRIPTION}',$property_description = $row['description']);
		$replace[] = array( '{PROPERTY_ADDRESS}',$property_address = $row['address']);
		$replace[] = array( '{PROPERTY_PRICE}',$price = $row['price']);
		
		$details = $row['beds'] . ' beds ' . $row['bath'] . ' baths ' . $row['sqft'] . ' sqft';
		$address = $row['address'];
		$twitter_share = $facebook_share = '';
		$crm_id = $row['crm'];
		
		$facebook_share .= 'https://www.facebook.com/sharer/sharer.php?u=http://bit.ly/' . $row['facebook_share'];
		$twitter_share .= 'https://twitter.com/home?status=http://bit.ly/' . $row['twitter_share'];
		
		$replace[] = array( '{PROPERTY_DETAILS}', $details );
		
		$replace[] = array('{PROSPECT_FIRST_NAME}', $first_name);
		$replace[] = array('{PROSPECT_LAST_NAME}', $last_name);
		
		$flyer = $row['flyer_id'];
		$flyer_name = $row['flyer_name'];
		
		if(!empty($row['auto_email'])) {
			$message_to_client = $row['auto_email'];
		}
		else {
			$message_to_client = $row['standard_response'];
		}
		
		foreach ($replace as $value) {
			$message_to_client = str_replace($value[0], $value[1], $message_to_client);
		}
		
		$HTMLEmailoutput = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $propertytitle; ?></title>
</head>

<body style="text-align:center">

<table cellpadding="0" cellspacing="0" border="0" style="font-family:Helvetica, Arial, sans-serif; max-width:700px; text-align:left; margin:auto; padding:20px;">
	<tr>
    	<td>
        	<table width="100%">
        		<tr>
                	<td style="padding-right:20px; width:20%;">
                		<img src="[BASE_URL_ADMIN]images/logo-126.jpg"  style="width:100%; max-width:126px; min-width:50px; height:auto" />
                    </td>
                   
               </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
        	<p>[MESSAGE_TO_CLIENT]</p>
        </td>
	</tr>
    <tr>
    	<td>
        	<table width="100%">
        		<tr>
                    <td style="padding-right:20px; padding-left:20px; color:#FFF; background:#00AEC7; max-width:100%;">
                        <h2>Share   :   <a href="[facebook_share]" target="_blank" style="color:#FFF;">Facebook</a>   |   <a href="[twitter_share]" target="_blank" style="color:#FFF;">Twitter</a></h2>
                    </td>
               </tr>
            </table>
        </td>
    </tr>
    
</table>
</body>
</html>';
		
		$HTMLEmailoutput = str_replace('[BASE_URL_ADMIN]', BASE_URL, $HTMLEmailoutput);
		
		$HTMLEmailoutput = str_replace('[MESSAGE_TO_CLIENT]', $message_to_client, $HTMLEmailoutput);
		
		$HTMLEmailoutput = str_replace('[twitter_share]', $twitter_share, $HTMLEmailoutput);
		$HTMLEmailoutput = str_replace('[facebook_share]', $facebook_share, $HTMLEmailoutput);
		
		include(MAIL_REQUESTS);

		$mail_requests->FromName = 'Requests From Float';
		$mail_requests->addAddress(strtolower($email));     // Add a recipient
		$mail_requests->addReplyTo($agent_email, $agent_first_name . ' ' . $agent_last_name);
		
		
		$mail_requests->WordWrap = 50;                                 // Set word wrap to 50 characters
		$mail_requests->isHTML(true);                                  // Set email format to HTML
		
		$mail_requests->Subject = $address;
		if ($flyer) {
			$flyer_url = 'http://images.listfloat.com/flyers/' . $flyer . '.pdf';
			$mail_requests->addStringAttachment(file_get_contents($flyer_url), $flyer_name);
			
		}
		
		$mail_requests->Body    = $HTMLEmailoutput;
		echo '<img src="' . BASE_URL . 'images/logo-126.jpg"  style="width:100%; max-width:75px; min-width:50px; height:auto" />';
		if($mail_requests->send()) {
			$HTMLEmailoutput_agent = '
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Leads - ' . $propertytitle . '</title>
</head>

<body style="text-align:center">

<table cellpadding="0" cellspacing="0" border="0" style="font-family:Helvetica, Arial, sans-serif; min-width:700px; text-align:left; margin:auto; padding:20px;">
	<tr>
    	<td>
        	<table width="100%">
        		<tr>
                	<td style="padding-right:20px; width:20%;">
                		<img src="' . BASE_URL . 'images/logo-126.jpg"  style="width:100%; max-width:75px; min-width:50px; height:auto" />
                    </td>
                   
               </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
        	<p>Hi, ' . $agent_first_name . ' ' . $agent_last_name . '</p>
        	<p>' . $message . '</p>
            <p>Thanks,<br />' . $first_name . ' ' . $last_name . '<br /><a href="mailto:' . $email . '">' . $email . '</a><br />' . $phone . '</p>
        </td>
	</tr>
    
    
</table>
</body>
</html>';
			
			$mail_requests = null;
			ob_clean();
			unset($mail_requests);
							
					include(MAIL_LEADS);
			
					$mail_leads->FromName = 'Leads From Float';
					$mail_leads->addAddress(strtolower($email));     // Add a recipient
					$mail_leads->addReplyTo($agent_email, $agent_first_name . ' ' . $agent_last_name);
					
					$mail_leads->WordWrap = 50;                                 // Set word wrap to 50 characters
					$mail_leads->isHTML(true);                                  // Set email format to HTML
					
					$mail_leads->Subject = 'Float Property Inquery: ' . $address;
					
					$mail_leads->Body    = $HTMLEmailoutput_agent;
					$mail_leads->send();
						
						
						
						/* 
						
						POST TO CRM SYSTEM
						
						if($crm_id != 0) {
							
							$q_get_crm = "SELECT * FROM crm WHERE id = $crm_id";
							$r_get_crm = mysqli_query($dbc, $q_get_crm);
							$row_get_crm = mysqli_fetch_array($r_get_crm, MYSQLI_ASSOC);
							
							
							$url = $row_get_crm['float_url'];
							$fields = array(
													'key' => urlencode(POST_CRM_HASH),
													
											);
							
							//url-ify the data for the POST
							$fields_string = '';
							foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
							rtrim($fields_string, '&');
							
							//open connection
							$ch = curl_init();
							
							//set the url, number of POST vars, POST data
							curl_setopt($ch,CURLOPT_URL, $url);
							curl_setopt($ch,CURLOPT_POST, count($fields));
							curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
							
							//execute post
							$result = curl_exec($ch);
							//close connection
							curl_close($ch);
							
							
						} */
							
							/* AGENT MAIL SECTION */
							
							
							?>
                            $("#load_modal_confirm .message").html("Your request has been sent!");
                            $("#ios_in_app_form").delay(1500).find("input[type=text], input[type=tel], input[type=email], textarea").val("");
                            <?php
		}
	}
	else {
		
		
		
		
		$errors_thrown = '';
		foreach ($error_message as $trigger) {
					$errors_thrown .= '<p>' . $trigger['description'] . '</p>';
		}
		?>
        	$("#load_modal_confirm .message").html("<?php echo $errors_thrown; ?>");
        <?php
	}
} // END ID CHECK


?>