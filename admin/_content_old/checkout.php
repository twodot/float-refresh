<?php

require_once('includes/config.inc.php');
require_login();
if (isset($user_id)) {
	 $q =" SELECT *, TIMESTAMPDIFF(DAY, NOW(), current_period_end) + 1 as days_left, TIMESTAMPDIFF(DAY, current_period_start, current_period_end) as days FROM subscription, subscription_types WHERE subscription.user_id = $user_id AND subscription.subscribe_level = subscription_types.stripe_id";
	$r = @mysqli_query ($dbc, $q);
	
	if( mysqli_num_rows($r) == 1) {
			
			
			Stripe::setApiKey(STRIPE_SECRET);
			
			
			
			
			
			$q_s_id ="SELECT * FROM users where user_id = $user_id";
			$r_s_id = @mysqli_query ($dbc, $q_s_id);
			$row_s_id = mysqli_fetch_array($r_s_id, MYSQLI_ASSOC);
			
			$stripe_customer = $row_s_id['stripe_id'];
			date_default_timezone_set("UTC"); 
			
			$cu = Stripe_Customer::retrieve("$stripe_customer");
			
			$q_card ="SELECT stripe_card_id FROM cards where user_id = $user_id";
			$r_card = @mysqli_query ($dbc, $q_card);
			$row_card = mysqli_fetch_array($r_card, MYSQLI_ASSOC);
			
			$card = $cu->cards->retrieve($row_card['stripe_card_id']);
			
			
			try {
				$charge = Stripe_Charge::create(array(
				  "amount" => $amount, 
				  "currency" => "usd",
				  "customer" => $stripe_customer,
				  "description" => "Purchase of $beacons Beacons"
				  )
				);
				
				$charge_id = $charge->id;
				
				$q_beacons = "INSERT INTO beacon_orders SET user_id = '$user_id', beacons = '$beacons', total = '$amount', charge_id = '$charge_id'";
				$r_beacons = @mysqli_query ($dbc, $q_beacons);
				
				$HTMLEmailoutput = $row_s_id['email'] . ' has ordered ' . $beacons . ' beacons';
				
				include(SMTP_MAILER);

				$mail->From = 'orders@' . EMAIL_URL;
				$mail->FromName = 'Order From Float';
				$mail->addAddress(SALES);     // Add a recipient
				$mail->addReplyTo('orders@' . EMAIL_URL, 'Order From Float');
				
				$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
				$mail->isHTML(true);                                  // Set email format to HTML
				
				$mail->Subject = 'A New order of '. $beacons . ' beacons has been submitted';
				
				$mail->Body    = $HTMLEmailoutput;

				if (!$mail->send()) {
					
					
					unset($mail);
				
					include(SMTP_MAILER);
	
					$mail->From = 'orders@' . EMAIL_URL;
					$mail->FromName = 'Order From Float';
					$mail->addAddress(SALES);     // Add a recipient
					$mail->addReplyTo('orders@' . EMAIL_URL, 'Order From Float');
					
					$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
					$mail->isHTML(true);                                  // Set email format to HTML
					
					$mail->Subject = 'Your order of '. $beacons . ' beacons has been received';
					
					include('email_beacons_ordered.php');
					$HTMLEmailoutput = ob_get_contents();
					ob_end_clean();
					
					$mail->Body    = $HTMLEmailoutput;
					$mail->send();
					
					
					echo '$("#error").html("<p>Your order has been accepted and is being processed.</p>").fadeIn(300);';
					echo '$("#error").delay(2000).fadeOut(1000);';	
				}
				else {
					
					unset($mail);
				
					include(SMTP_MAILER);
	
					$mail->From = 'orders@' . EMAIL_URL;
					$mail->FromName = 'Order From Float';
					$mail->addAddress(SALES);     // Add a recipient
					$mail->addReplyTo('orders@' . EMAIL_URL, 'Order From Float');
					
					$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
					$mail->isHTML(true);                                  // Set email format to HTML
					
					$mail->Subject = 'Your order of '. $beacons . ' beacons has been received';
					
					include('email_beacons_ordered.php');
					$HTMLEmailoutput = ob_get_contents();
					ob_end_clean();
					
					$mail->Body    = $HTMLEmailoutput;
					$mail->send();
					
					
					echo '$("#error").html("<p>Your order has been accepted and is being processed.</p>").fadeIn(300);';
					echo '$("#error").delay(2000).fadeOut(1000);';	
				}
				
			
			} catch(Stripe_CardError $e) {
				echo '$("#error").html("<p>Your Card could not be processed.</p>").fadeIn(300);';
				echo '$("#error").delay(2000).fadeOut(1000);';	
			}
		
	}
	else {
		echo '$("#error").html("<p>You havent chosen a subscription level!</p>").fadeIn(300);';
		echo '$("#error").delay(2000).fadeOut(1000);';	
	}
} // END USER ID SET VERIFICATION
?>

