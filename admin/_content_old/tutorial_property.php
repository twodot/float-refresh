<?php

require_once('includes/config.inc.php');
require_login();
$page_title = $page_name = 'Add Property';

include('includes/header.php');

?>

			
<script type="text/javascript">
	function initialize() {
	  var propertyLatLong = new google.maps.LatLng(47.63225,-122.209384);
	  var mapOptions = {
		zoom: 18,
		center: propertyLatLong, 
		//mapTypeId: google.maps.MapTypeId.HYBRID
	  }
	  var map = new google.maps.Map(document.getElementById('property-map'), mapOptions);
		
	  var image = 'https://s3-us-west-2.amazonaws.com/images.listfloat.com/assets/float_map_dot.png';
	  var marker = new google.maps.Marker({
		  position: propertyLatLong,
		  map: map,
		  title: 'Float Property', 
		  icon: image
	  });
	}
	
	
	function loadMap() {
	  var script = document.createElement('script');
	  script.type = 'text/javascript';
	  script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAiAePyQf5emFwHaIQlsvwFGX0VXmvEiNg&callback=initialize';
	  document.body.appendChild(script);
	}
	
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
	loadMap();		

jQuery(document).ready(function( $ ) {
	
	$("#property_setup").steps({
		headerTag: "h3",
		enableContentCache: true, 
		bodyTag: "section",
		transitionEffect: "slide",
		saveState: true,
		stepsOrientation: "vertical", 
		labels: {
			current: "current step:",
			pagination: "Pagination",
			finish: "Finish",
			next: "Save & Next",
			previous: "Previous",
			loading: "Loading ..."
		}
	});
	
})
</script>
<div id="property_setup">
	<h3><span class="add_property_name">Location</span><span class="add_property_icon">Icon</span></h3>
    <section>
            <h2 class="create_property_headline">Hi! Let’s Get Started</h2>
            <h3 class="create_property_subhead">Where is the Place?</h3>
            <div id="prop_map" class="details_hide_show">
                    <style>
                        #form_address {display:none;}
                    </style>
                    
                    <div id="property-map"></div>
            </div>
            <div id="address_holder">
                <form id="info_update_0">
                    <label for="address">Address</label>
                    <input type="text" name="address" id="address" value="1234 Main St. Float, USA" />
                    <label for="latitude">Latitude</label>
                    <input type="text" name="latitude" id="lat" value="47.63225" /> 
                    <label for="longetude">Longitude</label>
                    <input type="text" name="longitude" id="long" value="-122.209384" />
                    <div class="clear"></div>
                </form>
                <form id="remove_address_from_property_form">
                <button id="remove_address_from_property">Remove Address</button>
                </form>
            </div>
            
            <div id="no_address_found">
                <h1>The address can&rsquo;t be found.</h1><p>Please, tell us more.</p>
               
                 <div id="click_map_holder">
                        <div id="map_for_latitude_longitude"></div>
                        <p></p>
                        <p>Click the above map to set the latitude and longitude of the property</p>
                    </div>
                <form id="no_address_found_form" action="process_no_address_found" method="get" >
                    <input type="text" name="street" placeholder="Street" id="no_address_found_street" /><br />
                    <input type="text" name="city" placeholder="City" id="no_address_found_city" /><br />
                    <input type="text" name="state" placeholder="State" id="no_address_found_state" /><br />
                    <input type="text" name="zipcode" placeholder="zipcode" id="no_address_found_zipcode" /><br />
                    <input type="text" name="latitude" placeholder="latitude" id="no_address_found_latitude" /><div class="lat_long_click_arrow"><img src="https://s3-us-west-2.amazonaws.com/images.listfloat.com/assets/right_arrow.png" alt="Right Pointing Arrow" /></div><br />
                    <input type="text" name="longitude" placeholder="longitude" id="no_address_found_longitude" /><div class="lat_long_click_arrow"><img src="https://s3-us-west-2.amazonaws.com/images.listfloat.com/assets/right_arrow.png" alt="Right Pointing Arrow" /></div>
                   
                    
                    <input type="submit" name="submit" value="Get Address Details" id="process_no_address_found" />
                </form>
            </div>
            
    </section>
    <h3><span class="add_property_name">Details</span></h3>
    <section></section>
    
    <h3><span class="add_property_name">Marketing</span></h3>
    <section></section>
    
    <h3><span class="add_property_name">Media</span></h3>
    <section></section>
    
    <h3><span class="add_property_name">Feature / Tours</span></h3>
    <section></section>
    
    <h3><span class="add_property_name">Float Tour</span></h3>
    <section></section>
    
    <h3><span class="add_property_name">Neighborhood &amp; Schools</span></h3>
    <section></section>
    
    <h3><span class="add_property_name">Lift Off</span></h3>
    <section></section>
</div>

