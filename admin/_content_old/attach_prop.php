<?php

require_once('includes/config.inc.php');
require_login();
if ($user_id == $_GET['u']) {
	
	$q = "SELECT COUNT(*) as total_active, 
		
		(SELECT 
			total_properties 
	
			FROM 
			subscription_types 
	
			WHERE 
			subscription_types.id 
				IN (
					SELECT 
					subscribe_level 
					
					FROM 
					subscription  
					
					WHERE 
					user_id = $user_id) )
			as subscribe_level, 
		
		(SELECT active FROM subscription WHERE user_id = $user_id) as active
		
		FROM 
		properties 
		
		WHERE 
		user_id = $user_id 
		AND active = 1
		
		";
	$r = @mysqli_query ($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	
	$total_active = $row['total_active'];
	$subscribe_level = $row['subscribe_level'];
	$active = $row['active'];
	
	$q_listing = "
		SELECT 
		listing_name, 
		listing_email, 
		listing_phone, 
		listing_brokerage
		
		FROM 
		users 
		
		WHERE
		user_id = $user_id 
		
		";
	$r_listing = @mysqli_query ($dbc, $q_listing);
	$row_listing = mysqli_fetch_array($r_listing, MYSQLI_ASSOC);
	
		if(empty($row_listing['listing_name'])) {
			$listing_error = true;
		}
		else if(empty($row_listing['listing_email'])) {
			$listing_error = true;
		}
		else if(empty($row_listing['listing_phone'])) {
			$listing_error = true;
		}
		else if(empty($row_listing['listing_brokerage'])) {
			$listing_error = true;
		}
		else {
			$listing_error = false;
		}
	
	if($listing_error) {
		echo '$("#error").html(\'<p>You have incomplete listing information in your <a href="/account">account</a>.</p>\').fadeIn(300);';
		echo '$("#error").delay(4000).fadeOut(1000);';
	}
	else if (is_null($subscribe_level)) {
		echo '$("#error").html("<p>You have not started your subscription, please subscribe before attaching a property.</p>").fadeIn(300);';
		echo '$("#error").delay(4000).fadeOut(1000);';
	}
	else if ($active == 0) {
		?> $("#error").html('<p>Your account is not active <a href="/account">activate your account</a> before attaching properties.</p>').fadeIn(300); <?php
		echo '$("#error").delay(4000).fadeOut(1000);';
	}
	/* REMOVE FOR SUBSCRIBE LEVEL CHECK 
		else if ($total_active >= $subscribe_level) {
		echo '$("#error").html("<p>You have reached the maximum properties for your subscription level. Please either deactivate some properties or upgrade your subscription.</p>").fadeIn(300);';
		echo '$("#error").delay(4000).fadeOut(1000);';
	}*/
	else {
	
		if(isset($_GET)) {
			$trimmed = array_map('trim', $_GET);
			
			$property = (int) str_replace('property-', '', $trimmed['p']);
			
			$building = (int) str_replace('building-', '', $trimmed['bu']);
			
				$q = "
					UPDATE 
					properties 
					
					SET 
					building_id = $building  
					
					WHERE 
					id = $property 
					AND user_id = $user_id  
					
					";
				
				$r = @mysqli_query ($dbc, $q);		
		
				if (mysqli_affected_rows($dbc) ==1){
					
					$q = "SELECT * FROM properties WHERE id = $property";
					$r = @mysqli_query ($dbc, $q);
					$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
					$title= $row['title'];
					
					$q_build = "SELECT name FROM building WHERE id = $building";
					$r_build = @mysqli_query ($dbc, $q_build);
					$row_build = mysqli_fetch_array($r_build, MYSQLI_ASSOC);
					
					?> 											
						$("#sortable_props").prepend('<li id="li_<?php echo $property; ?>" class="attched_prop"><a href="/property/<?php echo $property; ?>"><?php echo $title; ?></a><a href="/detach_property?p=<?php echo $property; ?>&b=<?php echo $building; ?>" class="detach_property"><img src="/images/delete.png" class="detach_property_image" alt="Detach Property" title="Detach Property from <?php echo $row_build['name']; ?>"></a><li>').fadeIn(300); 
                        $("#property-<?php echo $property; ?>").remove();
						
                        available_property_building_drag();
					
					<?php
				
				
				}
				else {
					echo '$("#error").html("<p>There was an error, the property could not be attached to the building!</p>").fadeIn(300);';
					echo '$("#error").delay(2000).fadeOut(1000);';
				}
			
				
			
		} // END GET CHECK
	}// END SUBSCRIBE LEVEL CHECK
	
	if( mysqli_num_rows($r) == 0) {
		echo '$("#error").html("<p>Your account is not active, please activate your account before attaching properties.</p>").fadeIn(300);';
		echo '$("#error").delay(2000).fadeOut(1000);';
	}
} // END GET_[USER] == $USER_ID

?>

