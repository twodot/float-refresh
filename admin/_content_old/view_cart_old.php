<?php

if(!isset($_SESSION['cart']['id'])) {
	$_SESSION['cart']['id'] = md5(uniqid(rand(), true));
}
/*
$client = new

    SoapClient(

        "https://api.taxcloud.net/1.0/?wsdl"

    );


$apiLoginID = '12A679A0';
$apiKey = '28211E03-CCA2-457A-9C6C-B7DD361BA2D3';
$customerID = $user_id;
$cartID = $_SESSION['cart']['id'];

*/

require_login();
$page_title = $page_name = 'View Cart';

include('includes/header.php');
if(isset($_POST['submitted'])) {
	foreach($_POST as $sku=>$quantity) {
		if ($sku != 'submitted') {
			$quantity = (int) $_POST[$sku];
			$_SESSION['cart'][$sku]['quantity'] = $quantity;
		}
	}
}
/* 
?>

<script type="text/javascript">
$( document ).ready(function() {
	<?php
		include('includes/get_tax_beacons.php');
		include('includes/tax_cloud_set_address.php');
	?>
	
	
	 
	function FillTaxCloudCart() {
		if (myTaxCloud.cart.items.length == 0) {		 <?php
			$i=0;
			 foreach($_SESSION['cart'] as $sku => $quantity) {
				$q_price = "SELECT price, tic FROM products WHERE sku = '$sku'";
				$r_price = mysqli_query($dbc, $q_price);
				$row_price = mysqli_fetch_array($r_price, MYSQLI_ASSOC);
				
				$price = $row_price['price'];
				$tic = $row_price['tic'];
				$qty = $quantity['quantity'];
				
				

				 ?>
				myTaxCloud.cart.AddItem(new TaxCloudCartItem("<?php echo $sku; ?>", "Float Beacon", <?php echo $price; ?>, <?php echo $qty; ?>, <?php echo $tic; ?>));
			<?php 
			
			} // END FOR EACH
			?>
			
				
		}
	}
	FillTaxCloudCart();
	
	$('#TaxCloudPurchaseButton').click(function () {
		myTaxCloud.OpenPurchaseStep1();
		return false;
	});

	

})
</script>

<?php

*/

if (isset($user_id)) {
	$q =" SELECT *, TIMESTAMPDIFF(DAY, NOW(), current_period_end) + 1 as days_left, TIMESTAMPDIFF(DAY, current_period_start, current_period_end) as days FROM subscription, subscription_types WHERE subscription.user_id = $user_id AND subscription.subscribe_level = subscription_types.stripe_id";
	$r = @mysqli_query ($dbc, $q);
	$i = 0;
	$cart_array = array();
	if( mysqli_num_rows($r) == 1) {
		if (isset($_SESSION['cart'])) {
			echo '<form action="#" method="post">';
				$total = 0;
				foreach($_SESSION['cart'] as $sku => $quantity) {
					echo '<div class="cart_product_holder">';
						$q_price = "SELECT price, tic FROM products WHERE sku = '$sku'";
						$r_price = mysqli_query($dbc, $q_price);
						$row_price = mysqli_fetch_array($r_price, MYSQLI_ASSOC);
						$price = $row_price['price'];
						$tic = $row_price['tic'];
						$qty = $quantity['quantity'];
						echo '<div class="cart_product_sku">' . $sku . '</div>';
						echo '<div class="cart_quantity"><input type="text" name="' . $sku . '" value="' . $qty . '" /></div>';
						echo '<div class="cart_price">$' . $price * $qty . '</div>';
						$total = $total + ($price * $qty);
					echo '</div>'; // END PRODUCT HOLDER
				
					$cart_array[] = array('ArrayOfCartItem' => array('Index' => $i, 'ItemID' =>$sku, 'TIC'=>$tic, 'Price' => $price, 'Qty' => $qty )); 
									
					$i++;
				}
				
				echo '<div id="cart_total">Total - $' . $total . '</div>';
				
				echo '<input type="hidden" name="submitted" value="true" />';
				echo '<button formaction="#" >Update Cart</button>';
			echo '</div>';
			
		}
	
	}
	
	$q_address = "
		SELECT 
		shipping.street as ship_street, 
		shipping.city as ship_city, 
		shipping.state_name as ship_state, 
		shipping.zip as ship_zip
		
		FROM 
		shipping 
		
		WHERE 
		shipping.user_id = $user_id 
		";
		
	$r_address = @mysqli_query ($dbc, $q_address);
	$row_address = mysqli_fetch_array($r_address, MYSQLI_ASSOC);
	
	$cartItems = $cart_array;
	
	/*$destination = array();
	$destination['Address1'] = $row_address['ship_street'];
	$destination['City'] = $row_address['ship_city'];
	$destination['State'] = $row_address['ship_state']; 
	$destination['Zip'] = $row_address['ship_zip'];
	
	
	$origin = array();
	$origin['Address1'] = ADDRESS_STREET;
	$origin['Address2'] = ADDRESS_STREET2;
	$origin['City'] = ADDRESS_CITY;
	$origin['State'] = ADDRESS_STATE;
	$origin['Zip'] = ADDRESS_ZIP;
	
	
	
	$tax = $client->Lookup($apiLoginID, $apiKey, $user_id, $cartID, $cartItems, $origin, $destination);
	
	echo $tax->LookupResponse;
	
	var_dump($tax); */
	
} // END USER ID SET VERIFICATION

?>

