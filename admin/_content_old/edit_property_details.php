<?php

require_once('includes/config.inc.php');
require_login();

if(isset($_POST['submitted'])) {
	$trimmed = array_map('trim', $_POST);
	
	$beds = $dbc->real_escape_string($trimmed['beds']);
	$bath = $dbc->real_escape_string($trimmed['bath']);
	$sqft = $dbc->real_escape_string($trimmed['sqft']);
	$lot_size = $dbc->real_escape_string($trimmed['lot_size']);
	$property_type = $dbc->real_escape_string($trimmed['property_type']);
	$year_built = $dbc->real_escape_string($trimmed['year_built']);
	$community = $dbc->real_escape_string($trimmed['community']);
	$county = $dbc->real_escape_string($trimmed['county']);
	$mls = $dbc->real_escape_string($trimmed['mls']); 
	
	$property = $dbc->real_escape_string($trimmed['p']);
	
	echo $q = "
	UPDATE 
	properties 
	
	SET 
	beds = '$beds', 
	bath = '$bath', 
	sqft = '$sqft', 
	lot_size = '$lot_size', 
	property_type = '$property_type', 
	year_built = '$year_built', 
	community = '$community', 
	county = '$county', 
	mls = '$mls' 
	
	WHERE 
	id = $property 
	
	";
	if($r = @mysqli_query ($dbc, $q)) {
		header('Location: /property/' . $property );
	}//END INSERT CHECK 
	else {
		header('Location: /property/' . $property );
	}
	
} // END SUBMITTED CHECK

else {
	if(isset($_GET['p'])) {
		$property = (int) $_GET['p'];
		
		if($property != 0) {
			// echo $info_id;
			$q = "
				SELECT 
				*    
				
				FROM 
				properties  
				
				WHERE 
				id = $property 
			";
			
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			if ($user_id == $row['user_id']) {
				
				?><form action="/edit_property_details" method="post" class="edit_info_addition">
                	<p>Beds<br /><input type="text" class="edit_info_title" value="<?php echo $row['beds']; ?>" name="beds" placeholder="Beds" /></p>
                	<p>Baths<br /><input type="text" class="edit_info_title" value="<?php echo $row['bath'] + 0; ?>" name="bath" placeholder="Bath" /></p>
                    <p>SqFt<br /><input type="text" class="edit_info_title" value="<?php echo $row['sqft']; ?>" name="sqft" placeholder="SqFt" /></p>
                    <p>Lot Size<br /><input type="text" class="edit_info_title" value="<?php echo $row['lot_size']; ?>" name="lot_size" placeholder="Lot Size" /></p>
                    <p>Property Type<br /><input type="text" class="edit_info_title" value="<?php echo $row['property_type']; ?>" name="property_type" placeholder="Property Type" /></p>
                    <p>Year Built<br /><input type="text" class="edit_info_title" value="<?php echo $row['year_built']; ?>" name="year_built" placeholder="Year Built" /></p>
                    <p>Neighborhood<br /><input type="text" class="edit_info_title" value="<?php echo $row['community']; ?>" name="community" placeholder="Neighborhood" /></p>
                    <p>County<br /><input type="text" class="edit_info_title" value="<?php echo $row['county']; ?>" name="county" placeholder="County" /></p>
                    <p>MLS#<br /><input type="text" class="edit_info_title" value="<?php echo $row['mls']; ?>" name="mls" placeholder="MLS #" /></p>
                    
                    <input type="hidden" name="p" value="<?php echo $row['id']; ?>" />
                    <input type="submit" class="edit_save" name="Submit" value="SAVE" /><input type="hidden" name="submitted" value="true" /></form><?php 
				
			}
			else {
				echo '<p>You have reached this page in error</p>';
			}
		} // P = 0 CHECK
	}// ISSET P CHECK
}
	
?>

