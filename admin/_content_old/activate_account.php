<?php

require_once('includes/config.inc.php');
require_login();
if (isset($user_id)) {
	 $q =" SELECT *, TIMESTAMPDIFF(DAY, NOW(), current_period_end) + 1 as days_left, TIMESTAMPDIFF(DAY, current_period_start, current_period_end) as days FROM subscription, subscription_types WHERE subscription.user_id = $user_id AND subscription.subscribe_level = subscription_types.stripe_id";
	$r = @mysqli_query ($dbc, $q);
	
	if( mysqli_num_rows($r) == 1) {
		
		
			Stripe::setApiKey(STRIPE_SECRET);
			$q_s_id ="SELECT stripe_id FROM users where user_id = $user_id";
			$r_s_id = @mysqli_query ($dbc, $q_s_id);
			$row_s_id = mysqli_fetch_array($r_s_id, MYSQLI_ASSOC);
			
			$stripe_customer = $row_s_id['stripe_id'];
			date_default_timezone_set("UTC"); 
			
			$cu = Stripe_Customer::retrieve("$stripe_customer");
			
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		$sub_id = $row['subscribe_level'];
		
		if($row['active'] == 0) {
			
			$cu->plan = $sub_id;
			$cu->trial_end = strtotime("+1 minute");
			$cu->save();
			$start = $cu['subscriptions']['data'][0]['current_period_start'];
			$end = $cu['subscriptions']['data'][0]['current_period_end'];
			$trial_start = strtotime("+1 minute");
			$trial_end = strtotime("+1 minute");
			
			
			
			$q= "UPDATE 
				subscription 
				
				SET 
				active = 1, 
				deactivate_in = 0, 
				current_period_start = FROM_UNIXTIME($start), 
				current_period_end = FROM_UNIXTIME($end),
				trial_start = FROM_UNIXTIME($trial_start),  
				trial_end = FROM_UNIXTIME($trial_end) 
			
				WHERE user_id = $user_id"; 
				
			$r = @mysqli_query ($dbc, $q);
			
			$emailHash = md5(strtolower($_SESSION['login']['email']));
			$data ='{"interests": {"' . SUBSCRIPTION_TYPE_ACTIVE . '": true, "' . SUBSCRIPTION_TYPE_CANCELED . '": false}}';
			$url = '/lists/' . FLOAT_LIST_ID . '/members/' . $emailHash;
			$mcMemberJson = mc_request('PUT', $url, $data);
			$mcMember = json_decode($mcMemberJson);
			
			?>
				$("#active_inactive").html('Your account is active. <div id="deactivate">Deactivate your account</div>').fadeIn(300);
                $("#account_active_inactive").html('Your account is active. <div id="deactivate">Deactivate your account</div>').fadeIn(300);
			<?php
			
			
		}
		elseif ($row['active'] == 1){
			
			if ($row['deactivate_in'] == 1) {
				
				$emailHash = md5(strtolower($_SESSION['login']['email']));
				$data ='{"interests": {"' . SUBSCRIPTION_TYPE_ACTIVE . '": true, "' . SUBSCRIPTION_TYPE_CANCELED . '": false}}';
				$url = '/lists/' . FLOAT_LIST_ID . '/members/' . $emailHash;
				$mcMemberJson = mc_request('PUT', $url, $data);
				$mcMember = json_decode($mcMemberJson);
				
				
				$cu->plan = $sub_id;
				$cu->trial_end = "now";
				$cu->save();
				$start = $cu['subscriptions']['data'][0]['current_period_start'];
				$end = $cu['subscriptions']['data'][0]['current_period_end'];
				
				
				$q= "UPDATE 
					subscription 
					
					SET 
					active = 1, 
					deactivate_in = 0, 
					current_period_start = FROM_UNIXTIME($start), 
					current_period_end = FROM_UNIXTIME($end),
					trial_start = now(),  
					trial_end = now() 
				
					WHERE user_id = $user_id";
					
				$r = @mysqli_query ($dbc, $q);
				?>
					
                    	$("#active_inactive").html('Your account is now active.').fadeIn(300); 
                    	$("#account_active_inactive").html('Your account is now active.<div id="deactivate">Deactivate your account</div>').fadeIn(300);
                    	$("#active_inactive").removeClass("account_inactive");
                    	$("#active_inactive").addClass("account_active");
                        $("#active_inactive").slideDown(500);
                   		$("#active_inactive").delay(2000).slideUp(800);
                    
				<?php
				
			}
			else {
				$emailHash = md5(strtolower($_SESSION['login']['email']));
				$data ='{"interests": {"' . SUBSCRIPTION_TYPE_ACTIVE . '": false, "' . SUBSCRIPTION_TYPE_CANCELED . '": true}}';
				$url = '/lists/' . FLOAT_LIST_ID . '/members/' . $emailHash;
				$mcMemberJson = mc_request('PUT', $url, $data);
				$mcMember = json_decode($mcMemberJson);
				
				$cu_number = $cu['subscriptions']['data'][0]['id'];
				$subscription = $cu->subscriptions->retrieve($cu_number);
				$subscription->cancel("at_period_end=true");
				
				$q= "UPDATE subscription SET active = 1, deactivate_in = 1 WHERE user_id = $user_id"; 
				$r = @mysqli_query ($dbc, $q);
				
				?>
					$("#active_inactive").html('Your account is active, but will deactivate in <?php echo $row['days_left'] ?> days. <div id="activate">Activate your account</div>').fadeIn(300);
                    $("#account_active_inactive").html('Your account is active, but will deactivate in <?php echo $row['days_left'] ?> days. <div id="activate">Activate your account</div>').fadeIn(300);
					$("#current_subscription").html('<h4>Current Subscription</h4><?php if ($row['allowed_prop'] ==1) {echo 'Unlimited';}else {echo $row['allowed_prop'];} ?> Properties | <?php echo $row['days'] ?> Days').fadeIn(300);
					$("#days_left").html('You have <?php echo $row['days_left'] ?> days remaining before your account deactivates.').fadeIn(300);
                    $("#active_inactive").addClass("account_inactive");
                    $("#active_inactive").removeClass("account_active");
                    $("#active_inactive").slideDown(1000);
				<?php 
			
			}
			
		}
		echo 'activate_deactivare_sub()';
	}
	else {
		echo '$("#error").html("<p>You havent chosen a subscription level!</p>").fadeIn(300);';
		echo '$("#error").delay(2000).fadeOut(1000);';	
	}
} // END USER ID SET VERIFICATION
?>

