<?php

// Include the AWS SDK using the Composer autoloader
require '/home/ec2-user/vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;


require_once('includes/config.inc.php');
require_login();
$page_title = 'Manage Media';
include('includes/header.php');

include("includes/resize.php");
?>

<div id="dialog">
</div>

<?php
function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

if(isset($p)) {
	$i_sel = (int) $p;	
}

if(isset($_POST['submit'])) {
	
	if(isset($p)) {
		$i = (int) $p;
	
		if($i != 0) {
			
			$q = "SELECT serial_num from property_info WHERE id = $i";
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			if($row['serial_num'] != $_POST['hash']) {
				
				$hash = $_POST['hash'];
				
				$q = "UPDATE property_info SET serial_num = '$hash' WHERE id = $i";
				$r = @mysqli_query ($dbc, $q);
			
				$file_ary = reArrayFiles($_FILES['photos']);
				
				$q="SELECT COUNT(*) as property FROM building, property_info WHERE building.user_id = $user_id AND building.id = property_info.building_id AND property_info.id = $i";
				$r = @mysqli_query ($dbc, $q);
				$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
				if($row['property'] == 1) {
					
					$q="SELECT building.id as building_id, building.name as title, property_info.id as info_id, property_info.primary_info as primary_info  FROM building, property_info  WHERE building.user_id = $user_id AND building.id = property_info.building_id AND property_info.id = $i";
					$r = @mysqli_query ($dbc, $q);
					$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
					$p = $row['building_id'];
					
					
					foreach ($file_ary as $file) {
					   if (is_uploaded_file ($file['tmp_name'])){
							$temp = UPLOADS . md5($file['name']);
							if (move_uploaded_file($file['tmp_name'], $temp)){
								$image = $dbc->real_escape_string($file['name']);;
								$type = 'image/jpeg'; //$file['type'];
								
								$extension = strtolower(strrchr($file['name'], '.'));
								$file_name = str_replace($extension, '', $image);
								
								$q="INSERT INTO media SET building_id = $p, info_id = $i, user_id = $user_id, media_type = '$type', title = '$file_name'";
								$r = @mysqli_query ($dbc, $q);
								
								$id = mysqli_insert_id($dbc);
								
								rename ($temp, UPLOADS . "$id" . $extension);
								$image_rename =  '../float_req/images/' . "$id" . $extension;
								
								/* $image = new Imagick($image_rename);
								$width = $image->getImageWidth();
								$height = $image->getImageheight();
								$format = $image->identifyImage();
								$file_type = $format['mimetype'];
								*/
								$theFile = UPLOADS . "$id" . $extension;
								// Instantiate the client.
								$s3 = S3Client::factory(array('region' => 'us-west-2'));
								
								$theFileUploaded = UPLOADS . "thumb/$id" . $extension;
								
								// THUMBNAIL SECTION
								$thumbBucket = 'images.listfloat.com/thumb';
								$thumbS3 = S3Client::factory(array('region' => 'us-west-2'));
								$thumbnail = generate_image_thumbnail($theFile, $theFileUploaded, $id);	
	
								try {
									$upload = $thumbS3->upload($thumbBucket, $id . '.jpg', fopen($thumbnail, 'rb'), 'public-read');
								} catch(Exception $e) { 
								}					
								
								if (isset($thumbnail) && file_exists($thumbnail) && is_file($thumbnail)){
									unlink($thumbnail);
								}	
							
								
								// PHONE SECTION
								$phoneBucket = 'images.listfloat.com/phone';
								$phoneS3 = S3Client::factory(array('region' => 'us-west-2'));
								$phone = generate_image_phone($theFile, $theFileUploaded, $id);	
	
								try {
									$upload = $phoneS3->upload($phoneBucket, $id . '.jpg', fopen($phone, 'rb'), 'public-read');
								} catch(Exception $e) { 
								}					
								
								if (isset($phone) && file_exists($phone) && is_file($phone)){
									unlink($phone);
								}
								
								// TABLET SECTION
								$tabletBucket = 'images.listfloat.com/tablet';
								$tabletS3 = S3Client::factory(array('region' => 'us-west-2'));
								$tablet = generate_image_tablet($theFile, $theFileUploaded, $id);	
	
								try {
									$upload = $tabletS3->upload($tabletBucket, $id . '.jpg', fopen($tablet, 'rb'), 'public-read');
								} catch(Exception $e) { 
								}					
								
								if (isset($tablet) && file_exists($tablet) && is_file($tablet)){
									unlink($tablet);
								}
								
								if (isset($theFileUploaded) && file_exists($theFileUploaded) && is_file($theFileUploaded)){
									unlink($theFileUploaded);
								}
								
								if (isset($theFile) && file_exists($theFile) && is_file($theFile)){
									unlink($theFile);
								}
								
								/*
								$thumb = new Imagick($image_rename);
								$phone = new Imagick($image_rename);
								$tablet = new Imagick($image_rename);
								
								
								// DEFINE IMAGE SIZES
								// THUMBNAIL
								$thumb->resizeImage(150,150,Imagick::FILTER_LANCZOS,1, true);
								
								// PHONE
								if ($width > $height) {
									$phone->resizeImage(960,640,Imagick::FILTER_LANCZOS,1, true);
								}
								else {
									$phone->resizeImage(640,960,Imagick::FILTER_LANCZOS,1, true);
								}
								
								// TABLET
								if ($width > $height) {
									$tablet->resizeImage(1024,768,Imagick::FILTER_LANCZOS,1, true);
								}
								else {
									$tablet->resizeImage(768,1024,Imagick::FILTER_LANCZOS,1, true);
								}
								
								// WRITE IMAGES
								$thumb->writeImage(UPLOADS . 'thumb/' . $id . '.jpg');
								$phone->writeImage(UPLOADS . 'phone/' . $id . '.jpg');
								$tablet->writeImage(UPLOADS . 'tablet/' . $id . '.jpg');
								
								// DELETE IMAGE OBJECTS
								$phone->clear();
								$phone->destroy(); 
								
								$thumb->clear();
								$thumb->destroy(); 
								
								$tablet->clear();
								$tablet->destroy(); 
								
								if (isset($temp) && file_exists($temp) && is_file($temp)){
									unlink($temp);
								}
								if (isset($image_rename) && file_exists($image_rename) && is_file($image_rename)){
									unlink($image_rename);
								}
								
								*/
							}
							
						}
						
					} // END FOREACH
				}// END PROPERTY SELECT CHECK 
			} // SERIAL NUMBER CHECK //
		}  // END I INT CHECK
	}// P CHECK
	
} // END SUBMIT CHECK

if(isset($i)) {
		$i = (int) $i;
		$i_sel = (int) $i;
}

if(isset($i_sel)) {
	
	$i = (int) $i_sel;	
		
	if($i != 0) {
		
		$q="SELECT COUNT(*) as property, building.id as prop_id, building.name as top_title, property_info.title as prop_title FROM building, property_info  WHERE building.user_id = $user_id AND building.id = property_info.building_id AND property_info.id = $i";
		$r = @mysqli_query ($dbc, $q);
		$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
		
		if($row['property'] == 1) {
			$top_title = $row['top_title'];
			$property_info_title = $row['prop_title'];
			$building_id = $row['prop_id'];
			
			$q="SELECT building.id as building_id, building.name as title, property_info.id as info_id, property_info.primary_info as primary_info, property_info.primary_info as primary_info, media.title as media_title, media.id as media_id  FROM building, property_info, media  WHERE building.user_id = $user_id AND building.id = property_info.building_id AND property_info.id = $i AND media.building_id = building.id AND media.info_id = $i  AND media.user_id = $user_id AND media.deleted != 1 ORDER By gallery_order ASC";
			$r = @mysqli_query ($dbc, $q);
			$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			
			$primary_info = $row['primary_info'];
			
			if ($primary_info) {
				$q="SELECT building.id as building_id, building.name as title, property_info.id as info_id, property_info.primary_info as primary_info, property_info.primary_info as primary_info, media.title as media_title, media.id as media_id  FROM building, property_info, media  WHERE building.user_id = $user_id AND building.id = property_info.building_id AND building.id = $building_id AND building.id = property_info.building_id AND property_info.id = media.info_id AND media.building_id = building.id AND media.building_id = $building_id  AND media.user_id = $user_id AND media.deleted != 1 ORDER By main_gallery_order ASC";	
				$r = @mysqli_query ($dbc, $q);
				$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
			}
			
			?>
						
			<script>
			
			$(function() {
				$( "#media_sortable" ).sortable({ items: " li", placeholder: "media_sort_highlight", opacity: 0.5 });
				$( "#media_sortable" ).on( "sortupdate", function( event, ui ) {
					var sorted = $( "#media_sortable" ).sortable( "serialize", { key: "sort" } );
					$.ajax({
						url: "update_media_sort<?php if ($primary_info) {echo'_primary';} ?>?"+ sorted, 
						cache: false,
						
					})
				})
				$( "#media_sortable" ).disableSelection();
				
			
			});
					
			</script>
			
			<?php
			
			echo '<div id="media_holder">';
					echo '<h3>' . $top_title  . '</h3>';
					if (!empty($property_info_title)) {
						echo '<p>' . $property_info_title . '</p>';	
					}
					
					echo '<div class="edit_media"><p><a href="/building/' . $building_id . '">Back to building</a><br /><form action="/media_building/' . $i . '" method="post" enctype="multipart/form-data" id="form_' . $i . '" >
							<input type="hidden" value="' . md5(uniqid(rand(), true)) . '" name="hash" />
						  	<input type="file" multiple name="photos[]" accept="image/jpeg,image/png,image/gif" capture="camera" >
						  	<div id="submit_holder">
							<img src="/images/loading.gif" class="loader" />
							<input type="submit" name="submit" value="Submit" id="upload_image" />
							</div>
					  </form></p>';
					  
					echo '<ul id="media_sortable">';
					$r = @mysqli_query ($dbc, $q);
			while($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
				$image_name = str_replace('.jpg', '', $row['media_title']);
				echo '<li id="media_' . $row['media_id'] . '"';
				 if ($primary_info) {
					 if($row['primary_info'] == 0) {
					 	echo ' class="child_media" ';
					 }
				} 
				echo '><div class="image_container"><a href="/delete_image/' . $row['media_id'] . '" id="delete_image_' . $row['media_id'] . '" class="delete_image" ><img src="' . IMAGES . 'delete.png" alt="Delete image" title="Delete" /></a><div class="sort_image"><img src="http://images.listfloat.com/thumb/' . $row['media_id'] . '.jpg" /></div></div></li>';
				
			} // END FOREACH
				echo '</ul>';
				
			echo '<div>'; // END MEDIA HOLDER 
		}
	}
}
include('includes/footer.php');