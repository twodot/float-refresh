<?php

require_login();
$page_title = $page_name = 'My Account';

include('includes/header.php');

?>
<script type="text/javascript">
$( document ).ready(function() {
	
	
	$('#show_bill').click(function(e){
		 e.preventDefault();
		 $('#billing_form').slideToggle(300);
	})
	$('#show_ship').click(function(e){
		 e.preventDefault();
		 $('#shipping_form').slideToggle(300);
	})
	
	$("#edit_ship_button").click(function(){
		$("#current_ship_add").slideToggle(500);
		$("#edit_ship").slideToggle(500);
		$(".edit_ship_key").slideToggle(500);
	})
	
	$("#edit_bill_button").click(function(){
		$("#current_bill_add").slideToggle(500);
		$("#edit_bill").slideToggle(500);
		$(".edit_bill_key").slideToggle(500);
	})
	
	
	$('#update_ship').click(function(e){
		$("#current_ship_add").html('<img src="<?php echo IMAGES; ?>loading.gif" alt="loading" class="loading_gif" />').fadeIn(300);
		e.preventDefault();
		$.ajax({
			url: "update_add", 
			data: jQuery("#update_ship_form").serialize(),
			dataType:"script", 
			cache: false,
		}).done(function(data) {
			
		});
		
	})
	
	$('#update_bill').click(function(e){
		
		e.preventDefault();
		$.ajax({
			url: "update_add", 
			data: jQuery("#update_bill_form").serialize(),
			dataType:"script", 
			cache: false,
		}).done(function(data) {
			
		});
		
	})
	
	<?php
	
	if(isset($_GET['pc'])) {
	
		if (isset($_GET['e'])) {
			switch ($_GET['e']) {
				case 1:
				$alert = 'Your Passwords did not match';
				break;
				
				case 2:
				$alert = 'Please enter a valid password! (It must be between 4 and 20 charactors and contain only letters, numbers and the underscore.)';
				break;
				
				case 3:
				$alert = 'Your password was not changed. Make sure that your new password is different than the current. Contact the system <a href="mailto:'; $alert .= ADMIN; $alert .= '" class="body_link">admininistrator</a> if you think an error has occurred.';
				break;
				
				case 4:
				$alert = 'Please try again.';
				break;
				
				case 5:
				$alert = 'You did not enter a valid email';
				break;
				
				case 6:
				$alert = 'Your email was not changed. Make sure that your new email is different than the current. Contact the system <a href="mailto:'; $alert .= ADMIN; $alert .= '" class="body_link">admininistrator</a> if you think an error has occurred.';
				break;
				
				case 7:
				$alert = 'Please try again.';
				break;
				
				case 8:
				$alert = 'This email is already associated with a user';
				break;
				
				case 9:
				$alert = 'Your old password did not match';
				break;
				
				
			}
			echo '$("#error").html(\'<p>' . $alert  . '</p>\').fadeIn(300);';
			echo '$("#error").delay(2000).fadeOut(1000, function() {
				$("#error").removeClass("success");	
			});';	
		}
		else if (isset($_GET['s'])) {
			if(isset($_GET['sv'])){
				if ($_GET['sv'] == 1) {
				$alert = 'Your email has been changed.';
				}
			}
			else {
				$alert = 'Your password has been changed.';
			}
			?>
			$('#error').addClass('success');
			<?php
		echo '$("#error").html(\'<p>' . $alert  . '</p>\').fadeIn(300);';
		echo '$("#error").delay(2000).fadeOut(1000, function() {
			$("#error").removeClass("success");	
		});';	
		
		}
		
		
	}

?>
	
});

  var handler = StripeCheckout.configure({
	key: '<?php echo STRIPE_API; ?>',
	image: '<?php echo IMAGES; ?>logo-126.jpg',
	token: function(token, args) {
		 
		 var dataString = JSON.stringify(args);
		 
		 $.ajax({
			type: 'POST',
			url: "/change_addresses", 
			data: { address: dataString } , 
			cache: false,
		}).done(function(data) {
			window.location.replace('/update_card?t='+token.id);
		});
		 
		 
	}, 
	name: 'Float',
	email: '<?php echo $user_email; ?>', 
	allowRememberMe: false, 
	panelLabel: "Update Card", 
	currency: "USD", 
    shippingAddress: true, 
    billingAddress: true, 
  });
  

</script>
<?php



if (isset($_POST['submitted'])) {
	
	$trimmed = array_map('trim', $_POST);
	
	$street = $dbc->real_escape_string($trimmed['street']);
	$street_cont = $dbc->real_escape_string($trimmed['street_cont']);
	$apt_number = $dbc->real_escape_string($trimmed['apt_number']);
	$city = $dbc->real_escape_string($trimmed['city']);
	$state_name = $dbc->real_escape_string($trimmed['state']);
	$zip = $dbc->real_escape_string($trimmed['zip']);
	
	if (isset($_POST['same'])) {
		$r = add_addresses($user_id, $dbc, $street, $street_cont, $apt_number, $city, $state_name, $zip);
	}
	else if(isset($_POST['submit_ship'])) {
		$r = add_single_address('shipping',$user_id, $dbc, $street, $street_cont, $apt_number, $city, $state_name, $zip);
	}
	else if(isset($_POST['submit_bill'])) {
		$r = add_single_address('billing', $user_id, $dbc, $street, $street_cont, $apt_number, $city, $state_name, $zip);
	}
	
}

// SELECT FOR PROPERTY PRIMARY INFO


?>
<div id="error">
</div>

<div id="dialog">
</div>
<div id="customer_info">
<?php $q ="SELECT COUNT(*) as subscription FROM subscription WHERE user_id = $user_id";
	$r = @mysqli_query ($dbc, $q);
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	$has_subscription = $row['subscription'];
	
	if($has_subscription != 0) {
		
?>

<div class="customer_name">
<?php echo $_SESSION['login']['first_name'] . ' ' . $_SESSION['login']['last_name']  ; ?>
</div>
<div>
<p><?php echo $_SESSION['login']['email']; ?></p>
<?php 

$q="SELECT show_hints FROM users WHERE user_id = $user_id"; 
$r = @mysqli_query ($dbc, $q);
$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
?>

<p><a href="/transactions?l=10" id="show_transactions">View Transactions</a> </p>

<form id="show_hints" >
<input type="checkbox" id="hint" <?php if ($row['show_hints'] == 1) {echo 'checked="checked"';} ?> /> Show hints on how to use the site
</form>
<p><a id="change_email" href="/change_email">
	Change your email
</a>
</p>

<a id="change_password" href="/change_password">
	Change your password
</a>


<p><a href="/change_auto_response">
	Change your auto-response email message.
</a>
</p>


<?php 

echo '<p><a id="change_subscription" href="/change_subscription">Change your subscription</a></p>';


if ($UserInfo->address_count !=0) {
?>

<h4>Shipping Address</h4>
	<?php echo $UserInfo->ship_street; ?><br />
    <?php echo $UserInfo->ship_city ?>, 
    <?php echo $UserInfo->ship_state; ?> 
    <?php echo $UserInfo->ship_zip; ?><br />
<h4>Billing Address</h4>
	<?php echo $UserInfo->bill_street; ?><br />
    <?php echo $UserInfo->bill_city; ?>, 
    <?php echo $UserInfo->bill_state; ?> 
    <?php echo $UserInfo->bill_zip; ?><br />

<p class="hint">To change your billing or shipping address, please select &ldquo;Update Card&rdquo; in the billing information section to the right.</p>

<?php }// END ADDRESSES COUNT ?>

<?php 

$q ="SELECT *, TIMESTAMPDIFF(DAY, NOW(), trial_end) as trial_days_left FROM subscription, subscription_types WHERE subscription.user_id = $user_id AND subscription.subscribe_level = subscription_types.stripe_id";
$r = @mysqli_query ($dbc, $q);
if( mysqli_num_rows($r) == 1) {	
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	if ($row['trial_days_left'] > 0) {
		echo '<p>You have ' . $row['trial_days_left'] . ' days remaining on your free trial.</p>';
	}
	else {
		if($Subscription->free) {
			$sub_type = 'free';
		}
		else {
			$sub_type = 'paid';
		}
		echo '<p>You are currently on a ' . $sub_type . ' subscription.</p>';
	}
}
	} // END CHECK FOR SUBSCRIPTION 
$q ="SELECT *, TIMESTAMPDIFF(DAY, NOW(), current_period_end) + 1 as days_left, TIMESTAMPDIFF(DAY, current_period_start, current_period_end) as days FROM subscription, subscription_types WHERE subscription.user_id = $user_id AND subscription.subscribe_level = subscription_types.stripe_id";
$r = @mysqli_query ($dbc, $q);

if( mysqli_num_rows($r) == 1) {
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	if($row['active'] == 1) {
		if ($row['deactivate_in'] == 1) {
			echo '<div id="account_active_inactive">Your account is active, but will deactivate in ' . $Subscription->days_left . ' days, <div id="activate">Reactivate your account</div></div>';
			
		}
		else {
			echo '<div id="account_active_inactive">';
			echo 'Your account is active. ';
			echo '<div id="deactivate">Deactivate your account</div></div>';
			
		}
	}
	if($row['active'] == 0) {
	
		echo '<div id="account_active_inactive">Your account is inactive, <div id="activate">Activate your account</div></div>';
		echo '<div id="previous_subscription">Previous Subscription - ' . $row['allowed_prop'] . ' Properties | ' . $row['days'] . ' Days</div>';
	
	}
}
else {
	echo '<div id="select_subscription_alt">Please <a href="/select_subscription">start your subscription</a>.</div>';
}

?>
</div>
</div>
<?php

$listing_name = $UserInfo->listing_name;
$listing_email = $UserInfo->listing_email;
$listing_phone = $UserInfo->listing_phone;
$listing_brokerage = $UserInfo->listing_brokerage;
$broker_website = $UserInfo->broker_website;

if(empty($listing_name)) {
	$listing_error = true;
}
else if(empty($listing_email)) {
	$listing_error = true;
}
else if(empty($listing_phone)) {
	$listing_error = true;
}
else if(empty($listing_brokerage)) {
	$listing_error = true;
}
else {
	$listing_error = false;
}
echo '<div id="account_listing_info_holder">';
echo '<div class="listing_title">Listing Agent Contact Information</div>';
if($listing_error) {
	echo '<div class="listing_title_error">INCOMPLETE LISTING INFORMATION</div>';
}

$broker_website_link = '<a href="' . $broker_website . '" target="_blank">' . $broker_website . '</a>';

echo '<div id="account_listing_info">';
	echo '<div class="listing_name">'; if (empty($listing_name)){ echo '<span class="listing_error edit_listing_info">Name Missing </span>';}else {echo $listing_name;} echo '</div>';
	echo '<div class="listing_email">'; if (empty($listing_email)){ echo '<span class="listing_error edit_listing_info">Email Missing </span>';}else {echo $listing_email;} echo '</div>';
	echo '<div class="listing_phone">'; if (empty($listing_phone)){ echo '<span class="listing_error edit_listing_info">Phone Missing </span>';}else {echo $listing_phone;} echo '</div>';
	echo '<div class="listing_brokerage">'; if (empty($listing_brokerage)){ echo '<span class="listing_error edit_listing_info">Brokerage Missing </span>';} else {echo $listing_brokerage;} echo '</div>';
	echo '<div class="broker_website">'; if (empty($broker_website)){ echo '<span class="listing_error edit_listing_info">Website Missing </span>';} else {echo $broker_website_link;} echo '</div>';
	echo '<button class="edit_listing_info" type="submit">Update Information</button>';
echo '</div>'; // END LISTING INFO
echo '</div>'; // END LISTING INFO HOLDER

$r = get_card($user_id, $dbc);
$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
// if($row['total'] == 1) {
	
	$q_beacon = "SELECT * FROM products";
	$r_beacon = mysqli_query($dbc, $q_beacon);
	$row_beacons = mysqli_fetch_array($r_beacon, MYSQLI_ASSOC);
	
	$sku = $row_beacons['sku'];
	if($has_subscription != 0) {
		echo '<div id="payment_wrapper">';
		if (!isset($_SESSION['cart'][$sku]['quantity'])) {
			echo '<div id="order_beacons">';
				echo '<div class="order_beacons_title">Order Beacons</div>';
				
				echo '<form id="add_to_cart_form">
						<input type="hidden" value="' . $sku . '" name="product_sku" />
						<div class="select_holder" id="order_beacons_count">
							<select id="select_beacon_quantity" name="beacon_quantity">
								<option value="3">3</option>
								<option value="5">5</option>
								<option value="10">10</option>
							</select>
						</div>
						<div id="beacon_total">Total $' . BEACON_PRICE * 3 . '</div>
						<button id="add_to_cart" type="submit">Add to Cart</button>
					   </form>';
					   ?>
					   
					   <script> 
					   $( document ).ready(function() {
						   
							$('#select_beacon_quantity').change(function() {
								btotal = $('#select_beacon_quantity option:selected').val() * '<?php echo BEACON_PRICE; ?>'
								$('#beacon_total').html('Total $' + btotal);
							});
					   })
					   </script>
					   <?php
			echo '</div>'; // END ORDER BEACONS
		}
		if (!isset($_SESSION['cart'][$sku]['quantity'])) {
			echo '<style> #view_cart { display:none; }</style>';
		}
		
		echo '<div id="view_cart"><div id="cart_items">';
				if (isset($_SESSION['cart'][$sku]['quantity'])) {echo $_SESSION['cart'][$sku]['quantity'] . ' Items'; }
			echo '</div>View Cart</div>';
		
		echo '<div class="card_info">';
			echo '<div class="card_title">Billing Information</div>';
			echo '<div class="card"> **** **** **** ' . $row['card'] . '</div>';
			echo '<div class="card"> ' . $row['card_type'] . '</div>';
			echo '<div class="date">Exp ' . $row['exp_month'] . '/' . $row['exp_year'] . '</div>';
			echo '<button id="edit_card" type="submit">Update Card</button>';
		echo '</div>'; // END CARD INFO 
		echo '</div>'; // END PAYMENT WRAPPER
	} // END HAS SUBSCRIPTION CHECK
// }


?>

<script>
  document.getElementById('edit_card').addEventListener('click', function(e) {
	
	handler.open({
	  description: 'Edit Card'
	 
	});
	e.preventDefault();
  });
  
  
</script>

<div class="clear"></div>
<?php

/* 

SELECT SUBSCRIPTION WARNING

$q ="SELECT *, TIMESTAMPDIFF(DAY, NOW(), current_period_end) + 1 as days_left, TIMESTAMPDIFF(DAY, current_period_start, current_period_end) as days FROM subscription, subscription_types WHERE subscription.user_id = $user_id AND subscription.subscribe_level = subscription_types.stripe_id";
$r = @mysqli_query ($dbc, $q);

if( mysqli_num_rows($r) == 1) {
	$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
	if($row['active'] == 1) {
		if ($row['deactivate_in'] == 1) {
			echo '<div id="account_active_inactive">Your account is active and will deactivate in ' . $row['days_left'] . ' days. <div id="activate">Activate your account</div></div>';
			
		}
		else {
			echo '<div id="account_active_inactive">Your account is active. <div id="deactivate">Deactivate your account</div></div>';
			
		}
	}
	if($row['active'] == 0) {
	
		echo '<div id="account_active_inactive">Your account is inactive, <div id="activate">Activate your account</div></div>';
		echo '<div id="previous_subscription">Previous Subscription - ' . $row['allowed_prop'] . ' Properties | ' . $row['days'] . ' Days</div>';
	
	}
}
else {
	echo '<div id="select_subscription_alt">Please <a href="/select_subscription">start your subscription</a>.</div>';
}
*/

$q_subs =" SELECT * FROM subscription WHERE user_id = $user_id";
$r_subs = @mysqli_query ($dbc, $q_subs);
while($row_subs = mysqli_fetch_array($r_subs, MYSQLI_ASSOC)) {
	
}


include('includes/footer.php');



?>