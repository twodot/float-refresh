<?php

require_once('includes/config.inc.php');
include('includes/bitly.php');
require_login();
if ($user_id == $_GET['u']) {
if(isset($_GET['submitted'])) {
	$trimmed = array_map('trim', $_GET);
	
	$title = sanitize($trimmed['title'], $dbc);
	$id = sanitize($trimmed['u'], $dbc);
	
	$q_check = "
	SELECT 
	title 
	
	FROM 
	properties 
	
	WHERE
	title = '$title' 
	AND user_id = $user_id 
	AND deleted != 0 
	AND property_data_type = 'building' 
	
	";
	
	$r_check = @mysqli_query ($dbc, $q_check);
	
	if (mysqli_num_rows($r_check) > 0) {
			echo '$("#error").html("<p>This building already exists</p>").fadeIn(300)';
	
	}
	else { // NUM ROWS
			
	
		$q = "
			INSERT INTO 
			
			properties  
			
			(user_id, title, property_data_type)
			
			VALUES 
			
			(?, ?, ?)";
			
		$stmt= mysqli_prepare($dbc, $q);
		$type = "building";		
		mysqli_stmt_bind_param($stmt, 'iss', $id, $title, $type);
		
		mysqli_stmt_execute($stmt);
		
		if (mysqli_stmt_affected_rows($stmt) ==1){
			$ri = mysqli_stmt_insert_id($stmt);
			
			$results = bitly_v3_shorten(PUBLIC_BASE_URL . '/view_building/' . $ri, BITLY_APP_GENERIC_ACCESS, 'bit.ly');
			$short_url = $results['hash'];
			
			$q = "UPDATE properties SET short_url_hash = '$short_url' WHERE id = $ri";
			$r = @mysqli_query($dbc, $q);
			
			$q_info = "
			INSERT INTO 
			
			property_info 
			
			(properties_id, primary_info)
			
			VALUE 
			
			(?, ?)";
			
			$stmt_info= mysqli_prepare($dbc, $q_info);
			$primary = 1;		
			mysqli_stmt_bind_param($stmt_info, 'ii', $ri, $primary);
		
			mysqli_stmt_execute($stmt_info);
			
			mysqli_stmt_close($stmt_info);
			
			$q = "
			INSERT INTO 
			property_info 
			
			SET 
			properties_id = $ri, 
			title = 'Location',  
			location = 1";
			
			$r = mysqli_query($dbc, $q);
			
			?> 
            
            
            	$("#buildings .holder").prepend('<div class="building unassigned" id="b_con_<?php echo $ri ?>"><a href="/building/<?php echo $ri ?>"><?php echo $title; ?></a> </div>').fadeIn(300); 
				beacon_drag_building();
                unassigned_building();
                
                $('#add_button').slideDown(300);
				$('#add_building').slideUp(300);
                
				$("#building_title").val('');
			<?php
		
		
		}
		else {
			echo '$("#error").html("<p>The building could not be added</p>").fadeIn(300);';
		}
	
		mysqli_stmt_close($stmt);
	}
	
}

}
?>

