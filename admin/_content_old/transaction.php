<?php

require_once('includes/config.inc.php');
require_login();
		
		(int) $user_id;

		if($user_id != 0) {
			if(isset($_GET['t'])) {
				$t = (int) $_GET['t'];
				if ($t != 0) {
					
					$q = "
						SELECT 
						*    
						
						FROM 
						past_subscriptions  
						
						WHERE 
						user_id = $user_id 
						AND id = $t
						
						ORDER BY id 
						DESC
					";
					
					$r = @mysqli_query ($dbc, $q);
					$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
					
					$receipt_id = $row['event_id'];
					$charge_id = $row['charge_id'];
					$receipt_id = str_replace('evt_', '', $receipt_id);
					
					$refunded = $row['refunded'];
					
					$q_user = "
						SELECT 
						*    
						
						FROM 
						users  
						
						WHERE 
						user_id = $user_id 
						
					";
					
					$r_user = @mysqli_query ($dbc, $q_user);
					$row_user = mysqli_fetch_array($r_user, MYSQLI_ASSOC);
					
					$q_billing = "
						SELECT 
						*    
						
						FROM 
						addresses, 
						billing   
						
						WHERE 
						addresses.user_id = $user_id 
						AND addresses.billing_id = billing.billing_id 
						
					";
					
					$r_billing = @mysqli_query ($dbc, $q_billing);
					$row_billing = mysqli_fetch_array($r_billing, MYSQLI_ASSOC);
					
					echo '<div style="border:1px solid #999; width:800px; font-family:helvetica;">';
						echo '<table style="margin:50px;">';
						echo '<tr valign="top">
								<td style="padding-right:20px;">
									<img src="' . IMAGES . 'logo-126.jpg" alt="Float Logo" title="Float Logo" id="header_logo" width="100px" height="100px" />
								</td>
								<td style="padding-top:25px; width:200px;">Float Technologies LLC<br />' . FULL_ADDRESS . '</td>
								<td style="padding-top:25px; padding-left:20px;">Invoice #<br />' . $receipt_id . '</td>
								<td></td>
						</tr>';
						
						echo '<tr valign="top">
							<td></td>
							<td style="padding-top:25px;">BILL TO:<br />' . $row_user['first_name'] . ' ' . $row_user['last_name'] . '<br />' . $row_billing['street'] . '<br />' . $row_billing['city'] . ', ' . $row_billing['state_name'] . '  ' . $row_billing['zip'] . '</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>';
						
						$r = @mysqli_query ($dbc, $q);
						$row = mysqli_fetch_array($r, MYSQLI_ASSOC);
							
							switch ($row['event_type']) {
								
								case 'charge.succeeded':
									$title = 'Charge';
								break;
								
								case 'invoice.payment_succeeded':
									$title = 'Invoice Payment';
								break;
								
								case 'customer.subscription.updated':
									$title = 'Subscription Updated';
								break;
								
								case 'customer.subscription.deleted':
									$title = 'Subscription Canceled';
								break;
								
								default:
								  $title = 'Untitled Transaction';
									
							}
							$phpdate = strtotime( $row['invoice_date'] );
							$date_run = date("m / j / Y", $phpdate);
							
							echo '<tr valign="top" >
								<td></td>
								<td id="bill_to" style="padding-top:25px;">Date: ' . $date_run . ' </td>
								<td style="padding-top:25px;">Charge Type: ' . $title . '</td>
							</tr>
							</table>';
							echo '<table style="margin:50px;">';
								$q_check_beacons = "SELECT * FROM beacon_orders WHERE charge_id = '$charge_id'";
								$r_check_beacons = @mysqli_query($dbc, $q_check_beacons);
								if(mysqli_num_rows($r_check_beacons) == 1) {
									$row_check_beacons = mysqli_fetch_array($r_check_beacons, MYSQLI_ASSOC);
									echo '<tr valign="top">';
										echo '<td width="120px"></td><td style="padding:5px 20px; padding-left:0;">Product</td><td style="padding:5px 20px;">Product Number</td><td style="padding:5px 20px;">Quantity</td><td style="padding:5px 20px;">Price</td>';
									echo '</tr>';
									echo '<tr valign="top">';
										echo '<td></td><td style="padding:5px 20px; padding-left:0;">Beacon</td><td style="padding:5px 20px;">' . $row_check_beacons['sku'] . '</td><td style="padding:5px 20px;">' . $row_check_beacons['qty'] . '</td><td style="padding:5px 20px;">$' . $row_check_beacons['price'] . '</td>';
									echo '</tr>';
									
									echo '<tr valign="top">';
										echo '<td></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>';
									echo '</tr>';
									
									
									echo '<tr valign="top">';
										echo '<td></td><td>&nbsp;</td><td>&nbsp;</td><td style="padding:5px 20px;">Sub Total</td><td style="padding:5px 20px; text-align:right;">$' . $row_check_beacons['sub_total'] . '</td>';
									echo '</tr>';
									
									echo '<tr valign="top">';
										echo '<td></td><td>&nbsp;</td><td>&nbsp;</td><td style="padding:5px 20px;">Shipping</td><td style="padding:5px 20px; text-align:right;">$' . $row_check_beacons['shipping'] . '</td>';
									echo '</tr>';
									
									echo '<tr valign="top">';
										echo '<td></td><td>&nbsp;</td><td>&nbsp;</td><td style="padding:5px 20px;">Tax</td><td style="padding:5px 20px; text-align:right;">$' . $row_check_beacons['tax'] . '</td>';
									echo '</tr>';
									echo '<tr valign="top">';
										echo '<td></td><td>&nbsp;</td><td>&nbsp;</td><td style="padding:5px 20px;">Total</td><td style="padding:5px 20px;; text-align:right;">$' . $row['amount'] / 100 . '</td>';
									echo '</tr>';
								}
								else {
									
									$tax = $row['tax'] / 100;
									$subTotal = $row['amount'] / 100;
									$total = $tax + $subTotal;
									
									
									echo '<tr valign="top">';
										echo '<td width="120px"></td><td width="120px" style="padding:5px 20px; padding-left:0;"></td><td width="120px" style="padding:5px 20px;"></td><td width="60px" style="padding:5px 20px;"></td><td  width="60px" style="padding:5px 20px;"></td>';
									echo '</tr>';
									
									echo '<tr valign="top">';
										echo '<td></td><td>&nbsp;</td><td>&nbsp;</td><td style="padding:5px 20px;">Subtotal</td><td style="padding:5px 20px; text-align:right;">$' . $subTotal .'</td>';
									echo '</tr>';
									
									echo '<tr valign="top">';
										echo '<td></td><td>&nbsp;</td><td>&nbsp;</td><td style="padding:5px 20px;">Tax</td><td style="padding:5px 20px; text-align:right;">$' . $tax .'</td>';
									echo '</tr>';
									echo '<tr valign="top">';
										echo '<td></td><td>&nbsp;</td><td>&nbsp;</td><td style="padding:5px 20px;">Total</td><td style="padding:5px 20px;; text-align:right;">$' . $total . '</td>';
									echo '</tr>';
								}
								
						echo '</table>';
						echo '<div style="text-align:center; padding:10px 0px; width:100%; color:#fff; margin-top:100px;"></div>';
						if($refunded == 1) {
								echo '<div style="text-align:center; padding:10px 0px; width:100%; color:#fff; background-color:#ef4036;">This invoice has been refunded</div>';
							
						}
						echo '<div style="text-align:center; padding:10px 0px; width:100%; color:#fff; background-color:#00AEC7;">Thank you for using Float!</div>';
					echo '</div>';
			
			
				} // END INT T CHECK 
			} // END T CHECK 
			
		}// END PUSER INT CHECK
	
?>

