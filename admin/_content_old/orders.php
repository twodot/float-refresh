<?php

require_admin_login();
unset($_SESSION['pseudo_login']); 

$page_title = $page_name = 'Orders';

include('includes/header.php');
	
		$q_orders = "SELECT * FROM beacon_orders, users WHERE beacon_orders.order_fullfilled = 0 AND beacon_orders.user_id = users.user_id ORDER BY id";
		$r_orders = mysqli_query($dbc, $q_orders);
		echo '<table id="beacon_orders">
				<tr class="table_header">
					<td>Beacons Ordered</td>
					<td>Name</td>
					<td>Email</td>
					<td>Fulfill Order</td>
				</tr>';
		
		while($row_orders = mysqli_fetch_array($r_orders, MYSQLI_ASSOC)) {
			
			$customer_id = $row_orders['user_id'];
			$order_id = $row_orders['id'];
			
			echo '<tr><td>' . $row_orders['beacons'] . '</td><td><a href="/set_pseudo?id=' . $customer_id . '&order_id=' . $order_id . '">' . $row_orders['first_name'] . ' ' . $row_orders['last_name'] . '</a></td><td><a href="mailto:' . $row_orders['email'] . '">' . $row_orders['email']  . '</a></td><td><a href="/set_pseudo?id=' . $customer_id . '&order_id=' . $order_id . '">Fulfill Order -></a></tr>';
			
		}
		echo '</table>';
		
		
		
		
		
		$q_orders = "SELECT * FROM beacon_orders, users WHERE beacon_orders.order_fullfilled = 1 AND beacon_orders.tracking = '' AND beacon_orders.user_id = users.user_id ORDER BY id";
		$r_orders = mysqli_query($dbc, $q_orders);
		echo '<table id="beacon_orders">
				<tr class="table_header">
					<td>Name</td>
					<td>Email</td>
					<td>Start Shipment</td>
				</tr>';
		
		while($row_orders = mysqli_fetch_array($r_orders, MYSQLI_ASSOC)) {
			
			$customer_id = $row_orders['user_id'];
			$order_id = $row_orders['id'];
			
			echo '<tr><td><a href="/set_pseudo?id=' . $customer_id . '&order_id=' . $order_id . '">' . $row_orders['first_name'] . ' ' . $row_orders['last_name'] . '</a></td><td><a href="mailto:' . $row_orders['email'] . '">' . $row_orders['email']  . '</a></td><td><a href="/set_pseudo?id=' . $customer_id . '&order_id=' . $order_id . '">Start Shipment -></a></tr>';
			
		}
		echo '</table>';

include('includes/footer.php');

?>